<?php

return [
    // 默认磁盘
    'default' => env('filesystem.driver', 'local'),
    // 磁盘列表
    'disks'   => [
        'local'  => [
            'type' => 'local',
            'root' => app()->getRuntimePath() . 'storage',
        ],
        'public' => [
            // 磁盘类型
            'type'       => 'local',
            // 磁盘路径
            'root'       => app()->getRootPath() . 'public/storage',
            'url'        => '/storage',
            // 可见性
            'visibility' => 'public',
        ],
        // admin_cooperate_uploads磁盘配置信息
        'admin_cooperate_uploads' => [
            // 磁盘类型
            'type'       => 'local',
            // 磁盘路径
            'root'       => dirname(dirname(dirname(__FILE__))), // 合作端和后台端文件存放于/www/wwwroot/admin_cooperate_uploads下
            // 磁盘路径对应的外部URL路径
            'url'        => dirname(dirname(dirname(dirname(dirname(dirname(dirname(__FILE__))))))), // 合作端和后台端文件存放于/www/wwwroot/admin_cooperate_uploads下
            // 可见性
            'visibility' => 'public',
        ],
    ],
];
