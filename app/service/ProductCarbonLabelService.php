<?php


namespace app\service;


use app\model\product\DataStageModel;
use app\model\product\ProductCarbonLabelModel;
use app\model\product\ProductDataModel;
use think\validate\ValidateRule;

class ProductCarbonLabelService
{
    /**
     * @notes 获取碳标签列表页数据
     * @author fengweizhe
     * @param $page_size
     * @param $page_index
     * @param $filters
     * @return array
     */
    public static function getList($page_size, $page_index, $filters) {
        $list = ProductCarbonLabelModel::getCalculates($page_size, $page_index, $filters)->toArray();
        //包含阶段中文获取
        $stage_info = DataStageModel::getIdAndName();
        $stage_map =[];
        foreach ($stage_info as $k => $v) {
            $stage_map[$v['id']] = $v['name'];
        }
        $list_data_new = [];
        foreach ($list['data'] as $k => $v) {
            $stage = $v['stage'];
            $stage_arr = explode(',', $stage);
            $stage_str_info = '';
            foreach ($stage_arr as $kk => $vv) {
                if (isset($stage_map[$vv])) {
                    $stage_str_info = $stage_str_info . $stage_map[$vv] . '; ';
                }
            }
            $v['stage_name'] = $stage_str_info;
            $list_data_new[$k] = $v;
        }
        unset($list['data']);
        $list['data'] = $list_data_new;
        return $list;
    }

    /**
     * @notes 获取碳标签 扫面二位码后的详情数据
     * @author fengweizhe
     * @param $calculate_id
     * @param $introduce
     * @param $product_name
     * @return array
     */
    public static function getInfo($calculate_id, $introduce = '', $product_name = '') {
        $stage_info = DataStageModel::getIdAndName();
        $stage_map =[];
        foreach ($stage_info as $k => $v) {
            $stage_map[$v['id']] = $v['name'];
        }
        //通过calculate_id去product_data表中查询数据
        $return_data = [];
        $product_data_info = ProductDataModel::getDataByCalculateId($calculate_id)->toArray();
        $emissions_arr = array_column($product_data_info, 'emissions');
        $sum_emissions = array_sum($emissions_arr);

        //完成 树形结构的第三层和第四层 结构封装
        foreach ($product_data_info as $k => $v) {
            //这里把每一条数据需要计算的 碳排放量和 排放比率计算出来
            $temp['unique_no'] = $v['id'].$v['product_id'].$v['data_stage'].$v['category'];
            $temp['carbon_value_num'] = round($v['emissions'], 6);
            $temp['carbon_rate_num'] = ($sum_emissions == 0) ? (float)$v['emissions'] : (float)$v['emissions'] / $sum_emissions;
            // $temp['carbon_rate_num'] = number_format($temp['carbon_rate_num'], 10);
            $temp['carbon_value'] = $temp['carbon_value_num'].'kgCO2e';
            $temp['carbon_rate_num'] = round($temp['carbon_rate_num'] * 100, 6);
            $temp['carbon_rate'] = $temp['carbon_rate_num'] .'%';
            $temp['name'] = $v['name'];
            $temp['number'] = $v['number'];
            $temp['unit_str'] = $v['unit_str'];
            $temp['distance'] = $v['distance'];
            $temp['children'] = [];
            $return_data[$v['data_stage']][$v['category']][] = $temp;
        }
        //完成 树形结构的第二层封装
        $return_data_two = [];
        $return_data_one = [];
        foreach ($return_data as $kk => $vv) {
            $return_data_two = [];
            foreach ($vv as $kkk => $vvv) {
                $second_temp = [];
                $category_carbon_value_num_arr = array_column($vvv, 'carbon_value_num');
                $category_carbon_value_num_temp = array_sum($category_carbon_value_num_arr);
                $category_carbon_rate_num_arr = array_column($vvv, 'carbon_rate_num');
                $category_carbon_rate_num_temp = array_sum($category_carbon_rate_num_arr);
                $second_temp['carbon_value_num'] = $category_carbon_value_num_temp;
                $second_temp['carbon_rate_num'] = $category_carbon_rate_num_temp;
                $second_temp['carbon_value'] = $second_temp['carbon_value_num'].'kgCO2e';
                $second_temp['carbon_rate'] = $second_temp['carbon_rate_num'].'%';
                $second_temp['name'] = isset($stage_map[$kkk]) ? $stage_map[$kkk] : '';
                $second_temp['unique_no'] = $kkk;
                $second_temp['children'] = self::mergeRepeatNameData($vvv);;
                $return_data_two[] = $second_temp;
            }
            $return_data_one[$kk] = $return_data_two;
        }
        //完成 树形结构的第一层封装
        $final_data = [];
        foreach ($return_data_one as $kkk => $vvv) {
            $final_temp = [];
            $data_stage_carbon_value_num_arr = array_column($vvv, 'carbon_value_num');
            $data_stage_carbon_value_num_temp = array_sum($data_stage_carbon_value_num_arr);
            $data_stage_carbon_rate_num_arr = array_column($vvv, 'carbon_rate_num');
            $data_stage_carbon_rate_num_temp = array_sum($data_stage_carbon_rate_num_arr);
            $final_temp['carbon_value_num'] = $data_stage_carbon_value_num_temp;
            $final_temp['carbon_rate_num'] = $data_stage_carbon_rate_num_temp;
            $final_temp['carbon_value'] = $final_temp['carbon_value_num'].'kgCO2e';
            $final_temp['carbon_rate'] = $final_temp['carbon_rate_num'].'%';
            $final_temp['name'] = isset($stage_map[$kkk]) ? $stage_map[$kkk] : '';
            $final_temp['unique_no'] = $kkk;
            $final_temp['children'] = $vvv;
            $final_data[] = $final_temp;
        }
        //计算总计的排放量和排放比率
        $all_carbon_value_num_arr = array_column($final_data, 'carbon_value_num');
        $all_carbon_value_num_temp = array_sum($all_carbon_value_num_arr);
        $all_carbon_rate_num_arr = array_column($final_data, 'carbon_rate_num');
        $all_carbon_rate_num_temp = round(array_sum($all_carbon_rate_num_arr));
        $all['carbon_value_num'] = $all_carbon_value_num_temp;
        $all['carbon_rate_num'] = $all_carbon_rate_num_temp;
        $all['carbon_value'] = $all_carbon_value_num_temp.'kgCO2e';
        $all['carbon_rate'] = $all_carbon_rate_num_temp.'%';
        $all['name'] = '总计';
        $all['unique_no'] = '010010101010100101';
        $all['children'] = [];

        $return['introduce'] = $introduce;
        $return['product_name'] = $product_name;
        $return['tree'] = $final_data;
        $return['all'] = $all;

        return $return;
        //详情表是用到以下几个表 jy_product_data  material 是材质  materials 是运输类的(运输类的比较特别)
        //排放量 和 排放占比的计算  排放量的排放是 jy_product_data 中的 number x 计算因子(这个还未提供 暂时现写死)  排放占比也是要计算出来的
        //排放量和排放占比 都是计算最低一级的信息 上一级的数据都是需要用子集来计算累加出来的
        //match_type 这个类型 可以区分 是产品还是因子   需要是产品并且是供应商的情况下 就需要拆解了
        //product_calculate 中有一个stage字段 代表阶段
        //jy_data_stage中的pid  是product_calculate中的stage字段 data_stage中的id和name是分类的id和名称
        //然后category 是类别的字段 可以用这个来找出类别 然后至于 是产品还是因子的话  产品的话关联自己 因子的话就是自己了 自产的话关联
        //jy_product_data  如果是外采的话 关联供应链这里
    }

    /**
     *  mergeRepeatNameData 因业务场景需要,需要把相同名称的数据指标进行求和,只展示一个名称的数据,这里的合并只合并最后一级,就是合并不可拆分的内容,可继续拆分的不合并
     *
     * @param $data
     * @return mixed
     */
    protected static function mergeRepeatNameData($data) {
        $return_temp = [];
        $return_data = [];
        foreach ($data as $k => $v) {
            if (empty($v['children'])) {
                if (isset($return_temp[$v['name']])) {
                    $return_temp[$v['name']]['carbon_value_num'] =  $return_temp[$v['name']]['carbon_value_num'] + $v['carbon_value_num'];
                    $return_temp[$v['name']]['carbon_rate_num'] =  $return_temp[$v['name']]['carbon_rate_num'] + $v['carbon_rate_num'];
                    $return_temp[$v['name']]['carbon_value'] =  $return_temp[$v['name']]['carbon_value_num'].'kgCO2e';
                    $return_temp[$v['name']]['carbon_rate'] =  $return_temp[$v['name']]['carbon_rate_num'].'%';
                } else {
                    $return_temp[$v['name']] = $v;
                }
            } else {
                $return_temp[$k] = $v;
            }
        }
        foreach ($return_temp as $kk => $vv) {
            $return_data[] = $vv;
        }
        return $return_data;
    }
}