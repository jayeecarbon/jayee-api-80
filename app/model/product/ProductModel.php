<?php
namespace app\model\product;

use think\facade\Db;

/**
 * ProductModel
 */
class ProductModel extends Db {

    /**
     * 启用/禁用-启用
     */
    CONST STATE_YES = 1;

    /**
     * 启用/禁用-禁用
     */
    CONST STATE_NO = 2;

    /**
     * 是否删除-未删除
     */
    CONST IS_DEL_NO = 1;

    /**
     * 是否删除-已删除
     */
    CONST IS_DEL_YES = 2;

	//======================================================================
	// PUBLIC FUNCTIONS
	//======================================================================

    /**
     * getProducts 查询产品
     * 
     * @author wuyinghua
     * @param $page_ize
     * @param $page_index
     * @param $filters
	 * @return $list
     */
    public static function getProducts($page_size, $page_index, $filters) {
        $where = array();

        if ($filters['filter_product_name']) {
            $where[] = array(['jp.product_name', 'like', '%' . trim($filters['filter_product_name']) . '%']);
        }

        if ($filters['filter_product_no']) {
            $where[] = array(['jp.product_no', 'like', '%' . trim($filters['filter_product_no']) . '%']);
        }

        if ($filters['filter_product_spec']) {
            $where[] = array(['jp.product_spec', 'like', '%' . trim($filters['filter_product_spec']) . '%']);
        }

        if ($filters['filter_product_state']) {
            $where[] = array(['jp.state', '=', $filters['filter_product_state']]);
        }

        if ($filters['main_organization_id']) {
            $where[] = array(['jp.main_organization_id', '=', $filters['main_organization_id']]);
        }

        $list = Db::table('jy_product jp')
            ->field('jp.id, jp.product_name, jp.product_no, jp.product_spec, jp.files, jp.state, ju.username, jp.create_time')
            ->leftJoin('jy_product_material jpm', 'jp.id = jpm.product_id')
            ->leftJoin('jy_user ju', 'jp.create_by = ju.id')
            ->group('jp.id')
            ->where($where)
            ->order(['jp.modify_time'=>'desc', 'jp.create_time'=>'desc'])
            ->paginate(['list_rows'=>$page_size, 'page'=>$page_index]);

        return $list;
    }

    /**
     * getActiveProducts 查询所有启用产品
     * 
     * @author wuyinghua
     * @param $main_organization_id
	 * @return $list
     */
    public static function getActiveProducts($main_organization_id) {
        $list = Db::table('jy_product jp')
        ->field('jp.id, CONCAT_WS("--", jp.product_name, jp.product_no) product_name')
        ->where(['jp.state'=>self::STATE_YES, 'jp.main_organization_id'=>$main_organization_id])
        ->select();

        return $list;
    }

    /**
     * getAllProducts 查询所有产品
     * 
     * @author wuyinghua
     * @param $main_organization_id
	 * @return $list
     */
    public static function getAllProducts($main_organization_id) {
        $list = Db::table('jy_product jp')->where('main_organization_id', (int)$main_organization_id)->select();

        return $list;
    }

    /**
     * addProduct 添加产品
     * 
     * @author wuyinghua
     * @param $data
	 * @return $id
     */
    public static function addProduct($data) {

        Db::startTrans();
        try {
            // 新增产品
            $product_id = Db::table('jy_product')->insertGetId([
                'main_organization_id' => $data['main_organization_id'],
                'product_name' => $data['product_name'],
                'product_no'   => $data['product_no'],
                'product_spec' => $data['product_spec'],
                'state'        => $data['state'],
                'files'        => is_null($data['files']) ? NULL : implode(',', $data['files']),
                'create_by'    => $data['create_by'],
                'modify_by'    => $data['modify_by'],
                'create_time'  => $data['create_time'],
                'modify_time'  => $data['modify_time'],
            ]);

            // 更新原材料对应的产品ID
            Db::table('jy_product_material')->where('virtual_id', $data['virtual_id'])->update(['product_id' => $product_id]);

            // 获取产品文件列表
            $list = Db::table('jy_file')->where('virtual_id', $data['virtual_id'])->select();
            
            // 删除产品文件
            foreach ($list as $key => $value) {
                if (!in_array($value['id'], $data['files'])) {
                  Db::table('jy_file')->where('id', $value['id'])->delete();
                }
            }

            // 清除virtual_id
            Db::table('jy_product_material')->where('virtual_id', $data['virtual_id'])->update(['virtual_id' => NULL]);
            Db::table('jy_file')->where('virtual_id', $data['virtual_id'])->update(['virtual_id' => NULL]);

            Db::commit();

            return true;
        } catch (\Exception $e) {
            Db::rollback();

            return false;
        }
    }

    /**
     * editProduct 编辑产品
     * 
     * @author wuyinghua
     * @param $data
	 * @return $edit
     */
    public static function editProduct($data) {

        Db::startTrans();
        try {
            // 产品文件
            $list = Db::table('jy_product')->where('id', (int)$data['id'])->find();
            $files = explode(',', $list['files']);

            // 编辑产品
            Db::table('jy_product')->where('id', (int)$data['id'])->update([
                'product_name' => $data['product_name'],
                'product_no'   => $data['product_no'],
                'product_spec' => $data['product_spec'],
                'files'        => is_null($data['files']) ? NULL : implode(',', $data['files']),
                'modify_by'    => $data['modify_by'],
                'modify_time'  => $data['modify_time'],
            ]);

            // 删除产品文件
            foreach ($files as $value) {
                if ($data['files'] != NULL) {
                    if (!in_array($value, $data['files'])) {
                    Db::table('jy_file')->where('id', $value)->delete();
                    }
                }
            }

            Db::commit();

            return true;
        } catch (\Exception $e) {
            Db::rollback();

            return false;
        }
    }

    /**
     * updateState 更新产品状态
     * 
     * @author wuyinghua
     * @param $data
	 * @return $edit
     */
    public static function updateState($data) {
        $edit = Db::table('jy_product')->where('id', (int)$data['id'])->update($data);

        return $edit;
    }

    /**
     * seeProduct 查看产品详情（原材料清单）
     * 
     * @author wuyinghua
     * @param $id
	 * @return $list
     */
    public static function seeProduct($id) {
        $list = Db::table('jy_product_material jpm')
            ->field('jpm.product_material_name, jpm.product_material_no, jpm.product_material_spec, jpm.material, 0 + CAST(jpm.number AS CHAR) count, jpm.unit, jpm.unit_type, ju.name unit_str, jpm.weight, jpm.source, jo.name title')
            ->leftJoin('jy_organization jo', 'jpm.supplier_id = jo.id')
            ->leftJoin('jy_unit ju', 'ju.id = jpm.unit')
            ->where('jpm.product_id', (int)$id)
            ->select();

        return $list;
    }

    /**
     * getDataById
     * 
     * @author wuyinghua
     * @param $id
	 * @return $list
     */
    public static function getDataById($id)
    {
        $list = Db::table('jy_product jy')
            ->where('jy.id', (int)$id)
            ->find();
        return $list;
    }

    /**
     * seeProductCalculate 查看产品详情（核算概览）
     * 
     * @author wuyinghua
     * @param $id
	 * @return $list
     */
    public static function seeProductCalculate($id) {
        $list = Db::table('jy_product_calculate jpc')
            ->field('jpc.id, 0 + CAST(jpc.number AS CHAR) number, jpc.unit, jyu.name unit_str, CONCAT_WS("-", LEFT(jpc.week_start, 7), LEFT(jpc.week_start, 7)) week, 0 + CAST(jpc.emissions AS CHAR) emissions, 0 + CAST(jpc.coefficient AS CHAR) coefficient, ju.username calculate_username')
            ->leftJoin('jy_user ju', 'jpc.modify_by = ju.id')
            ->leftJoin('jy_unit jyu', 'jyu.id = jpc.unit')
            ->where(['jpc.product_id' => (int)$id, 'jpc.is_del' => self::IS_DEL_NO])
            ->select();

        return $list;
    }

    /**
     * getCarconLabelAddFormProduct
     * 
     * @author wuyinghua
     * @param $main_organization_id
	 * @return $list
     */
    public static function getCarconLabelAddFormProduct($main_organization_id) {
        $list = Db::table('jy_product jp')
            ->field('jp.id, jp.product_name, jp.product_no')
            ->where(['jp.main_organization_id' => (int)$main_organization_id, 'jp.state' => self::STATE_YES])
            ->order('jp.id', 'desc')
            ->select();

        return $list;
    }

    /**
     * getFiles 查询产品文件列表
     * 
     * @author wuyinghua
     * @param $id
	 * @return $list
     */
    public static function getFiles($id) {
        $list = Db::table('jy_file jf')
            ->field('jf.id, jf.source_name name, jf.file_path')
            ->where('jf.id', $id)
            ->order('jf.id', 'desc')
            ->find();

        return $list;
    }

    /**
     * getFile 查询产品文件列表
     * 
     * @author wuyinghua
     * @param $id
	 * @return $list
     */
    public static function getFile($id) {
        $list = Db::table('jy_file jf')
            ->field('jf.id, jf.source_name, jf.file_path')
            ->where('id', $id)
            ->find();

        return $list;
    }

    /**
     * addProductFile 添加产品文件
     * 
     * @author wuyinghua
     * @param $data
	 * @return $id
     */
    public static function addProductFile($data) {
        $add = Db::table('jy_file')->insert($data);

        return $add;
    }

    /**
     * delProductFile 删除产品文件
     * 
     * @author wuyinghua
     * @param $data
	 * @return $del
     */
    public static function delProductFile($data) {
        $del = Db::table('jy_file')->where('id', $data['id'])->delete();

        return $del;
    }
}