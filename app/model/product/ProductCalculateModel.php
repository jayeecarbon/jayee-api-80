<?php
namespace app\model\product;

use think\facade\Db;

/**
 * ProductCalculateModel
 */
class ProductCalculateModel extends Db {

    /**
     * 是否删除-未删除
     */
    CONST IS_DEL_NO = 1;

    /**
     * 是否删除-已删除
     */
    CONST IS_DEL_YES = 2;

    /**
     * 匹配类型-因子
     */
    CONST MATCH_TYPE_FACTOR = 1;

    /**
     * 匹配类型-产品
     */
    CONST MATCH_TYPE_PRODUCT = 2;

	//======================================================================
	// PUBLIC FUNCTIONS
	//======================================================================

    /**
     * getCalculates 查询产品核算列表
     * 
     * @author wuyinghua
     * @param $page_ize
     * @param $page_index
     * @param $filters
	 * @return $list
     */
    public static function getCalculates($page_size, $page_index, $filters) {
        $where = [];
        $whereor = [];

        if ($filters['filter_product_name']) {
            $where[] = array(['jp.product_name', 'like', '%' . trim($filters['filter_product_name']) . '%']);
        }

        if ($filters['filter_product_no']) {
            $where[] = array(['jp.product_no', 'like', '%' . trim($filters['filter_product_no']) . '%']);
        }

        if ($filters['filter_product_state']) {
            $where[] = array(['jpc.state', 'like', '%' . trim($filters['filter_product_state']) . '%']);
        }

        if ($filters['filter_week_start']) {
            $whereor[] = array(['jpc.week_start', '>=', $filters['filter_week_start']], ['jpc.week_start', '<=', $filters['filter_week_end']]);
        }

        if ($filters['filter_week_end']) {
            $whereor[] = array(['jpc.week_end', '>=', $filters['filter_week_start']], ['jpc.week_end', '<=', $filters['filter_week_end']]);
        }

        if ($filters['filter_week_start'] && $filters['filter_week_end']) {
            $whereor[] = array(['jpc.week_start', '<=', $filters['filter_week_start']], ['jpc.week_end', '>=', $filters['filter_week_end']]);
        }

        if ($filters['main_organization_id']) {
            $where[] = array(['jpc.main_organization_id', '=', $filters['main_organization_id']]);
        }

        // 数据管理选择产品时，只能展示已经完成状态的的产品表
        if (!empty($filters['filter_state'])) {
            $whereor[] = array(['jpc.state', '=', $filters['filter_state']]);
        }

        $where[] = array(['jpc.is_del', '=', self::IS_DEL_NO]);

        $list = Db::table('jy_product_calculate jpc')
            ->field('jpc.id, jpc.product_id, 0 + CAST(jpc.number AS CHAR) number, jpc.unit_type, jpc.unit, jyu.name unit_str, jpc.scope, jpc.stage, jpc.week_start, jpc.week_end, CONCAT_WS("-", jpc.week_start, jpc.week_end) week, jpc.remarks, jpc.state, DATE_FORMAT(jpc.modify_time, "%Y-%m-%d") modify_time, 0 + CAST(jpc.emissions AS CHAR) emissions, 0 + CAST(jpc.coefficient AS CHAR) coefficient')
            ->field('jp.product_name, jp.product_no, jp.product_spec, jp.files')
            ->field('ju.username')
            ->leftJoin('jy_product jp', 'jp.id = jpc.product_id')
            ->leftJoin('jy_user ju', 'ju.id = jpc.modify_by')
            ->leftJoin('jy_unit jyu', 'jyu.id = jpc.unit')
            ->where($where)
            ->where(function($query) use ($whereor){$query->whereOr($whereor);})
            ->order(['jpc.modify_time'=>'desc', 'jpc.create_time'=>'desc'])
            ->paginate(['list_rows' => $page_size, 'page' => $page_index]);

        return $list;
    }

    /**
     * getCalculate 获取产品核算
     * 
     * @author wuyinghua
     * @param $id
	 * @return $list
     */
    public static function getCalculate($id) {
        $list = Db::table('jy_product_calculate jpc')
        ->field('jpc.id, jpc.product_id, 0 + CAST(jpc.number AS CHAR) number, jpc.unit_type, jpc.unit, ju.name unit_str, jpc.scope, jpc.stage, jpc.week_start, jpc.week_end, CONCAT_WS("-", jpc.week_start, jpc.week_end) week, jpc.remarks, jpc.state, DATE_FORMAT(jpc.modify_time, "%Y-%m-%d") modify_time, 0 + CAST(jpc.emissions AS CHAR) emissions, 0 + CAST(jpc.coefficient AS CHAR) coefficient')
        ->field('jp.product_name, jp.product_no, jp.product_spec')
        ->leftJoin('jy_unit ju', 'ju.id = jpc.unit')
        ->leftJoin('jy_product jp', 'jp.id = jpc.product_id')
        ->where('jpc.id', (int)$id)
        ->find();

        return $list;
    }

    /**
     * getStageName 获取产品核算阶段名称
     * 
     * @author wuyinghua
     * @param $id
	 * @return $list
     */
    public static function getStageName($id) {
        $list = Db::table('jy_data_stage jds')->field('jds.name')->where('jds.id', (int)$id)->find();

        return $list;
    }

    /**
     * addCalculate 添加产品核算
     * 
     * @author wuyinghua
     * @param $data
	 * @return $add
     */
    public static function addCalculate($data) {

        Db::startTrans();
        try {
            // 获取产品核算新增id
            $add_id = Db::table('jy_product_calculate')->insertGetid($data);

            // 获取当前产品下的原材料
            $list = Db::table('jy_product_material jpm')->where('jpm.product_id', (int)$data['product_id'])->select();

            foreach ($list as $value) {
                $product_data['product_calculate_id'] = $add_id;
                $product_data['product_id'] = $value['product_id'];
                $product_data['data_stage'] = 1;
                $product_data['category'] = 6;
                $product_data['name'] = $value['product_material_name'];
                $product_data['number'] = $value['number'];
                $product_data['unit'] = $value['unit'];
                $product_data['material'] = $value['material'];
                $product_data['unit_type'] = $value['unit_type'];
                $product_data['code'] = $value['product_material_no'];
                $product_data['specs'] = $value['product_material_spec'];
                $product_data['source'] = $value['source'];
                $product_data['supplier_id'] = $value['supplier_id'];
                $product_data['match_type'] = self::MATCH_TYPE_FACTOR; // 新增产品核算带入原材料阶段的原材料类型的排放源默认匹配类型有产品修改为因子
                $product_data['create_by'] = $data['create_by'];
                $product_data['modify_by'] = $data['modify_by'];
                $product_data['create_time'] = $data['create_time'];
                $product_data['modify_time'] = $data['modify_time'];
    
                // 将产品原材料清单写入数据管理原材料阶段
                Db::table('jy_product_data')->insert($product_data);
            }

            Db::commit();

            return true;
        } catch (\Exception $e) {
            Db::rollback();

            return false;
        }
    }

    /**
     * editCalculate 编辑产品核算
     * 
     * @author wuyinghua
     * @param $data
	 * @return $edit
     */
    public static function editCalculate($data) {
        // 获取最新的排放量
        $list = Db::table('jy_product_calculate jpc')->field('jpc.emissions')
        ->where('jpc.id', (int)$data['id'])->find();
        $data['emissions'] = $list['emissions'];
        $data['coefficient'] = $list['emissions'] / $data['number'];

        $edit = Db::table('jy_product_calculate')->where('id', (int)$data['id'])->update($data);
        return $edit;
    }

    /**
     * delCalculate 删除产品核算
     * 
     * @author wuyinghua
     * @param $id
	 * @return $del
     */
    public static function delCalculate($id) {

        Db::startTrans();
        try {

            // 删除产品核算
            Db::table('jy_product_calculate')->where('id', (int)$id)->update(['is_del' => self::IS_DEL_YES]);

            // 删除产品核算下的数据管理
            Db::table('jy_product_data')->where('product_calculate_id', (int)$id)->update(['is_del' => self::IS_DEL_YES]);

            Db::commit();

            return true;
        } catch (\Exception $e) {
            Db::rollback();

            return false;
        }
    }

    /**
     * updateState 更新产品核算状态
     * 
     * @author wuyinghua
     * @param $data
	 * @return $edit
     */
    public static function updateState($data) {
        $edit = Db::table('jy_product_calculate')->where('id', (int)$data['id'])->update($data);

        return $edit;
    }

    /**
     * seeCalculateData 查看产品核算详情
     * 
     * @author wuyinghua
     * @param $id
	 * @return $list
     */
    public static function seeCalculateData($id) {
        $list = Db::table('jy_product_data jpd')
            ->field('jpd.data_stage, jpd.name, jpd.category category_id, jds.name category, 0 + CAST(jpd.number AS CHAR) number, 
            jpd.unit, ju.name unit_str, jpd.match_type, jpd.match_id, 0 + CAST(jpd.emissions AS CHAR) emissions, 
            0 + CAST(jpd.coefficient AS CHAR) coefficient, jpd.factor_title, jpd.factor_molecule, jpd.factor_denominator')
            ->leftJoin('jy_data_stage jds', 'jds.id = jpd.category')
            ->leftJoin('jy_unit ju', 'ju.id = jpd.unit')
            ->order(['jpd.modify_time'=>'desc', 'jpd.create_time'=>'desc'])
            ->where(['jpd.product_calculate_id' => (int)$id, 'jpd.is_del' => self::IS_DEL_NO])
            ->select();

        return $list;
    }
 
    /**
     * getCalculateWeek 获取产品核算周期
     * 
     * @author wuyinghua
     * @param $product_id
	 * @return $list
     */
    public static function getCalculateWeek($product_id) {
        $list = Db::table('jy_product_calculate jpc')
            ->field('jpc.id, jpc.week_start, jpc.week_end')
            ->where(['jpc.product_id' => (int)$product_id, 'is_del' => self::IS_DEL_NO])
            ->select();

        return $list;
    }

    /**
     * getCalculateWeekByProdectIds 获取产品核算周期 (通过产品id的数组)
     *
     * @author wuyinghua
     * @param $product_id_arr
     * @return $list
     */
    public static function getCalculateWeekByProdectIds($product_id_arr) {
        $list = Db::table('jy_product_calculate jpc')
            ->field('jpc.id, jpc.product_id, jpc.week_start, jpc.week_end')
            ->whereIn('jpc.product_id',$product_id_arr)
            ->select();

        return $list;
    }


    /**
     * getCalculateByProuctIdAndWeekDate 获取产品核算信息(通过产品id,核算周期)
     * 
     * @author wuyinghua
     * @param $product_id
     * @param $week_start
     * @param $week_end
     * @return array|\think\Collection|Db[]
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function getCalculateByProuctIdAndWeekDate($product_id, $week_start, $week_end) {
        $list = Db::table('jy_product_calculate jpc')
            ->field('jpc.id, jpc.product_id, jpc.week_start, jpc.week_end, jpc.stage')
            ->where('jpc.product_id',(int)$product_id)
            ->where('jpc.week_start',$week_start)
            ->where('jpc.week_end',$week_end)
            ->find();
        return $list;
    }
}