<?php
declare (strict_types = 1);

namespace app\model\product;

use think\facade\Db;

/**
 * FactorModel
 */
class FactorModel extends Db {

	//======================================================================
	// PUBLIC FUNCTIONS
	//======================================================================

    /**
     * getFactors 查询排放因子
     * 
     * @param $page_ize
     * @param $page_index
     * @param $filters
	 * @return $list
     */
    public function getFactors($page_size, $page_index, $filters) {
        $where = array();

        if ($filters['filter_factor_name'] && !empty($filters['filter_factor_name'])) {
            $where[] = array(['jf.title', 'like', '%' . trim($filters['filter_factor_name']) . '%']);
            $organization_id = [99999999, $filters['organization_id']];
        }else{
            $organization_id = [$filters['organization_id']];
        }

        if ($filters['filter_country'] && !empty($filters['filter_country'])) {
            $where[] = array(['jf.country', 'like', '%' . trim($filters['filter_country']) . '%']);
        }

        if ($filters['filter_year'] && !empty($filters['filter_year'])) {
            $where[] = array(['jf.year', 'like', '%' . trim($filters['filter_year']) . '%']);
        }

        if (!empty($filters['filter_factor_source'])) {
            $where[] = array(['jf.factor_source', '=', $filters['filter_factor_source']]);
        }

        $list = Db::table('jy_factor jf')
            ->field('jf.id, jf.title, jf.model, 0 + CAST(jf.factor_value AS CHAR) factor_value, CONCAT_WS("/", jf.molecule, jf.denominator) unit, jf.year, jf.mechanism, jf.country, jf.factor_source')
            ->where($where)
            ->whereNotNull('factor_value')
            ->whereIn('jf.organization_id',$organization_id)
            ->order('jf.id', 'asc')
            ->paginate(['list_rows'=>$page_size, 'page'=>$page_index]);

        return $list;
    }

    /**
     * seeFactor 查看排放因子详情
     * 
     * @param $id
	 * @return $list
     */
    public static function seeFactor($id) {
        $list = Db::table('jy_factor jf')
            ->field('jf.id, jf.title, jf.factor_id, jf.model, jf.grade, jf.uncertainty, jf.describtion, 0 + CAST(jf.factor_value AS CHAR) factor_value, jf.molecule, jf.denominator, jf.mechanism, jf.year, jf.country, jf.region, jf.file_name')
            ->where('jf.id', (int)$id)
            ->find();

        return $list;
    }

    /**
     * seeShotFactor 查看排放因子快照详情
     * 
     * @param $id
	 * @return $list
     */
    public static function seeShotFactor($id) {
        $list = Db::table('jy_factor_snapshot jf')
            ->field('jf.id, jf.title, jf.factor_id, jf.model, jf.grade, jf.uncertainty, jf.describtion, 0 + CAST(jf.factor_value AS CHAR) factor_value, jf.molecule, jf.denominator, jf.mechanism, jf.year, jf.country, jf.region, jf.file_name')
            ->where('jf.id', (int)$id)
            ->find();

        return $list;
    }

    /**
     * getCountrys 发布国家/组织列表
     * 
	 * @return $list
     */
    public function getCountrys() {
        $list = Db::table('jy_factor')->field('DISTINCT(country)')->select();

        return $list;
    }

    /**
     * getYears 发布年份
     * 
	 * @return $list
     */
    public function getYears() {
        $list = Db::table('jy_factor')->field('DISTINCT(year)')->order('year', 'desc')->select();

        return $list;
    }
}
