<?php
namespace app\model\product;

use app\model\supply\SupplierModel;
use app\model\supply\SupplierCustomerRelationModel;
use think\facade\Db;

/**
 * ProductMaterialModel
 */
class ProductMaterialModel extends Db {

	//======================================================================
	// PUBLIC FUNCTIONS
	//======================================================================

    /**
     * getMaterials 查询原材料
     * 
     * @author wuyinghua
     * @param $page_ize
     * @param $page_index
	 * @return $list
     */
    public static function getMaterials($product_id, $virtual_id, $page_size, $page_index) {
        $list = Db::table('jy_product_material jpm')
            ->field('jpm.id, jpm.product_material_name, jpm.product_material_no, jpm.product_material_spec, 0 + CAST(jpm.number AS CHAR) count, jpm.unit_type, jpm.unit, ju.name unit_str, 0 + CAST(jpm.weight AS CHAR) weight, jpm.source, jpm.material, jpm.supplier_id supplier_id, jo.name title')
            ->leftJoin('jy_organization jo', 'jo.id = jpm.supplier_id')
            ->leftJoin('jy_unit ju', 'ju.id = jpm.unit')
            ->where('product_id', (int)$product_id)
            ->where('virtual_id', $virtual_id)
            ->order(['jpm.modify_time'=>'desc', 'jpm.create_time'=>'desc'])
            ->paginate(['list_rows'=>$page_size, 'page'=>$page_index]);

        return $list;
    }

    /**
     * addMaterial 添加原材料
     * 
     * @author wuyinghua
     * @param $data
	 * @return $add
     */
    public static function addMaterial($data) {
        $add = Db::table('jy_product_material')->insert($data);

        return $add;
    }

    /**
     * editMaterial 编辑原材料
     * 
     * @author wuyinghua
     * @param $data
	 * @return $edit
     */
    public static function editMaterial($data) {
        $edit = Db::table('jy_product_material')->where('id', (int)$data['id'])->update($data);

        return $edit;
    }

    /**
     * delMaterial 删除原材料
     * 
     * @author wuyinghua
     * @param $data
	 * @return $del
     */
    public static function delMaterial($data) {
        $where = array();

        if (isset($data['product_id'])) {
            $where[] = array(['product_id', '=', (int)$data['product_id']]);
        }

        $del = Db::table('jy_product_material')->where('id', (int)$data['id'])->where($where)->delete();

        return $del;
    }

    /**
     * getSuppliers 获取供应商
     * 
     * @author wuyinghua
     * @param $main_id
	 * @return $list
     */
    public static function getSuppliers($main_id) {
        $list = Db::table('jy_supplier_customer_relation jscr')
        ->leftJoin('jy_organization jo', 'jscr.relation_id = jo.id')
        ->field('jo.id, jo.name supplier_name')
        ->group('jo.id')
        ->where('jscr.main_id', (int)$main_id)
        ->where('state', (int)SupplierModel::STATE_YES)
        ->where('jscr.is_customer', (int)SupplierCustomerRelationModel::IS_CUSTOMER_YES)
        ->select();

        return $list;
    }

    /**
     * getAllProductMaterials 查询所有原材料
     * 
     * @author wuyinghua
     * @param $main_organization_id
     * @param $product_id
	 * @return $list
     */
    public static function getAllProductMaterials($main_organization_id, $product_id) {
        $list = Db::table('jy_product_material jpm')->where(['main_organization_id'=>(int)$main_organization_id,'product_id'=>(int)$product_id])->select();

        return $list;
    }

    /**
     * getProductMaterial 查询原材料
     * 
     * @author wuyinghua
     * @param $id
	 * @return $list
     */
    public static function getProductMaterial($id) {
        $list = Db::table('jy_product_material jpm')->where('id', (int)$id)->find();

        return $list;
    }
}