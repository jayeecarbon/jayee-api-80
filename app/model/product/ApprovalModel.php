<?php
declare (strict_types = 1);

namespace app\model\product;

use app\model\data\ParamModel;
use think\facade\Db;

/**
 * ApprovalModel
 */
class ApprovalModel extends Db {

	//======================================================================
	// PUBLIC FUNCTIONS
	//======================================================================

    /**
     * getApprovals 查询数据审批列表
     * 
     * @author wuyinghua
     * @param $page_ize
     * @param $page_index
     * @param $filters
	 * @return $list
     */
    public static function getApprovals($page_size, $page_index, $filters) {
        $where = array();
        $whereor = array();

        $where[] = array(['jpc.state', '=', ParamModel::STATE_REVIEW]);

        if ($filters['filter_product_name']) {
            $where[] = array(['jp.product_name', 'like', '%' . trim($filters['filter_product_name']) . '%']);
        }

        if ($filters['filter_product_no']) {
            $where[] = array(['jp.product_no', 'like', '%' . trim($filters['filter_product_no']) . '%']);
        }

        if ($filters['filter_week_start']) {
            $whereor[] = array(['jpc.week_start', '>=', $filters['filter_week_start']], ['jpc.week_start', '<=', $filters['filter_week_end']]);
        }

        if ($filters['filter_week_end']) {
            $whereor[] = array(['jpc.week_end', '>=', $filters['filter_week_start']], ['jpc.week_end', '<=', $filters['filter_week_end']]);
        }

        if ($filters['filter_week_start'] && $filters['filter_week_end']) {
            $whereor[] = array(['jpc.week_start', '<=', $filters['filter_week_start']], ['jpc.week_end', '>=', $filters['filter_week_end']]);
        }

        if ($filters['main_organization_id']) {
            $where[] = array(['jpc.main_organization_id', '=', $filters['main_organization_id']]);
        }

        $list = Db::table('jy_product_calculate jpc')
            ->field('jpc.id, jpc.product_id, 0 + CAST(jpc.number AS CHAR) number, jpc.unit_type, jpc.unit, ju.name unit_str, jpc.scope, jpc.stage, jpc.week_start, jpc.week_end, CONCAT_WS("-", jpc.week_start, jpc.week_end) week, jpc.remarks, jpc.state, DATE_FORMAT(jpc.modify_time, "%Y-%m-%d") modify_time, 0 + CAST(jpc.emissions AS CHAR) emissions, 0 + CAST(jpc.coefficient AS CHAR) coefficient')
            ->field('jp.product_name, jp.product_no, jp.product_spec, jp.files')
            ->leftJoin('jy_product jp', 'jp.id = jpc.product_id')
            ->leftJoin('jy_unit ju', 'ju.id = jpc.unit')
            ->where($where)
            ->where(function($query) use ($whereor){$query->whereOr($whereor);})
            ->order(['jpc.modify_time'=>'desc', 'jpc.create_time'=>'desc'])
            ->paginate(['list_rows' => $page_size, 'page' => $page_index]);

        return $list;
    }

    /**
     * getAllApproval 查询所有待数据审批数据
     * 
     * @author wuyinghua
     * @param $main_organization_id
	 * @return $list
     */
    public static function getAllApproval($main_organization_id) {
        $list = Db::table('jy_product_calculate jpc')
            ->field('jpc.id, jpc.product_id, 0 + CAST(jpc.number AS CHAR) number, jur.username, jpc.unit_type, jpc.unit, ju.name unit_str, jpc.scope, jpc.stage, jpc.week_start, jpc.week_end, CONCAT_WS("-", jpc.week_start, jpc.week_end) week, jpc.remarks, jpc.state, jpc.modify_time, 0 + CAST(jpc.emissions AS CHAR) emissions, 0 + CAST(jpc.coefficient AS CHAR) coefficient')
            ->field('jp.product_name, jp.product_no, jp.product_spec, jp.files')
            ->leftJoin('jy_product jp', 'jp.id = jpc.product_id')
            ->leftJoin('jy_unit ju', 'ju.id = jpc.unit')
            ->leftJoin('jy_user jur', 'jpc.modify_by = jur.id')
            ->where(['jpc.state'=>ParamModel::STATE_REVIEW, 'jpc.main_organization_id'=>(int)$main_organization_id])
            ->order(['jpc.modify_time'=>'desc', 'jpc.create_time'=>'desc'])
            ->select();

        return $list;
    }

    /**
     * updateState 更新数据审批状态
     * 
     * @author wuyinghua
     * @param $data
	 * @return $edit
     */
    public static function updateState($data) {
        $edit = Db::table('jy_product_calculate')->where('id', (int)$data['id'])->update($data);

        return $edit;
    }
}