<?php

namespace app\model\emission;

use think\facade\Config;
use think\facade\Db;


/**
 * QualityModel
 */
class EmissionModel extends Db
{

    public function getList($page_size, $page_index, $filters)
    {
        $where = array();
        if (isset($filters['name']) && !empty($filters['name'])) {
            $where[] = array(['r.name', 'like', '%' . trim($filters['name']) . '%']);
        }
        if (isset($filters['active']) && !empty($filters['active'])) {
            $where[] = array(['r.active', 'like', '%' . trim($filters['active']) . '%']);
        }
        if (isset($filters['source_id']) && !empty($filters['source_id'])) {
            $where[] = array(['r.source_id', '=', $filters['source_id']]);
        }
        if (isset($filters['ghg_cate']) && !empty($filters['ghg_cate'])) {
            $where[] = array(['r.ghg_cate', '=', $filters['ghg_cate']]);
        }
        if (isset($filters['iso_cate']) && !empty($filters['iso_cate'])) {
            $where[] = array(['r.iso_cate', '=', $filters['iso_cate']]);
        }

        $list = Db::table('jy_emission_source r')
            ->field('r.id,r.name,r.source_id,r.active,g.name ghg_cate,h.name ghg_type,gh.name iso_cate,ghg.name iso_type,
            r.modify_time,r.modify_by,u.username')
            ->leftJoin('jy_ghg_template g', 'g.id=r.ghg_cate')
            ->leftJoin('jy_ghg_template h', 'h.id=r.ghg_type')
            ->leftJoin('jy_ghg_template gh', 'gh.id=r.iso_cate')
            ->leftJoin('jy_ghg_template ghg', 'ghg.id=r.iso_type')
            ->leftJoin('jy_user u', 'u.id=r.modify_by')
            ->where($where)
            ->where('r.main_organization_id', $filters['main_organization_id'])
            ->order('r.modify_time', 'desc')
            ->paginate(['list_rows' => $page_size, 'page' => $page_index])
            ->toArray();

        return $list;
    }


    public static function add($data)
    {
        $gas = $data['gas'];
        $gas_one = array_unique(array_column($gas, 'molecule'));
        if (count($gas_one) > 1) {
            return 3;
        }
        unset($data['gas']);

        Db::startTrans();
        try {
            $add_id = Db::table('jy_emission_source')->insertGetId($data);
            $gas_use = [];
            foreach ($gas as $k => $v) {
                if ($data['active_unit'][0] == 6) {
                    if ($data['active_unit'] != $v['denominator']) {
                        return 2;
                    }
                } else {
                    if ($data['active_unit'][0] != $v['denominator'][0]) {
                        return 2;
                    }
                }

                $one = [];
                $one['gas'] = (string)$v['gas'];
                $one['gas_type'] = (string)$v['gas_type'];
                $one['factor_value'] = $v['factor_value'];
                $one['molecule'] = $v['molecule'];
                $one['denominator'] = $v['denominator'];
                $one['emi_id'] = $add_id;
                $one['create_time'] = date('Y-m-d H:i:s');
                $one['create_by'] = $data['create_by'];
                $gas_use[] = $one;
            }

            Db::table('jy_emission_gas')->insertAll($gas_use);

            Db::commit();
            return 1;
        } catch (\think\exception\ValidateException $e) {
            Db::rollback();
            return false;
        }
    }


    public static function getInfoById($id)
    {
        $gas_all = [
            ['gas' => '二氧化碳当量（CO₂e）', 'gas_type' => '', 'factor_value' => '', 'denominator' => '', 'molecule' => ''],
            ['gas' => '二氧化碳（CO₂）', 'gas_type' => '二氧化碳（CO₂）', 'factor_value' => '', 'denominator' => '', 'molecule' => ''],
            ['gas' => '甲烷（CH₄）', 'gas_type' => '甲烷（CH₄）', 'factor_value' => '', 'denominator' => '', 'molecule' => ''],
            ['gas' => '氧化亚氮（N₂O）', 'gas_type' => '氧化亚氮（N₂O）', 'factor_value' => '', 'denominator' => '', 'molecule' => ''],
            ['gas' => '氢氟碳化物 (HFCs)', 'gas_type' => '', 'factor_value' => '', 'denominator' => '', 'molecule' => ''],
            ['gas' => '全氟碳化物 (PFCs)', 'gas_type' => '', 'factor_value' => '', 'denominator' => '', 'molecule' => ''],
            ['gas' => '六氟化硫（SF₆）', 'gas_type' => '六氟化硫（SF₆）', 'factor_value' => '', 'denominator' => '', 'molecule' => ''],
            ['gas' => '三氟化氮（NF₃）', 'gas_type' => '三氟化氮（NF₃）', 'factor_value' => '', 'denominator' => '', 'molecule' => '']
        ];
        $list = Db::table('jy_emission_source r')
            ->field('r.id,r.name,r.source_id,r.active,r.ghg_cate,r.ghg_type,r.iso_cate,r.iso_type,r.active_data,
            r.active_unit,r.active_department,
            r.active_type,r.active_score,r.unitary_ratio,r.factor_type,r.factor_score,r.factor_source,
            r.year,g.name ghg_cate_name,h.name ghg_type_name,
            gh.name iso_cate_name,ghg.name iso_type_name')
            ->leftJoin('jy_ghg_template g', 'g.id=r.ghg_cate')
            ->leftJoin('jy_ghg_template h', 'h.id=r.ghg_type')
            ->leftJoin('jy_ghg_template gh', 'gh.id=r.iso_cate')
            ->leftJoin('jy_ghg_template ghg', 'ghg.id=r.iso_type')
            ->where('r.id', (int)$id)
            ->find();
             if ($list) {
                 $active_unit = explode(',', $list['active_unit']);
                 $list['ghg_cate'] = ['id' => $list['ghg_cate'], 'name' => $list['ghg_cate_name']];
                 $list['ghg_type'] = ['id' => $list['ghg_type'], 'name' => $list['ghg_type_name']];
                 $list['iso_cate'] = ['id' => $list['iso_cate'], 'name' => $list['iso_cate_name']];
                 $list['iso_type'] = ['id' => $list['iso_type'], 'name' => $list['iso_type_name']];
                 $list['active_unit'] = $active_unit;

                 $list['active_type'] = ['id' => $list['active_type'], 'name' => Config::get('emission.active_type')[$list['active_type'] - 1]['name']];
                 $list['factor_type'] = ['id' => $list['factor_type'], 'name' => Config::get('emission.factor_type')[$list['factor_type'] - 1]['name']];
                 $gas = Db::table('jy_emission_gas')
                     ->field('id,gas,gas_type,factor_value,molecule,denominator')
                     ->where('emi_id', (int)$id)
                     ->select()
                     ->toArray();


                 if ($gas) {
                     foreach ($gas_all as $gkk => $gvv) {
                         foreach ($gas as $k => $v) {
                             if ($gvv['gas'] == $v['gas']) {
                                 $denominator = explode(',', $v['denominator']);
                                 if ($denominator) {
                                     $denominators = [];
                                     foreach ($denominator as $vv) {
                                         $denominators[] = intval($vv);
                                     }
                                     $v['denominator'] = $denominators;
                                 }

                                 $gvv['id'] = $v['id'];
                                 $gvv['gas'] = $v['gas'];
                                 $gvv['factor_value'] = $v['factor_value'];
                                 $gvv['gas_type'] = $v['gas_type'];
                                 $gvv['denominator'] = $v['denominator'];
                                 $gvv['molecule'] = (int)$v['molecule'];
                                 $gas_all[$gkk] = $gvv;
                             }
                         }
                     }
                     $list['gas'] = $gas_all;
                 }

             }
        return $list;
    }

    public static function edit($data)
    {
        Db::startTrans();
        try {
            $use_gas =[];
            $gas = $data['gas'];
            if(empty($gas)){
                return 4;
            }else{
                foreach ($gas as $gasv){
                    $use_gas[]=$gasv['molecule'];
                }
               $count =  count(array_unique($use_gas));
              
               if($count!=1){
                   return 3;
               }
            }
            foreach($gas as $gv){
                $use_gas[]=$gv['molecule'];
            };

            unset($data['gas']);
            Db::table('jy_emission_source')
                ->save($data);
            Db::table('jy_emission_gas')->where('emi_id', $data['id'])->delete();
            foreach ($gas as $k => $v) {
                unset($v['id']);
                if ($data['active_unit'][0] != $v['denominator'][0]) {
                    return 2;
                }
                $v['emi_id'] = $data['id'];
                $v['create_time'] = date('Y-m-d H:i:s');
                $v['create_by'] = $data['modify_by'];
                $gas[$k] = $v;
            }
            Db::table('jy_emission_gas')->insertAll($gas);
            Db::commit();
            return 1;
        } catch (\think\exception\ValidateException $e) {
            Db::rollback();
            return false;
        }
    }

    public static function del($id)
    {
        Db::startTrans();
        try {
            Db::table('jy_emission_source')->where('id', $id)->delete();
            Db::table('jy_emission_gas')->where('emi_id', $id)->delete();
            Db::table('jy_calculate_model_emission')->where('emission_id', $id)->delete();
            Db::commit();
            return true;
        } catch (\think\exception\ValidateException $e) {
            Db::rollback();
            return false;
        }
    }

    //复制
    public static function copy($id, $main_organization_id, $userid)
    {
        $info = Db::table('jy_emission_source')
            ->field('name,source_id,active,ghg_cate,ghg_type,iso_cate,iso_type,active_data,active_unit,
            active_department,active_type,active_score,unitary_ratio,factor_type,factor_score,factor_source,year')
            ->where('id', (int)$id)
            ->find();
        $info['name'] = $info['name'] . '(副本)';
        $info['source_id'] = substr(implode(NULL, array_map('ord', str_split(substr(uniqid(), 7, 13), 1))), 0, 10);
        $info['create_time'] = date('Y-m-d H:i:s');
        $info['modify_time'] = date('Y-m-d H:i:s');
        $info['create_by'] = $userid;
        $info['modify_by'] = $userid;
        $info['modify_time'] = date('Y-m-d H:i:s');
        $info['main_organization_id'] = $main_organization_id;

        $gas = Db::table('jy_emission_gas')
            ->field('gas,gas_type,factor_value,molecule,denominator')
            ->where('emi_id', (int)$id)
            ->select()
            ->toArray();
        Db::startTrans();
        try {
            $add_id = Db::table('jy_emission_source')->insertGetId($info);
            if ($gas) {
                foreach ($gas as $k => $v) {
                    $v['emi_id'] = $add_id;
                    $v['create_time'] = date('Y-m-d H:i:s');
                    $v['create_by'] = $userid;
                    $gas[$k] = $v;
                }
                Db::table('jy_emission_gas')->insertAll($gas);
            }
            Db::commit();
            return true;
        } catch (\think\exception\ValidateException $e) {
            Db::rollback();
            return false;
        }

    }

}