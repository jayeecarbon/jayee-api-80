<?php

namespace app\model\attestation;

use think\facade\Db;

/**
 * FactorModel
 */
class AttestationModel extends Db
{

    /**
     * getcount 统计认证数据
     *
     * @return $del
     */
    const  status_name = [
             '1'   =>'待付款',
             '2'   => '待认证',
             '3'   => '待线上核查',
             '4'  => '待现场核查',
             '5'  => '已驳回',
             '6'  => '现场核查中',
             '7'  => '认证完成',
             '8'  => '待补充材料',
            '10'  => '证书已派发',
            '12' => '已取消',
            '13' => '已退款',
        ];
    const ip = "http://47.122.18.241:81";
    public static function getcount($main_organization_id)
    {
        //已申请项目
        $aready = Db::table('jy_attestation')
            ->whereIn('status', [1,2, 3, 4, 5, 6, 7, 8, 10,13])
            ->where('main_organization_id',$main_organization_id)
            ->count();
        //已认证项目
        $finish = Db::table('jy_attestation')
            ->where('status', 10)
            ->where('main_organization_id',$main_organization_id)
            ->count();
        //申请中项目
        $useing = $aready-$finish;

        $return = [];
        $return['aready'] = $aready;
        $return['useing'] = $useing;
        $return['finish'] = $finish;
        return $return;

    }

    /**
     * getList 查询认证列表
     *
     * @param $page_ize
     * @param $page_index
     * @param $filters
     * @return $list
     */
    public function getList($page_size, $page_index, $filters)
    {
        $where = array();

        if (isset($filters['product_name']) && !empty($filters['product_name'])) {
            $where[] = array(['jcp.product_name', 'like', '%' . trim($filters['product_name']) . '%']);
        }
        if (isset($filters['status']) && !empty($filters['status'])) {
            $where[] = array(['jcp.status', '=', $filters['status']]);
        }
        if (isset($filters['attestation_type']) && !empty($filters['attestation_type'])) {
            $where[] = array(['jcp.attestation_type', '=', $filters['attestation_type']]);
        }
        if (isset($filters['create_time_start']) && !empty($filters['create_time_start'])) {
            $where[] = array(['jcp.create_time', '>=', $filters['create_time_start']]);
        }
        if (isset($filters['create_time_end']) && !empty($filters['create_time_end'])) {
            $where[] = array(['jcp.create_time', '<=', $filters['create_time_end']]);
        }

        $list = Db::table('jy_attestation jcp')
            ->field('jcp.id,jcp.product_name,jcp.attestation_type,jcp.product_no,jcp.status,
            jcp.create_time,o.name organization_name,jcp.files,jcp.report_files,jcp.certificate_files,jcp.report_files')
            ->leftJoin('jy_organization o', 'jcp.main_organization_id=o.id')
            ->where($where)
            ->where('jcp.main_organization_id',$filters['main_organization_id'])
            ->order('jcp.modify_time', 'desc')
            ->paginate(['list_rows' => $page_size, 'page' => $page_index]);

        return $list;
    }

    /**
     * getAllApproval 获取认证列表中待处理数据
     *
     * @param $main_organization_id
     * @return $list
     */
    public static function getAllApproval($main_organization_id) {
        $list = Db::table('jy_attestation jcp')
            ->field('jcp.id,jcp.product_name,jcp.attestation_type,jcp.product_no,jcp.status,jcp.data_from,jcp.modify_by,
            jcp.create_time,jcp.modify_time,o.name organization_name,jcp.files,jcp.report_files,jcp.certificate_files,jcp.report_files')
            ->leftJoin('jy_organization o', 'jcp.main_organization_id=o.id')
            ->whereIn('jcp.status', '2,5,10')
            ->where('jcp.main_organization_id',$main_organization_id)
            ->order('jcp.modify_time', 'desc')
            ->select();

        return $list;
    }

    /**
     * add 添加认证
     *
     * @param $data
     * @return $add
     */
    public static function add($data, $user_id)
    {
        //查询组织
        Db::startTrans();
        try {

            $add_id = Db::table('jy_attestation')->insertGetId($data);
            /*Db::table('jy_attestation_course')
                ->insert(
                    [
                    'attestation_id' => $add_id,
                    'status' => 1,
                    'title' => '提交认证申请',
                    'create_by' => $user_id,
                    'create_time' => date('Y-m-d H:i:s'),
                    'data_from' => 1
                ]
                );
            Db::table('jy_attestation_course')
                ->insert(
                    [
                        'attestation_id' => $add_id,
                        'status' => 1,
                        'title' => '待线上核查',
                        'create_by' => $user_id,
                        'create_time' => date('Y-m-d H:i:s'),
                        'data_from' => 1
                    ]
                );*/
            //添加订单表数据
            Db::table('jy_attestation_order')
                ->insert([
                    'attestation_id' => $add_id,
                    'status' => 1,
                    'order_num' => date('Ymd') . mt_rand(100, 999) . mt_rand(1000, 9999) . mt_rand(1, 9),
                    'create_by' => $user_id,
                    'create_time' => date('Y-m-d H:i:s'),
                    'modify_time'=>date('Y-m-d H:i:s'),
                    'data_from' => 1
                ]);

            Db::commit();
            return true;
        } catch (\think\exception\ValidateException $e) {
            Db::rollback();
            return false;
        }
    }

    /**
     * del 删除
     *
     * @return $del
     */
    public static function del($data)
    {
        Db::startTrans();
        try {
            $del = Db::table('jy_attestation')->where('id', $data['id'])->delete();
            Db::table('jy_attestation_original')->where('attestation_id', $data['id'])->delete();
            Db::table('jy_attestation_original_info')->where('attestation_id', $data['id'])->delete();
            Db::table('jy_attestation_transport_info')->where('attestation_id', $data['id'])->delete();
            Db::commit();
            return true;
        } catch (\think\exception\ValidateException $e) {
            Db::rollback();
            return false;
        }
    }

    /**
     * getInfoById 获取详情
     *
     * @return $del
     */
    public static function getInfoById($id)
    {

        $list = Db::table('jy_attestation jcp')
            ->field('jcp.id,jcp.username,jcp.mobile,jcp.email,jcp.attestation_type,jcp.product_name,
            jcp.product_no,jcp.client_name,jcp.client_address,jcp.client_detail_address,jcp.produce_name,
            jcp.produce_address,jcp.produce_detail_address,jcp.company_name,jcp.company_address,
            jcp.company_detail_address,
            jcp.data_time_border_start,jcp.data_time_border_end,jcp.system_boder,jcp.functions,jcp.files,
            jcp.from,jcp.product_id,jcp.product_calculate_id,jcp.status,jcp.data_from')
            ->where('jcp.id', (int)$id)
            ->find();


        $product = [];
        if ($list) {
            $status_name = self::status_name[$list['status']];
            $list['status_name'] = $status_name;
            $file = Db::table('jy_file f')
                ->field('f.id,f.file_path,f.source_name')
                ->whereIn('f.id', $list['files'])
                ->select()
                ->toArray();
            if ($file) {
                foreach ($file as $k => $v) {
                    $v['file_path'] = self::ip . $v['file_path'];
                    $file[$k] = $v;
                }
            }
            $list['files'] = $file;
            //查询产品
            if ($list['from'] == 1) {
                $product = Db::table('jy_product_calculate jpc')
                    ->field('0 + CAST(jpc.number AS CHAR) number, jpc.unit_type, jpc.unit,
             jyu.name unit_str, jpc.scope, jpc.stage,jpc.id product_calculate_id, jpc.week_start, jpc.week_end, 
             CONCAT_WS("-", jpc.week_start, jpc.week_end) week, jpc.remarks, jpc.state,
              DATE_FORMAT(jpc.modify_time, "%Y-%m-%d") modify_time, 0 + CAST(jpc.emissions AS CHAR) emissions,
               0 + CAST(jpc.coefficient AS CHAR) coefficient')
                    ->field('jp.id,jp.product_name, jp.product_no, jp.product_spec, jp.files')
                    ->leftJoin('jy_product jp', 'jp.id = jpc.product_id')
                    ->leftJoin('jy_unit jyu', 'jyu.id = jpc.unit')
                    ->where('jpc.id', (int)$list['product_calculate_id'])
                    ->find();

            } else {
                $product = [];
            }
        }
        $list['product'] = $product;
        //认证流程
        $process = Db::table('jy_attestation_course c')
            ->field('c.id,c.title,c.verify_start_time,c.status,c.create_by,c.verify_end_time,c.verify_mobile,c.retinue,
            c.content,c.photo_files,c.report_files,c.certificate_files,c.verify_name,c.data_from,c.create_time,c.reject_cause,
            c.verify_plan_file,c.statement')
            ->where('c.attestation_id', $id)
            ->order('c.id', 'desc')
            ->select()
            ->toArray();
        $return_pro = [];
        if ($process) {
            $process_one = ['待线上核查', '待预约现场核查', '待现场核查', '待补充材料', '已完成认证，待派发证书'];
            $process_two = ['提交认证申请', '已通过线上核查' ];

            foreach ($process as $k => $v) {
                $arr_new = [];
                if (in_array($v['title'], $process_one)) {
                    $arr_new['id'] = $v['id'];
                    $arr_new['title'] = $v['title'];
                } elseif (in_array($v['title'], $process_two)) {
                    $arr_new['id'] = $v['id'];
                    $arr_new['title'] = $v['title'];
                    $arr_new['create_time'] = $v['create_time'];
                }  elseif ($v['title'] == '未通过线上核查') {
                    $arr_new['id'] = $v['id'];
                    $arr_new['title'] = $v['title'];
                    $arr_new['create_time'] = $v['create_time'];
                    $arr_new['reject_cause'] = $v['reject_cause'];
                } elseif ($v['title'] == '退回补充材料') {
                    $arr_new['id'] = $v['id'];
                    $arr_new['title'] = $v['title'];
                    $arr_new['create_time'] = $v['create_time'];
                    $arr_new['statement'] = $v['statement'];
                }elseif ($v['title'] == '已预约现场核查') {
                    $arr_new['id'] = $v['id'];
                    $arr_new['title'] = $v['title'];
                    $arr_new['verify_name'] = $v['verify_name'];
                    $arr_new['verify_mobile'] = $v['verify_mobile'];
                    $arr_new['verify_start_time'] = $v['verify_start_time'];
                    $arr_new['verify_end_time'] = $v['verify_end_time'];
                    $arr_new['retinue'] = $v['retinue'];
                    $arr_new['content'] = $v['content'];
                    $arr_new['verify_plan_file'] = "";
                    $arr_new['create_time'] = $v['create_time'];
                    if (!empty($v['verify_plan_file'])) {
                        $verify_files = Db::table('jy_file')
                            ->field('id,file_path,source_name name')
                            ->whereIn('id', $v['verify_plan_file'])
                            ->select()
                            ->toArray();
                        foreach ($verify_files as $vk => $fv) {
                            $fv['file_path'] = self::ip . $fv['file_path'];
                            $verify_files[$vk] = $fv;
                        }
                        $arr_new['verify_plan_file']=$verify_files;
                    }
                } elseif ($v['title'] == '现场核查已通过') {
                    $arr_new['id'] = $v['id'];
                    $arr_new['title'] = $v['title'];
                    $arr_new['create_time'] = $v['create_time'];
                } elseif ($v['title'] == '证书已派发') {
                    $arr_new['id'] = $v['id'];
                    $arr_new['title'] = $v['title'];
                    $arr_new['certificate_files'] = $v['certificate_files'];
                    $arr_new['report_files'] = $v['report_files'];
                    $arr_new['create_time'] = $v['create_time'];
                }
                $arr_new['username']=[];
                if ($v['data_from'] == 1) {
                    $find_name = Db::table('jy_user')
                        ->field('username')
                        ->where('id', $v['create_by'])
                        ->find();
                    if($find_name){
                        $arr_new['username'] = $find_name['username'];
                    }
                } elseif ($v['data_from'] == 2) {
                    $find_name = Db::table('jy_mange_user')
                        ->field('username')
                        ->where('id', $v['create_by'])
                        ->find();
                    if($find_name){
                        $arr_new['username'] = $find_name['username'];
                    }
                } elseif ($v['data_from'] == 3) {
                    $find_name = Db::table('jy_cooperate_user')
                        ->field('username')
                        ->where('id', $v['create_by'])
                        ->find();
                    if($find_name){
                        $arr_new['username'] = $find_name['username'];
                    }
                }

                if ($v['title'] == '待线上核查') {
                    unset($arr_new['username']);
                }


                if (!empty($v['photo_files'])) {
                    $photo_files = Db::table('jy_file')
                        ->field('id,file_path,source_name name')
                        ->whereIn('id', $v['photo_files'])
                        ->select()
                        ->toArray();
                    $big_file = [];
                    foreach ($photo_files as $pk => $pv) {
                        $pv['file_path'] =self::ip . $pv['file_path'];
                        $big_file[] = $pv['file_path'];
                        $photo_files[$pk] = $pv;
                    }
                    $arr_new['photo_files'] = $photo_files;
                    $arr_new['photo_count'] = count($photo_files);
                    $arr_new['big_pic_list'] = $big_file;

                }
                if (!empty($v['report_files'])) {
                    $report_files = Db::table('jy_file')
                        ->field('id,file_path,source_name name')
                        ->whereIn('id', $v['report_files'])
                        ->select()
                        ->toArray();
                    $arr_new['report_files'] = $report_files;
                }
                if (!empty($v['certificate_files'])) {
                    $certificate_files = Db::table('jy_file')
                        ->field('id,file_path,source_name name')
                        ->whereIn('id', $v['certificate_files'])
                        ->select()
                        ->toArray();
                    $arr_new['certificate_files'] = $certificate_files;
                }
                $return_pro[] = $arr_new;
            }
        }
        $list['process'] = $return_pro;
        return $list;
    }

    /**
     * getTitleById 获取认证名称
     *
     * @return $del
     */
    public static function getTitleById($id)
    {
        $list = Db::table('jy_attestation jcp')
            ->field('jcp.product_name')
            ->where('jcp.id', (int)$id)
            ->find();
        if ($list) {
            return $list['product_name'];
        } else {
            return "";
        }

    }


    /**
     * updates 修改
     *
     * @return $del
     */
       public static function updates($data)
    {
        $find_info = Db::table('jy_attestation')->field('product_calculate_id')->where('id',$data['id'])->find();

        if($data['product_calculate_id']==$find_info['product_calculate_id']){

            Db::table('jy_attestation')
                ->save($data);
            return true;
        }else{

           // $insert_form = self::insertForm($data['id'],$data['product_calculate_id'],$data['modify_by']);
            //查询表单数据
            $product_data = Db::table('jy_product_data jpd')
                ->field('jpd.id,jpd.data_stage, jpd.name,st.name data_stages,jpd.category,jpd.category category_id, jds.name category_name,
            0 + CAST(jpd.number AS CHAR) number, jpd.unit, ju.type_id unit_type, ju.name unit_str,
             jpd.match_type, jpd.match_id, 0 + CAST(jpd.emissions AS CHAR) emissions,
              0 + CAST(jpd.coefficient AS CHAR) coefficient, jpd.materials, jpd.units, jpd.numbers,jpd.source,
              jpd.factor_title,jpd.factor_molecule,jpd.factor_denominator,jpd.factor_year,jpd.factor_mechanism')
                ->leftJoin('jy_data_stage jds', 'jds.id = jpd.category')
                ->leftJoin('jy_data_stage st', 'st.id = jpd.data_stage')
                ->leftJoin('jy_unit ju', 'ju.id = jpd.unit')
                ->order(['jpd.modify_time' => 'desc', 'jpd.create_time' => 'desc'])
                ->where(['jpd.product_calculate_id' => (int)$data['product_calculate_id'], 'jpd.is_del' => 1])
                ->select()
                ->toArray();
           // halt($product_data);

            Db::startTrans();
            try {
                //删除表单数据
                Db::table('jy_attestation_original')->where('attestation_id', $data['id'])->delete();
                //删除运输
                Db::table('jy_attestation_transport_info')->where('attestation_id', $data['id'])->delete();
                //删除对比数据
                Db::table('jy_attestation_original_info')
                    ->where('attestation_id', $data['id'])->delete();

                if ($product_data) {

                    foreach ($product_data as $v) {
                        if($v['match_type']==1 &&$v['factor_title']==NULL && $v['coefficient']==NULL && $v['factor_molecule']==NULL){
                            continue;
                        }
                        // 通过match_id = id
                        // source = 1 查jy_product_calculate核算产品表 source = 2 查jy_customer_product客户产品表
                        $one_original_data = [];
                        $one_transport_data = [];
                        $one_original_data['data_stage'] = $v['data_stages'];
                        $one_original_data['data_stage_code'] = $v['data_stage'];
                        $one_original_data['category'] = $v['category_name'];
                        $one_original_data['category_code'] = $v['category_id'];
                        $one_original_data['attestation_id'] = $data['id'];
                        $one_original_data['name'] = $v['name'];
                        $one_original_data['unit_type'] = $v['unit_type'];
                        $one_original_data['unit'] = $v['unit'];
                        $one_original_data['number'] = $v['number'];
                        $one_original_data['factor_type'] = $v['match_type'];
                        $one_original_data['factor_source'] = $v['source'];//来源 判断产品
                        $one_original_data['factor_from'] = 2;
                        $one_original_data['data_from'] = 1;
                        $one_original_data['create_time'] = date('Y-m-d H:i:s');
                        $one_original_data['modify_time'] = date('Y-m-d H:i:s');
                        $one_original_data['create_by'] = $data['modify_by'];

                        //因子数据
                        if($v['match_type']==1){
                           $factor_info =  Db::table('jy_factor_snapshot')->where('id', $v['match_id'])->find();
                            $one_original_data['factor_id'] = $v['match_id'];
                            $one_original_data['factor_title'] = $factor_info['title'];
                            $one_original_data['factor_value'] =$factor_info['factor_value'];
                            $one_original_data['factor_molecule'] =$factor_info['molecule'];
                            $one_original_data['factor_denominator'] = $factor_info['denominator'];
                            $one_original_data['factor_year'] =$factor_info['year'];
                            $one_original_data['factor_mechanism'] = $factor_info['mechanism'];
                    }else{
                            $one_original_data['factor_title'] = "";
                            $one_original_data['factor_value'] = "";
                            $one_original_data['factor_molecule'] = "";
                            $one_original_data['factor_denominator'] = "";
                            $one_original_data['factor_year'] = "";
                            $one_original_data['factor_mechanism'] = "";
                        }
                        $one_original_data['factory_data_list']= NULL;
                        if($v['match_type']==2 && $v['category_name']=='原材料'){
                            //产品id
                            $use_data = [];
                            if($v['match_id']!=NULL){
                                $product_factor_list =  self::getProductFactor($v['match_id'],$v['source'],$use_data);
                                $one_original_data['factory_data_list'] = $product_factor_list ? json_encode($product_factor_list):json_encode([]);
                            }else{
                                $one_original_data['factory_data_list'] =json_encode([]);
                            }

                        }
                        // halt($one_original_data);
                        $original_id = Db::table('jy_attestation_original')->insertGetId($one_original_data);
                        if (in_array($v['category_id'], [8, 12, 14, 16, 18])) {
                            //运输数据
                            $materials = explode(',', $v['materials']);
                            $units = explode(',', $v['units']);
                            $numbers = explode(',', $v['numbers']);
                            if (!empty($materials) && !empty($units) && !empty($numbers)) {
                                foreach ($materials as $kk => $vv) {
                                    $one_transport_data['attestation_id'] = $data['id'];
                                    $one_transport_data['original_id'] = $original_id;
                                    $one_transport_data['product_name'] = $materials[$kk];
                                    $one_transport_data['number'] = $numbers[$kk];
                                    $one_transport_data['unit'] = $units[$kk];
                                    Db::table('jy_attestation_transport_info')->insertGetId($one_transport_data);
                                }
                            }
                        }
                    }
                };
                Db::table('jy_attestation')
                    ->save($data);
                Db::commit();
                return true;
            } catch (\think\exception\ValidateException $e) {
                Db::rollback();
                return false;
            }
        }
    }
       public static function getProductFactor($match_id,$source,$use_data){
        $product_data=[];
           if($source==1){
               //自产

               $pc = Db::table('jy_product_calculate')->where('id',$match_id)->find();
               $product_data = Db::table('jy_product_data jpd')
                   ->field('jpd.id, jpd.name,jpd.match_id,jpd.source,jpd.match_type,ju.name unit_str,
                   ut.unit_name unit_type_str,0 + CAST(jpd.number AS CHAR) number')
                   ->leftJoin('jy_unit_type ut', 'ut.id = jpd.unit_type')
                   ->leftJoin('jy_data_stage jds', 'jds.id = jpd.category')
                   ->leftJoin('jy_product_calculate ca','ca.product_id=jpd.product_id')
                   ->leftJoin('jy_unit ju', 'ju.id = jpd.unit')
                   ->order(['jpd.modify_time' => 'desc', 'jpd.create_time' => 'desc'])
                   ->where(['jpd.product_id' => (int)$pc['product_id'], 'jpd.is_del' => 1,'jpd.product_calculate_id'=>$match_id])
                   ->where('ca.state',1)
                   ->select()
                   ->toArray();

               if($product_data){
                   foreach ($product_data as $v){
                       if($v['match_type']==1){
                           //因子
                           $one_original_data=[];
                           $factor_info =  Db::table('jy_factor_snapshot')
                               ->field('id,title,0 + CAST(factor_value AS CHAR) factor_value,molecule,denominator,year,
                               mechanism')
                               ->where('id', $v['match_id'])->find();
                           $one_original_data['id'] = $factor_info['id'];
                           $one_original_data['name'] = $v['name'];
                           $one_original_data['title'] = $factor_info['title'];
                           $one_original_data['factor_value'] =$factor_info['factor_value'];
                           $one_original_data['molecule'] =$factor_info['molecule'];
                           $one_original_data['denominator'] = $factor_info['denominator'];
                           $one_original_data['year'] =$factor_info['year'];
                           $one_original_data['mechanism'] = $factor_info['mechanism'];
                           $one_original_data['unit_str'] = $v['unit_str'];
                           $one_original_data['unit_type_str'] = $v['unit_type_str'];
                           $one_original_data['number'] = $v['number'];
                           $use_data[] =$one_original_data;
                       }else{
                           //产品
                           self::getProductFactor($v['match_id'],$v['source'],$use_data);
                       }
                   }
                   return $use_data;
               }else{
                   return $use_data;
               }
           }elseif($source==2){
               //外采
               $product_data = Db::table('jy_customer_product_discharge jpd')
                   ->field('jpd.name,0 + CAST(jpd.value AS CHAR) number,jpd.unit,jpd.year,jpd.factor_name title,jpd.factor_source  mechanism,
                   ju.name unit_str,0 + CAST(jpd.coefficient AS CHAR) factor_value,jpd.factor_unit  molecule,ut.unit_name unit_type_str')
                   ->leftJoin('jy_unit_type ut', 'ut.id = jpd.unit_type')
                   ->leftJoin('jy_customer_product pro', 'pro.id = jpd.customer_product_id')
                   ->leftJoin('jy_unit ju', 'ju.id = jpd.unit')
                   ->order(['jpd.modify_time' => 'desc', 'jpd.create_time' => 'desc'])
                   ->where(['jpd.customer_product_id' => (int)$match_id, 'jpd.is_del' => 0])
                   ->where('pro.audit_state',4)
                   ->select()
                   ->toArray();
               if($product_data){
                   foreach ($product_data as $v){
                           //因子
                           $use_data[] =$v;
                   }
                   return $use_data;
               }else{
                   return $use_data;
               }
           }

    }
    //系统选择插入表单数据
    public static function insertForm($attestation_id, $product_calculate_id, $create_by)
    {
        //查询表单数据
        $product_data = Db::table('jy_product_data jpd')
            ->field('jpd.data_stage, jpd.name,st.name data_stages,jpd.category category_id, jds.name category,
            0 + CAST(jpd.number AS CHAR) number, jpd.unit, ju.type_id unit_type, ju.name unit_str,
             jpd.match_type, jpd.match_id, 0 + CAST(jpd.emissions AS CHAR) emissions,
              0 + CAST(jpd.coefficient AS CHAR) coefficient, jpd.materials, jpd.units, jpd.numbers,jpd.source')
            ->leftJoin('jy_data_stage jds', 'jds.id = jpd.category')
            ->leftJoin('jy_data_stage st', 'st.id = jpd.data_stage')
            ->leftJoin('jy_unit ju', 'ju.id = jpd.unit')
            ->order(['jpd.modify_time' => 'desc', 'jpd.create_time' => 'desc'])
            ->where(['jpd.product_calculate_id' => (int)$product_calculate_id, 'jpd.is_del' => 1])
            ->select()
            ->toArray();

        Db::startTrans();
        try {
            //删除表单数据
            Db::table('jy_attestation_original')->where('attestation_id', $attestation_id)->delete();
            //删除运输
            Db::table('jy_attestation_transport_info')->where('attestation_id', $attestation_id)->delete();
            //删除对比数据
            Db::table('jy_attestation_original_info')
                ->where('attestation_id', $attestation_id)->delete();
            if ($product_data) {
                foreach ($product_data as $v) {

                    // 通过match_id = id
                    // source = 1 查jy_product_calculate核算产品表 source = 2 查jy_customer_product客户产品表
                    $one_original_data = [];
                    $one_transport_data = [];
                    if($v['match_type']==1){
                        $one_original_data['data_stage'] = $v['data_stages'];
                        $one_original_data['data_stage_code'] = $v['data_stage'];
                        $one_original_data['category'] = $v['category'];
                        $one_original_data['category_code'] = $v['category_id'];
                        $one_original_data['attestation_id'] = $attestation_id;
                        $one_original_data['name'] = $v['name'];
                        $one_original_data['unit_type'] = $v['unit_type'];
                        $one_original_data['unit'] = $v['unit'];
                        $one_original_data['number'] = $v['number'];
                        $one_original_data['factor_id'] = $v['match_id'];
                        $one_original_data['factor_type'] = $v['match_type'];
                        $one_original_data['factor_source'] = $v['source'];//来源 判断产品
                        $one_original_data['factor_from'] = 2;
                        $one_original_data['data_from'] = 1;
                        $one_original_data['create_time'] = date('Y-m-d H:i:s');
                        $one_original_data['modify_time'] = date('Y-m-d H:i:s');
                        $one_original_data['create_by'] = $create_by;
                        // halt($one_original_data);

                        $original_id = Db::table('jy_attestation_original')->insertGetId($one_original_data);
                        if (in_array($v['category_id'], [8, 12, 14, 16, 18])) {
                            //运输数据
                            $materials = explode(',', $v['materials']);
                            $units = explode(',', $v['units']);
                            $numbers = explode(',', $v['numbers']);
                            if (!empty($materials) && !empty($units) && !empty($numbers)) {
                                foreach ($materials as $kk => $vv) {
                                    $one_transport_data['attestation_id'] = $attestation_id;
                                    $one_transport_data['original_id'] = $original_id;
                                    $one_transport_data['product_name'] = $materials[$kk];
                                    $one_transport_data['number'] = $numbers[$kk];
                                    $one_transport_data['unit'] = $units[$kk];
                                    Db::table('jy_attestation_transport_info')->insertGetId($one_transport_data);
                                }
                            }
                        }
                    }


                }
            };
            Db::commit();
            return true;
        } catch (\think\exception\ValidateException $e) {
            Db::rollback();
            return false;
        }


    }

    /**
     * getdata 获取表单数据
     *
     * @return $del
     */
    public static function getdata($id)
    {
        $data = Db::table('jy_attestation a')
            ->field('a.id attestation_id,a.product_name,from,a.system_boder,a.product_no,
            a.data_time_border_start,a.data_time_border_end,a.system_boder,a.product_id,a.product_calculate_id,a.status')
            ->where('id', (int)$id)
            ->find();

        $return = [];
        $product_list = [];

        if ($data) {
            if ($data['system_boder'] == 1) {

                $list = Db::table('jy_data_stage')
                    ->field('id,pid,name')
                    ->where('pid', 0)
                    ->whereIn('name', ['原材料获取', '生产制造', '分销零售'])
                    ->order('id', 'asc')
                    ->select();
                foreach ($list as $k => $v) {
                    $children = Db::table('jy_data_stage s')
                        ->field('s.name as data_stage,s.id,s.pid')
                        ->where('pid', $v['id'])
                        ->select()->toArray();
                    foreach ($children as $kk => $vv) {
                        $vv['num'] = 0;
                        $vv['id'] = (string)$vv['id'];
                        $vv['pid'] = (string)$vv['pid'];
                        $children[$kk] = $vv;
                    }
                    $return[$k]['data_stage'] = $v['name'];
                    $return[$k]['id'] = (string)$v['id'];
                    $return[$k]['pid'] = (string)$v['pid'];
                    $return[$k]['num'] = 0;
                    $return[$k]['children'] = $children;
                }

            } elseif ($data['system_boder'] == 2) {

                $list = Db::table('jy_data_stage')
                    ->field('id,pid,name')
                    ->where('pid', 0)
                    ->order('id', 'asc')
                    ->select();
                foreach ($list as $k => $v) {
                    $children = Db::table('jy_data_stage s')
                        ->field('s.name as data_stage,s.id,s.pid')
                        ->where('pid', $v['id'])
                        ->select()
                        ->toArray();
                    foreach ($children as $kk => $vv) {
                        $vv['num'] = 0;
                        $vv['id'] = (string)$vv['id'];
                        $vv['pid'] = (string)$vv['pid'];
                        $children[$kk] = $vv;
                    }
                    $return[$k]['data_stage'] = $v['name'];
                    $return[$k]['id'] = (string)$v['id'];
                    $return[$k]['pid'] = (string)$v['pid'];
                    $return[$k]['num'] = 0;
                    $return[$k]['children'] = $children;
                }
            }
            foreach ($return as $v) {
                $one_data = [];
                $pro_one = Db::table('jy_product_data')
                    ->field('name,id')
                    ->where([
                        'product_calculate_id' => $data['product_calculate_id'],
                        'data_stage' => $v['id']
                    ])->order('id', 'asc')
                    ->select()
                    ->toArray();
                $one_data['id'] = $v['id'];
                $one_data['data_stage'] = $v['data_stage'];
                $one_data['product_data'] = $pro_one;
                $product_list[] = $one_data;
            }
        } else {
            return false;
        }

        $form_list = Db::table('jy_attestation_original s')
            ->field('s.id,s.data_stage,s.data_stage_code,s.category,s.category_code,s.name,s.number,s.unit_type,
            s.unit,s.source,s.factor_id,s.content,s.err_status,s.err_msg,s.files,s.type,s.factor_type,s.factor_source,s.factor_from,
            s.factor_title,s.factor_value,s.factor_molecule,s.factor_source,s.factor_denominator,s.factor_year,s.factor_mechanism,s.factory_data_list')
            ->where('s.attestation_id', $id)
            ->order('s.id', 'desc')
            ->select()
            ->toArray();


        $soureList = [];
        if ($form_list) {
            foreach ($form_list as $k => $v) {
                $one_data = [];
                $material = [];
                $material['name'] = $v['name'];
                $material['number'] = $v['number'];
                $material['unit_type'] = $v['unit_type'];
                $material['unit'] = $v['unit'];
                $material['source'] = $v['source'];

                $one_data['id'] = $v['id'];
                $one_data['type'] = $v['type'];
                $one_data['content'] = $v['content'];
                $one_data['err_msg'] = $v['err_msg'];
                $one_data['err_status'] = $v['err_status'];
                $one_data['material'] = $material;
                $one_data['data_stage'] = $v['data_stage'];
                $one_data['data_stage_code'] = $v['data_stage_code'];
                $one_data['category'] = $v['category'];
                $one_data['category_code'] = $v['category_code'];
                $one_data['factor_from'] = $v['factor_from'];
                if($v['factory_data_list']){
                    $one_data['factory_data_list'] = json_decode($v['factory_data_list']);
                }else{
                    $one_data['factory_data_list'] = NULL;
                }

                //文件数据
                $v_file = Db::table('jy_file s')
                    ->field('s.id,s.source_name name,s.file_path')
                    ->whereIn('id', $v['files'])
                    ->select()
                    ->toArray();
                $one_data['files'] = $v_file;
                //因子数据或者产品数据
                $factor_data=[];
                $factor_data['id'] = $v['factor_id'];
                $factor_data['title'] =$v['factor_title'];
                $factor_data['factor_value'] = number_format($v['factor_value'], 6);
                $factor_data['molecule'] = $v['factor_molecule'];
                $factor_data['denominator'] = $v['factor_denominator'];
                $factor_data['year'] = $v['factor_year'];
                $factor_data['mechanism'] = $v['factor_mechanism'];
                $one_data['factor'] = $factor_data;
                //对比数据
                $com = Db::table('jy_attestation_original_info s')
                    ->where('original_id', (int)$v['id'])
                    ->select()
                    ->toArray();
                if ($com) {
                    foreach ($com as $kk => $vv) {
                        $files = Db::table('jy_file s')
                            ->field('s.id,s.source_name name,s.file_path')
                            ->whereIn('id', $vv['files'])
                            ->select()
                            ->toArray();
                        $vv['files'] = $files;
                        $com[$kk] = $vv;
                    }
                    $one_data['compareDataList'] = $com;
                }
                //运输数据
                $trans = [];
                if (in_array($v['category_code'], [8, 12, 14, 16, 18])) {
                    $trans = Db::table('jy_attestation_transport_info s')
                        ->where('original_id', $v['id'])
                        ->select()
                        ->toArray();
                    if ($trans) {
                        $one_data['transport'] = $trans;
                    }
                } else {
                    $one_data['transport'] = [];
                }
                $soureList[] = $one_data;
            }
        }

        $back = [];
        $data['soureList'] = $soureList;
        $data['product_list'] = $product_list;
        $back['leftmenu'] = $return;
        $back['topdata'] = $data;
        return $back;
    }

    /**
     * addform 添加认证表单
     *
     * @param $data
     * @return $add
     */
    public static function addform($data)
    {
        $exists = Db::table('jy_attestation_original')
            ->where(
                [
                    'data_stage_code' => $data['data_stage_code'],
                    'category_code' => $data['category_code'],
                    'name' => $data['name'],
                    'attestation_id'=>$data['attestation_id']
                ]
            )->find();
        if ($exists) {
            return false;
        }
        $material = $data['compareDataList'];
        $transport = $data['transport'];
        unset($data['compareDataList']);
        unset($data['transport']);
        unset($data['del_transport']);
        unset($data['del_compareDataList']);
        $snapshot_info =  Db::table('jy_factor')
            ->field('title,factor_id,language,title_language,model,describtion,mechanism,mechanism_short,grade,uncertainty,
            organization_id,year,country,region,file_name,factor_value,molecule,denominator,unit_type,factor_source,sort,status,f23,f24')
            ->where('id',$data['factor_id'])->find();

        Db::startTrans();
        try {

            $snapshot_info['organization_id'] = $data['organization_id'];
            $snapshot_info['create_time'] = date('Y-m-d H:i:s');
            $snapshot_info['create_by'] = $data['create_by'];
           $snapshot_id =  Db::table('jy_factor_snapshot')->insertGetId($snapshot_info);

           unset($data['organization_id']);
            $data['factor_id'] = $snapshot_id;
            $original_id = Db::table('jy_attestation_original')->insertGetId($data);

            foreach ($material as $k => $v) {
                unset($v['unitTypeOptions']);
                unset($v['valiFileListText']);
                unset($v['unitOptions']);
                unset($v['fileList']);
                $v['attestation_id'] = $data['attestation_id'];
                $v['original_id'] = $original_id;
                $v['create_time'] = date('Y-m-d H:i:s');
                $material[$k] = $v;
            }
            Db::table('jy_attestation_original_info')->insertAll($material);

            if (in_array($data['category_code'], [8, 12, 14, 16, 18])) {
                //运输
                foreach ($transport as $k => $v) {
                    $v['attestation_id'] = $data['attestation_id'];
                    $v['original_id'] = $original_id;
                    $v['create_time'] = date('Y-m-d H:i:s');
                    $transport[$k] = $v;
                }
                Db::table('jy_attestation_transport_info')->insertAll($transport);
            }
            Db::commit();
            return true;
        } catch (\think\exception\ValidateException $e) {
            Db::rollback();
            return false;
        }
    }

    //编辑认证表单
    public static function editform($data)
    {
        $exists = Db::table('jy_attestation_original')
            ->where(
                [
                    'data_stage_code' => $data['data_stage_code'],
                    'category_code' => $data['category_code'],
                    'name' => $data['name'],
                    'attestation_id'=>$data['attestation_id']
                ]
            )
            ->where('id', '<>', $data['id'])
            ->find();

        if ($exists) {
            return 2;
        }
        $material = $data['compareDataList'];
        $transport = $data['transport'];
        $del_transport = $data['del_transport'];//运输
        $del_material = $data['del_compareDataList'];//对比
        unset($data['compareDataList']);
        unset($data['transport']);
        unset($data['del_compareDataList']);
        unset($data['del_transport']);
        if($data['factor_id']){
           $original_info =  Db::table('jy_attestation_original')->where('id',$data['id'])->find();
           if($original_info['factor_id']!=$data['factor_id']){
               $snapshot_info =  Db::table('jy_factor')
                   ->field('title,factor_id,language,title_language,model,describtion,mechanism,mechanism_short,grade,uncertainty,
            organization_id,year,country,region,file_name,factor_value,molecule,denominator,unit_type,factor_source,sort,status,f23,f24')
                   ->where('id',$data['factor_id'])->find();
           }
        }

        Db::startTrans();
        try {
            if($data['factor_id']){
                if($original_info['factor_id']!=$data['factor_id']){
                    $snapshot_info['organization_id'] = $data['organization_id'];
                    $snapshot_info['create_time'] = date('Y-m-d H:i:s');
                    $snapshot_info['create_by'] = $data['modify_by'];
                    $snapshot_id =  Db::table('jy_factor_snapshot')->insertGetId($snapshot_info);
                    $data['factor_id'] = $snapshot_id;
                }
            }
            unset($data['organization_id']);
            $update = Db::table('jy_attestation_original')->save($data);

            foreach ($material as $k => $v) {
                unset($v['unitTypeOptions']);
                unset($v['valiFileListText']);
                unset($v['unitOptions']);
                unset($v['fileList']);

                if (!empty($v['id'])) {
                    $v['modify_time'] = date('Y-m-d H:i:s', time());
                    Db::table('jy_attestation_original_info')->save($v);
                } else {
                    $v['attestation_id'] = $data['attestation_id'];
                    $v['original_id'] = $data['id'];
                    $v['create_time'] = date('Y-m-d H:i:s', time());
                    Db::table('jy_attestation_original_info')->insert($v);
                }
            }

            if (in_array($data['category_code'], [8, 12, 14, 16, 18])) {
                //运输
                foreach ($transport as $k => $v) {

                    if (!empty($v['id'])) {
                        $v['create_time'] = date('Y-m-d H:i:s', time());
                        Db::table('jy_attestation_transport_info')->save($v);
                    } else {
                        unset($v['id']);
                        $v['attestation_id'] = $data['attestation_id'];
                        $v['original_id'] = $data['id'];
                        $v['create_time'] = date('Y-m-d H:i:s', time());
                        Db::table('jy_attestation_transport_info')->insert($v);
                    }
                }
            }
            //删除运输
            if (!empty($del_transport)) {
                Db::table('jy_attestation_transport_info')->whereIn('id', $del_transport)->delete();
            }
            //删除对比数据
            if (!empty($del_material)) {
                Db::table('jy_attestation_original_info')
                    ->whereIn('id', $del_material)->delete();
            }
            Db::commit();
            return 1;
        } catch (\think\exception\ValidateException $e) {
            Db::rollback();
            return false;
        }
    }

    /**
     * delform 删除表单数据
     *
     * @return $del
     */
    public static function delform($data)
    {
        Db::startTrans();
        try {
            Db::table('jy_attestation_original')->where('id', $data['id'])->delete();
            Db::table('jy_attestation_original_info')->where('original_id', $data['id'])->delete();
            //删除运输
            if (in_array($data['category_code'], [8, 12, 14, 16, 18])) {
                Db::table('jy_attestation_transport_info')->where('original_id', $data['id'])->delete();
            }
            Db::commit();
            return true;
        } catch (\think\exception\ValidateException $e) {
            Db::rollback();
            return false;
        }
    }

    /**
     * getTitleById 获取认证名称
     *
     * @return $del
     */
    public static function getFromTitleById($id)
    {
        $list = Db::table('jy_attestation_original jcp')
            ->field('jcp.name')
            ->where('jcp.id', (int)$id)
            ->find();
        if ($list) {
            return $list['name'];
        } else {
            return "";
        }

    }

    /**
     * back 撤回
     *
     * @return $del
     */
    public static function back($id, $userid)
    {
        Db::startTrans();
        try {
            Db::table('jy_attestation')->where('id', $id)->update([
                'status' => 2,
                'modify_time' => date('Y-m-d H:i:s'),
                'data_from' => 1,
                'modify_by' => $userid
            ]);
            Db::table('jy_attestation_course')
                ->where('attestation_id', $id)->delete();

            $back = Db::table('jy_attestation_original')->where('attestation_id', $id)->update([
                'type' => 1,
                'modify_time' => date('Y-m-d H:i:s'),
                'data_from' => 1,
                'modify_by' => $userid
            ]);
            Db::commit();
            return true;
        } catch (\think\exception\ValidateException $e) {
            Db::rollback();
            return false;
        }
    }

    /**
     * submitattestation 提交认证
     *
     * @return $del
     */
    public static function submitattestation($id, $userid)
    {

        $get_status = Db::table('jy_attestation')->field('status')->where('id', $id)->find();
        $has_form = Db::table('jy_attestation_original')->field('id,source,files')->where('attestation_id', $id)->select()->toArray();

        if(empty($has_form)){
            return 2;
        }
        if($has_form){
            foreach ($has_form as $v){

                if($v['source']==null||$v['files']==null){

                    return 2;
                }
            }
            $compary= Db::table('jy_attestation_original_info')->field('id')->where('original_id', $v['id'])
                ->select()->toArray();
            if(empty($compary)){
                return 3;
            }
        }

        Db::startTrans();
        try {
            if ($get_status['status'] == 8) {
                $submit = Db::table('jy_attestation')->where('id', $id)->update([
                    'status' => 6,
                    'modify_time' => date('Y-m-d H:i:s', time()),
                    'modify_by' => $userid,
                    'data_from' => 1
                ]);
                Db::table('jy_attestation_course')->where([
                    'attestation_id' => $id,
                    'title' => '待补充材料'
                ])
                    ->update([
                        'title' => '待现场核查',
                        'create_time' => date('Y-m-d H:i:s', time()),
                        'create_by' => $userid,
                        'data_from' => 1
                    ]);

            } elseif ($get_status['status'] == 2 ||$get_status['status'] == 5) {
                $submit = Db::table('jy_attestation')->where('id', $id)->update([
                    'status' => 3,
                    'modify_time' => date('Y-m-d H:i:s', time()),
                    'modify_by' => $userid,
                    'data_from' => 1
                ]);
                Db::table('jy_attestation_course')
                    ->insert(
                        [
                            'attestation_id' => $id,
                            'status' => 1,
                            'title' => '提交认证申请',
                            'create_by' => $userid,
                            'create_time' => date('Y-m-d H:i:s'),
                            'data_from' => 1
                        ]
                    );
                Db::table('jy_attestation_course')
                    ->insert([
                        'attestation_id' => $id,
                        'status' => 3,
                        'title' => '待线上核查',
                        'create_by' => $userid,
                        'create_time' => date('Y-m-d H:i:s'),
                        'data_from' => 1
                    ]);

            }else{
                Db::table('jy_attestation')->where('id', $id)->update([
                    'status' => 3,
                    'modify_time' => date('Y-m-d H:i:s', time()),
                    'modify_by' => $userid,
                    'data_from' => 2
                ]);
            }


            Db::commit();
            return 1;
        } catch (\think\exception\ValidateException $e) {
            Db::rollback();
            return false;
        }
    }

    public static function getCitys()
    {
        $list = Db::table('jy_city')
            ->field('id, pid, name, level')
            ->select();
        return $list;
    }

    /**
     * getCertificate 下载证书
     *
     * @param $data
     * @return $add
     */
    public static function getCertificate($id)
    {
        $info = Db::table('jy_attestation a')
            ->field('a.certificate_files')
            ->where('a.id', (int)$id)
            ->find();
        $list = Db::table('jy_file')
            ->field('id,source_name,file_path')
            ->whereIn('id', $info['certificate_files'])
            ->select()
            ->toArray();
        return $list;
    }

    /**
     * getReport 下载报告
     *
     * @param $data
     * @return $add
     */
    public static function getReport($id)
    {
        $info = Db::table('jy_attestation a')
            ->field('a.report_files')
            ->where('a.id', (int)$id)
            ->find();
        $list = Db::table('jy_file')
            ->field('id,source_name,file_path')
            ->whereIn('id', $info['report_files'])
            ->select()
            ->toArray();
        return $list;

    }

    //核算产品列表
    public static function getCalculates($page_size, $page_index, $filters)
    {
        $where = array();
        $whereor = [];
        if ($filters['filter_product_name']) {
            $where[] = array(['jp.product_name', 'like', '%' . trim($filters['filter_product_name']) . '%']);
        }

        if ($filters['filter_product_no']) {
            $where[] = array(['jp.product_no', 'like', '%' . trim($filters['filter_product_no']) . '%']);
        }

        if ($filters['filter_week_start']) {
            $whereor[] = array(['jpc.week_start', '>=', $filters['filter_week_start']], ['jpc.week_start', '<=', $filters['filter_week_end']]);
        }

        if ($filters['filter_week_end']) {
            $whereor[] = array(['jpc.week_end', '>=', $filters['filter_week_start']], ['jpc.week_end', '<=', $filters['filter_week_end']]);
        }

        if ($filters['filter_week_start'] && $filters['filter_week_end']) {
            $whereor[] = array(['jpc.week_start', '<=', $filters['filter_week_start']], ['jpc.week_end', '>=', $filters['filter_week_end']]);
        }
        $main_organization_id = $filters['main_organization_id'];

        $where[] = array(['jpc.is_del', '=', 1]);
        $list = Db::table('jy_product_calculate jpc')
            ->field('jpc.id, jpc.product_id, 0 + CAST(jpc.number AS CHAR) number, jpc.unit_type, jpc.unit, jyu.name unit_str, jpc.scope, jpc.stage, jpc.week_start, jpc.week_end, CONCAT_WS("-", jpc.week_start, jpc.week_end) week, jpc.remarks, jpc.state, DATE_FORMAT(jpc.modify_time, "%Y-%m-%d") modify_time, 0 + CAST(jpc.emissions AS CHAR) emissions, 0 + CAST(jpc.coefficient AS CHAR) coefficient')
            ->field('jp.product_name, jp.product_no, jp.product_spec, jp.files')
            ->field('ju.username')
            ->leftJoin('jy_product jp', 'jp.id = jpc.product_id')
            ->leftJoin('jy_user ju', 'ju.id = jpc.modify_by')
            ->leftJoin('jy_unit jyu', 'jyu.id = jpc.unit')
            ->where('jpc.state', 1)
            ->where($where)
            ->where('jpc.main_organization_id',$main_organization_id)
            ->whereIn('jpc.scope', [1, 2])
            ->where(function ($query) use ($whereor) {
                $query->whereOr($whereor);
            })
            ->order(['jpc.modify_time' => 'desc', 'jpc.create_time' => 'desc'])
            ->paginate(['list_rows' => $page_size, 'page' => $page_index]);

        return $list;
    }

    public static function getShotFactor($id) {
        $list = Db::table('jy_factor_snapshot jf')
            ->field('jf.id,jf.title,jf.factor_id,jf.model,jf.describtion,jf.describtion,jf.molecule
            ,jf.denominator,jf.year,jf.country,jf.region,jf.grade,jf.status,jf.factor_source,jf.create_by,
            jf.create_time,jf.file_name,jf.mechanism,jf.uncertainty,jf.factor_value')
            ->where('jf.id', (int)$id)
            ->find();

        return $list;
    }
}