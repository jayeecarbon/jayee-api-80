<?php
declare (strict_types = 1);

namespace app\model\data;

use think\facade\Db;

/**
 * ParamModel
 */
class ParamModel extends Db {

    /**
     * 启用/禁用-启用
     */
    CONST STATE_YES = 1;

    /**
     * 启用/禁用-禁用
     */
    CONST STATE_NO = 2;

    /**
     * 是否删除-未删除
     */
    CONST IS_DEL_NO = 1;

    /**
     * 是否删除-已删除
     */
    CONST IS_DEL_YES = 2;

    /**
     * 状态-已完成
     */
    CONST STATE_COMPLETED = 1;

    /**
     * 状态-待审核
     */
    CONST STATE_REVIEW = 2;

    /**
     * 状态-编辑中
     */
    CONST STATE_EDIT = 3;

    /**
     * 组织核算状态-已完成
     */
    CONST ORG_STATE_COMPLETED = 2;

    /**
     * 组织核算状态-核算中
     */
    CONST ORG_STATE_REVIEWING = 1;

    /**
     * 组织核算状态-下拉选择map
     */
    CONST ORG_STATE_SELECT_MAP = [
        ['id'=>self::ORG_STATE_REVIEWING, 'name'=>'核算中'],
        ['id'=>self::ORG_STATE_COMPLETED, 'name'=>'已完成'],
    ];

    /**
     * 组织核算状态-map
     */
    CONST ORG_STATE_MAP = [
        self::ORG_STATE_REVIEWING => '核算中',
        self::ORG_STATE_COMPLETED => '已完成',
    ];

    /**
     * 组织核算详情状态-下拉选择map
     */
    CONST ORGDETAIL_STATE_SELECT_MAP = [
        ['id'=>self::ORGDETAIL_STATE_COLLECT, 'name'=>'待收集'],
        ['id'=>self::ORGDETAIL_STATE_SUBMIT, 'name'=>'待提交'],
        ['id'=>self::ORGDETAIL_STATE_REVIEW_FACTORY, 'name'=>'工厂审核中'],
        ['id'=>self::ORGDETAIL_STATE_REVIEW_BRANCH, 'name'=>'分公司审核中'],
        ['id'=>self::ORGDETAIL_STATE_REVIEW_MAIN, 'name'=>'母公司审核中'],
        ['id'=>self::ORGDETAIL_STATE_PASS, 'name'=>'审核通过'],
        ['id'=>self::ORGDETAIL_STATE_FAILED, 'name'=>'审核不通过'],
    ];

    /**
     * 组织核算数据管理状态-下拉选择map 流转数据管理时没有待收集状态，待提交状态名称变为未填报
     */
    CONST ORGDATA_STATE_SELECT_MAP = [
        ['id'=>self::ORGDETAIL_STATE_SUBMIT, 'name'=>'未填报'],
        ['id'=>self::ORGDETAIL_STATE_REVIEW_FACTORY, 'name'=>'工厂审核中'],
        ['id'=>self::ORGDETAIL_STATE_REVIEW_BRANCH, 'name'=>'分公司审核中'],
        ['id'=>self::ORGDETAIL_STATE_REVIEW_MAIN, 'name'=>'母公司审核中'],
        ['id'=>self::ORGDETAIL_STATE_PASS, 'name'=>'审核通过'],
        ['id'=>self::ORGDETAIL_STATE_FAILED, 'name'=>'审核不通过'],
    ];

    /**
     * 组织核算详情状态-map
     */
    CONST ORGDETAIL_STATE_MAP = [
        self::ORGDETAIL_STATE_COLLECT => '待收集',
        self::ORGDETAIL_STATE_SUBMIT => '待提交',
        self::ORGDETAIL_STATE_REVIEW_FACTORY => '工厂审核中',
        self::ORGDETAIL_STATE_REVIEW_BRANCH => '分公司审核中',
        self::ORGDETAIL_STATE_REVIEW_MAIN => '母公司审核中',
        self::ORGDETAIL_STATE_PASS => '审核通过',
        self::ORGDETAIL_STATE_FAILED => '审核不通过',
    ];

    /**
     * 组织核算数据管理状态-map 流转数据管理时没有待收集状态，待提交状态名称变为未填报
     */
    CONST ORGDATA_STATE_MAP = [
        self::ORGDETAIL_STATE_SUBMIT => '未填报',
        self::ORGDETAIL_STATE_REVIEW_FACTORY => '工厂审核中',
        self::ORGDETAIL_STATE_REVIEW_BRANCH => '分公司审核中',
        self::ORGDETAIL_STATE_REVIEW_MAIN => '母公司审核中',
        self::ORGDETAIL_STATE_PASS => '审核通过',
        self::ORGDETAIL_STATE_FAILED => '审核不通过',
    ];

    /**
     * 组织核算数据详情分类-组织数据
     */
    CONST ORGDETAIL_DATA_ORG = 1;

    /**
     * 组织核算数据详情分类-设备数据
     */
    CONST ORGDETAIL_DATA_FAC = 2;

    /**
     * 组织核算详情状态-待收集
     */
    CONST ORGDETAIL_STATE_COLLECT = 1;

    /**
     * 组织核算详情状态-待提交
     */
    CONST ORGDETAIL_STATE_SUBMIT = 2;

    /**
     * 组织核算详情状态-工厂审核中
     */
    CONST ORGDETAIL_STATE_REVIEW_FACTORY = 3;

    /**
     * 组织核算详情状态-分公司审核中
     */
    CONST ORGDETAIL_STATE_REVIEW_BRANCH = 4;

    /**
     * 组织核算详情状态-母公司审核中
     */
    CONST ORGDETAIL_STATE_REVIEW_MAIN = 5;

    /**
     * 组织核算详情状态-审核通过
     */
    CONST ORGDETAIL_STATE_PASS = 6;

    /**
     * 组织核算详情状态-审核不通过
     */
    CONST ORGDETAIL_STATE_FAILED = 7;

    /**
     * 阶段类型-原材料获取阶段
     */
    CONST STAGE_TYPE_RAW = 1;

    /**
     * 阶段类型-生产制造阶段
     */
    CONST STAGE_TYPE_MANUFACTURING = 2;

    /**
     * 阶段类型-分销零售阶段
     */
    CONST STAGE_TYPE_DISTRIBUTION= 3;

    /**
     * 阶段类型-使用阶段
     */
    CONST STAGE_TYPE_USE = 4;

    /**
     * 阶段类型-废弃处置阶段
     */
    CONST STAGE_TYPE_SCRAP = 5;

    /**
     * 阶段类型-原材料获取阶段-原材料类型
     */
    CONST STAGE_TYPE_RAW_CATEGORY_RAW = 6;

    /**
     * 阶段类型-原材料获取阶段-其他材料类型
     */
    CONST STAGE_TYPE_RAW_CATEGORY_OTHER = 7;

    /**
     * 阶段类型-原材料获取阶段-运输类型
     */
    CONST STAGE_TYPE_RAW_CATEGORY_TRANSPORT = 8;

    /**
     * 阶段类型-原材料获取阶段-能耗类型
     */
    CONST STAGE_TYPE_RAW_CATEGORY_ENERGY = 9;

    /**
     * 阶段类型-生产制造阶段-辅助材料类型
     */
    CONST STAGE_TYPE_MANUFACTURING_CATEGORY_AUXILIARY = 10;

    /**
     * 阶段类型-生产制造阶段-直接逸散类型
     */
    CONST STAGE_TYPE_MANUFACTURING_CATEGORY_ESCAPE= 11;

    /**
     * 阶段类型-生产制造阶段-运输类型
     */
    CONST STAGE_TYPE_MANUFACTURING_CATEGORY_TRANSPORT= 12;

    /**
     * 阶段类型-生产制造阶段-能耗类型
     */
    CONST STAGE_TYPE_MANUFACTURING_CATEGORY_ENERGY = 13;

    /**
     * 阶段类型-分销零售阶段-运输类型
     */
    CONST STAGE_TYPE_DISTRIBUTION_TRANSPORT= 14;

    /**
     * 阶段类型-分销零售阶段-能耗类型
     */
    CONST STAGE_TYPE_DISTRIBUTION_ENERGY= 15;

    /**
     * 阶段类型-使用阶段-运输类型
     */
    CONST STAGE_TYPE_USE_TRANSPORT = 16;

    /**
     * 阶段类型-使用阶段-能耗类型
     */
    CONST STAGE_TYPE_USE_ENERGY = 17;

    /**
     * 阶段类型-废弃处置阶段-运输类型
     */
    CONST STAGE_TYPE_SCRAP_TRANSPORT  = 18;

    /**
     * 阶段类型-废弃处置阶段-能耗类型
     */
    CONST STAGE_TYPE_SCRAP_ENERGY = 19;

    /**
     * 阶段类型-分销零售阶段-物料类型
     */
    CONST STAGE_TYPE_DISTRIBUTION_MATERIAL= 20;

    /**
     * 阶段类型-使用阶段-物料类型
     */
    CONST STAGE_TYPE_USE_MATERIAL = 21;

    /**
     * 阶段类型-废弃处置阶段-物料类型
     */
    CONST STAGE_TYPE_SCRAP_MATERIAL = 22;

    /**
     * 阶段类型-废弃处置阶段-直接逸散类型
     */
    CONST STAGE_TYPE_SCRAP_ESCAPE = 23;

    /**
     * 来源类型-自产
     */
    CONST SOURCE_TYPE_SELF = 1;

    /**
     * 来源类型-外采
     */
    CONST SOURCE_TYPE_EXTERNAL = 2;

    /**
     * 匹配类型-因子
     */
    CONST MATCH_TYPE_FACTOR = 1;

    /**
     * 匹配类型-产品
     */
    CONST MATCH_TYPE_PRODUCT = 2;

    /**
     * 分页一页显示数量
     */
    CONST pageSize = 10;

    /**
     * 分页当前页码
     */
    CONST pageIndex = 1;

    /**
     * 运输类-map
     */
    CONST TRANSPORT = [
        self::STAGE_TYPE_RAW_CATEGORY_TRANSPORT,
        self::STAGE_TYPE_MANUFACTURING_CATEGORY_TRANSPORT,
        self::STAGE_TYPE_DISTRIBUTION_TRANSPORT,
        self::STAGE_TYPE_USE_TRANSPORT,
        self::STAGE_TYPE_SCRAP_TRANSPORT
    ];
}
