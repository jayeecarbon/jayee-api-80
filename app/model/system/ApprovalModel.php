<?php
namespace app\model\system;

use think\facade\Db;

/**
 * ApprovalModel
 */
class ApprovalModel extends Db {

    /**
     * 是否开启-开启
     */
    CONST IS_ON_YES = 1;

    /**
     * 是否开启-关闭
     */
    CONST IS_ON_NO = 2;

    /**
     * 审批节点-工厂审批
     */
    CONST APPROVAL_NODE_FACTORY = 1;

    /**
     * 审批节点-分公司审批
     */
    CONST APPROVAL_NODE_BRANCH = 2;

    /**
     * 审批节点-母公司审批
     */
    CONST APPROVAL_NODE_PARENT = 3;

    /**
     * 审批节点类型
     */
    CONST APPROVAL_NODE_TYPE = [
        ['id'=>self::APPROVAL_NODE_FACTORY, 'name'=>'工厂审批'],
        ['id'=>self::APPROVAL_NODE_BRANCH, 'name'=>'分公司审批'],
        ['id'=>self::APPROVAL_NODE_PARENT, 'name'=>'母公司审批'],
    ];

	//======================================================================
	// PUBLIC FUNCTIONS
	//======================================================================

    /**
     * getApprovals 查询业务流程
     * 
     * @author wuyinghua
     * @param $main_organization_id
	 * @return $list
     */
    public static function getApprovals($main_organization_id) {
        $list = Db::table('jy_approval ja')
            ->field('ja.id, ja.name, ja.factory, ja.branch_company, ja.parent_company')
            ->where('main_organization_id', (int)$main_organization_id)
            ->order('ja.sort', 'asc')
            ->select();

        return $list;
    }

    /**
     * getApproval 查询业务流程
     * 
     * @author wuyinghua
     * @param $id
	 * @return $list
     */
    public static function getApproval($id) {
        $list = Db::table('jy_approval ja')
            ->field('ja.id, ja.name, ja.factory, ja.branch_company, ja.parent_company')
            ->where('id', (int)$id)
            ->find();

        return $list;
    }

    /**
     * editApproval 编辑审批
     * 
     * @author wuyinghua
     * @param $data
	 * @return $list
     */
    public static function editApproval($data) {
        $edit = Db::table('jy_approval')->where('id', (int)$data['id'])->update($data);

        return $edit;
    }

    /**
     * addApproval 新增默认审批（母公司、分公司、工厂）
     * 
     * @author wuyinghua
     * @param $main_organization_id
     * @param $userid
	 * @return $list
     */
    public static function addApproval($main_organization_id, $userid) {
        Db::startTrans();
        try {
            Db::table('jy_approval')
            ->insert([
                'main_organization_id' => (int)$main_organization_id,
                'name'                 => '组织碳核算数据上报',
                'factory'              => self::IS_ON_YES,
                'branch_company'       => self::IS_ON_YES,
                'parent_company'       => self::IS_ON_YES,
                'sort'                 => 1,
                'create_by'            => (int)$userid,
                'create_time'          => date('Y-m-d H:i:s'),
                'modify_by'            => (int)$userid,
                'modify_time'          => date('Y-m-d H:i:s')
            ]);

            Db::table('jy_approval')
            ->insert([
                'main_organization_id' => (int)$main_organization_id,
                'name'                 => '产品碳足迹核算',
                'factory'              => self::IS_ON_YES,
                'branch_company'       => self::IS_ON_YES,
                'parent_company'       => self::IS_ON_YES,
                'sort'                 => 2,
                'create_by'            => (int)$userid,
                'create_time'          => date('Y-m-d H:i:s'),
                'modify_by'            => (int)$userid,
                'modify_time'          => date('Y-m-d H:i:s')
            ]);

            Db::table('jy_approval')
            ->insert([
                'main_organization_id' => (int)$main_organization_id,
                'name'                 => '供应产品填报',
                'factory'              => self::IS_ON_YES,
                'branch_company'       => self::IS_ON_YES,
                'parent_company'       => self::IS_ON_YES,
                'sort'                 => 3,
                'create_by'            => (int)$userid,
                'create_time'          => date('Y-m-d H:i:s'),
                'modify_by'            => (int)$userid,
                'modify_time'          => date('Y-m-d H:i:s')
            ]);

            Db::commit();

            return true;
        } catch (\Exception $e) {
            Db::rollback();

            return false;
        }
    }

 
}