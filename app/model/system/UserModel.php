<?php
namespace app\model\system;

use think\facade\Db;

/**
 * UserModel
 */
class UserModel extends Db {

    /**
     * 用户角色类型-ADMIN
     */
    CONST ROLE_TYPE_ADMIN = 1;

    /**
     * 用户角色类型-注册默认角色
     */
    CONST ROLE_TYPE_DEFAULT = 2;

    /**
     * 加密KEY
     */
    CONST ENCRYPT_KEY = 'www.jayeecarbon.com';

    /**
     * 是否使用-未使用
     */
    CONST USE_TYPE_NO = 1;

    /**
     * 是否使用-已使用
     */
    CONST USE_TYPE_YES = 2;

    /**
     * 是否启用-已启用
     */
    CONST STATE_TYPE_YES = 1;

    /**
     * 是否启用-未启用
     */
    CONST STATE_TYPE_NO = 2;

	//======================================================================
	// PUBLIC FUNCTIONS
	//======================================================================

    /**
     * getUsers 查询用户
     * 
     * @author wuyinghua
     * @param $page_size
     * @param $page_index
     * @param $filters
	 * @return $list
     */
    public static function getUsers($page_size, $page_index, $filters) {
        $where = [];

        if ($filters['filter_user_name']) {
            $where[] = array(['ju.username', 'like', '%' . trim($filters['filter_user_name']) . '%']);
        }

        if ($filters['filter_telephtone']) {
            $where[] = array(['ju.telephone', 'like', '%' . trim($filters['filter_telephtone']) . '%']);
        }

        if ($filters['filter_user_state']) {
            $where[] = array(['ju.state', '=' , trim($filters['filter_user_state'])]);
        }

        if ($filters['main_organization_id']) {
            $where[] = array(['ju.main_organization_id', '=', $filters['main_organization_id']]);
        }

        $list = Db::table('jy_user ju')
            ->field('ju.id, ju.username, ju.telephone, ju.state, jo.name organization, ju.role_id')
            ->leftJoin('jy_organization jo', 'ju.main_organization_id = jo.id')
            ->where($where)
            ->order('ju.id', 'desc')
            ->paginate(['list_rows'=>$page_size, 'page'=>$page_index]);

        return $list;
    }

    /**
     * seeUser 查看用户详情
     * 
     * @author wuyinghua
     * @param $id
	 * @return $list
     */
    public static function seeUser($id) {
        $list = Db::table('jy_user ju')
            ->field('ju.id, ju.username, ju.telephone, ju.state, ju.role_id, ju.organization_id, ju.main_organization_id, ju.last_time, jo.name organization_name')
            ->leftJoin('jy_organization jo', 'ju.main_organization_id = jo.id')
            ->where('ju.id', (int)$id)
            ->find();

        return $list;
    }

    /**
     * addUser 添加用户
     * 
     * @author wuyinghua
     * @param $data
	 * @return $list
     */
    public static function addUser($data) {
        $add = Db::table('jy_user')->insertGetId($data);

        return $add;
    }

    /**
     * updateUser 更新用户
     * 
     * @author wuyinghua
     * @param $data
	 * @return $edit
     */
    public static function updateUser($data) {
        $edit = Db::table('jy_user')->where('id', (int)$data['id'])->update($data);

        return $edit;
    }

     /**
     * getUserByTelephone 通过手机号查找用户
     * 
     * @author wuyinghua
     * @param $data
	 * @return $list
     */
    public static function getUserByTelephone($data) {
        $list = Db::table('jy_user')->where('telephone', $data)->select();

        return $list;
    }

     /**
     * getUserByName 通过用户名查找用户
     * 
     * @author wuyinghua
     * @param $data
	 * @return $list
     */
    public static function getUserByName($data) {
        $list = Db::table('jy_user')->where('username', $data)->select();

        return $list;
    }

     /**
     * getRole 获取角色
     * 
     * @author wuyinghua
     * @param $id
	 * @return $list
     */
    public static function getRole($id) {
        $list = Db::table('jy_role')->where('id', (int)$id)->find();

        return $list;
    }  

     /**
     * getRoles 获取角色列表
     * 
     * @author wuyinghua
     * @param $main_organization_id
	 * @return $list
     */
    public static function getRoles($main_organization_id) {
        $list = Db::table('jy_role')
            ->field('id, title name')
            ->where('main_organization_id', (int)$main_organization_id)
            ->whereOr('id', self::ROLE_TYPE_ADMIN)
            ->select();

        return $list;
    }

    /**
     * getRoleArr 获取角色数组
     * 
     * @author wuyinghua
     * @param $ids
	 * @return $list
    */
    public static function getRoleArr($ids) {
        $list = Db::table('jy_role')->field('id, title name')->whereIn('id', $ids)->select();

        return $list;
    }
}