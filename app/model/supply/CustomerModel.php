<?php


namespace app\model\supply;


use think\facade\Db;

class CustomerModel extends Db
{
    /**
     * 启用/禁用-启用
     */
    CONST STATE_YES = 1;
    /**
     * 启用/禁用-禁用
     */
    CONST STATE_NO = 2;

    CONST pageSize = 10; //TODO

    CONST pageIndex = 0; //TODO

    CONST STATE_MAP = [
        self::STATE_YES => '启用',
        self::STATE_NO => '禁用',
    ];

    CONST STATE_SELECT_MAP = [
        ['id'=>self::STATE_YES, 'name'=>'启用'],
        ['id'=>self::STATE_NO, 'name'=>'禁用']
    ];


    /**
     * getAllDataById   通过main_id获取所有该供应商相关联的客户信息
     *
     * @param $main_id
     * @return array|\think\Collection|Db[]
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function getAllDataById($main_id, $title, $industry_id) {
        $where = array();
        if (isset($title) && $title != '') {
            $where[] = array(['jo.name', 'like', '%' . trim($title) . '%']);
        }

        if (isset($industry_id) &&  $industry_id != 0 && $industry_id != null) {
            $where[] = array(['jo.industry_id', '=', $industry_id]);
        }

        $list = Db::table('jy_supplier_customer_relation jscr')
            ->field('jo.id, jo.industry_id, jo.name title, ji.name industry_name')
            ->leftJoin('jy_organization jo', 'jscr.relation_id = jo.id')
            ->leftJoin('jy_industry ji', 'ji.id = jo.industry_id')
            ->where($where)
            ->where('jscr.main_id', (int)$main_id)
            ->where('jscr.is_supplier', (int)SupplierCustomerRelationModel::ID_SUPPLIER_YES)
            ->order('jo.id', 'desc')
            ->select()
            ->toArray();

        return $list;
    }

    /**
     * list 查询供应商的客户列表
     *
     * @param $page_ize
     * @param $page_index
     * @param $filters
     * @return $list
     */
    public static function getList($page_size, $page_index, $filters) {
        $where = array();
        if (isset($filters['title']) && $filters['title'] != '') {
            $where[] = array(['jo.name', 'like', '%' . trim($filters['title']) . '%']);
        }
        if (isset($filters['state']) &&  $filters['state'] != '') {
            $where[] = array(['jscr.state', '=', trim($filters['state'])]);
        }
        if (isset($filters['industry_id']) &&  $filters['industry_id'] != '') {
            $where[] = array(['jo.industry_id', '=', trim($filters['industry_id'])]);
        }

        $list = Db::table('jy_supplier_customer_relation jscr')
            ->field('jo.id, jo.industry_id, jo.name title, jscr.state, jscr.risk_state, ji.name as industry_name')
            ->leftJoin('jy_organization jo', 'jscr.relation_id = jo.id')
            ->leftJoin('jy_industry ji', 'ji.id = jo.industry_id')
            ->where($where)
            ->where('jscr.main_id', (int)$filters['main_id'])
            ->where('jscr.is_supplier', (int)SupplierCustomerRelationModel::ID_SUPPLIER_YES)
            ->order('jo.id', 'desc')
            ->paginate(['list_rows' => $page_size, 'page' => $page_index]);

        return $list;
    }

    /**
     * addForm 供应商端新增产品获取 相应客户列表信息
     *
     * @param $main_id
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function addForm($main_id) {

        $list = Db::table('jy_supplier_customer_relation jscr')
            ->field('jo.id, jo.name title')
            ->leftJoin('jy_organization jo', 'jscr.relation_id = jo.id')
            ->leftJoin('jy_industry ji', 'ji.id = jo.industry_id')
            ->where('jscr.main_id', (int)$main_id)
            ->where('jscr.is_supplier', (int)SupplierCustomerRelationModel::ID_SUPPLIER_YES)
            ->order('jo.id', 'desc')
            ->select()
            ->toArray();

        return $list;
    }

    /**
     * getDataById 获取客户的信息（通过id来获取）
     * @param $data
     * @return $list
     */
    public static function getDataById($data) {

        $list = Db::table('jy_organization jo')
            ->field('jo.id, jo.industry_id, jo.name title, jscr.state, jscr.risk_state,
            ji.name as industry_name, jo.contact_person user_name, jo.telephone phone, jo.email,
            jo.create_by, jo.modify_by, jo.create_time, jo.modify_time')
            ->leftJoin('jy_industry ji', 'ji.id = jo.industry_id')
            ->leftJoin('jy_supplier_customer_relation jscr', 'jscr.relation_id = jo.id')
            ->where('jo.id', (int)$data['id'])
            ->where('jscr.main_id', (int)$data['main_id'])
            ->find();

        return $list;
    }

    /**
     * updateState 客户启用/禁用
     * @param $data
     * @return int
     * @throws \think\db\exception\DbException
     */
    public static function updateState($data) {
        $update_state = Db::table('jy_supplier_customer_relation')->where(['relation_id' => (int)$data['id'], 'main_id' => (int)$data['main_id']])->update(['state' => $data['state']]);

        return $update_state;
    }
}