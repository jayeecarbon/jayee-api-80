<?php


namespace app\model\supply;


use think\facade\Db;

class CustomerProductModel extends Db
{
    /**
     * 是否第三方认证-有
     */
    CONST AUTH_HAVE = 1;

    /**
     * 石否第三方认证-无
     */
    CONST AUTH_NOT_HAVE = 2;

    /**
     * 启用/禁用-启用
     */
    CONST STATE_YES = 1;
    /**
     * 启用/禁用-禁用
     */
    CONST STATE_NO = 2;
    /**
     * 是否删除-未删除
     */
    CONST IS_DEL_NOT_DEL = 0;
    /**
     * 是否删除-已删除
     */
    CONST IS_DEL_DEL = 1;

    /**
     * 审核状态-编辑中
     */
    CONST AUDIT_STATE_EDITING = 1;

    /**
     * 审核状态-未填报
     */
    CONST AUDIT_STATE_NOT_FILL = 2;

    /**
     * 审核状态-待审核
     */
    CONST AUDIT_STATE_NOT_VERIFY = 3;

    /**
     * 审核状态-已审核
     */
    CONST AUDIT_STATE_VERIFY = 4;

    /**
     * 审核状态-禁用
     */
    CONST AUDIT_STATE_NOT_USE = 5;

    /**
     * 是否有第三方认证map
     */
    CONST AUTH_MAP = [
        self::AUTH_HAVE => '有',
        self::AUTH_NOT_HAVE => '无'
    ];

    /**
     * 是否第三方认证-下拉选择map
     */
    CONST AUTH_SELECT_MAP = [
        ['id'=>self::AUTH_HAVE, 'name'=>'有'],
        ['id'=>self::AUTH_NOT_HAVE, 'name'=>'无'],
    ];

    /**
     * 填报状态map
     */
    CONST AUDIT_STATE_MAP = [
        self::AUDIT_STATE_EDITING => '编辑中',
        self::AUDIT_STATE_NOT_FILL => '未填报',
        self::AUDIT_STATE_NOT_VERIFY => '待审核',
        self::AUDIT_STATE_VERIFY => '已审核',
        self::AUDIT_STATE_NOT_USE => '禁用'
    ];

    /**
     * 填报状态-下拉选择map
     */
    CONST AUDIT_STATE_SELECT_MAP = [
        ['id'=>self::AUDIT_STATE_EDITING, 'name'=>'编辑中'],
        ['id'=>self::AUDIT_STATE_NOT_FILL, 'name'=>'未填报'],
        ['id'=>self::AUDIT_STATE_NOT_VERIFY, 'name'=>'待审核'],
        ['id'=>self::AUDIT_STATE_VERIFY, 'name'=>'已审核'],
        ['id'=>self::AUDIT_STATE_NOT_USE, 'name'=>'禁用']
    ];

    CONST pageSize = 10; //TODO

    CONST pageIndex = 0; //TODO

    /**
     * getList  采购者供应产品列表
     * 
     * @param $page_size
     * @param $page_index
     * @param $filters
     * @return $list
     */
    public static function getList($page_size, $page_index, $filters) {
        $where = array();
        if (isset($filters['product_name']) && $filters['product_name'] != '') {
            $where[] = array(['jcp.product_name', 'like', '%' . trim($filters['product_name']) . '%']);
        }
        if (isset($filters['product_type']) && $filters['product_type'] != '') {
            $where[] = array(['jcp.product_type', 'like', '%' . trim($filters['product_type']) . '%']);
        }
        if (isset($filters['audit_state']) && $filters['audit_state'] != '') {
            $where[] = array(['jcp.audit_state', '=', trim($filters['audit_state'])]);
        }
        if (isset($filters['auth']) && $filters['auth'] != '') {
            $where[] = array(['jcp.auth', '=', trim($filters['auth'])]);
        }
        if (isset($filters['customer_id']) && $filters['customer_id'] != '') {
            $where[] = array(['jcp.customer_id', '=', trim($filters['customer_id'])]);
        }
        if (isset($filters['supplier_name']) && $filters['supplier_name'] != '') {
            $where[] = array(['jo.name', 'like', '%' . trim($filters['supplier_name']) . '%']);
        }
        if (isset($filters['supplier_id']) && $filters['supplier_id'] != '') {
            $where[] = array(['jcp.supplier_id', '=', trim($filters['supplier_id'])]);
        }

        $list = Db::table('jy_customer_product jcp')
            ->field('jcp.id, jcp.product_name, jcp.product_model, jcp.product_type, jcp.week_start, jcp.week_end, 
            0 + CAST(jcp.number AS CHAR) number, jcp.auth, jcp.audit_state, jcp.supplier_id, jcp.unit_type, jcp.unit, jo.name title,
            ju.name as unit_str, jo.name as supplier_name, 0 + CAST(jcp.coefficient AS CHAR) coefficient')
            ->leftJoin('jy_organization jo', 'jcp.supplier_id = jo.id')
            ->leftJoin('jy_unit ju', 'jcp.unit = ju.id')
            ->where($where)
            ->where('jcp.state', self::STATE_YES)
            ->where('jcp.is_del', self::IS_DEL_NOT_DEL)
            ->order(['jcp.modify_time'=>'desc', 'jcp.create_time'=>'desc'])
            ->paginate(['list_rows' => $page_size, 'page' => $page_index]);

        return $list;
    }

    /**
     * getAuditList  报送审批列表
     * 
     * @param $page_size
     * @param $page_index
     * @param $filters
     * @return $list
     */
    public static function getAuditList($page_size, $page_index, $filters) {
        $where = array();
        if (isset($filters['product_name']) && $filters['product_name'] != '') {
            $where[] = array(['jcp.product_name', 'like', '%' . trim($filters['product_name']) . '%']);
        }
        if (isset($filters['product_type']) && $filters['product_type'] != '') {
            $where[] = array(['jcp.product_type', 'like', '%' . trim($filters['product_type']) . '%']);
        }
        if (isset($filters['audit_state']) && $filters['audit_state'] != '') {
            $where[] = array(['jcp.audit_state', '=', trim($filters['audit_state'])]);
        }
        if (isset($filters['auth']) && $filters['auth'] != '') {
            $where[] = array(['jcp.auth', '=', trim($filters['auth'])]);
        }
        if (isset($filters['customer_id']) && $filters['customer_id'] != '') {
            $where[] = array(['jcp.customer_id', '=', trim($filters['customer_id'])]);
        }
        if (isset($filters['supplier_name']) && $filters['supplier_name'] != '') {
            $where[] = array(['jo.name', 'like', '%' . trim($filters['supplier_name']) . '%']);
        }
        if (isset($filters['supplier_id']) && $filters['supplier_id'] != '') {
            $where[] = array(['jcp.supplier_id', '=', trim($filters['supplier_id'])]);
        }

        $list = Db::table('jy_customer_product jcp')
            ->field('jcp.id, jcp.product_name, jcp.product_model, jcp.product_type, jcp.week_start, jcp.week_end, 
            0 + CAST(jcp.number AS CHAR) number, jcp.auth, jcp.audit_state, jcp.supplier_id, jcp.unit_type, jcp.unit, jo.name title,
            ju.name as unit_str, jo.name as supplier_name, 0 + CAST(jcp.coefficient AS CHAR) coefficient')
            ->leftJoin('jy_organization jo', 'jcp.supplier_id = jo.id')
            ->leftJoin('jy_unit ju', 'jcp.unit = ju.id')
            ->where($where)
            ->where('jcp.is_del', self::IS_DEL_NOT_DEL)
            ->order('jcp.id', 'desc')
            ->paginate(['list_rows' => $page_size, 'page' => $page_index]);

        return $list;
    }

    /**
     * getAuditList  获取所有待审核数据
     * 
     * @param $customer_id
     * @param $supplier_id
     * @return $list
     */
    public static function getAllAudits($customer_id, $supplier_id) {

        $list = Db::table('jy_customer_product')
            ->where(['customer_id'=>$customer_id, 'supplier_id'=>$supplier_id])
            ->where('is_del', self::IS_DEL_NOT_DEL)
            ->where('audit_state', self::AUDIT_STATE_NOT_VERIFY)
            ->select();

        return $list;
    }

    /**
     * getAllAuditByCustomer  获取所有报送审批数据
     * 
     * @param $customer_id
     * @return $list
     */
    public static function getAllAuditByCustomer($customer_id) {

        $list = Db::table('jy_customer_product jcp')
            ->field('jcp.*, jo.name title, ju.username')
            ->leftJoin('jy_organization jo', 'jcp.supplier_id = jo.id')
            ->leftJoin('jy_user ju', 'jcp.modify_by = ju.id')
            ->where(['jcp.customer_id'=>$customer_id])
            ->where('jcp.is_del', self::IS_DEL_NOT_DEL)
            ->where('jcp.audit_state', self::AUDIT_STATE_NOT_VERIFY)
            ->select();

        return $list;
    }

    /**
     * addCustomerProduct  新增我是采购者的供应产品
     * @param $data
     * @return int|string
     */
    public static function addCustomerProduct($data) {
        $add = Db::table('jy_customer_product')->insert($data);

        return $add;
    }

    /**
     * addCustomerProducts  新增我是采购者的供应产品（多条）
     * @param $data
     * @return int|string
     */
    public static function addCustomerProducts($data) {
        $add = Db::table('jy_customer_product')->insertAll($data);

        return $add;
    }

    /**
     * addCustomerProductAndGetId  新增我是采购者的产品并获取主键id
     * @param $data
     * @return int|string
     */
    public static function addCustomerProductAndGetId($data) {
        $add = Db::table('jy_customer_product')->insertGetId($data);

        return $add;
    }

    /**
     * editCustomerProduct 编辑我是采购者的供应产品
     *
     * @param $data
     * @return $edit
     */
    public static function editCustomerProduct($data) {
        $edit = Db::table('jy_customer_product')->where('id', (int)$data['id'])->update($data);

        return $edit;
    }

    /**
     * getDataById 获取采购端客户产品详情（通过主键id来获取）
     *
     * @param $id
     * @return $list
     */
    public static function getDataById($id) {

        $list = Db::table('jy_customer_product jcp')
            ->field('jcp.id, jcp.supplier_id, jcp.product_name, jcp.product_model, jcp.product_type, jo.name supplier_name, jcp.week_start, jcp.week_end, jcp.unit, ju.name unit_str, jcp.unit_type')
            ->field('0 + CAST(jcp.emissions AS CHAR) emissions, 0 + CAST(jcp.coefficient AS CHAR) coefficient, 0 + CAST(jcp.number AS CHAR) number, jcp.file_url, jcp.auth')
            ->leftJoin('jy_organization jo', 'jo.id = jcp.supplier_id')
            ->leftJoin('jy_unit ju', 'ju.id = jcp.unit')
            ->where('jcp.id', (int)$id)
            ->find();

        return $list;
    }

    /**
     * delCustomerProduct 删除产品
     *
     * @param $data
     * @return $del
     */
    public static function delCustomerProduct($data) {
        $del = Db::table('jy_customer_product')->where('id', (int)$data['id'])->update($data);

        return $del;
    }

    /**
     * updateAuditState 供应商产品审核状态字段变更 编辑中/删除/未填报/待审核/已审核  可以修改成为这五种状态
     * @param $id
     * @param $audit_state
     * @return int
     * @throws \think\db\exception\DbException
     */
    public static function updateAuditState($id, $audit_state) {
        $update_state = Db::table('jy_customer_product')->where('id', (int)$id)->update(['audit_state' => $audit_state]);

        return $update_state;
    }

    /**
     * updateState 供应商产品状态字段变更
     * @param $id
     * @param $state
     * @return $update_state
     */
    public static function updateState($id, $state) {
        $update_state = Db::table('jy_customer_product')->where('id', (int)$id)->update(['state' => $state]);

        return $update_state;
    }

    /**
     * updateProductModel 供应商产品状态字段变更
     * @param $id
     * @param $product_model
     * @return $update_state
     */
    public static function updateProductModel($id, $product_model) {
        $update_product_model = Db::table('jy_customer_product')->where('id', (int)$id)->update(['product_model' => $product_model]);

        return $update_product_model;
    }

    /**
     * getNotVerifyListBySupplerId  根据供应商id获取客户要求填报的产品信息列表
     *
     * @param $supplier_id
     * @return array|\think\Collection|Db[]
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function getNotVerifyListBySupplerId($supplier_id, $product_name, $customer_name) {
        $where = array();
        if (isset($product_name) && $product_name != '') {
            $where[] = array(['jcp.product_name', 'like', '%' . trim($product_name) . '%']);
        }

        //先通过采购客户姓名查询到 供应商id 再进行查询
        if (isset($customer_name) &&  $customer_name != '') {
            $where[] = array(['jo.name', 'like', '%' . trim($customer_name) . '%']);
        }

        $list = Db::table('jy_customer_product jcp')
            ->field('jo.name title, jcp.id as customer_product_id, jcp.product_name, jo.id as customer_id, jcp.modify_by, ju.username, jcp.modify_time')
            ->join('jy_organization jo', 'jcp.customer_id = jo.id')
            ->join('jy_supplier_customer_relation jscr', 'jcp.customer_id = jscr.relation_id')
            ->leftJoin('jy_user ju', 'jcp.modify_by = ju.id')
            ->where('jcp.supplier_id', (int)$supplier_id)
            ->where('jscr.main_id', (int)$supplier_id)
            ->where($where)
            ->where('jscr.is_supplier', self::STATE_YES)
            ->where('jcp.audit_state', self::AUDIT_STATE_NOT_FILL)
            ->where('jscr.state', self::STATE_YES)
            ->select()
            ->toArray();

        return $list;
    }

    /**
     * getAllProducts 查询供应商所有产品
     * 
     * @param $supplier_id
     * @param $id
	 * @return $list
     */
    public static function getAllProducts($supplier_id, $id) {
        $list = Db::table('jy_customer_product')
        ->where('is_del', self::IS_DEL_NOT_DEL)
        ->where('supplier_id', $supplier_id)
        ->where('state', self::STATE_YES)
        ->where('id', '<>', $id)
        ->select();

        return $list;
    }
}