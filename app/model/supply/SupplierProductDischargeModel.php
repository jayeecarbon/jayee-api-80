<?php


namespace app\model\supply;


use think\facade\Db;

class SupplierProductDischargeModel extends Db
{

    /**
     * 是否删除-未删除
     */
    CONST IS_DEL_NOT_DEL = 0;
    /**
     * 是否删除-已删除
     */
    CONST IS_DEL_DEL = 1;
    /**
     * addSupplierProductDischarge  新增我是供应商的供应产品的排放源
     * @param $data
     * @return int|string
     */
    public static function addSupplierProductDischarge($data) {
        $add = Db::table('jy_supplier_product_discharge')->insertAll($data);

        return $add;
    }

    /**
     * getDataByProductId 获取供应商端自己添加产品碳核算信息详情（通过产品id来获取）
     *
     * @param $supplier_product_id
     * @return $list|array
     */
    public static function getDataByProductId($supplier_product_id) {
        $list = Db::table('jy_supplier_product_discharge jspd')
            ->field('jspd.id, jspd.type_id, jspd.name, jspd.loss_type,
            0 + CAST(jspd.value AS CHAR) value, jspd.unit_type, jspd.unit, jspd.factor_source, jspd.factor_unit,
             supplier_product_id, jspd.create_by, jspd.modify_by, jspd.create_time, jspd.modify_time, jspd.factor_name, jspd.year,
              jspd.is_del, jft.name as type_name, ju.name as unit_str, 0 + CAST(jspd.emissions AS CHAR) emissions, 0 + CAST(jspd.coefficient AS CHAR) coefficient')
            ->leftJoin('jy_factor_type jft', 'jspd.type_id = jft.id')
            ->leftJoin('jy_unit ju', 'jspd.unit = ju.id')
            -> where('jspd.supplier_product_id', (int)$supplier_product_id)
            -> where('jspd.is_del', (int)self::IS_DEL_NOT_DEL)
            -> select();

        return $list;
    }

    /**
     * delProductDischarge 删除产品排放源
     *
     * @param $id
     * @return $del
     */
    public static function delProductDischarge($id) {
        $del = Db::table('jy_supplier_product_discharge')->where('supplier_product_id', (int)$id)->update(['is_del'=>SupplierProductDischargeModel::IS_DEL_DEL]);

        return $del;
    }
}