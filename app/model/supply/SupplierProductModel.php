<?php

namespace app\model\supply;

use think\facade\Db;

class SupplierProductModel extends Db
{
    /**
     * 是否第三方认证-有
     */
    CONST AUTH_HAVE = 1;

    /**
     * 石否第三方认证-无
     */
    CONST AUTH_NOT_HAVE = 2;

    /**
     * 启用/禁用-启用
     */
    CONST STATE_YES = 1;
    /**
     * 启用/禁用-禁用
     */
    CONST STATE_NO = 2;
    /**
     * 是否删除-未删除
     */
    CONST IS_DEL_NOT_DEL = 0;
    /**
     * 是否删除-已删除
     */
    CONST IS_DEL_DEL = 1;

    /**
     * 审核状态-编辑中
     */
    CONST AUDIT_STATE_EDITING = 1;

    /**
     * 审核状态-未填报
     */
    CONST AUDIT_STATE_NOT_FILL = 2;

    /**
     * 审核状态-待审核
     */
    CONST AUDIT_STATE_NOT_VERIFY = 3;

    /**
     * 审核状态-已审核
     */
    CONST AUDIT_STATE_VERIFY = 4;

    /**
     * 审核状态-禁用
     */
    CONST AUDIT_STATE_NOT_USE = 5;

    /**
     * 是否有第三方认证map
     */
    CONST AUTH_MAP = [
        self::AUTH_HAVE => '有',
        self::AUTH_NOT_HAVE => '无'
    ];

    /**
     * 是否第三方认证-下拉选择map
     */
    CONST AUTH_SELECT_MAP = [
        ['id'=>self::AUTH_HAVE, 'name'=>'有'],
        ['id'=>self::AUTH_NOT_HAVE, 'name'=>'无'],
    ];

    /**
     * 填报状态map
     */
    CONST AUDIT_STATE_MAP = [
        self::AUDIT_STATE_EDITING => '编辑中',
        self::AUDIT_STATE_NOT_FILL => '未填报',
        self::AUDIT_STATE_NOT_VERIFY => '待审核',
        self::AUDIT_STATE_VERIFY => '已审核',
        self::AUDIT_STATE_NOT_USE => '禁用'
    ];

    /**
     * 填报状态-下拉选择map
     */
    CONST AUDIT_STATE_SELECT_MAP = [
        ['id'=>self::AUDIT_STATE_EDITING, 'name'=>'编辑中'],
        ['id'=>self::AUDIT_STATE_NOT_FILL, 'name'=>'待审核'],
        ['id'=>self::AUDIT_STATE_NOT_VERIFY, 'name'=>'未填报'],
        ['id'=>self::AUDIT_STATE_VERIFY, 'name'=>'已审核'],
        ['id'=>self::AUDIT_STATE_NOT_USE, 'name'=>'禁用']
    ];

    CONST pageSize = 10; //TODO

    CONST pageIndex = 0; //TODO

    /**
     * addSupplierProduct  新增我是供应商的供应产品
     * @param $data
     * @return int|string
     */
    public static function addSupplierProduct($data) {
        $add = Db::table('jy_supplier_product')->insertGetId($data);

        return $add;
    }

    /**
     * getProductCountByCustomerIdArrString 供应商端-供应商自己添加的产品中-通过客户id数组的字符端来寻找每个客户有多少产品
     *
     * @param $supplier_id
     * @param $customer_arr_id_str
     * @return \think\Paginator
     * @throws \think\db\exception\DbException
     */
    public static function getProductCountByCustomerIdArrString($supplier_id, $customer_arr_id_str) {

        $list = Db::table('jy_customer_product jcp')
            ->field('count(jcp.id) as count, jcp.customer_id')
            ->where(['supplier_id'=>$supplier_id, 'audit_state'=>self::AUDIT_STATE_VERIFY])
            ->where('customer_id', 'in', $customer_arr_id_str)
            ->group('jcp.customer_id')
            ->select()
            ->toArray();

        return $list;
    }

    public static function getList($page_size, $page_index, $filters) {
        $where = array();
        if (isset($filters['product_name']) && $filters['product_name'] != '') {
            $where[] = array(['jsp.product_name', 'like', '%' . trim($filters['product_name']) . '%']);
        }
        if (isset($filters['product_type']) &&  $filters['product_type'] != '') {
            $where[] = array(['jsp.product_type', '=', trim($filters['product_type'])]);
        }
        if (isset($filters['auth']) &&  $filters['auth'] != '') {
            $where[] = array(['jsp.auth', '=', trim($filters['auth'])]);
        }
        if (isset($filters['supplier_id']) &&  $filters['supplier_id'] != '') {
            $where[] = array(['jsp.supplier_id', '=', trim($filters['supplier_id'])]);
        }

        $list = Db::table('jy_supplier_product jsp')
            ->field('jsp.id, jsp.product_name, jsp.product_model, jsp.product_type, jsp.week_start, jsp.week_end, 
            0 + CAST(jsp.number AS CHAR) number, jsp.auth, jsp.audit_state, jsp.supplier_id, jsp.unit_type, jsp.unit, jo.name title,
            ju.name as unit_str, jo.name as supplier_name, 0 + CAST(jsp.emissions AS CHAR) emissions, 0 + CAST(jsp.coefficient AS CHAR) coefficient')
            ->join('jy_organization jo', 'jsp.supplier_id = jo.id')
            ->leftJoin('jy_unit ju', 'jsp.unit = ju.id')
            ->where($where)
            ->where('is_del', '=', self::IS_DEL_NOT_DEL)
            ->order(['jsp.modify_time'=>'desc', 'jsp.create_time'=>'desc'])
            ->paginate(['list_rows' => $page_size, 'page' => $page_index]);

        return $list;
    }

    /**
     * getDataById 获取供应商产品详情（通过主键id来获取）
     *
     * @param $id
     * @return $list
     */
    public static function getDataById($id) {
        $list = Db::table('jy_supplier_product jsp')
            ->field('jsp.id, jsp.product_name, jsp.product_model, jsp.product_type, jsp.supplier_id, 
            jsp.auth, jsp.state, jsp.file_url, jsp.create_by, jsp.modify_by, jsp.create_time,jsp.modify_time,
             jsp.audit_state, jsp.week_start, jsp.week_end, 0 + CAST(jsp.number AS CHAR) number, jsp.unit_type, jsp.unit, 
             jsp.customer_id, jsp.is_del, ju.name as unit_str, 0 + CAST(jsp.emissions AS CHAR) emissions, 0 + CAST(jsp.coefficient AS CHAR) coefficient')
            ->leftJoin('jy_unit ju', 'jsp.unit = ju.id')
            ->where('jsp.id', (int)$id)
            ->find();

        return $list;
    }

    /**
     * editSupplierProduct 编辑我是供应商的供应产品
     *
     * @param $data
     * @return $edit
     */
    public static function editSupplierProduct($data) {
        $edit = Db::table('jy_supplier_product')->where('id', (int)$data['id'])->update($data);

        return $edit;
    }

    /**
     * delSupplierProduct 删除产品
     *
     * @param $id
     * @return $del
     */
    public static function delSupplierProduct($id) {
        $del = Db::table('jy_supplier_product')->where('id', (int)$id)->update(['is_del'=>self::IS_DEL_DEL]);

        return $del;
    }

    /**
     * addSupplierProducts  新增我是供应商的供应产品（多条）
     * @param $data
     * @return int|string
     */
    public static function addSupplierProducts($data) {
        $add = Db::table('jy_supplier_product')->insertAll($data);

        return $add;
    }
    
    /**
     * getSupplierState 根据供应商ID采购者ID获取状态
     *
     * @param $main_id
     * @param $relation_id
     * @return \think\Paginator
     * @throws \think\db\exception\DbException
     */
    public static function getSupplierState($main_id, $relation_id) {
        $list = Db::table('jy_supplier_customer_relation')
            ->where(['main_id'=>$main_id, 'relation_id'=>$relation_id])
            ->find();

        return $list;
    }

    /**
     * getFactorIdByName 根据排放源类型名称获取ID
     *
     * @param $factor_name
     * @return \think\Paginator
     * @throws \think\db\exception\DbException
     */
    public static function getFactorIdByName($factor_name) {
        $list = Db::table('jy_factor_type')
            ->where('name', $factor_name)
            ->find();

        return $list;
    }

    /**
     * getUnitIdByName 根据单位名称获取ID
     *
     * @param $unit_name
     * @return \think\Paginator
     * @throws \think\db\exception\DbException
     */
    public static function getUnitIdByName($unit_name) {
        $list = Db::table('jy_unit')
            ->where('name', $unit_name)
            ->find();

        return $list;
    }

    /**
     * getAllProducts 查询所有产品
     * 
     * @param $supplier_id
     * @param $id
	 * @return $list
     */
    public static function getAllProducts($supplier_id, $id) {
        $list = Db::table('jy_supplier_product')
        ->where('is_del', self::IS_DEL_NOT_DEL)
        ->where('supplier_id', $supplier_id)
        ->where('id', '<>', $id)
        ->select();

        return $list;
    }
}