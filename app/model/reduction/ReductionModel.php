<?php

namespace app\model\reduction;

use think\facade\Config;
use think\facade\Db;


/**
 * ReductionModel
 */
class ReductionModel extends Db
{
    /**
     * getList 查询列表
     *
     * @param $page_ize
     * @param $page_index
     * @param $filters
     * @return $list
     */
    public function getReductionList($page_size, $page_index, $filters)
    {
        $where = array();
        if (isset($filters['name']) && !empty($filters['name'])) {
            $where[] = array(['r.name', 'like', '%' . trim($filters['name']) . '%']);
        }
        if (isset($filters['organization_id']) && !empty($filters['organization_id'])) {
            $where[] = array(['r.organization_id', '=', $filters['organization_id']]);
        }else{
            $where[] = array(['r.main_organization_id', '=', $filters['main_organization_id']]);
        }

        $list = Db::table('jy_reduction r')
            ->field('o.name organization_name,r.id,r.name,0+CAST(r.total_reduction_low AS CHAR) total_reduction_low,
            0+CAST(r.total_reduction_high AS CHAR) total_reduction_high,
            r.total_unit,0+CAST(r.unit_low AS CHAR)unit_low,0+CAST(r.unit_high AS CHAR) unit_high,r.molecule,r.denominator,
            r.modify_time,r.modify_by,u.username')
            ->leftJoin('jy_organization o', 'r.organization_id=o.id')
            ->leftJoin('jy_user u','u.id=r.modify_by')
            ->where($where)
            ->order('r.modify_time', 'desc')
            ->paginate(['list_rows' => $page_size, 'page' => $page_index])
            ->toArray();
        $data = $list['data'];

        if($data){
                  foreach ($data as $k=>$v){
                        if($v['molecule']){
                            $v['molecule'] =  Config::get('emission.molecule')[$v['molecule']-1]['name'];
                        }
                        if($v['total_unit']){
                            $v['total_unit'] =  Config::get('emission.molecule')[$v['total_unit']-1]['name'];
                        }
                        $fm = explode(',',$v['denominator']);

                        if(count($fm)==2){
                            $denominator = Db::table('jy_unit')->where('id',$fm[1])->find();
                            $denominator_name =  $denominator['name'];
                        }else{
                    $denominator_name="";
                }

                $v['unit_data'] = [$v['unit_low'],$v['unit_high']];
                $v['denominator'] = $denominator_name;
                $data[$k] = $v;
            }
            $list['data'] = $data;
        }
        return $list;
    }

    /**
     * add 添加
     *
     * @param $data
     * @return $add
     */
    public static function add($data)
    {
        $add = Db::table('jy_reduction')->insertGetId($data);
        if($add){
            return true;
        }else{
            return false;
        }

    }

    /**
     * del 删除
     *
     * @return $del
     */
    public static function del($id)
    {
            $del = Db::table('jy_reduction')->where('id', $id)->delete();
            if($del){
                return true;
            }else{
                return false;
            }
    }

    /**
     * getInfoById 获取详情
     *
     * @return $del
     */
    public static function getInfoById($id)
    {

        $list = Db::table('jy_reduction r')
            ->where('r.id', (int)$id)
            ->find();
        $list['type'] = explode(',',$list['type']);
        if($list['type']){
            $one=[];
            foreach ($list['type'] as $v){
                $one[]=(int)$v;
            }
            $list['type']= $one;
        }
        $denominator = explode(',',$list['denominator']);

        if(count($denominator)==2){
            $list['molecule_name'] =  Config::get('emission.molecule')[$list['molecule']-1]['name'];
            if($list['total_unit']>0){
                $list['total_unit_name'] =  Config::get('emission.molecule')[$list['total_unit']-1]['name'];
            }
            $denominator_use=[];
            foreach ($denominator as $v){
                $denominator_use[]=(int)$v;
            }
            $denominator=$denominator_use;
        }
        $list['denominator'] =$denominator;

        return $list;
    }


    /**
     * updates 修改
     *
     * @return $del
     */
       public static function edit($data)
    {
            $update = Db::table('jy_reduction')
                ->save($data);
            if($update){
                return true;
            }
            return false;
    }

    public static function organizationList($id)
    {

        $list = Db::table('jy_organization')
            ->field('id,name')
            ->where('main_organization_id',$id)
            ->select()
            ->toArray();
        return $list;
    }


}