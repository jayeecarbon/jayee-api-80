<?php
declare (strict_types = 1);

namespace app\model\organization;

use app\model\data\ParamModel;
use think\facade\Config;
use think\facade\Db;

/**
 * @mixin \think\Model
 */
class CarbonCulateModel extends Db
{
    /**
     * getCalculateExamples 查询核算
     * 
     * @author wuyinghua
     * @param $page_ize
     * @param $page_index
     * @param $filters
	 * @return $list
     */
    public static function getCarbonCulates($page_size, $page_index, $filters) {
        $where = [];

        if (isset($filters['organization_id']) && $filters['organization_id'] != 0) {
            $where[] = array(['joc.organization_id', '=', $filters['organization_id']]);
        }

        if (isset($filters['calculate_year']) && $filters['calculate_year'] != '') {
            $where[] = array(['joc.calculate_year', '=', $filters['calculate_year']]);
        }

        if (isset($filters['state']) && $filters['state'] != '') {
            $where[] = array(['joc.state', '=', trim($filters['state'])]);
        }

        $where[] = array(['joc.main_organization_id', '=', $filters['main_organization_id']]);

        $list = Db::table('jy_organization_calculate joc')
            ->field('joc.id, joc.calculate_name, joc.calculate_year, 0 + CAST(joc.emissions AS CHAR) emissions, joc.state, joc.organization_id, jo.name organization_name, ju.username, joc.create_time')
            ->leftJoin('jy_organization jo', 'joc.organization_id = jo.id')
            ->leftJoin('jy_user ju', 'joc.create_by = ju.id')
            ->where($where)
            ->order(['joc.modify_time'=>'desc', 'joc.create_time'=>'desc'])
            ->paginate(['list_rows'=>$page_size, 'page'=>$page_index]);

        return $list;
    }

    /**
     * getCarbonCulate 获取核算
     *
     * @author wuyinghua
     * @param $id
     * @return $list
     */
    public static function getCarbonCulate($id) {
        $list = Db::table('jy_organization_calculate joc')
            ->field('joc.id, joc.calculate_name, joc.calculate_year, joc.contrast_year, joc.organization_id, jo.pid, jo.name organization_name')
            ->leftJoin('jy_organization jo', 'joc.organization_id = jo.id')
            ->where('joc.id', (int)$id)
            ->find();

        return $list;
    }

    /**
     * getCompleteCarbonCulate 获取完成的核算
     *
     * @author wuyinghua
     * @param $id
     * @return $list
     */
    public static function getCompleteCarbonCulate($id) {
        $list = Db::table('jy_organization_calculate joc')
            ->field('joc.id, joc.calculate_year')
            ->where(['joc.organization_id'=>(int)$id, 'joc.state'=>ParamModel::ORG_STATE_COMPLETED])
            ->select();

        return $list;
    }

    /**
     * getCarbonCulateByOrg 根据组织获取核算
     *
     * @author wuyinghua
     * @param $id
     * @return $list
     */
    public static function getCarbonCulateByOrg($id) {
        $list = Db::table('jy_organization_calculate joc')
            ->field('joc.id, joc.calculate_name, joc.calculate_year, joc.contrast_year, joc.organization_id, jo.pid, jo.name organization_name')
            ->leftJoin('jy_organization jo', 'joc.organization_id = jo.id')
            ->where('joc.organization_id', (int)$id)
            ->find();

        return $list;
    }

    /**
     * getCarbonCulateDetail 获取核算详情
     *
     * @author wuyinghua
     * @param $id
     * @return $list
     */
    public static function getCarbonCulateDetail($id) {
        $list = Db::table('jy_organization_calculate_detail jocd')
            ->field('jocd.id, jocd.calculate_year, jocd.calculate_month, joc.calculate_name, jo.name organization_name, jo.type, 0 + CAST(jocd.emissions AS CHAR) emissions, jocd.state, jocd.files')
            ->leftJoin('jy_organization_calculate joc', 'jocd.organization_calculate_id = joc.id')
            ->leftJoin('jy_organization jo', 'jocd.organization_id = jo.id')
            ->where('jocd.id', (int)$id)
            ->find();

        return $list;
    }

    /**
     * getCarbonCulateDetailByCal 获取核算详情
     *
     * @author wuyinghua
     * @param $organization_calculate_id
     * @return $list
     */
    public static function getCarbonCulateDetailByCal($organization_calculate_id) {
        $list = Db::table('jy_organization_calculate_detail jocd')
            ->where(['jocd.organization_calculate_id'=>(int)$organization_calculate_id, 'jocd.is_del'=>ParamModel::IS_DEL_NO])
            ->select();

        return $list;
    }

    /**
     * getCarbonCulateDetailByOrg 通过组织获取核算详情
     *
     * @author wuyinghua
     * @param $organization_calculate_id
     * @param $organization_id
     * @return $list
     */
    public static function getCarbonCulateDetailByOrg($organization_calculate_id, $organization_id) {
        $list = Db::table('jy_organization_calculate_detail jocd')
            ->where(['jocd.organization_calculate_id'=>(int)$organization_calculate_id, 'jocd.organization_id'=>(int)$organization_id, 'jocd.is_del'=>ParamModel::IS_DEL_NO])
            ->select();

        return $list;
    }

    /**
     * getCarbonCulateFacilityDetail 获取核算设备详情
     *
     * @author wuyinghua
     * @param $id
     * @return $list
     */
    public static function getCarbonCulateFacilityDetail($id) {
        $list = Db::table('jy_organization_calculate_detail_facility jocdf')
            ->field('jocdf.id, jocdf.organization_calculate_detail_id, jocd.calculate_year, jocd.calculate_month, jocdf.facility_name, jocdf.facility_no, joc.calculate_name, 0 + CAST(abs(jocdf.emissions) AS CHAR) emissions, jocdf.files')
            ->leftJoin('jy_organization_calculate joc', 'jocdf.organization_calculate_id = joc.id')
            ->leftJoin('jy_organization_calculate_detail jocd', 'jocdf.organization_calculate_detail_id = jocd.id')
            ->where('jocdf.id', (int)$id)
            ->find();

        return $list;
    }

    /**
     * getCarbonCulateFacilityDetailEmission 获取核算设备关联的排放源
     *
     * @author wuyinghua
     * @param $organization_calculate_detail_id
     * @param $facility_no
     * @return $list
     */
    public static function getCarbonCulateFacilityDetailEmission($organization_calculate_detail_id, $facility_no) {
        $list = Db::table('jy_organization_calculate_detail_facility jocdf')
            ->field('jocdf.organization_calculate_detail_emission_id id, jocdf.id facility_id, jocde.emission_name, jocde.active_unit, 0 + CAST(abs(jocdf.active_value) AS CHAR) active_value, jocde.source_id, 0 + CAST(abs(jocdf.emissions) AS CHAR) emissions, jocde.active, g.name ghg_cate, h.name ghg_type, gh.name iso_cate, ghg.name iso_type')
            ->leftJoin('jy_organization_calculate_detail_emission jocde', 'jocdf.organization_calculate_detail_emission_id = jocde.id')
            ->leftJoin('jy_ghg_template g','g.id = jocde.ghg_cate')
            ->leftJoin('jy_ghg_template h','h.id = jocde.ghg_type')
            ->leftJoin('jy_ghg_template gh','gh.id = jocde.iso_cate')
            ->leftJoin('jy_ghg_template ghg','ghg.id = jocde.iso_type')
            ->where(['jocdf.organization_calculate_detail_id'=>(int)$organization_calculate_detail_id, 'jocdf.facility_no'=>$facility_no, 'jocde.is_del'=>ParamModel::IS_DEL_NO])
            ->select();

        return $list;
    }

    /**
     * getCarbonCulateFacility 获取核算设备
     *
     * @author wuyinghua
     * @param $id
     * @return $list
     */
    public static function getCarbonCulateFacility($id) {
        $list = Db::table('jy_organization_calculate_detail_facility jocdf')
            ->field('jocdf.id, jocdf.facility_name, jocdf.facility_no, SUM(0 + CAST(abs(jocdf.emissions) AS CHAR)) emissions')
            ->group('jocdf.facility_no')
            ->where(['jocdf.organization_calculate_detail_id'=>(int)$id, 'jocdf.is_del'=>ParamModel::IS_DEL_NO])
            ->select();

        return $list;
    }

    /**
     * getCarbonCulateDetailOrgEmission 获取核算详情组织数据排放源
     *
     * @author wuyinghua
     * @param $id
     * @return $list
     */
    public static function getCarbonCulateDetailOrgEmission($id) {
        $list = Db::table('jy_organization_calculate_detail_emission jocde')
            ->field('jocde.id, jocde.emission_name, 0 + CAST(abs(jocde.active_value) AS CHAR) active_value, jocde.active_unit, 0 + CAST(abs(jocde.emissions) AS CHAR) emissions, jocde.source_id, jocde.active, g.name ghg_cate, h.name ghg_type, gh.name iso_cate, ghg.name iso_type')
            ->leftJoin('jy_ghg_template g','g.id = jocde.ghg_cate')
            ->leftJoin('jy_ghg_template h','h.id = jocde.ghg_type')
            ->leftJoin('jy_ghg_template gh','gh.id = jocde.iso_cate')
            ->leftJoin('jy_ghg_template ghg','ghg.id = jocde.iso_type')
            ->where(['jocde.organization_calculate_detail_id'=>(int)$id, 'is_del'=>ParamModel::IS_DEL_NO])
            ->order(['jocde.id'=>'desc'])
            ->select();

        return $list;
    }

    /**
     * getCarbonCulateDetailEmission 获取核算详情中排放源数据
     *
     * @author wuyinghua
     * @param $id
     * @return $list
     */
    public static function getCarbonCulateDetailEmission($id) {

        $list = Db::table('jy_organization_calculate_detail_emission jocde')
            ->field('jocde.*, jocd.calculate_year, jocd.calculate_month, jo.name organization_name')
            ->leftJoin('jy_organization_calculate_detail jocd', 'jocde.organization_calculate_detail_id = jocd.id')
            ->leftJoin('jy_organization jo', 'jocd.organization_id = jo.id')
            ->where('jocde.id', (int)$id)
            ->find();

        return $list;
    }

    /**
     * getGasById 获取气体数据
     *
     * @author wuyinghua
     * @param $id
     * @return $list
     */
    public static function getGasById($id) {

        $list = Db::table('jy_organization_calculate_detail_emission_gas')
            ->where('emi_id', (int)$id)
            ->select();

        return $list;
    }

    /**
     * getInfoById 获取排放源详情
     *
     * @author wuyinghua
     * @param $id
     * @return $list
     */
    public static function getInfoById($id) {
        $gas_all = [
            ['gas' => '二氧化碳当量（CO₂e）', 'gas_type' => '', 'factor_value' => '', 'denominator' => '', 'molecule' => ''],
            ['gas' => '二氧化碳（CO₂）', 'gas_type' => '二氧化碳（CO₂）', 'factor_value' => '', 'denominator' => '', 'molecule' => ''],
            ['gas' => '甲烷（CH₄）', 'gas_type' => '甲烷（CH₄）', 'factor_value' => '', 'denominator' => '', 'molecule' => ''],
            ['gas' => '氧化亚氮（N₂O）', 'gas_type' => '氧化亚氮（N₂O）', 'factor_value' => '', 'denominator' => '', 'molecule' => ''],
            ['gas' => '氢氟碳化物 (HFCs)', 'gas_type' => '', 'factor_value' => '', 'denominator' => '', 'molecule' => ''],
            ['gas' => '全氟碳化物 (PFCs)', 'gas_type' => '', 'factor_value' => '', 'denominator' => '', 'molecule' => ''],
            ['gas' => '六氟化硫（SF₆）', 'gas_type' => '六氟化硫（SF₆）', 'factor_value' => '', 'denominator' => '', 'molecule' => ''],
            ['gas' => '三氟化氮（NF₃）', 'gas_type' => '三氟化氮（NF₃）', 'factor_value' => '', 'denominator' => '', 'molecule' => '']
        ];

        $list = Db::table('jy_organization_calculate_detail_emission jcme')
            ->field('jcme.id, jcme.emission_name name, jcme.source_id, jcme.active, jcme.ghg_cate, jcme.ghg_type, jcme.iso_cate, jcme.iso_type, jcme.active_data,
            jcme.active_unit, jcme.active_department, jcme.active_type, jcme.active_score, jcme.unitary_ratio, jcme.factor_type, jcme.factor_score,
            jcme.factor_source, jcme.year, g.name ghg_cate_name, h.name ghg_type_name, gh.name iso_cate_name, ghg.name iso_type_name')
            ->leftJoin('jy_ghg_template g','g.id = jcme.ghg_cate')
            ->leftJoin('jy_ghg_template h','h.id = jcme.ghg_type')
            ->leftJoin('jy_ghg_template gh','gh.id = jcme.iso_cate')
            ->leftJoin('jy_ghg_template ghg','ghg.id = jcme.iso_type')
            ->where('jcme.id', (int)$id)
            ->find();

        if ($list) {
            $active_unit = explode(',', $list['active_unit']);
            $list['ghg_cate'] = ['id'=>$list['ghg_cate'], 'name'=>$list['ghg_cate_name']];
            $list['ghg_type'] = ['id'=>$list['ghg_type'], 'name'=>$list['ghg_type_name']];
            $list['iso_cate'] = ['id'=>$list['iso_cate'], 'name'=>$list['iso_cate_name']];
            $list['iso_type'] = ['id'=>$list['iso_type'], 'name'=>$list['iso_type_name']];
            $list['active_unit'] = $active_unit;

            $list['active_type'] = ['id'=>$list['active_type'], 'name'=>Config::get('emission.active_type')[$list['active_type']-1]['name']];
            $list['factor_type'] = ['id'=>$list['factor_type'], 'name'=> Config::get('emission.factor_type')[$list['factor_type']-1]['name']];

            $gas = Db::table('jy_organization_calculate_detail_emission_gas')
                ->where('emi_id', (int)$id)
                ->select()
                ->toArray();

            if ($gas) {
                foreach ($gas_all as $gkk => $gvv) {
                    foreach ($gas as $k => $v) {
                        if ($gvv['gas'] == $v['gas']) {
                            $denominator = explode(',', $v['denominator']);
                            if ($denominator) {
                                $denominators = [];
                                foreach ($denominator as $vv) {
                                    $denominators[] = intval($vv);
                                }
                                $v['denominator'] = $denominators;
                            }

                            $gvv['id'] = $v['id'];
                            $gvv['gas'] = $v['gas'];
                            $gvv['factor_value'] = $v['factor_value'];
                            $gvv['gas_type'] = $v['gas_type'];
                            $gvv['denominator'] = $v['denominator'];
                            $gvv['molecule'] = (int)$v['molecule'];
                            $gas_all[$gkk] = $gvv;
                        }
                    }
                }
                $list['gas'] = $gas_all;
            }
        }

        return $list;
    }

    /**
     * getAllCarbonCulateByOrg 查询所有核算
     * 
     * @author wuyinghua
	 * @return $list
     */
    public static function getAllCarbonCulateByOrg($organization_id, $calculate_year) {
        $list = Db::table('jy_organization_calculate')->where(['organization_id'=>(int)$organization_id, 'calculate_year'=>$calculate_year])->select();

        return $list;
    }

    /**
     * addCarbonCulate 新增碳核算
     * 
     * @author wuyinghua
     * @param $data
     */
    public static function addCarbonCulate($data) {
        Db::startTrans();
        try {
            $num = 0;
            $organization_id_arr = explode(',', $data['organization_id']);
            foreach ($organization_id_arr as $organization_id) {
                $model_data = Db::table('jy_calculate_model jcm')
                    ->field('jcm.id, jcm.organization_id, jo.pid')
                    ->leftJoin('jy_organization jo', 'jcm.organization_id = jo.id')
                    ->where(['jcm.is_del'=>ParamModel::IS_DEL_NO, 'jcm.organization_id'=>$organization_id])
                    ->find();

                $emission_arr = Db::table('jy_calculate_model_emission jcme')
                    ->field('jcme.id jcme_id, jcme.emission_id, jes.*')
                    ->leftJoin('jy_emission_source jes', 'jes.id = jcme.emission_id')
                    ->where('jcme.calculate_model_id', (int)$model_data['id'])
                    ->select()
                    ->toArray();
                // 组织核算表中直插入一条数据
                if ($num == 0) {
                    $insert_id = Db::table('jy_organization_calculate')
                    ->insertGetId([
                        'main_organization_id' => (int)$data['main_organization_id'],
                        'organization_id'      => (int)$data['choice_organization'],
                        'calculate_model_id'   => (int)$model_data['id'],
                        'calculate_year'       => $data['calculate_year'],
                        'contrast_year'        => $data['contrast_year'],
                        'calculate_name'       => $data['calculate_name'],
                        'create_by'            => (int)$data['userid'],
                        'create_time'          => date('Y-m-d H:i:s'),
                        'modify_by'            => (int)$data['userid'],
                        'modify_time'          => date('Y-m-d H:i:s')
                    ]);
                }

                // 插入12个月的核算详情数据
                for ($calculate_month = 1; $calculate_month < 13; $calculate_month++) {
                    $insert_detail_id = Db::table('jy_organization_calculate_detail')
                    ->insertGetId([
                        'organization_calculate_id' => (int)$insert_id,
                        'main_organization_id'      => (int)$data['main_organization_id'],
                        'organization_id'           => (int)$organization_id,
                        'parent_organization_id'    => (int)$model_data['pid'],
                        'calculate_model_id'        => (int)$model_data['id'],
                        'calculate_year'            => $data['calculate_year'],
                        'calculate_month'           => $calculate_month,
                        'create_by'                 => (int)$data['userid'],
                        'create_time'               => date('Y-m-d H:i:s'),
                        'modify_by'                 => (int)$data['userid'],
                        'modify_time'               => date('Y-m-d H:i:s')
                    ]);
    
                    // 插入组织核算详情关联排放源表
                    foreach ($emission_arr as $key => $value) {
                        $gass_arr = Db::table('jy_emission_gas')->where('emi_id', (int)$value['emission_id'])->select()->toArray();
                        $facility_arr = Db::table('jy_calculate_model_emission_facility jcmef')
                            ->leftJoin('jy_facility jf', 'jf.id = jcmef.facility_id')
                            ->where('jcmef.calculate_model_emission_id', (int)$value['jcme_id'])
                            ->select()
                            ->toArray();
                        $emi_id = Db::table('jy_organization_calculate_detail_emission')
                        ->insertGetId([
                            'organization_calculate_id'        => (int)$insert_id,
                            'organization_calculate_detail_id' => (int)$insert_detail_id,
                            'calculate_model_id'               => (int)$model_data['id'],
                            'emission_name'                    => $value['name'],
                            'source_id'                        => $value['source_id'],
                            'active'                           => $value['active'],
                            'ghg_cate'                         => (int)$value['ghg_cate'],
                            'iso_cate'                         => (int)$value['iso_cate'],
                            'ghg_type'                         => (int)$value['ghg_type'],
                            'iso_type'                         => (int)$value['iso_type'],
                            'active_data'                      => $value['active_data'],
                            'active_unit'                      => $value['active_unit'],
                            'active_department'                => $value['active_department'],
                            'active_type'                      => (int)$value['active_type'],
                            'active_score'                     => $value['active_score'],
                            'unitary_ratio'                    => $value['unitary_ratio'],
                            'factor_type'                      => (int)$value['factor_type'],
                            'factor_score'                     => $value['factor_score'],
                            'factor_source'                    => $value['factor_source'],
                            'year'                             => $value['year'],
                            'create_by'                        => (int)$data['userid'],
                            'create_time'                      => date('Y-m-d H:i:s'),
                            'modify_by'                        => (int)$data['userid'],
                            'modify_time'                      => date('Y-m-d H:i:s')
                        ]);
    
                        // 插入组织核算详情-排放源-温室气体排放表
                        foreach ($gass_arr as $k => $v) {
                            Db::table('jy_organization_calculate_detail_emission_gas')
                            ->insert([
                                'organization_calculate_id' => (int)$insert_id,
                                'emi_id'                    => (int)$emi_id,
                                'calculate_model_id'        => (int)$model_data['id'],
                                'gas'                       => $v['gas'],
                                'gas_type'                  => $v['gas_type'],
                                'factor_value'              => $v['factor_value'],
                                'molecule'                  => $v['molecule'],
                                'denominator'               => $v['denominator'],
                                'create_by'                 => (int)$data['userid'],
                                'create_time'               => date('Y-m-d H:i:s')
                            ]);
                        }
    
                        // 插入组织核算详情-排放源-设备表
                        foreach ($facility_arr as $k => $v) {
                            Db::table('jy_organization_calculate_detail_facility')
                            ->insert([
                                'organization_calculate_id'                 => (int)$insert_id,
                                'calculate_model_id'                        => (int)$model_data['id'],
                                'organization_calculate_detail_id'          => (int)$insert_detail_id,
                                'organization_calculate_detail_emission_id' => (int)$emi_id,
                                'facility_name'                             => $v['facility_name'],
                                'facility_no'                               => $v['facility_no'],
                                'create_by'                                 => (int)$data['userid'],
                                'create_time'                               => date('Y-m-d H:i:s'),
                                'modify_by'                                 => (int)$data['userid'],
                                'modify_time'                               => date('Y-m-d H:i:s')
                            ]);
                        }
                    }
                }

                $num++;
            }

            Db::commit();

            return true;
        } catch (\Exception $e) {
            Db::rollback();

            return false;
        }
    }

    /**
     * editCarbonCulate 编辑核算
     * 
     * @author wuyinghua
     * @param $data
	 * @return $edit
     */
    public static function editCarbonCulate($data) {
        Db::startTrans();
        try {
            $organization_id_arr = explode(',', $data['organization_id']);
            // 更新核算表对比年度
            Db::table('jy_organization_calculate')->where('id', (int)$data['id'])->update([
                'contrast_year' => $data['contrast_year'],
                'modify_by'     => $data['modify_by'],
                'modify_time'   => $data['modify_time'],
            ]);

            // 将核算相关的数据is_del状态变更
            Db::table('jy_organization_calculate_detail')->where('organization_calculate_id', (int)$data['id'])->update([
                'is_del' => ParamModel::IS_DEL_YES,
            ]);
            Db::table('jy_organization_calculate_detail_emission')->where('organization_calculate_id', (int)$data['id'])->update([
                'is_del' => ParamModel::IS_DEL_YES,
            ]);
            Db::table('jy_organization_calculate_detail_facility')->where('organization_calculate_id', (int)$data['id'])->update([
                'is_del' => ParamModel::IS_DEL_YES,
            ]);

            foreach ($organization_id_arr as $organization_id) {
                $model_data = Db::table('jy_calculate_model jcm')
                    ->field('jcm.id, jcm.organization_id, jo.pid')
                    ->leftJoin('jy_organization jo', 'jcm.organization_id = jo.id')
                    ->where(['jcm.is_del'=>ParamModel::IS_DEL_NO, 'jcm.organization_id'=>$organization_id])
                    ->find();

                $emission_arr = Db::table('jy_calculate_model_emission jcme')
                    ->field('jcme.id jcme_id, jcme.emission_id, jes.*')
                    ->leftJoin('jy_emission_source jes', 'jes.id = jcme.emission_id')
                    ->where('jcme.calculate_model_id', (int)$model_data['id'])
                    ->select()
                    ->toArray();

                // 12个月的核算详情数据 没有则插入，
                for ($calculate_month = 1; $calculate_month < 13; $calculate_month++) {
                    $calculate_detail_data = Db::table('jy_organization_calculate_detail jocd')
                    ->where(['jocd.organization_calculate_id'=>(int)$data['id'], 'jocd.organization_id'=>(int)$organization_id, 'jocd.calculate_year'=>$data['calculate_year'], 'jocd.calculate_month'=>$calculate_month])
                    ->find();

                    if ($calculate_detail_data != NULL) {
                        Db::table('jy_organization_calculate_detail jocd')
                        ->where(['jocd.organization_calculate_id'=>(int)$data['id'], 'jocd.organization_id'=>(int)$organization_id, 'jocd.calculate_year'=>$data['calculate_year'], 'jocd.calculate_month'=>$calculate_month])
                        ->update([
                            'is_del'        => ParamModel::IS_DEL_NO,
                            'modify_by'     => $data['modify_by'],
                            'modify_time'   => $data['modify_time'],
                        ]);

                        Db::table('jy_organization_calculate_detail_emission jocde')
                        ->where(['jocde.organization_calculate_detail_id'=>(int)$calculate_detail_data['id']])
                        ->update([
                            'is_del'        => ParamModel::IS_DEL_NO,
                            'modify_by'     => $data['modify_by'],
                            'modify_time'   => $data['modify_time'],
                        ]);

                        Db::table('jy_organization_calculate_detail_facility jocdf')
                        ->where(['jocdf.organization_calculate_detail_id'=>(int)$calculate_detail_data['id']])
                        ->update([
                            'is_del'        => ParamModel::IS_DEL_NO,
                            'modify_by'     => $data['modify_by'],
                            'modify_time'   => $data['modify_time'],
                        ]);
                    } else {
                        $insert_detail_id = Db::table('jy_organization_calculate_detail')
                        ->insertGetId([
                            'calculate_model_id'        => (int)$model_data['id'],
                            'organization_calculate_id' => (int)$data['id'],
                            'organization_id'           => (int)$organization_id,
                            'parent_organization_id'    => (int)$model_data['pid'],
                            'main_organization_id'      => (int)$data['main_organization_id'],
                            'calculate_year'            => $data['calculate_year'],
                            'calculate_month'           => $calculate_month,
                            'create_by'                 => (int)$data['userid'],
                            'create_time'               => date('Y-m-d H:i:s'),
                            'modify_by'                 => (int)$data['userid'],
                            'modify_time'               => date('Y-m-d H:i:s')
                        ]);

                        // 插入组织核算详情关联排放源表
                        foreach ($emission_arr as $key => $value) {
                            $gass_arr = Db::table('jy_emission_gas')->where('emi_id', (int)$value['emission_id'])->select()->toArray();
                            $facility_arr = Db::table('jy_calculate_model_emission_facility jcmef')
                                ->leftJoin('jy_facility jf', 'jf.id = jcmef.facility_id')
                                ->where('jcmef.calculate_model_emission_id', (int)$value['jcme_id'])
                                ->select()
                                ->toArray();
                            $emi_id = Db::table('jy_organization_calculate_detail_emission')
                            ->insertGetId([
                                'organization_calculate_id'        => (int)$data['id'],
                                'organization_calculate_detail_id' => (int)$insert_detail_id,
                                'calculate_model_id'               => (int)$model_data['id'],
                                'emission_name'                    => $value['name'],
                                'source_id'                        => $value['source_id'],
                                'active'                           => $value['active'],
                                'ghg_cate'                         => (int)$value['ghg_cate'],
                                'iso_cate'                         => (int)$value['iso_cate'],
                                'ghg_type'                         => (int)$value['ghg_type'],
                                'iso_type'                         => (int)$value['iso_type'],
                                'active_data'                      => $value['active_data'],
                                'active_unit'                      => $value['active_unit'],
                                'active_department'                => $value['active_department'],
                                'active_type'                      => (int)$value['active_type'],
                                'active_score'                     => $value['active_score'],
                                'unitary_ratio'                    => $value['unitary_ratio'],
                                'factor_type'                      => (int)$value['factor_type'],
                                'factor_score'                     => $value['factor_score'],
                                'factor_source'                    => $value['factor_source'],
                                'year'                             => $value['year'],
                                'create_by'                        => (int)$data['userid'],
                                'create_time'                      => date('Y-m-d H:i:s'),
                                'modify_by'                        => (int)$data['userid'],
                                'modify_time'                      => date('Y-m-d H:i:s')
                            ]);
        
                            // 插入组织核算详情-排放源-温室气体排放表
                            foreach ($gass_arr as $k => $v) {
                                Db::table('jy_organization_calculate_detail_emission_gas')
                                ->insert([
                                    'organization_calculate_id' => (int)$data['id'],
                                    'emi_id'                    => (int)$emi_id,
                                    'calculate_model_id'        => (int)$model_data['id'],
                                    'gas'                       => $v['gas'],
                                    'gas_type'                  => $v['gas_type'],
                                    'factor_value'              => $v['factor_value'],
                                    'molecule'                  => $v['molecule'],
                                    'denominator'               => $v['denominator'],
                                    'create_by'                 => (int)$data['userid'],
                                    'create_time'               => date('Y-m-d H:i:s')
                                ]);
                            }
        
                            // 插入组织核算详情-排放源-设备表
                            foreach ($facility_arr as $k => $v) {
                                Db::table('jy_organization_calculate_detail_facility')
                                ->insert([
                                    'organization_calculate_id'                 => (int)$data['id'],
                                    'calculate_model_id'                        => (int)$model_data['id'],
                                    'organization_calculate_detail_id'          => (int)$insert_detail_id,
                                    'organization_calculate_detail_emission_id' => (int)$emi_id,
                                    'facility_name'                             => $v['facility_name'],
                                    'facility_no'                               => $v['facility_no'],
                                    'create_by'                                 => (int)$data['userid'],
                                    'create_time'                               => date('Y-m-d H:i:s'),
                                    'modify_by'                                 => (int)$data['userid'],
                                    'modify_time'                               => date('Y-m-d H:i:s')
                                ]);
                            }
                        }
                    }

                }
            }


            Db::commit();

            return true;
        } catch (\Exception $e) {
            Db::rollback();

            return false;
        }


        $edit = Db::table('jy_organization_calculate')->where('id', (int)$data['id'])->update([
            'contrast_year' => $data['contrast_year'],
            'modify_by'     => $data['modify_by'],
            'modify_time'   => $data['modify_time'],
        ]);

        return $edit;
    }

    /**
     * getCalculateDetails 查询核算详情列表
     * 
     * @author wuyinghua
     * @param $organization_calculate_id
     * @param $calculate_year
     * @param $calculate_month
	 * @return $list
     */
    public static function getCalculateDetails($organization_calculate_id, $calculate_year, $calculate_month) {
        $list = Db::table('jy_organization_calculate_detail jocd')
            ->field('jocd.id, jocd.organization_calculate_id, jocd.organization_id, jocd.parent_organization_id pid, jo.name organization_name, jo.type organization_type, jocd.calculate_year, jocd.calculate_month, 0 + CAST(jocd.emissions AS CHAR) emissions, jocd.state')
            ->leftJoin('jy_organization jo', 'jocd.organization_id = jo.id')
            ->where(['jocd.organization_calculate_id'=>(int)$organization_calculate_id, 'jocd.calculate_year'=>$calculate_year, 'jocd.calculate_month'=>$calculate_month, 'jocd.is_del'=>ParamModel::IS_DEL_NO])
            ->order(['jocd.parent_organization_id'=>'asc'])
            ->select();

        return $list;
    }


    /**
     * collect 发起收集任务
     * 
     * @author wuyinghua
     * @param $data
     * @param $is_all
	 * @return $list
     */
    public static function collect($data, $is_all = false) {
        if ($is_all) {
            $update = Db::table('jy_organization_calculate_detail')->where(['organization_calculate_id'=>(int)$data['organization_calculate_id'], 'state'=>ParamModel::ORGDETAIL_STATE_COLLECT])->update([
                'state'             => (int)$data['state'],
                'modify_by'         => (int)$data['userid'],
                'modify_time'       => date('Y-m-d H:i:s')
            ]);
        } else {
            $update = Db::table('jy_organization_calculate_detail')->where('id', (int)$data['id'])->update([
                'state'             => (int)$data['state'],
                'modify_by'         => (int)$data['userid'],
                'modify_time'       => date('Y-m-d H:i:s')
            ]);
        }

        return $update;
    }
}
