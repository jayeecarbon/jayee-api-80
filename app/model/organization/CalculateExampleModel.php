<?php
declare (strict_types = 1);

namespace app\model\organization;

use app\model\data\ParamModel;
use think\facade\Config;
use think\facade\Db;

/**
 * @mixin \think\Model
 */
class CalculateExampleModel extends Db {
    /**
     * getCalculateExamples 查询核算模型
     * 
     * @author wuyinghua
     * @param $page_ize
     * @param $page_index
     * @param $filters
	 * @return $list
     */
    public static function getCalculateExamples($page_size, $page_index, $filters) {
        $where = [];

        if (isset($filters['organization_id']) && $filters['organization_id'] != 0) {
            $where[] = array(['jcm.organization_id', '=', $filters['organization_id']]);
        }

        if (isset($filters['model_name']) && $filters['model_name'] != '') {
            $where[] = array(['jcm.model_name', 'like', '%' . trim($filters['model_name']) . '%']);
        }

        $where[] = array(['jcm.main_organization_id', '=', $filters['main_organization_id']]);

        $list = Db::table('jy_calculate_model jcm')
            ->field('jcm.id, jcm.gwp_id, jg.name gwp_name, jcm.model_name, jcm.description, jcm.organization_id, jo.name organization_name, ju.username, jcm.create_time')
            ->leftJoin('jy_organization jo', 'jcm.organization_id = jo.id')
            ->leftJoin('jy_user ju', 'jcm.create_by = ju.id')
            ->leftJoin('jy_gwp jg', 'jcm.gwp_id = jg.id')
            ->where($where)
            ->where('jcm.is_del', ParamModel::IS_DEL_NO)
            ->order(['jcm.modify_time'=>'desc', 'jcm.create_time'=>'desc'])
            ->paginate(['list_rows'=>$page_size, 'page'=>$page_index]);

        return $list;
    }

    /**
     * getCalculateExampleByOrg 查询组织的核算模型
     * 
     * @author wuyinghua
     * @param $organization_id
	 * @return $list
     */
    public static function getCalculateExampleByOrg($organization_id) {
        $list = Db::table('jy_calculate_model jcm')
            ->field('jcm.id, jcm.gwp_id, jg.name gwp_name, jcm.model_name, jcm.description, jcm.organization_id, jo.pid, jo.name organization_name, ju.username, jcm.create_time')
            ->leftJoin('jy_organization jo', 'jcm.organization_id = jo.id')
            ->leftJoin('jy_user ju', 'jcm.create_by = ju.id')
            ->leftJoin('jy_gwp jg', 'jcm.gwp_id = jg.id')
            ->where(['jcm.is_del'=>ParamModel::IS_DEL_NO, 'jcm.organization_id'=>$organization_id])
            ->find();

        return $list;
    }

    /**
     * getAllGwps 查询全部GWP
     * 
     * @author wuyinghua
	 * @return $list
     */
    public static function getAllGwps() {
        $list = Db::table('jy_gwp')->where('state', ParamModel::STATE_YES)->select();

        return $list;
    }

    /**
     * addCalculateExample 添加核算模型
     * 
     * @author wuyinghua
     * @param $data
	 * @return $add
     */
    public static function addCalculateExample($data) {
        $add = Db::table('jy_calculate_model')->insert($data);

        return $add;
    }

    /**
     * editCalculateExample 编辑核算模型
     * 
     * @author wuyinghua
     * @param $data
	 * @return $edit
     */
    public static function editCalculateExample($data) {

        $edit = Db::table('jy_calculate_model')->where('id', (int)$data['id'])->update([
            'model_name'      => $data['model_name'],
            'gwp_id'          => $data['gwp_id'],
            'organization_id' => $data['organization_id'],
            'description'     => $data['description'],
            'modify_by'       => $data['modify_by'],
            'modify_time'     => $data['modify_time'],
        ]);

        return $edit;
    }

    /**
     * getCalculateExample 获取核算模型
     *
     * @author wuyinghua
     * @param $id
     * @return $list
     */
    public static function getCalculateExample($id) {

        $list = Db::table('jy_calculate_model jcm')
            ->field('jcm.id, jcm.model_name, jcm.gwp_id, jg.name gwp_name, jg.en_short_name, jcm.organization_id, jo.name organization_name, jcm.description')
            ->leftJoin('jy_organization jo', 'jcm.organization_id = jo.id')
            ->leftJoin('jy_gwp jg', 'jcm.gwp_id = jg.id')
            ->where('jcm.id', (int)$id)
            ->find();

        return $list;
    }

    /**
     * delCalculateExample 删除核算模型
     * 
     * @author wuyinghua
     * @param $id
	 * @return $del
     */
    public static function delCalculateExample($id) {
        $del = Db::table('jy_calculate_model')->where('id', (int)$id)->update(['is_del' => ParamModel::IS_DEL_YES]);

        return $del;
    }

    /**
     * copyCalculateExample 复制核算模型
     * 
     * @author wuyinghua
     * @param $data
     * @param $userid
	 * @return $copy
     */
    public static function copyCalculateExample($data, $userid) {
        Db::startTrans();
        try {
            $list = Db::table('jy_calculate_model')->where('id', (int)$data['id'])->find();
            $emission_arr = Db::table('jy_calculate_model_emission')->where('calculate_model_id', (int)$data['id'])->select()->toArray();
            $model_insert_id = Db::table('jy_calculate_model')
                ->insertGetId([
                    'model_name'           => $data['model_name'],
                    'main_organization_id' => (int)$list['main_organization_id'],
                    'organization_id'      => (int)$data['organization_id'],
                    'gwp_id'               => (int)$data['gwp_id'],
                    'description'          => $data['description'],
                    'create_by'            => (int)$userid,
                    'create_time'          => date('Y-m-d H:i:s'),
                    'modify_by'            => (int)$userid,
                    'modify_time'          => date('Y-m-d H:i:s')
                ]);

            // 插入核算模型关联排放源表 todo 复制设备表
            foreach ($emission_arr as $key => $value) {
                $emi_id = Db::table('jy_calculate_model_emission')
                ->insertGetId([
                    'calculate_model_id' => (int)$model_insert_id,
                    'emission_id'        => (int)$value['emission_id'],
                    'create_by'          => (int)$userid,
                    'create_time'        => date('Y-m-d H:i:s'),
                    'modify_by'          => (int)$userid,
                    'modify_time'        => date('Y-m-d H:i:s')
                ]);

                $facility_arr = Db::table('jy_calculate_model_emission_facility')->where(['calculate_model_id'=>(int)$data['id'],'calculate_model_emission_id'=>(int)$value['id']])->select()->toArray();
                foreach ($facility_arr as $k => $v) {
                    Db::table('jy_calculate_model_emission_facility')
                    ->insertGetId([
                        'calculate_model_id'          => (int)$model_insert_id,
                        'calculate_model_emission_id' => (int)$emi_id,
                        'facility_id'                 => (int)$v['facility_id'],
                        'create_by'                   => (int)$userid,
                        'create_time'                 => date('Y-m-d H:i:s'),
                        'modify_by'                   => (int)$userid,
                        'modify_time'                 => date('Y-m-d H:i:s')
                    ]);
                }
            }
            Db::commit();

            return true;
        } catch (\Exception $e) {
            Db::rollback();

            return false;
        }
    }

    /**
     * getEmissions 排放源管理列表
     * 
     * @author wuyinghua
     * @param $page_ize
     * @param $page_index
     * @param $filters
	 * @return $list
     */
    public static function getEmissions($page_size, $page_index, $filters) {
        $where = [];
        $where[] = array(['jcme.calculate_model_id', '=', $filters['calculate_model_id']]);

        $list = Db::table('jy_calculate_model_emission jcme')
            ->field('jcme.id, jcme.calculate_model_id, jes.name emission_name, jes.source_id, jes.active, g.name ghg_cate, h.name ghg_type, gh.name iso_cate, ghg.name iso_type')
            ->leftJoin('jy_emission_source jes','jes.id = jcme.emission_id')
            ->leftJoin('jy_ghg_template g','g.id = jes.ghg_cate')
            ->leftJoin('jy_ghg_template h','h.id = jes.ghg_type')
            ->leftJoin('jy_ghg_template gh','gh.id = jes.iso_cate')
            ->leftJoin('jy_ghg_template ghg','ghg.id = jes.iso_type')
            ->where($where)
            ->order(['jcme.modify_time'=>'desc', 'jcme.create_time'=>'desc'])
            ->paginate(['list_rows'=>$page_size, 'page'=>$page_index]);

        return $list;
    }

    /**
     * getFacilityByEmissionId 获取设备数据通过核算模型关联排放源Id
     *
     * @author wuyinghua
     * @param $calculate_model_emission_id
     * @return $list
     */
    public static function getFacilityByEmissionId($calculate_model_emission_id) {

        $list = Db::table('jy_calculate_model_emission_facility')
            ->where('calculate_model_emission_id', (int)$calculate_model_emission_id)
            ->select();

        return $list;
    }

    /**
     * getEmissionModelSource 获取核算模型中排放源数据
     *
     * @author wuyinghua
     * @param $id
     * @return $list
     */
    public static function getEmissionModelSource($id) {

        $list = Db::table('jy_calculate_model_emission jcme')
            ->where('jcme.id', (int)$id)
            ->find();

        return $list;
    }

    /**
     * getEmissionBySource 根据排放源ID获取核算模型中排放源数据
     *
     * @author wuyinghua
     * @param $emission_id
     * @param $calculate_model_id
     * @return $list
     */
    public static function getEmissionBySource($emission_id, $calculate_model_id) {

        $list = Db::table('jy_calculate_model_emission jcme') 
            ->where(['jcme.emission_id'=>$emission_id, 'calculate_model_id'=>$calculate_model_id])
            ->find();

        return $list;
    }

    /**
     * getInfoById 获取排放源详情
     *
     * @author wuyinghua
     * @param $id
     * @return $list
     */
    public static function getInfoById($id) {

        $list = Db::table('jy_emission_source jcme')
            ->field('jcme.*, g.name ghg_cate_name, h.name ghg_type_name, gh.name iso_cate_name, ghg.name iso_type_name')
            ->leftJoin('jy_ghg_template g','g.id = jcme.ghg_cate')
            ->leftJoin('jy_ghg_template h','h.id = jcme.ghg_type')
            ->leftJoin('jy_ghg_template gh','gh.id = jcme.iso_cate')
            ->leftJoin('jy_ghg_template ghg','ghg.id = jcme.iso_type')
            ->where('jcme.id', (int)$id)
            ->find();

        if ($list) {
            $active_unit = explode(',', $list['active_unit']);
            $list['ghg_cate'] = ['id'=>$list['ghg_cate'], 'name'=>$list['ghg_cate_name']];
            $list['ghg_type'] = ['id'=>$list['ghg_type'], 'name'=>$list['ghg_type_name']];
            $list['iso_cate'] = ['id'=>$list['iso_cate'], 'name'=>$list['iso_cate_name']];
            $list['iso_type'] = ['id'=>$list['iso_type'], 'name'=>$list['iso_type_name']];
            $list['active_unit'] = $active_unit;

            $list['active_type'] = ['id'=>$list['active_type'], 'name'=>Config::get('emission.active_type')[$list['active_type']-1]['name']];
            $list['factor_type'] = ['id'=>$list['factor_type'], 'name'=> Config::get('emission.factor_type')[$list['factor_type']-1]['name']];

            $gas = Db::table('jy_emission_gas')
                ->where('emi_id', (int)$id)
                ->select()
                ->toArray();

            if($gas){
                foreach ($gas as $k => $v){
                    $v['denominator'] = explode(',', $v['denominator']);
                    $v['molecule'] = ['id'=>$v['molecule'], 'name'=>Config::get('emission.molecule')[$v['molecule']-1]['name']];
                    $gas[$k]=$v;
                }
            }
            $list['gas'] = $gas;
        }

        return $list;
    }

    /**
     * getEmissionSource 获取排放源数据
     *
     * @author wuyinghua
     * @param $id
     * @return $list
     */
    public static function getEmissionSource($id) {

        $list = Db::table('jy_emission_source') 
            ->where('id', (int)$id)
            ->find();

        return $list;
    }

    /**
     * getFacilities 关联设备列表
     * 
     * @author wuyinghua
     * @param $filters
	 * @return $list
     */
    public static function getFacilities($filters) {
        $where = [];
        $where[] = array(['jesf.calculate_model_emission_id', '=', $filters['calculate_model_emission_id']]);

        $list = Db::table('jy_calculate_model_emission_facility jesf')
            ->field('jesf.id, jesf.calculate_model_emission_id, jf.facility_name, jf.facility_no')
            ->leftJoin('jy_facility jf','jf.id = jesf.facility_id')
            ->where($where)
            ->order(['jesf.modify_time'=>'desc', 'jesf.create_time'=>'desc'])
            ->select();

        return $list;
    }

    /**
     * emissionChoice 选择排放源
     * 
     * @author wuyinghua
     * @param $data
	 * @return $copy
     */
    public static function emissionChoice($data) {
        Db::startTrans();
        try {
            foreach ($data['ids'] as $id) {
                Db::table('jy_calculate_model_emission')
                    ->insertGetId([
                        'calculate_model_id' => (int)$data['calculate_model_id'],
                        'emission_id'        => (int)$id,
                        'create_by'          => (int)$data['userid'],
                        'create_time'        => date('Y-m-d H:i:s'),
                        'modify_by'          => (int)$data['userid'],
                        'modify_time'        => date('Y-m-d H:i:s')
                ]);
            }

            Db::commit();

            return true;
        } catch (\Exception $e) {
            Db::rollback();

            return false;
        }
    }

    /**
     * delEmissionSource 删除核算模型关联的排放源
     * 
     * @author wuyinghua
     * @param $id
	 * @return $del
     */
    public static function delEmissionSource($id) {
        Db::startTrans();
        try {
            Db::table('jy_calculate_model_emission')->where('id', (int)$id)->delete();
            Db::table('jy_calculate_model_emission_facility')->where('calculate_model_emission_id', (int)$id)->delete();

            Db::commit();

            return true;
        } catch (\Exception $e) {
            Db::rollback();

            return false;
        }
    }

    /**
     * addEmissionSourceFacility 添加核算模型-排放源关联的设备
     * 
     * @author wuyinghua
     * @param $data
	 * @return $add
     */
    public static function addEmissionSourceFacility($data) {
        Db::startTrans();
        try {
            // 查询calculate_model_id
            $emission_data = Db::table('jy_calculate_model_emission jcme')
                ->where('jcme.id', (int)$data['calculate_model_emission_id'])
                ->find();
            $calculate_model_id = $emission_data['calculate_model_id'];
            // 清空该排放源的设备
            Db::table('jy_calculate_model_emission_facility')->where('calculate_model_emission_id', (int)$data['calculate_model_emission_id'])->delete();

            // 遍历新增设备
            foreach ($data['ids'] as $facility_no) {
                // 查询设备信息
                $facility_data = Db::table('jy_facility jf')
                    ->field('jf.id, jf.facility_name, jf.facility_no')
                    ->where('jf.facility_no', $facility_no)
                    ->find();

                Db::table('jy_calculate_model_emission_facility')
                ->insert([
                    'calculate_model_id'          => (int)$calculate_model_id,
                    'calculate_model_emission_id' => (int)$data['calculate_model_emission_id'],
                    'facility_id'                 => (int)$facility_data['id'],
                    'create_by'                   => $data['userid'],
                    'modify_by'                   => $data['userid'],
                    'create_time'                 => date('Y-m-d H:i:s'),
                    'modify_time'                 => date('Y-m-d H:i:s')
                ]);
            }

            Db::commit();

            return true;
        } catch (\Exception $e) {
            Db::rollback();

            return false;
        }
    }

    /**
     * getEmissionSourceFacility 获取排放源关联的设备数据
     *
     * @author wuyinghua
     * @param $id
     * @return $list
     */
    public static function getEmissionSourceFacility($id) {
        $list = Db::table('jy_calculate_model_emission_facility') 
            ->where('id', (int)$id)
            ->find();

        return $list;
    }

    /**
     * delEmissionSourceFacility 删除排放源关联的设备
     * 
     * @author wuyinghua
     * @param $id
	 * @return $del
     */
    public static function delEmissionSourceFacility($id) {
        $del = Db::table('jy_calculate_model_emission_facility')->where('id', (int)$id)->delete();

        return $del;
    }
}
