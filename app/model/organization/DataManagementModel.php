<?php
declare (strict_types = 1);

namespace app\model\organization;

use app\model\data\ParamModel;
use think\facade\Config;
use think\facade\Db;

/**
 * @mixin \think\Model
 */
class DataManagementModel extends Db
{
    /**
     * getDataManagements 查询数据管理列表
     * 
     * @author wuyinghua
     * @param $page_ize
     * @param $page_index
     * @param $filters
	 * @return $list
     */
    public static function getDataManagements($page_size, $page_index, $filters) {
        $where = [];

        if (isset($filters['organization_id']) && $filters['organization_id'] != 0) {
            $where[] = array(['jocd.organization_id', '=', $filters['organization_id']]);
        }

        if (isset($filters['calculate_year']) && $filters['calculate_year'] != '') {
            $where[] = array(['jocd.calculate_year', '=', $filters['calculate_year']]);
        }

        if (isset($filters['state']) && $filters['state'] != '') {
            $where[] = array(['jocd.state', '=', trim($filters['state'])]);
        }

        $where[] = array(['jocd.main_organization_id', '=', $filters['main_organization_id']]);
        $where[] = array(['jocd.is_del', '=', ParamModel::IS_DEL_NO]);

        $list = Db::table('jy_organization_calculate_detail jocd')
            ->field('jocd.id, jocd.calculate_year, jocd.calculate_month, 0 + CAST(jocd.emissions AS CHAR) emissions, jocd.state, jocd.organization_id, jo.name organization_name, ju.username, jocd.create_time, jocd.modify_time')
            ->leftJoin('jy_organization jo', 'jocd.organization_id = jo.id')
            ->leftJoin('jy_user ju', 'jocd.create_by = ju.id')
            ->where($where)
            ->whereIn('jocd.state', 
                ParamModel::ORGDETAIL_STATE_SUBMIT . ',' .
                ParamModel::ORGDETAIL_STATE_REVIEW_FACTORY . ',' .
                ParamModel::ORGDETAIL_STATE_REVIEW_BRANCH . ',' .
                ParamModel::ORGDETAIL_STATE_REVIEW_MAIN . ',' .
                ParamModel::ORGDETAIL_STATE_PASS . ',' .
                ParamModel::ORGDETAIL_STATE_FAILED
            )
            ->order(['jocd.modify_time'=>'desc', 'jocd.create_time'=>'desc'])
            ->paginate(['list_rows'=>$page_size, 'page'=>$page_index]);

        return $list;
    }

    /**
     * getAllOrganizationCalculate 获取所有待审批组织核算
     * 
     * @author wuyinghua
     * @param $main_organization_id
	 * @return $list
     */
    public static function getAllOrganizationCalculate($main_organization_id) {
        $list = Db::table('jy_organization_calculate_detail jocd')
            ->field('jocd.id, joc.calculate_name, jocd.calculate_year, jocd.calculate_month, 0 + CAST(jocd.emissions AS CHAR) emissions, jocd.state, jocd.organization_id, jo.name organization_name, ju.username, jocd.create_time, jocd.modify_time')
            ->leftJoin('jy_organization_calculate joc', 'jocd.organization_calculate_id = joc.id')
            ->leftJoin('jy_organization jo', 'jocd.organization_id = jo.id')
            ->leftJoin('jy_user ju', 'jocd.modify_by = ju.id')
            ->where(['jocd.main_organization_id'=>(int)$main_organization_id, 'jocd.is_del'=>ParamModel::IS_DEL_NO])
            ->whereIn('jocd.state', 
                ParamModel::ORGDETAIL_STATE_REVIEW_FACTORY . ',' .
                ParamModel::ORGDETAIL_STATE_REVIEW_BRANCH . ',' .
                ParamModel::ORGDETAIL_STATE_REVIEW_MAIN
            )
            ->order(['jocd.modify_time'=>'desc', 'jocd.create_time'=>'desc'])
            ->select();

        return $list;
    }

    /**
     * editCarbonCulateFacilityEmission 录入设备排放源活动数据
     *
     * @author wuyinghua
     * @param $data
     * @return $edit
     */
    public static function editCarbonCulateFacilityEmission($data) {
        // 总排放量排除范围一-碳封存/直接排放或清除-土地利用外的汇总排放量
        if (($data['ghg_cate'] == 1 && $data['ghg_type'] == 8) || ($data['iso_cate'] == 29 && $data['iso_type'] == 39)) {
            $data['active_value'] = 0 - $data['active_value'];
        }

        Db::startTrans();
        try {
            Db::table('jy_organization_calculate_detail_facility')->where('id', (int)$data['facility_id'])->update([
                'active_value'          => $data['active_value'], //活动数据
                'active_value_standard' => $data['active_value'], // 活动数据（基准单位，换算后的数据）
                'emissions'             => $data['active_value'] * $data['gwp'],
                'modify_by'             => (int)$data['userid'],
                'modify_time'           => date('Y-m-d H:i:s')
            ]);

            Db::table('jy_organization_calculate_detail')->where('id', (int)$data['organization_calculate_detail_id'])->update([
                'modify_by'   => (int)$data['userid'],
                'modify_time' => date('Y-m-d H:i:s')
            ]);
            Db::commit();

            return true;
        } catch (\Exception $e) {
            Db::rollback();

            return false;
        }
    }

    /**
     * editCarbonCulateDetailEmissionFile 核算详情排放源活动数据文件修改
     *
     * @author wuyinghua
     * @param $id
     * @param $file
     * @return $edit
     */
    public static function editCarbonCulateDetailEmissionFile($id, $files) {
        $edit = Db::table('jy_organization_calculate_detail')->where('id', (int)$id)->update(['files'=>$files]);

        return $edit;
    }

    /**
     * editCarbonCulateFacilityEmission 设备排放源活动数据文件修改
     *
     * @author wuyinghua
     * @param $id
     * @param $file
     * @return $edit
     */
    public static function editCarbonCulateFacilityEmissionFile($id, $files) {
        $edit = Db::table('jy_organization_calculate_detail_facility')->where('id', (int)$id)->update(['files'=>$files]);

        return $edit;
    }

    /**
     * editCarbonCulateDetailEmission 录入核算详情排放源活动数据
     *
     * @author wuyinghua
     * @param $data
     * @return $edit
     */
    public static function editCarbonCulateDetailEmission($data) {
        Db::startTrans();
        try {
            // 先删除排放源相关的count_gas数据后再插入数据
            Db::table('jy_organization_calculate_count_gas')->where('emi_id', (int)$data['id'])->delete();
            // 往碳核算- 排放量计算表（jy_organization_calculate_count_gas）插入数据，用于生成文件的数据
            $gwp_value = 1;
            $emission_gas_list = Db::table('jy_organization_calculate_detail_emission_gas')->where('emi_id', (int)$data['id'])->select()->toArray();//return $emission_gas_list;
            $molecule_arr = Config::get('emission.molecule');

            $gases_name = '';

            $co2e_factor_value = NULL; // co2e排放因子
            $tco2e = NULL; // 二氧化碳当量（tCO₂e）
            $co2_factor_value = NULL; // co2排放因子
            $co2_quality = NULL; // 气体质量=活动数据*排放因子
            $co2_gwp = NULL; // co2gwp
            $co2_tco2e = NULL; // 二氧化碳当量=排放因子*gwp
            $ch4_factor_value = NULL; // co2排放因子
            $ch4_quality = NULL; // 气体质量=活动数据*排放因子
            $ch4_gwp = NULL; // co2gwp
            $ch4_tco2e = NULL; // 二氧化碳当量=排放因子*gwp
            $n2o_factor_value = NULL; // co2排放因子
            $n2o_quality = NULL; // 气体质量=活动数据*排放因子
            $n2o_gwp = NULL; // co2gwp
            $n2o_tco2e = NULL; // 二氧化碳当量=排放因子*gwp
            $sf6_factor_value = NULL; // co2排放因子
            $sf6_quality = NULL; // 气体质量=活动数据*排放因子
            $sf6_gwp = $gwp_value; // co2gwp
            $sf6_tco2e = NULL; // 二氧化碳当量=排放因子*gwp
            $nf3_factor_value = NULL; // co2排放因子
            $nf3_quality = NULL; // 气体质量=活动数据*排放因子
            $nf3_gwp = $gwp_value; // co2gwp
            $nf3_tco2e = NULL; // 二氧化碳当量=排放因子*gwp
            $hfcs_factor_value = NULL; // co2排放因子
            $hfcs_quality = NULL; // 气体质量=活动数据*排放因子
            $hfcs_gwp = $gwp_value; // co2gwp
            $hfcs_tco2e = NULL; // 二氧化碳当量=排放因子*gwp
            $pfcs_factor_value = NULL; // co2排放因子
            $pfcs_quality = NULL; // 气体质量=活动数据*排放因子
            $pfcs_gwp = $gwp_value; // co2gwp
            $pfcs_tco2e = NULL; // 二氧化碳当量=排放因子*gwp

            foreach ($emission_gas_list as $key => $value) {//return $value['gas'];
                $molecule = $value['molecule']; // 排放因子单位（分子）
                // 1. 其余气体数据（现在七种）2. 二氧化碳当量数据(gwp_value = 1)
                if ($value['gas'] != '二氧化碳当量（CO₂e）') {
                    $gase_data = Db::table('jy_gases')->where(['type'=>$value['gas_type'], 'name'=>$value['gas']])->find();//return $value;
                    if ($gase_data != NULL) {
                        $gwp_value = $gase_data[$data['en_short_name']];
                    }
                }

                // 获取分子单位（基准单位为tCO2e）、分母单位，并转化为基准单位
                foreach ($molecule_arr as $k => $v) {
                    if ($value['molecule'] == $v['id']) {
                        $molecule_conversion_ratio = $v['conversion_ratio']; // 分子换算比例
                    }
                }

                // 分母转换为基准单位
                $denominator_arr = explode(',', $value['denominator']);
                $unit_data = Db::table('jy_unit jyu')->where('jyu.id', (int)$denominator_arr[1])->find();
                $denominator_conversion_ratio = $unit_data['conversion_ratio']; // 分母换算比例
                $factor_value = $value['factor_value'] * $molecule_conversion_ratio / $denominator_conversion_ratio * $data['conversion_ratio']; // 排放因子按基准单位换算后的值

                // 二氧化碳当量（CO₂e）
                if ($value['gas'] == '二氧化碳当量（CO₂e）') {
                    $co2e_factor_value = $factor_value; // co2e排放因子
                    $tco2e = $factor_value; // 二氧化碳当量（tCO₂e）
                    $gases_name .= ',CO₂e';
                }

                // 二氧化碳（CO₂）
                if ($value['gas'] == '二氧化碳（CO₂）') {
                    $co2_factor_value = $factor_value; // co2排放因子
                    $co2_quality = $data['active_value_standard'] * $factor_value; // 气体质量=活动数据*排放因子
                    $co2_gwp = $gwp_value; // co2gwp
                    $co2_tco2e = $factor_value * $gwp_value; // 二氧化碳当量=排放因子*gwp
                    $gases_name .= ',CO₂';
                }

                // 甲烷（CH₄）
                if ($value['gas'] == '甲烷（CH₄）') {
                    $ch4_factor_value = $factor_value; // co2排放因子
                    $ch4_quality = $data['active_value_standard'] * $factor_value; // 气体质量=活动数据*排放因子
                    $ch4_gwp = $gwp_value; // co2gwp
                    $ch4_tco2e = $factor_value * $gwp_value; // 二氧化碳当量=排放因子*gwp
                    $gases_name .= ',CH₄';
                }

                // 氧化亚氮（N₂O）
                if ($value['gas'] == '氧化亚氮（N₂O）') {
                    $n2o_factor_value = $factor_value; // co2排放因子
                    $n2o_quality = $data['active_value_standard'] * $factor_value; // 气体质量=活动数据*排放因子
                    $n2o_gwp = $gwp_value; // co2gwp
                    $n2o_tco2e = $factor_value * $gwp_value; // 二氧化碳当量=排放因子*gwp
                    $gases_name .= ',N₂O';
                }

                // 六氟化硫（SF₆）
                if ($value['gas'] == '六氟化硫（SF₆）') {
                    $sf6_factor_value = $factor_value; // co2排放因子
                    $sf6_quality = $data['active_value_standard'] * $factor_value; // 气体质量=活动数据*排放因子
                    $sf6_gwp = $gwp_value; // co2gwp
                    $sf6_tco2e = $factor_value * $gwp_value; // 二氧化碳当量=排放因子*gwp
                    $gases_name .= ',SF₆';
                }

                // 三氟化氮（NF₃）
                if ($value['gas'] == '三氟化氮（NF₃）') {
                    $nf3_factor_value = $factor_value; // co2排放因子
                    $nf3_quality = $data['active_value_standard'] * $factor_value; // 气体质量=活动数据*排放因子
                    $nf3_gwp = $gwp_value; // co2gwp
                    $nf3_tco2e = $factor_value * $gwp_value; // 二氧化碳当量=排放因子*gwp
                    $gases_name .= ',NF₃';
                }

                // 氢氟碳化物（HFCs）
                if ($value['gas'] == '氢氟碳化物（HFCs）') {
                    $hfcs_factor_value = $factor_value; // co2排放因子
                    $hfcs_quality = $data['active_value_standard'] * $factor_value; // 气体质量=活动数据*排放因子
                    $hfcs_gwp = $gwp_value; // co2gwp
                    $hfcs_tco2e = $factor_value * $gwp_value; // 二氧化碳当量=排放因子*gwp
                    $gases_name .= ',HFCs';
                }

                // 全氟碳化物（PFCs）
                if ($value['gas'] == '全氟碳化物（PFCs）') {
                    $pfcs_factor_value = $factor_value; // co2排放因子
                    $pfcs_quality = $data['active_value_standard'] * $factor_value; // 气体质量=活动数据*排放因子
                    $pfcs_gwp = $gwp_value; // co2gwp
                    $pfcs_tco2e = $factor_value * $gwp_value; // 二氧化碳当量=排放因子*gwp
                    $gases_name .= ',PFCs';
                }
            }
            $gases_name = substr($gases_name, 1);

            Db::table('jy_organization_calculate_count_gas')
            ->insert([
                'organization_calculate_detail_id' => (int)$data['organization_calculate_detail_id'],
                'organization_calculate_id'        => (int)$data['organization_calculate_id'],
                'emi_id'                           => (int)$data['id'],
                'molecule'                         => $molecule, // 排放因子单位（分子）
                'gases'                            => $gases_name, // 气体名称以逗号分隔
                // 二氧化碳当量（CO₂e）
                'co2e_factor_value'                => $co2e_factor_value, // co2e排放因子
                'tco2e'                            => $tco2e, // 二氧化碳当量（tCO₂e）
                // 二氧化碳（CO₂）
                'co2_factor_value'                 => $co2_factor_value, // co2排放因子
                'co2_quality'                      => $co2_quality, // 气体质量=活动数据*排放因子
                'co2_gwp'                          => $co2_gwp, // co2gwp
                'co2_tco2e'                        => $co2_tco2e, // 二氧化碳当量=排放因子*gwp
                // 甲烷（CH₄）
                'ch4_factor_value'                 => $ch4_factor_value, // co2排放因子
                'ch4_quality'                      => $ch4_quality, // 气体质量=活动数据*排放因子
                'ch4_gwp'                          => $ch4_gwp, // co2gwp
                'ch4_tco2e'                        => $ch4_tco2e, // 二氧化碳当量=排放因子*gwp
                // 氧化亚氮（N₂O）
                'n2o_factor_value'                 => $n2o_factor_value, // co2排放因子
                'n2o_quality'                      => $n2o_quality, // 气体质量=活动数据*排放因子
                'n2o_gwp'                          => $n2o_gwp, // co2gwp
                'n2o_tco2e'                        => $n2o_tco2e, // 二氧化碳当量=排放因子*gwp
                // 六氟化硫（SF₆）
                'sf6_factor_value'                 => $sf6_factor_value, // co2排放因子
                'sf6_quality'                      => $sf6_quality, // 气体质量=活动数据*排放因子
                'sf6_gwp'                          => $sf6_gwp, // co2gwp
                'sf6_tco2e'                        => $sf6_tco2e, // 二氧化碳当量=排放因子*gwp
                // 三氟化氮（NF₃）
                'nf3_factor_value'                 => $nf3_factor_value, // co2排放因子
                'nf3_quality'                      => $nf3_quality, // 气体质量=活动数据*排放因子
                'nf3_gwp'                          => $nf3_gwp, // co2gwp
                'nf3_tco2e'                        => $nf3_tco2e, // 二氧化碳当量=排放因子*gwp
                // 氢氟碳化物（HFCs）
                'hfcs_factor_value'                => $hfcs_factor_value, // co2排放因子
                'hfcs_quality'                     => $hfcs_quality, // 气体质量=活动数据*排放因子
                'hfcs_gwp'                         => $hfcs_gwp, // co2gwp
                'hfcs_tco2e'                       => $hfcs_tco2e, // 二氧化碳当量=排放因子*gwp
                // 全氟碳化物（PFCs）
                'pfcs_factor_value'                => $pfcs_factor_value, // co2排放因子
                'pfcs_quality'                     => $pfcs_quality, // 气体质量=活动数据*排放因子
                'pfcs_gwp'                         => $pfcs_gwp, // co2gwp
                'pfcs_tco2e'                       => $pfcs_tco2e, // 二氧化碳当量=排放因子*gwp
            ]);

            // 总排放量排除范围一-碳封存/直接排放或清除-土地利用外的汇总排放量
            if (($data['ghg_cate'] == 1 && $data['ghg_type'] == 8) || ($data['iso_cate'] == 29 && $data['iso_type'] == 39)) {
                $data['active_value'] = 0 - $data['active_value'];
            }

            Db::table('jy_organization_calculate_detail_emission')->where('id', (int)$data['id'])->update([
                'active_value'          => $data['active_value'], //活动数据
                'active_value_standard' => $data['active_value'], // 活动数据（基准单位，换算后的数据）
                'emissions'             => $data['active_value'] * $data['gwp'],
                'modify_by'             => (int)$data['userid'],
                'modify_time'           => date('Y-m-d H:i:s')
            ]);

            // 合计核算详情表-月
            $detail_emission_list = Db::table('jy_organization_calculate_detail_emission')->where(['organization_calculate_detail_id'=>(int)$data['organization_calculate_detail_id'], 'is_del'=>ParamModel::IS_DEL_NO])->select()->toArray();
            $detail_emission_sum = array_sum(array_column($detail_emission_list, 'emissions'));
            Db::table('jy_organization_calculate_detail')->where('id', (int)$data['organization_calculate_detail_id'])->update([
                'emissions'   => $detail_emission_sum,
                'modify_by'   => (int)$data['userid'],
                'modify_time' => date('Y-m-d H:i:s')
            ]);

            // 合计核算表-年
            $detail_data = Db::table('jy_organization_calculate_detail')->where('id', (int)$data['organization_calculate_detail_id'])->find();
            $calculate_model_id = $detail_data['calculate_model_id'];
            $detail_list = Db::table('jy_organization_calculate_detail')->where(['calculate_model_id'=>(int)$calculate_model_id, 'is_del'=>ParamModel::IS_DEL_NO])->select()->toArray();
            $emission_sum = array_sum(array_column($detail_list, 'emissions'));
            Db::table('jy_organization_calculate')->where('id', (int)$calculate_model_id)->update(['emissions' => $emission_sum]);
            Db::commit();

            return true;
        } catch (\Exception $e) {
            Db::rollback();

            return false;
        }
    }

    /**
     * getGase 获取气体数据
     *
     * @author wuyinghua
     * @param $gas_type
     * @param $gas
     * @return $list
     */
    public static function getGase($gas_type, $gas) {

        $list = Db::table('jy_gases')
            ->where(['type'=>$gas_type, 'name'=>$gas])
            ->find();

        return $list;
    }

    /**
     * updateState 提交核算数据
     *
     * @author wuyinghua
     * @param $data
     * @return $edit
     */
    public static function updateState($data) {

        $edit = Db::table('jy_organization_calculate_detail')->where('id', (int)$data['id'])->update([
            'state'       => $data['state'],
            'modify_by'   => (int)$data['userid'],
            'modify_time' => date('Y-m-d H:i:s')
        ]);

        return $edit;
    }
}
