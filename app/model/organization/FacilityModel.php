<?php
declare (strict_types = 1);

namespace app\model\organization;

use app\model\data\ParamModel;
use think\facade\Db;

/**
 * @mixin \think\Model
 */
class FacilityModel extends Db {
    /**
     * getFacilities 查询设备
     * 
     * @author wuyinghua
     * @param $page_ize
     * @param $page_index
     * @param $filters
	 * @return $list
     */
    public static function getFacilities($page_size, $page_index, $filters) {
        $where = [];

        if ($filters['facility_name']) {
            $where[] = array(['jf.facility_name', 'like', '%' . trim($filters['facility_name']) . '%']);
        }

        if ($filters['facility_no']) {
            $where[] = array(['jf.facility_no', 'like', '%' . trim($filters['facility_no']) . '%']);
        }

        if ($filters['main_organization_id']) {
            $where[] = array(['jf.main_organization_id', '=', $filters['main_organization_id']]);
        }

        $list = Db::table('jy_facility jf')
            ->field('jf.id, jf.facility_name, jf.facility_no, jf.organization_id, jo.name organization_name, ju.username, jf.create_time')
            ->leftJoin('jy_organization jo', 'jf.organization_id = jo.id')
            ->leftJoin('jy_user ju', 'jf.create_by = ju.id')
            ->where($where)
            ->where('jf.is_del', ParamModel::IS_DEL_NO)
            ->order(['jf.modify_time'=>'desc', 'jf.create_time'=>'desc'])
            ->paginate(['list_rows'=>$page_size, 'page'=>$page_index]);

        return $list;
    }

    /**
     * getAllFacilities 获取所有设备
     * 
     * @author wuyinghua
     * @param $main_organization_id
	 * @return $list
     */
    public static function getAllFacilities($main_organization_id) {
        $list = Db::table('jy_facility jf')
            ->field('jf.id, jf.facility_name, jf.facility_no')
            ->where('jf.main_organization_id', (int)$main_organization_id)
            ->order(['jf.modify_time'=>'desc', 'jf.create_time'=>'desc'])
            ->select();

        return $list;
    }

    /**
     * getAllEmissionFacilities 获取所有排放源绑定设备 todo 好像没用到
     * 
     * @author wuyinghua
     * @param $calculate_model_emission_id
	 * @return $list
     */
    public static function getAllEmissionFacilities($calculate_model_emission_id) {
        $list = Db::table('jy_calculate_model_emission_facility jf')
            ->field('jf.id, jf.facility_name, jf.facility_no')
            ->where('jf.calculate_model_emission_id', (int)$calculate_model_emission_id)
            ->order(['jf.modify_time'=>'desc', 'jf.create_time'=>'desc'])
            ->select();

        return $list;
    }

    /**
     * addFacility 添加设备
     * 
     * @author wuyinghua
     * @param $data
	 * @return $add
     */
    public static function addFacility($data) {
        $add = Db::table('jy_facility')->insert($data);

        return $add;
    }

    /**
     * editFacility 编辑设备
     * 
     * @author wuyinghua
     * @param $data
	 * @return $edit
     */
    public static function editFacility($data) {

        $edit = Db::table('jy_facility')->where('id', (int)$data['id'])->update([
            'facility_name'   => $data['facility_name'],
            'facility_no'     => $data['facility_no'],
            'organization_id' => $data['organization_id'],
            'modify_by'       => $data['modify_by'],
            'modify_time'     => $data['modify_time'],
        ]);

        return $edit;
    }

    /**
     * delFacility 删除设备
     * 
     * @author wuyinghua
     * @param $id
	 * @return $del
     */
    public static function delFacility($id) {
        Db::startTrans();
        try {
            Db::table('jy_facility')->where('id', (int)$id)->delete();
            Db::table('jy_calculate_model_emission_facility')->where('facility_id', (int)$id)->delete();
            Db::commit();
            return true;
        } catch (\think\exception\ValidateException $e) {
            Db::rollback();
            return false;
        }
    }

    /**
     * getFacility 获取设备数据
     *
     * @author wuyinghua
     * @param $id
     * @return $list
     */
    public static function getFacility($id) {

        $list = Db::table('jy_facility jf')
            ->field('jf.id, jf.facility_name, jf.facility_no, jf.organization_id, jo.name organization_name')
            ->leftJoin('jy_organization jo', 'jf.organization_id = jo.id')
            ->where('jf.id', (int)$id)
            ->find();

        return $list;
    }

    /**
     * getFacilityByNo 根据编码获取设备数据
     *
     * @author wuyinghua
     * @param $facility_no
     * @return $list
     */
    public static function getFacilityByNo($facility_no) {

        $list = Db::table('jy_facility jf')
            ->where('jf.facility_no', $facility_no)
            ->find();

        return $list;
    }
}
