<?php
namespace app\model\admin;

use think\facade\Db;

/**
 * DictionaryModel
 */
class DictionaryModel extends Db {

	//======================================================================
	// PUBLIC FUNCTIONS
	//======================================================================

    /**
     * getDictionaries 查询数据字典
     * 
     * @author wuyinghua
     * @param $page_ize
     * @param $page_index
     * @param $filters
	 * @return $list
     */
    public function getDictionaries($page_size, $page_index, $filters) {
        $where = array();

        if ($filters['filter_unit_type']) {
            $where[] = array(['jd.type', 'like', '%' . trim($filters['filter_unit_type']) . '%']);
        }

        if ($filters['filter_unit_name']) {
            $where[] = array(['jd.name', 'like', '%' . trim($filters['filter_unit_name']) . '%']);
        }

        $list = Db::table('jy_dictionary jd')
            ->field('jd.id, jd.name, jd.is_base, 0 + CAST(jd.conversion_ratio AS CHAR) conversion_ratio, jd.modify_time, ju.username, jut.unit_name type')
            ->leftJoin('jy_unit_type jut', 'jd.type_id = jut.id')
            ->leftJoin('jy_user ju', 'jd.create_by = ju.id')
            ->where($where)
            ->order(['jd.type_id'=>'asc', 'jd.id'=>'asc'])
            ->paginate(['list_rows'=>$page_size, 'page'=>$page_index]);

        return $list;
    }

    /**
     * getDictionary 查询数据字典
     * 
     * @author wuyinghua
     * @param $type
	 * @return $list
     */
    public function getDictionary($type) {
        $where = array();

        if (!empty($type)) {
            $where[] = array(['jd.type', '=', $type]);
        }

        $list = Db::table('jy_dictionary jd')
            ->field('jd.id, jd.name, jd.type_id, jut.unit_name type')
            ->leftJoin('jy_unit_type jut', 'jd.type_id = jut.id')
            ->where($where)
            ->order(['jd.type_id'=>'asc', 'jd.id'=>'asc'])
            ->select();

        return $list;
    }

    /**
     * getUnitType 获取单位类型
     * 
     * @author wuyinghua
     * @param $type
	 * @return $list
     */
    public function getUnitType() {

        $list = Db::table('jy_unit_type jut')
            ->field('jut.id, jut.unit_name')
            ->order('jut.sort', 'asc')
            ->select();

        return $list;
    }
}
