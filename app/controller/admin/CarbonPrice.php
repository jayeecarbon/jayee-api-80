<?php
namespace app\controller\admin;

use app\BaseController;
use app\model\admin\CarbonPriceModel;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use \PhpOffice\PhpSpreadsheet\IOFactory;
use think\Request;

/**
 * CarbonPrice
 */
class CarbonPrice extends BaseController {

	//======================================================================
	// PUBLIC FUNCTIONS
	//======================================================================

    /**
     * index 碳价列表
     * 
     * @author wuyinghua
	 * @return void
     */
    public function index() {
        // 过滤条件：区域、开始时间、结束时间
        $filters = [
            'filter_city' => isset($_GET['filterCity']) ? $_GET['filterCity'] : '',
            'filter_time_start' => isset($_GET['filterTimeStart']) ? $_GET['filterTimeStart'] : '',
            'filter_time_end' => isset($_GET['filterTimeEnd']) ? $_GET['filterTimeEnd'] : ''
        ];

        $db = new CarbonPriceModel();
        $list = $db->getCarbonPrices($filters)->toArray();
        $citys = $db->getCitys()->toArray();

        $data['code'] = 200;
        $data['data']['list'] = $list;
        $data['data']['citys'] = $citys;

        return json($data);
    }

    /**
     * export 导出
     * 
     * @author wuyinghua
	 * @return void
     */
    public function export() {
        $header = ["A1" => "区域", "B1" => "时间","C1" => "成交均价（元/吨）","D1" => "成交量（吨）","E1" => "成交额（元）","F1" => "日涨跌幅"];
        $db = new CarbonPriceModel();
        $list = $db->getCarbonPrices($filters = [])->toArray();

        $arr = [];
        foreach ($list as $key => $value){
            $arr[$key] = array_values($value);
        }

        $type= true;
        $fileName = '碳价管理' . time();

        self::getExport($header,$type,array_values($arr),$fileName);
    }

    /**
     * getExport
     * 
     * @author wuyinghua
	 * @return void
     */
    public static function getExport($header = [], $type =false, $data = [], $fileName = "碳价管理") {
        $preadsheet = new Spreadsheet();
        $sheet = $preadsheet->getActiveSheet();

        foreach ($header as $k => $v) {
            $sheet->setCellValue($k, $v);
        }

        $sheet->fromArray($data, null, "A2");
        $sheet->getDefaultColumnDimension()->setWidth(12);

        if ($type) {
            header("Content-Type:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            $type = "Xlsx";
            $suffix = "xlsx";
        } else {
            header("Content-Type:application/vnd.ms-excel");
            $type = "Xls";
            $suffix = "xls";
        }

        ob_end_clean();
        header("Content-Disposition:attachment;filename=$fileName.$suffix");
        header("Cache-Control:max-age=0");

        $writer = IOFactory::createWriter($preadsheet, $type);
        $writer->save("php://output");
    }

    /**
     * import 导入
     * 
     * @author wuyinghua
	 * @return void
     */
    public function import(Request $request) {
        $db = new CarbonPriceModel();

        $file[]= $request->file('file1');
        try {
            validate(['file' => 'filesize:51200|fileExt:xls,xlsx'])
                ->check($file);
            $savename =\think\facade\Filesystem::disk('public')->putFile( 'file', $file[0]);
            $fileExtendName = substr(strrchr($savename, '.'), 1);


            if ($fileExtendName == 'xlsx') {
                $objReader = IOFactory::createReader('Xlsx');
            } else {
                $objReader = IOFactory::createReader('Xls');
            }

            $objReader->setReadDataOnly(TRUE);

            $objPHPExcel = $objReader->load('storage/'.$savename);
            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();
            \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn);
            $lines = $highestRow - 1;
            if ($lines <= 0) {
                echo('数据不能为空！');
                exit();
            }

            for ($j = 2; $j <= $highestRow; $j++) {
                $data[$j - 2] = [
                    'city' => $objPHPExcel->getActiveSheet()->getCell("A" . $j)->getValue(),
                    'time' => $objPHPExcel->getActiveSheet()->getCell("B" . $j)->getValue(),
                    'trading_avg_price' => $objPHPExcel->getActiveSheet()->getCell("C" . $j)->getValue(),
                    'turnover' => $objPHPExcel->getActiveSheet()->getCell("D" . $j)->getValue(),
                    'amount' => $objPHPExcel->getActiveSheet()->getCell("E" . $j)->getValue(),
                    'daily_percent' => $objPHPExcel->getActiveSheet()->getCell("F" . $j)->getValue(),
                ];
            }

            $add = $db->addCarbonPrice($data);

            if ($add) {
                return json(['code'=>200, 'message'=>"添加成功"]);
            } else {
                return json(['code'=>404, 'message'=>"添加失败"]);
            }
        } catch (\think\exception\ValidateException $e) {
            return $e->getMessage();
        }
    }
}