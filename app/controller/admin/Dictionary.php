<?php
declare (strict_types = 1);

namespace app\controller\admin;

use app\BaseController;
use app\validate\DictionaryValidate;
use think\exception\ValidateException;
use app\model\admin\DictionaryModel;
use app\model\system\OperationModel;
use think\Request;

class Dictionary extends BaseController {

	//======================================================================
	// PUBLIC FUNCTIONS
	//======================================================================

    /**
     * index 数据字典列表
     * 
     * @author wuyinghua
	 * @return void
     */
    public function index() {
        $page_size = isset($_GET['pageSize']) ? $_GET['pageSize'] : '';
        $page_index = isset($_GET['pageIndex']) ? $_GET['pageIndex'] : '';

        // 过滤条件：单位分类、单位名称
        $filters = [
            'filter_unit_type' => isset($_GET['filterUnitType']) ? $_GET['filterUnitType'] : '',
            'filter_unit_name' => isset($_GET['filterUnitName']) ? $_GET['filterUnitName'] : ''
        ];

        $db = new DictionaryModel();
        $list = $db->getDictionaries($page_size, $page_index, $filters)->toArray();
        $unit_types = $db->getUnitType()->toArray();

        $data['code'] = 200;
        $data['data']['list'] = $list['data'];
        $data['data']['unit_types'] = $unit_types;
        $data['data']['total'] = $list['total'];

        return json($data);
    }

    /**
     * see 数据字典数组
     * 
     * @author wuyinghua
	 * @return void
     */
    public function see() {
        $type = isset($_GET['type']) ? $_GET['type'] : '';

        $db = new DictionaryModel();
        $list = $db->getDictionary($type)->toArray();

        $array = array();
        $dictionaries = array();

        foreach ($list as $key => $value) {
            if (!in_array($value['type_id'], $array)) {
                array_push($array, $value['type_id']);
                $dictionaries[$key]['id'] = $value['type_id'];
                $dictionaries[$key]['unit_type'] = $value['type'];
                $dictionaries[$key]['units'][] = $this->initDatas($list, $value['type_id']);
            }
        }

        $data['code'] = 200;
        $data['data']['list'] = array_values($dictionaries);

        return json($data);
    }

    /**
     * initDatas
     * 
     * @author wuyinghua
     * @param $list
     * @param $type_id
	 * @return $array
     */
    private function initDatas($list, $type_id = '') {
        foreach ($list as $value) {
            if ($type_id == $value['type_id']) {
                $array[] = [
                    'unit_id' => $value['id'],
                    'name'    => $value['name']
                ];
            }
        }

        return $array;
    }

	//======================================================================
	// PROTECTED FUNCTIONS
	//======================================================================

    /**
     * validateForm 验证
     * 
     * @author wuyinghua
	 * @return void
     */
    protected function validateForm() {
        $data = request()->param();

        try {
            validate(DictionaryValidate::class)->check($data);

            return  true;
        } catch (ValidateException $e) {
            
            return $e->getError();
        }
    }
}
