<?php
declare (strict_types = 1);

namespace app\controller\organization;

use app\BaseController;
use app\model\organization\DataManagementModel;
use app\model\organization\CarbonCulateModel;
use app\model\organization\CalculateExampleModel;
use app\model\system\OrganizationModel;
use app\model\admin\UnitModel;
use app\model\system\ApprovalModel;
use app\model\system\OperationModel;
use app\model\data\ParamModel;
use think\facade\Config;
use think\facade\Db;

class DataManagement extends BaseController
{
    /**
     * 数据管理列表
     *
     * @author wuyinghua
     * @return \think\Response
     */
    public function index() {
        $data_redis = $this->request->middleware('data_redis');
        $page_size = isset($_GET['pageSize']) ? $_GET['pageSize'] : ParamModel::pageSize;
        $page_index = isset($_GET['pageIndex']) ? $_GET['pageIndex'] : ParamModel::pageIndex;
        $main_organization_id = isset($data_redis['main_organization_id']) ? $data_redis['main_organization_id'] : '';

        // 过滤条件：组织、状态、核算年
        $filters = [
            'organization_id' => isset($_GET['organization_id']) ? $_GET['organization_id'] : '',
            'state' => isset($_GET['state']) ? $_GET['state'] : '',
            'calculate_year' => isset($_GET['calculate_year']) ? $_GET['calculate_year'] : '',
            'main_organization_id' => $main_organization_id
        ];

        $list = DataManagementModel::getDataManagements($page_size, $page_index, $filters)->toArray();
        $organization_all = OrganizationModel::getAllFiledOrganizations($main_organization_id)->toArray();
        $organization = [];
        foreach ($organization_all as $key => $value) {
            $organization[$key]['id'] = $value['id'];
            $organization[$key]['name'] = $value['name'];
        }
        $state_map = ParamModel::ORGDATA_STATE_MAP;
        $list_new = [];
        foreach ($list['data'] as $k => $v) {
            if (isset($state_map[$v['state']])) {
                $v['state_name'] = $state_map[$v['state']];
            } else {
                $v['state_name'] = '';
            }

            $list_new[$k] = $v;
        }

        $data['code'] = 200;
        $data['data']['list'] = $list_new;
        $data['data']['total'] = $list['total'];
        array_unshift($organization, ['id'=>0,'name'=>'全部']);
        $data['data']['organization_map'] = $organization;
        $data['data']['state_map'] = ParamModel::ORGDATA_STATE_SELECT_MAP;

        return json($data);
    }

    /**
     * dataSee 核算数据
     *
     * @author wuyinghua
     * @return void
     */
    public function dataSee() {
        $id = isset($_GET['id']) ? $_GET['id'] : '';
        $org_data = CarbonCulateModel::getCarbonCulateDetail($id);
        $facility_data = CarbonCulateModel::getCarbonCulateFacility($id)->toArray();
        if ($org_data == null) {
            return json(['code'=>201, 'message'=>"未查询到组织相关信息"]);
        }

        $state_map = ParamModel::ORGDATA_STATE_MAP;
        $org_data['state_name'] = $state_map[$org_data['state']];

        $data['code'] = 200;
        $data['data']['org_data'] = $org_data;
        $data['data']['facility_data'] = $facility_data;

        return json($data);
    }

    /**
     * edit 核算编辑（录入数据）
     * 
     * @author wuyinghua
	 * @return void
     */
    public function edit() {
        $params_payload = $this->req_payload;
        $params_payload_arr = json_decode($params_payload, true);
        $data_redis = $this->request->middleware('data_redis');

        // 修改：录入一个排放源的活动数据 -> 一次录入多个排放源的活动数据
        $data['active_arr'] = $params_payload_arr['active_arr'];
        $data['files'] = implode(',', $params_payload_arr['files']);
        $data['ids'] = $params_payload_arr['ids'];
        $data['data_type'] = $params_payload_arr['data_type']; // 区分设备还是组织
        Db::startTrans();
        try {
            // 修改文件
            if ($data['data_type'] == ParamModel::ORGDETAIL_DATA_ORG) {
                DataManagementModel::editCarbonCulateDetailEmissionFile($data['ids'], $data['files']);
            } else {
                DataManagementModel::editCarbonCulateFacilityEmissionFile($data['ids'], $data['files']);
            }

            foreach ($data['active_arr'] as $key => $value) {
                $data['id'] = $value['id'];
                $emission_data = CarbonCulateModel::getCarbonCulateDetailEmission($data['id']);
                if ($emission_data == null) {
                    return json(['code'=>201, 'message'=>"未查询到排放源" . $emission_data['emission_name'] . "的相关信息"]);
                }

                if (!is_numeric($value['active_value'])) {
                    return json(['code'=>201, 'message'=>"活动数据必须为数字"]);
                }

                $data['ghg_cate'] = $emission_data['ghg_cate'];
                $data['iso_cate'] = $emission_data['iso_cate'];
                $data['ghg_type'] = $emission_data['ghg_type'];
                $data['iso_type'] = $emission_data['iso_type'];
                $model_data = CalculateExampleModel::getCalculateExample($emission_data['calculate_model_id']);
                $data['active_value'] = $value['active_value'];
                $data['en_short_name'] = $model_data['en_short_name'];
                $data['emission_name'] = $emission_data['emission_name'];
                $data['active_value_standard'] = $value['active_value'];
                $data['calculate_model_id'] = $emission_data['calculate_model_id'];
                $data['organization_calculate_id'] = $emission_data['organization_calculate_id'];
                $data['organization_calculate_detail_id'] = $emission_data['organization_calculate_detail_id'];

                // 将排放源的活动数据单位转换为基准单位
                $active_unit = explode(',', $emission_data['active_unit']);
                $unit_data = UnitModel::findUnit($active_unit[1]);
                $data['conversion_ratio'] = $unit_data['conversion_ratio'];
                $data['userid'] = isset($data_redis['userid']) ? $data_redis['userid'] : '';
                $gwp = self::getGwpData($data['id'], $data['en_short_name'], $data['conversion_ratio']);
                $data['gwp'] = $gwp['sum_gwp'];
                // 判断为录入设备排放源活动数据还是录入核算详情排放源活动数据
                if (isset($value['facility_id'])) {
                    $data['facility_id'] = $value['facility_id'];
                    DataManagementModel::editCarbonCulateFacilityEmission($data);
                } else {
                    // 录入核算详情排放源活动数据，需要往碳核算- 排放量计算表（jy_organization_calculate_count_gas）插入数据，用于生成文件的数据
                    DataManagementModel::editCarbonCulateDetailEmission($data);
                }
            }
            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '组织碳核算';
            $data_log['type'] = '功能操作';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $this->request->pathinfo();
            $data_log['log'] = '录入活动数据';
            OperationModel::addOperation($data_log);

            Db::commit();
            return json(['code'=>200, 'message'=>"录入成功"]);
        } catch (\Exception $e) {
            Db::rollback();
            return json(['code'=>201, 'message'=>$e->getMessage().$e->getLine()]);
        }
    }

    /**
     * submit 提交数据
     *
     * @author wuyinghua
     * @return void
     */
    public function submit() {
        if (request()->isPost()) {
            $params_payload = $this->req_payload;
            $params_payload_arr = json_decode($params_payload, true);
            $data_redis = $this->request->middleware('data_redis');
            $data['userid'] = $data_redis['userid'];
            $data['id'] = $params_payload_arr['id'];
            if ($data['id'] == '' || $data['id'] == null || $data['id'] == 0) {
                return json(['code'=>201, 'message'=>"参数id错误"]);
            }

            $culate_detail_data = CarbonCulateModel::getCarbonCulateDetail($data['id']);
            if ($culate_detail_data == NULL || ($culate_detail_data['state'] != ParamModel::ORGDETAIL_STATE_SUBMIT && $culate_detail_data['state'] != ParamModel::ORGDETAIL_STATE_FAILED)) {
                return json(['code'=>201, 'message'=>"参数id错误"]);
            }

            // 提交数据之前需要判断 当前组织核算流程是 母公司审核、分公司审核、工厂审核 获取state
            $approval_data = ApprovalModel::getApprovals($data_redis['main_organization_id']);
            if ($approval_data == NULL) {
                return json(['code'=>201, 'message'=>"当前组织未设置审批设置，请联系管理员"]);
            }

            foreach ($approval_data as $key => $value) {
                if ($value['name'] == '组织碳核算数据上报') {
                    $factory = $value['factory'];
                    $branch_company = $value['branch_company'];
                    $parent_company = $value['parent_company'];
                }
            }

            // 判断审批设置中为母公司审核、分公司审核、工厂审核，优先以工厂审核，同时上一级也可以审批
            if ($factory == ParamModel::STATE_YES) {
                $data['state'] = ParamModel::ORGDETAIL_STATE_REVIEW_FACTORY;
            } elseif ($branch_company == ParamModel::STATE_YES) {
                $data['state'] = ParamModel::ORGDETAIL_STATE_REVIEW_BRANCH;
            } elseif ($parent_company == ParamModel::STATE_YES) {
                $data['state'] = ParamModel::ORGDETAIL_STATE_REVIEW_MAIN;
            }

            // 判断当前组织核算详情下面是否有空emissions （从jy_organization_calculate_detail_emission和jy_organization_calculate_detail_facility）
            $emission_data = CarbonCulateModel::getCarbonCulateDetailOrgEmission($data['id']);
            foreach ($emission_data as $key => $value) {
                if ($emission_data != NULL && $value['emissions'] == NULL) {
                    return json(['code'=>201, 'message'=>"请填写活动数据"]);
                }
            }
            $facility_data = CarbonCulateModel::getCarbonCulateFacility($data['id']);
            foreach ($facility_data as $key => $value) {
                if ($facility_data != NULL && $value['emissions'] == NULL) {
                    return json(['code'=>201, 'message'=>"请填写活动数据"]);
                }
            }

            $update = DataManagementModel::updateState($data);
            if ($update) {
                // 添加操作日志
                $data_log['main_organization_id'] = $data_redis['main_organization_id'];
                $data_log['user_id'] = $data_redis['userid'];
                $data_log['module'] = '组织碳核算';
                $data_log['type'] = '功能操作';
                $data_log['time'] = date('Y-m-d H:i:s');
                $data_log['log'] = '提交' . $culate_detail_data['calculate_name'] . $culate_detail_data['calculate_month'] . '月核算数据';
                $data_log['url'] = $this->request->pathinfo();
                OperationModel::addOperation($data_log);
                return json(['code'=>200, 'message'=>"提交成功"]);
            } else {
                return json(['code'=>404, 'message'=>"提交失败"]);
            }

        } else {
            return json(['code'=>404, 'message'=>"操作失败"]);
        }
    }

    /**
     * getGwpData 获取排放源气体GWP乘上因子数值的合计值
     * 
     * @author wuyinghua
     * @param $id
     * @param $en_short_name gwp类型英文简写（gwp_6、gwp_5、gwp_4）
	 * @return void
     */
    public static function getGwpData($id, $en_short_name, $conversion_ratio) {
        // gas 部分分为 二氧化碳当量 和 七种气体 两部分 二氧化碳当量不要取GWP值 ，其余的需要去乘上gwp值
        // 根据 emi_id 从 jy_organization_calculate_detail_emission_gas 中找到排放气体 array
        // 遍历array 根据 gas 和 gas_type 以及 calculate_model_id 到 jy_gases 中找到gwp值
        // 同一条排放源的gas中分子一定是相同的，分母类型一定相同，需要做单位换算
        // 根据分母中的分母单位 查找jy_unit表进行换算，返回一致的单位
        // $sum_gwp = 0;
        $molecule = 'tCO2e';
        $denominator = '';
        $gwp_values = [];
        $gas_data = CarbonCulateModel::getGasById($id)->toArray();
        //活动数据类别
        $molecule_arr = Config::get('emission.molecule');
        foreach ($gas_data as $key => $value) {
            $gwp_value = 1;
            $factor_value = $value['factor_value'];
            // 获取分子单位（基准单位为tCO2e）、分母单位，并转化为基准单位
            foreach ($molecule_arr as $k => $v) {
                if ($value['molecule'] == $v['id']) {

                    $molecule_conversion_ratio = $v['conversion_ratio'];
                }
            }
            
            // 分母转换为基准单位
            $denominator_arr = explode(',', $value['denominator']);
            $unit_type_data = UnitModel::getUnitTypeById($denominator_arr[0]);
            $denominator = $unit_type_data['base_unit'];
            $unit_data = UnitModel::findUnit($denominator_arr[1]);
            $denominator_conversion_ratio = $unit_data['conversion_ratio'];

            // 1. 其余气体数据（现在七种）2. 二氧化碳当量数据(gwp_value = 1)
            if ($value['gas'] != '二氧化碳当量（CO₂e）') {
                $gase_data = DataManagementModel::getGase($value['gas_type'], $value['gas']);
                if ($gase_data != null) {
                    $gwp_value = $gase_data[$en_short_name];
                }
            }

            $gwp_values[$key] = $gwp_value * $factor_value * $molecule_conversion_ratio / $denominator_conversion_ratio * $conversion_ratio;
            $gwp_value_datas[$key]['gwp_value'] = $gwp_value;
            $gwp_value_datas[$key]['factor_value'] = $factor_value;
            $gwp_value_datas[$key]['molecule_conversion_ratio'] = $molecule_conversion_ratio;
            $gwp_value_datas[$key]['denominator_conversion_ratio'] = $denominator_conversion_ratio;
            $gwp_value_datas[$key]['conversion_ratio'] = $conversion_ratio;
        }
        $data['gas_data'] = $gas_data;
        $data['molecule'] = $molecule;
        $data['denominator'] = $denominator;
        $data['gwp_values'] = $gwp_values;
        $data['gwp_value_datas'] = $gwp_value_datas;
        $data['sum_gwp'] = array_sum($gwp_values);

        return $data;
    }

}
