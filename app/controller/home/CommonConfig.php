<?php
declare (strict_types = 1);

namespace app\controller\home;

use app\BaseController;
use app\model\home\CommonConfigModel;
use think\Request;

class CommonConfig extends BaseController {

	//======================================================================
	// PUBLIC FUNCTIONS
	//======================================================================

    /**
     * 菜单模块列表
     *
     * @author wuyinghua
     * @param $request
     * @return void
     */
    public function index(Request $request) {

        $data_redis = $request->middleware('data_redis');
        $user_id = $data_redis['userid'];

        $db = new CommonConfigModel();
        $list_arr = $db->getUserMenus($user_id)->toArray();

        $list = array();
        foreach ($list_arr as $value) {
            $list[] = $value['menu_id'];
        }

        $data['code'] = 200;
        $data['data']['list'] = $list;

        return json($data);
    }

    /**
     * 用户常用功能配置
     *
     * @author wuyinghua
     * @param $request
     * @return void
     */
    public function deal(Request $request) {
        $data_redis = $request->middleware('data_redis');
        $data['user_id'] = $data_redis['userid'];
        $data['menu_id'] = $_POST['menu_id'];
        $state = $_POST['state'];

        $db = new CommonConfigModel();
        $user_menu = $db->getUserMenus($data['user_id'])->toArray();
        $menu_ids = array();
        foreach ($user_menu as $value) {
            $menu_ids[] = $value['menu_id'];
        }

        // 用户常用配置启用就添加，禁用就删除
        if (!in_array($data['menu_id'], $menu_ids) && $state == 1) {
            // 用户常用配置最多设置10个
            if (count($user_menu) < 14) {
                $deal = $db->addUserMenus($data);
            } else {
                return json(['code'=>201, 'message'=>"配置超出数量"]);
            }
        } else {
            $deal = $db->delUserMenus($data);
        }

        if ($deal) {
            return json(['code'=>200, 'message'=>"配置成功"]);
        } else {
            return json(['code'=>404, 'message'=>"配置失败"]);
        }
    }
}
