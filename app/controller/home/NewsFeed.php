<?php
declare (strict_types = 1);

namespace app\controller\home;

use app\BaseController;
use app\model\admin\FileModel;
use app\model\home\NewsFeedModel;

class NewsFeed extends BaseController {
	//======================================================================
	// PUBLIC FUNCTIONS
	//======================================================================

    /**
     * 资讯列表
     *
     * @author wuyinghua
     * @return void
     */
    public function index() {
        $page_size = isset($_GET['pageSize']) ? $_GET['pageSize'] : '';
        $page_index = isset($_GET['pageIndex']) ? $_GET['pageIndex'] : '';

        // 过滤条件：资讯标题
        $filter = isset($_GET['filterTitle']) ? $_GET['filterTitle'] : '';

        $db = new NewsFeedModel();
        $list = $db->getNews($page_size, $page_index, $filter)->toArray();

        $data['code'] = 200;
        $data['data']['list'] = $list['data'];
        $data['data']['total'] = $list['total'];

        return json($data);
    }

    /**
     * see 查看资讯详情
     * 
     * @author wuyinghua
	 * @return void
     */
    public function see() {
        $id = $_GET['id'];

        $db = new NewsFeedModel();
        $list = $db->seeNew($id);
        $file_url = $list['file_url'];
        $list['files'] = [];
        if ($file_url != '') {
            $file_url_arr = explode(',', $file_url);
            foreach ($file_url_arr as $k => $v) {
                $file_data = FileModel::getInfoById($v);
                $list['files'][] = $file_data;
            }
        }
        $data['code'] = 200;
        $data['data'] = $list;

        return json($data);
    }
}
