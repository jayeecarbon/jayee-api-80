<?php
declare (strict_types = 1);

namespace app\controller\home;

use app\BaseController;
use app\model\system\FileModel;
use app\model\home\NoticesModel;
use app\validate\NoticeValidate;
use think\exception\ValidateException;
use think\Request;

class Notices extends BaseController {
	//======================================================================
	// PUBLIC FUNCTIONS
	//======================================================================

    /**
     * 通知列表
     *
     * @author wuyinghua
     * @return void
     */
    public function index() {
        $page_size = isset($_GET['pageSize']) ? $_GET['pageSize'] : NoticesModel::pageSize;
        $page_index = isset($_GET['pageIndex']) ? $_GET['pageIndex'] : NoticesModel::pageIndex;
        $db = new NoticesModel();
        $list = $db->getNotices($page_size, $page_index)->toArray();


        $data['code'] = 200;
        $data['data']['list'] = $list['data'];
        $data['data']['total'] = $list['total'];


        return json($data);
    }

    /**
     * see 查看通知详情
     * 
     * @author wuyinghua
	 * @return void
     */
    public function see() {
        $id = $_GET['id'];

        $db = new NoticesModel();
        $list = $db->seeNotice($id);
        $file_url = $list['file_url'];
        $list['files'] = [];
        if ($file_url != '') {
            $file_url_arr = explode(',', $file_url);
            foreach ($file_url_arr as $k => $v) {
                $file_data = FileModel::getInfoById($v);
                if ($file_data != null) {
                    $list['files'][] = $file_data;
                }
            }
        }
        $data['code'] = 200;
        $data['data'] = $list;

        return json($data);
    }

    /**
     * @notes 消息新增/发布
     * 
     * @author wuyinghua
     * @param Request $request
     * @return \think\response\Json
     */
    public function add(Request $request) {
        if (request()->isPost() && $this->validateForm() === true) {
            $params_payload = $this->req_payload;
            $params_payload_arr = json_decode($params_payload, true);
            $data_redis = $request->middleware('data_redis');
            $data['author'] = 'admin';//todo 因为这个没有默认值 从token中取 这里临时给个默认值
            if ($data_redis != null) {
                $data['author'] = $data_redis['user'];
            }
            $data['title'] = $params_payload_arr['title'];
            $data['type'] = $params_payload_arr['type'];
            $data['content'] = $params_payload_arr['content'];
            $data['create_time'] = date('Y-m-d H:i:s');
            $data['modify_time'] = date('Y-m-d H:i:s');
            if (isset($params_payload_arr['file_url'])) {
                $data['file_url'] = $params_payload_arr['file_url'];
            }

            $add = NoticesModel::add($data);

            if ($add) {
                return json(['code'=>200, 'message'=>"添加成功"]);
            } else {
                return json(['code'=>201, 'message'=>"添加失败"]);
            }
        } else {
            return json(['code'=>404, 'message'=>$this->validateForm()]);
        }
    }

    /**
     * @notes 消息编辑
     * 
     * @author wuyinghua
     * @return \think\response\Json
     */
    public function edit(Request $request) {
        if (request()->isPost() && $this->validateForm() === true) {
            $params_payload = $this->req_payload;
            $params_payload_arr = json_decode($params_payload, true);

            $data['id'] = $params_payload_arr['id'];
            $data['title'] = $params_payload_arr['title'];
            $data['content'] = $params_payload_arr['content'];
            $data['modify_time'] = date('Y-m-d H:i:s');
            if (isset($params_payload_arr['file_url'])) {
                $data['file_url'] = $params_payload_arr['file_url'];
            }

            NoticesModel::edit($data);
            return json(['code'=>200, 'message'=>"编辑成功"]);
        } else {
            return json(['code'=>404, 'message'=>$this->validateForm()]);
        }
    }

    /**
     * @notes del 删除
     * 
     * @author wuyinghua
     * @param Request $request
     * @return \think\response\Json
     */
    public function del(Request $request) {
        $params_payload = $this->req_payload;
        $params_payload_arr = json_decode($params_payload, true);
        $data['id'] = isset($params_payload_arr['id']) ? $params_payload_arr['id'] : '';
        if ($data['id'] == '' || $data['id'] == null || $data['id'] == 0) {
            return json(['code'=>201, 'message'=>"参数id错误"]);
        }
        $data['is_del'] = NoticesModel::IS_DEL_DEL;

        NoticesModel::del($data);

        return json(['code'=>200, 'message'=>"删除成功"]);
    }

    /**
     * @notes 获取通知列表信息
     * 
     * @author wuyinghua
     * @return \think\response\Json
     */
    public function list() {
        $title = isset($_GET['title']) ? $_GET['title'] : '';
        $type = isset($_GET['type']) ? $_GET['type'] : '';
        $page_size = isset($_GET['pageSize']) ? $_GET['pageSize'] : NoticesModel::pageSize;
        $page_index = isset($_GET['pageIndex']) ? $_GET['pageIndex'] : NoticesModel::pageIndex;

        $list = NoticesModel::getList($page_size, $page_index, ['title'=>$title, 'type'=>$type])->toArray();

        $type_select_map = NoticesModel::TYPE_SELECT_MAP;
        $type_map = NoticesModel::TYPE_MAP;

        $list_new = [];
        foreach ($list['data'] as $k => $v) {
            if (isset($type_map[$v['type']])) {
                $v['type_name'] = $type_map[$v['type']];
            } else {
                $v['type_name'] = '';
            }
            $list_new[$k] = $v;
        }
        unset($list['data']);
        $data['code'] = 200;
        $data['data']['list'] = $list_new;
        $data['data']['type'] = $type_select_map;
        $data['data']['total'] = $list['total'];
        return json($data);
    }

    /**
     * @notes 详情接口
     * 
     * @author wuyinghua
     * @return \think\response\Json
     */
    public function info() {
        $id = isset($_GET['id']) ? $_GET['id'] : '';
        $info = NoticesModel::getDataById($id);
        $file_url = $info['file_url'];
        $info['files'] = [];
        if ($file_url != '') {
            $file_url_arr = explode(',', $file_url);
            foreach ($file_url_arr as $k => $v) {
                $file_data = FileModel::getInfoById($v);
                $info['files'][] = $file_data;
            }
        }
        if ($info == null) {
            return json(['code'=>201, 'message'=>"未查询到客户供应商相关信息"]);
        }
        $data['code'] = 200;
        $data['data'] = $info;

        return json($data);
    }

    /**
     * @notes 上传图片
     * 
     * @author wuyinghua
     * @return \think\response\Json
     */
    public function upload() {
        $data_redis = $this->request->middleware('data_redis');
        $file = $this->request->file();
        $ip = 'http://www.jayeecloud.com:81';
        try {
            // 验证文件大小格式
            validate(['file' => 'fileSize:52428800|fileExt:png,jpg'])
                ->check($file);

            $savename = \think\facade\Filesystem::disk('admin_cooperate_uploads')->putFile('admin_cooperate_uploads', $file['file']);
            $url = str_replace('\\', '/', $savename);

            $data['virtual_id'] = isset($_POST['virtual_id']) ? $_POST['virtual_id'] : NULL;
            $data['file_path'] = str_replace('admin_cooperate_uploads', '', $url);
            $data['source_name'] = $_FILES['file']['name'];
            $data['file_size'] = $_FILES['file']['size'];
            $data['main_organization_id'] = $data_redis['main_organization_id'];
            $data['create_by'] = $data_redis['userid'];
            $data['modify_by'] = $data_redis['userid'];
            $data['create_time'] = date('Y-m-d H:i:s');
            $data['modify_time'] = date('Y-m-d H:i:s');
            $data['module'] = '消息管理';
            $data['id'] = guid();
            $add = FileModel::addFile($data);

            if ($add) {
                $datasmg['code'] = 200;
                $datasmg['id'] = $data['id'];
                $datasmg['name'] = $data['source_name'];
                $datasmg['file_path'] = $ip . $data['file_path'];
                $datasmg['msg'] = "上传成功"; // 前端要求， mseeage -> msg，不提示弹框
            } else {
                $datasmg['code'] = 404; // 前端要求，上传失败 404 -> 200，不提示弹框
                $datasmg['name'] = $data['source_name']; // 前端要求，上传失败返回文件名
                $datasmg['msg'] = "上传失败";
            }

            return json($datasmg);
        } catch (ValidateException $e) {
            $datasmg['code'] = 200; // 前端要求，验证失败 404 -> 200，不提示弹框
            $datasmg['name'] = $_FILES['file']['name']; // 前端要求，验证失败返回文件名
            $datasmg['msg'] = $e->getError();

            return json($datasmg);
        }
    }

    /**
     * validateForm 验证
     *
     * @author wuyinghua
     * @return void
     */
    protected function validateForm() {
        $data = request()->param();

        try {
            validate(NoticeValidate::class)->check($data);

            return  true;
        } catch (ValidateException $e) {

            return $e->getError();
        }
    }
}
