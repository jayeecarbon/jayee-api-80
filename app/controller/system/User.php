<?php
namespace app\controller\system;

use app\BaseController;
use app\validate\UserValidate;
use app\validate\UserRegisterValidate;
use think\exception\ValidateException;
use app\model\system\UserModel;
use app\model\home\CommonConfigModel;
use app\model\system\OrganizationModel;
use app\model\system\OperationModel;
use app\model\system\ApprovalModel;

/**
 * User
 */
class User extends BaseController {

    //======================================================================
    // PUBLIC FUNCTIONS
    //======================================================================

    /**
     * index 用户列表
     * 
     * @author wuyinghua
     * @return void
     */
    public function index() {
        $data_redis = $this->request->middleware('data_redis');
        $page_size = isset($_GET['pageSize']) ? $_GET['pageSize'] : '';
        $page_index = isset($_GET['pageIndex']) ? $_GET['pageIndex'] : '';
        $main_organization_id = isset($data_redis['main_organization_id']) ? $data_redis['main_organization_id'] : '';

        // 过滤条件：用户姓名、手机号、状态、组织
        $filters = [
            'filter_user_name' => isset($_GET['filterUserName']) ? $_GET['filterUserName'] : '',
            'filter_telephtone' => isset($_GET['filterTelephone']) ? $_GET['filterTelephone'] : '',
            'filter_user_state' => isset($_GET['filterState']) ? $_GET['filterState'] : '',
            'main_organization_id' => $main_organization_id
        ];

        $list = UserModel::getUsers($page_size, $page_index, $filters)->toArray();

        // 用户角色
        foreach ($list['data'] as &$value) {
            $value['roles'] = UserModel::getRoleArr($value['role_id']);
        }

        $role_array = UserModel::getRoles($main_organization_id)->toArray();

        $data['code'] = 200;
        $data['data']['list'] = $list['data'];
        $data['data']['roles'] = $role_array;
        $data['data']['total'] = $list['total'];

        return json($data);
    }

    /**
     * register 用户注册
     * 
     * @author wuyinghua
     * @return void
     */
    public function register() {

        if ($this->request->isPost() && $this->validateForm() === true) {
            $params_payload = $this->req_payload;
            $params_payload_arr = json_decode($params_payload, true);
            $data['username'] = $params_payload_arr['user_phone'];
            $data['password'] = md5($params_payload_arr['password'] . 'jieyitan');
            $data['telephone'] = $params_payload_arr['user_phone']; // 注册用户名为手机号
            $data['state'] = UserModel::STATE_TYPE_YES;
            $data['create_time'] = date('Y-m-d H:i:s');

            $list_tel = UserModel::getUserByTelephone($data['telephone'])->toArray();

            if (count($list_tel) > 0) {
                return json(['code'=>201, 'message'=>"手机号已注册"]);
            }

            $list_name = UserModel::getUserByName($data['username'])->toArray();

            if (count($list_name) > 0) {
                return json(['code'=>201, 'message'=>"用户名已注册"]);
            }

            // 用户注册-新增组织
            $data_organization['name'] = $params_payload_arr['organization_name'];
            $data_organization['code'] = $params_payload_arr['organization_code'];
            $data_organization['pid'] = OrganizationModel::TYPE_PARENT_COMPANY;
            $data_organization['type'] = '总公司';
            $data_organization['create_time'] = date('Y-m-d H:i:s');

            $list_code = OrganizationModel::getOrganizationByCode($data_organization['code'])->toArray();

            if (count($list_code) > 0) {
                return json(['code'=>201, 'message'=>"您所在的组织已经完成注册，请联系您所在组织的管理员，如果被虚假注册请联系客服 021-34202775"]);
            }

            $add_id = OrganizationModel::addOrganization($data_organization);
            $update_data['id'] = $add_id;
            $update_data['main_organization_id'] = $add_id;
            OrganizationModel::editOrganization($update_data);

            $data['main_organization_id'] = $add_id;
            $data['role_id'] = UserModel::ROLE_TYPE_ADMIN;
            $user_id = UserModel::addUser($data);

            // todo 暂定新增用户默认配置常用功能【用户管理：5，角色管理：6，产品管理：9，产品核算：10，核算报告：11，数据审批：12，碳标签管理：13】 后期定下来再删掉
            $menu_ids = [5,6,9,10,11,12,13];
            foreach ($menu_ids as $value) {
                $menu_data['user_id'] = $user_id;
                $menu_data['menu_id'] = $value;
                CommonConfigModel::addUserMenus($menu_data);
            }

            // 添加组织的审批设置
            ApprovalModel::addApproval($data['main_organization_id'], $user_id);

            // 添加操作日志
            $data_log['main_organization_id'] = $update_data['main_organization_id'];
            $data_log['user_id'] = $user_id;
            $data_log['module'] = '系统设置';
            $data_log['type'] = '用户注册';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $this->request->pathinfo();
            $data_log['log'] = '注册用户：' . $data['username'];

            OperationModel::addOperation($data_log);

            if ($user_id) {
                return json(['code'=>200, 'message'=>"注册成功"]);
            } else {
                return json(['code'=>201, 'message'=>"注册失败"]);
            }


        } else {
            return json(['code'=>404, 'message'=>$this->validateForm()]);
        }
    }

    /**
     * add 用户添加
     * 
     * @author wuyinghua
     * @return void
     */
    public function add() {

        if ($this->request->isPost() && $this->validateForm() === true) {
            $data_redis = $this->request->middleware('data_redis');
            $data['username'] = $_POST['username'];
            $data['telephone'] = $_POST['telephone'];

            $list_tel = UserModel::getUserByTelephone($data['telephone'])->toArray();

            if (count($list_tel) > 0) {
                return json(['code'=>404, 'message'=>"手机号已注册"]);
            }

            $list_name = UserModel::getUserByName($data['username'])->toArray();

            if (count($list_name) > 0) {
                return json(['code'=>404, 'message'=>"用户名已注册"]);
            }

            $data['password'] = md5($_POST['password'] . 'jieyitan');
            $roles = explode(',', $_POST['role_id']);
            $data['role_id'] = implode(',', array_filter(array_unique($roles)));
            $data['create_by'] = $data_redis['userid'];
            $data['main_organization_id'] = $data_redis['main_organization_id']; // 新增用户的母公司默认为当前母公司
            $data['state'] = UserModel::STATE_TYPE_YES;
            $data['create_time'] = date('Y-m-d H:i:s');

            $add_id = UserModel::addUser($data);

            // todo 暂定新增用户默认配置常用功能【用户管理：5，角色管理：6，产品管理：9，产品核算：10，核算报告：11，数据审批：12，碳标签管理：13】 后期定下来再删掉
            $menu_ids = [5,6,9,10,11,12,13];
            foreach ($menu_ids as $value) {
                $menu_data['user_id'] = $add_id;
                $menu_data['menu_id'] = $value;
                CommonConfigModel::addUserMenus($menu_data);
            }

            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '系统设置';
            $data_log['type'] = '功能操作';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $this->request->pathinfo();
            $data_log['log'] = '添加用户：' . $data['username'];

            OperationModel::addOperation($data_log);

            if ($add_id) {
                return json(['code'=>200, 'message'=>"添加成功"]);
            } else {
                return json(['code'=>404, 'message'=>"添加失败"]);
            }

        } else {
            return json(['code'=>404, 'message'=>$this->validateForm()]);
        }
    }

    /**
     * state 更新用户状态
     * 
     * @author wuyinghua
     * @param $request
     * @return void
     */
    public function state() {

        if ($this->request->isPost()) {
            $data_redis = $this->request->middleware('data_redis');

            $data['id'] = $_POST['id'];
            $data['modify_by'] = $data_redis['userid'];
            $data['username'] = $_POST['username'];
            $data['state'] = $_POST['state'];
            $data['modify_time'] = date('Y-m-d H:i:s');

            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '系统设置';
            $data_log['type'] = '功能操作';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $this->request->pathinfo();
            $data_log['log'] = '更新用户' . $data['username'] . '的状态';

            OperationModel::addOperation($data_log);
            $edit = UserModel::updateUser($data);

            if ($edit) {
                return json(['code'=>200, 'message'=>"修改成功"]);
            } else {
                return json(['code'=>404, 'message'=>"修改失败"]);
            }

        } else {
            return json(['code'=>404, 'message'=>"修改失败"]);
        }
    }

    /**
     * see 查看用户详情
     * 
     * @author wuyinghua
     * @return void
     */
    public function see() {
        $id = $_GET['id'];
        $list = UserModel::seeUser($id);
        $list['roles'] = UserModel::getRoleArr($list['role_id']);

        // 获取用户的组织
        $user_organization = explode(',', $list['organization_id']);
        $list['organization_id'] = [];
        $list['organization_id'] = $user_organization;

        // 显示用户所在集团下的所有组织
        $organizations_list = OrganizationModel::getAllOrganizations()->toArray();
        $trees = Organization::getTree($organizations_list, 0, 3);

        foreach ($trees as $value) {
            if ($value['id'] == $list['main_organization_id']) {
                $list['organizations'] = array($value);
            }
        }

        $data['code'] = 200;
        $data['data'] = $list;

        return json($data);
    }

    /**
     * role 更新用户角色
     * 
     * @author wuyinghua
     * @return void
     */
    public function role() {

        if ($this->request->isPost()) {
            $data_redis = $this->request->middleware('data_redis');

            $data['id'] = $_POST['id'];
            $data['role_id'] = $_POST['role_id'];
            $data['modify_by'] = $data_redis['userid'];
            $data['username'] = $_POST['username'];
            $data['modify_time'] = date('Y-m-d H:i:s');

            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '系统设置';
            $data_log['type'] = '功能操作';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $this->request->pathinfo();
            $data_log['log'] = '更新用户' . $data['username'] . '的角色';

            OperationModel::addOperation($data_log);
            $edit = UserModel::updateUser($data);

            if ($edit) {
                return json(['code'=>200, 'message'=>"配置成功"]);
            } else {
                return json(['code'=>404, 'message'=>"配置失败"]);
            }

        } else {
            return json(['code'=>404, 'message'=>"配置失败"]);
        }
    }

    /**
     * group 更新用户组织
     * 
     * @author wuyinghua
     * @return void
     */
    public function group() {

        if ($this->request->isPost()) {
            $data_redis = $this->request->middleware('data_redis');

            $data['id'] = $_POST['id'];
            $data['organization_id'] = $_POST['organization_id'];
            $data['modify_by'] = $data_redis['userid'];
            $data['modify_time'] = date('Y-m-d H:i:s');
            $user_info = UserModel::seeUser($data['id']);

            $organization_list = explode(',', $_POST['organization_id']);
            // 按升序排列阶段
            sort($organization_list);
            $data['organization_id'] = implode(',', $organization_list);

            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '系统设置';
            $data_log['type'] = '功能操作';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $this->request->pathinfo();
            $data_log['log'] = '更新用户' . $user_info['username'] . '的组织';

            OperationModel::addOperation($data_log);
            $edit = UserModel::updateUser($data);

            if ($edit) {
                return json(['code'=>200, 'message'=>"配置成功"]);
            } else {
                return json(['code'=>404, 'message'=>"配置成功"]);
            }

        } else {
            return json(['code'=>404, 'message'=>"配置成功"]);
        }
    }

    /**
     * checkInTree 检查是否在树型结构中，并添加状态
     * 
     * @author wuyinghua
     * @param $data
     * @param $pid
     * @param $level
	 * @return $data
     */
    public static function checkInTree($data, $pid, $level, $array) {
        $tree = [];
        foreach($data as $k => $v) {
            $level_value = isset($v['level']) ? $v['level'] : '';
            if ($level_value == $level) {
                break;
            }
            if($v['pid'] == $pid) {
                $v['children'] = self::checkInTree($data, $v['id'], $level, $array);
                if(in_array($v['id'], $array)) {
                    $v['is_on'] = 1;
                } else {
                    $v['is_on'] = 2;
                }
                $tree[] = $v;
                unset($data[$k]);
            }
        }

        return $tree;
    }

    /**
     * 加密解密
     *
     * @author wuyinghua
     * @param  string  $string
     * @param  string  $operation
     * @param  string  $key
     */
    public static function encrypt($string, $operation, $key='') {
        $key = md5($key);
        $key_length = strlen($key);
          $string = $operation == 'D' ? base64_decode($string) : substr(md5($string.$key), 0, 8) . $string;
        $string_length = strlen($string);
        $rndkey = $box = array();
        $result = '';
        for ($i = 0; $i <= 255; $i++) {
               $rndkey[$i] = ord($key[$i%$key_length]);
            $box[$i] = $i;
        }

        for ($j = $i = 0; $i < 256; $i++) {
            $j = ($j + $box[$i] + $rndkey[$i]) % 256;
            $tmp = $box[$i];
            $box[$i] = $box[$j];
            $box[$j] = $tmp;
        }

        for ($a = $j = $i = 0; $i < $string_length; $i++) {
            $a = ($a + 1)%256;
            $j = ($j + $box[$a])%256;
            $tmp = $box[$a];
            $box[$a] = $box[$j];
            $box[$j] = $tmp;
            $result .= chr(ord($string[$i])^($box[($box[$a] + $box[$j])%256]));
        }

        if ($operation == 'D') {
            if(substr($result, 0, 8) == substr(md5(substr($result, 8).$key), 0, 8)) {
                return substr($result, 8);
            } else {
                return'';
            }
        } else {
            return str_replace('=', '', base64_encode($result));
        }
    }

    //======================================================================
    // PROTECTED FUNCTIONS
    //======================================================================

    /**
     * validateForm 验证
     * 
     * @author wuyinghua
     * @return void
     */
    protected function validateForm() {
        $data = request()->param();

        try {
            if (isset($data['user_phone'])) {
                validate(UserRegisterValidate::class)->check($data);
            } else {
                validate(UserValidate::class)->check($data);
            }
            return  true;
        } catch (ValidateException $e) {
            
            return $e->getError();
        }
    }
}