<?php
namespace app\controller\system;

use app\BaseController;
use app\controller\system\File;
use app\model\system\FileModel;
use app\model\system\OperationModel;

/**
 * Operation
 */
class Operation extends BaseController {

	//======================================================================
	// PUBLIC FUNCTIONS
	//======================================================================

    /**
     * index 操作日志列表
     * 
     * @author wuyinghua
	 * @return void
     */
    public function index() {
        $data_redis = $this->request->middleware('data_redis');
        $page_size = isset($_GET['pageSize']) ? $_GET['pageSize'] : '';
        $page_index = isset($_GET['pageIndex']) ? $_GET['pageIndex'] : '';
        $main_organization_id = isset($data_redis['main_organization_id']) ? $data_redis['main_organization_id'] : '';

        $module_list = FileModel::getModules()->toArray();

        $modules = [];
        foreach ($module_list as $key => $value) {
            $modules[$key] = $value['title'];
        }

        // 过滤条件：操作人、操作模块、操作开始时间、操作结束时间
        $filters = [
            'filter_user_name' => isset($_GET['filterUserName']) ? $_GET['filterUserName'] : '',
            'filter_module' => isset($_GET['filterModule']) ? $_GET['filterModule'] : '',
            'filter_time_start' => isset($_GET['filteTimeStart']) ? $_GET['filteTimeStart'] : '',
            'filter_time_end' => isset($_GET['filteTimeEnd']) ? $_GET['filteTimeEnd'] : '',
            'main_organization_id' => $main_organization_id,
            'modules' => $modules
        ];

        $list = OperationModel::getOperations($page_size, $page_index, $filters)->toArray();

        $data['code'] = 200;
        $data['data']['list'] = $list['data'];
        $data['data']['modules'] = $module_list;
        $data['data']['total'] = $list['total'];

        return json($data);
    }

    /**
     * export 导出操作日志
     * 
     * @author wuyinghua
	 * @return void
     */
    public function export() {
        // 读取需要导出的数据
        $list = OperationModel::getAllOperation();

        // excel 的数据
        $xlsData = [];
        foreach ($list as $k => $v) {
            $xlsData[] = [
                // 'ID' => $v['id'],
                '操作人姓名' => $v['username'],
                '操作人工号' => $v['user_id'],
                '操作类型' => $v['type'],
                '操作模块' => $v['module'],
                '操作时间' => $v['time'],
                '操作日志' => $v['log'],
            ];
        }
        if (count($xlsData) == 0) {
            return '暂无日志数据';
        }

        // 编辑表格的title，返回的数据格式是：$xlsCell = ['操作人姓名', '操作人工号', '操作类型', ...]
        $length = count($xlsData);
        foreach ($xlsData[$length - 1] as $k => $v) {
            $xlsCell[] = $k;
        }

        // 设置对应的excel坐标
        $setWidth = ['A', 'B', 'C', 'D', 'E', 'F'];

        // 调用自定义方法
        File::exportExcel('操作日志表', $xlsCell, $xlsData, $setWidth);
    }
}