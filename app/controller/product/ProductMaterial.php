<?php
namespace app\controller\product;

use app\BaseController;
use app\validate\ProductMaterialValidate;
use think\exception\ValidateException;
use app\model\product\ProductMaterialModel;
use app\model\system\OperationModel;
use think\Request;

/**
 * Product
 */
class ProductMaterial extends BaseController {

	//======================================================================
	// PUBLIC FUNCTIONS
	//======================================================================

    /**
     * index 原材料列表
     * 
     * @author wuyinghua
	 * @return void
     */
    public function index(Request $request) {
        $data_redis = $request->middleware('data_redis');
        $main_id = $data_redis['main_organization_id'];
        $product_id = isset($_GET['product_id']) ? $_GET['product_id'] : '';
        $virtual_id = isset($_GET['virtual_id']) ? $_GET['virtual_id'] : NULL;
        $page_size = isset($_GET['pageSize']) ? $_GET['pageSize'] : '';
        $page_index = isset($_GET['pageIndex']) ? $_GET['pageIndex'] : '';

        $list = ProductMaterialModel::getMaterials($product_id, $virtual_id, $page_size, $page_index)->toArray();

        // 获取供应商
        $data_suppliers = ProductMaterialModel::getSuppliers($main_id)->toArray();

        $data['code'] = 200;
        $data['data']['list'] = $list['data'];
        $data['data']['data_suppliers'] = $data_suppliers;
        $data['data']['total'] = $list['total'];

        return json($data);
    }

    /**
     * add 原材料添加
     * 
     * @author wuyinghua
     * @param $request
	 * @return void
     */
    public function add(Request $request) {

        if (request()->isPost() && $this->validateForm() === true) {
            $data_redis = $request->middleware('data_redis');

            // 新增产品时的添加原材料不需要传产品id，编辑产品时添加原材料需要传产品id
            if (isset($_POST['product_id'])) {
                $data['product_id'] = $_POST['product_id'];
            }

            $data['virtual_id'] = empty($_POST['virtual_id']) ? NULL : $_POST['virtual_id']; // 虚拟产品id
            $data['main_organization_id'] = $data_redis['main_organization_id'];
            $data['product_material_name'] = $_POST['product_material_name'];
            $data['product_material_no'] = empty($_POST['product_material_no']) ? NULL : $_POST['product_material_no'];
            $data['product_material_spec'] = empty($_POST['product_material_spec']) ? NULL : $_POST['product_material_spec'];
            $data['material'] = empty($_POST['material']) ? NULL : $_POST['material'];
            $data['number'] = empty($_POST['number']) ? NULL : $_POST['number'];
            $data['unit_type'] = empty($_POST['unit_type']) ? NULL : $_POST['unit_type'];
            $data['unit'] = empty($_POST['unit']) ? NULL : $_POST['unit'];
            $data['source'] = empty($_POST['source']) ? NULL : $_POST['source'];
            $data['weight'] = empty($_POST['weight']) ? NULL : $_POST['weight'];
            $data['supplier_id'] = empty($_POST['supplier_id']) ? NULL : $_POST['supplier_id'];
            $data['create_by'] = $data_redis['userid'];
            $data['create_time'] = date('Y-m-d H:i:s');

            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '产品碳足迹';
            $data_log['type'] = '功能操作';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $request->pathinfo();
            $data_log['log'] = '添加产品原材料：' . $data['product_material_name'];

            // 验证当前产品（原材料名称 + 原材料编号）是否存在
            if ($this->checkProductMaterial($data_redis['main_organization_id'], $data['product_id'], $data['product_material_name'], $data['product_material_no'])) {
                $add = ProductMaterialModel::addMaterial($data);
            } else {
                return json(['code'=>201, 'message'=>"原材料已存在，请重新输入"]);
            }

            OperationModel::addOperation($data_log);

            if ($add) {
                return json(['code'=>200, 'message'=>"添加成功"]);
            } else {
                return json(['code'=>404, 'message'=>"添加失败"]);
            }

        } else {
            return json(['code'=>404, 'message'=>$this->validateForm()]);
        }
    }

    /**
     * edit 原材料修改
     * 
     * @author wuyinghua
     * @param $request
	 * @return void
     */
    public function edit(Request $request) {

        if (request()->isPost() && $this->validateForm() === true) {
            $data_redis = $request->middleware('data_redis');

            $data['id'] = $_POST['id'];
            $data['product_material_name'] = $_POST['product_material_name'];
            $data['product_material_no'] = empty($_POST['product_material_no']) ? NULL : $_POST['product_material_no'];
            $data['product_material_spec'] = empty($_POST['product_material_spec']) ? NULL : $_POST['product_material_spec'];
            $data['material'] = empty($_POST['material']) ? NULL : $_POST['material'];
            $data['number'] = empty($_POST['number']) ? NULL : $_POST['number'];
            $data['unit_type'] = empty($_POST['unit_type']) ? NULL : $_POST['unit_type'];
            $data['unit'] = empty($_POST['unit']) ? NULL : $_POST['unit'];
            $data['source'] = empty($_POST['source']) ? NULL : $_POST['source'];
            $data['weight'] = empty($_POST['weight']) ? NULL : $_POST['weight'];
            $data['supplier_id'] = empty($_POST['supplier_id']) ? NULL : $_POST['supplier_id'];
            $data['modify_by'] = $data_redis['userid'];
            $data['modify_time'] = date('Y-m-d H:i:s');

            // 切换为自产时清空供应商
            if ($data['source'] == 1) {
                $data['supplier_id'] = NULL;
            }

            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '产品碳足迹';
            $data_log['type'] = '功能操作';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $request->pathinfo();
            $data_log['log'] = '编辑产品原材料：' . $data['product_material_name'];

            OperationModel::addOperation($data_log);
            $edit = ProductMaterialModel::editMaterial($data);

            if ($edit) {
                return json(['code'=>200, 'message'=>"修改成功"]);
            } else {
                return json(['code'=>404, 'message'=>"修改失败"]);
            }

        } else {
            return json(['code'=>404, 'message'=>$this->validateForm()]);
        }
    }

    /**
     * del 原材料删除
     * 
     * @author wuyinghua
     * @param $request
	 * @return void
     */
    public function del(Request $request) {
        $data_redis = $request->middleware('data_redis');

        $data['id'] = $_POST['id'];
        $material_info = ProductMaterialModel::getProductMaterial($data['id']);

        // 新增产品中删除原材料，没有对应的product_id
        if (!empty($_POST['product_id'])) {
            $data['product_id'] = $_POST['product_id'];
        }

        // 添加操作日志
        $data_log['main_organization_id'] = $data_redis['main_organization_id'];
        $data_log['user_id'] = $data_redis['userid'];
        $data_log['module'] = '产品碳足迹';
        $data_log['type'] = '功能操作';
        $data_log['time'] = date('Y-m-d H:i:s');
        $data_log['url'] = $request->pathinfo();
        $data_log['log'] = '删除产品原材料：' . $material_info['product_material_name'];

        OperationModel::addOperation($data_log);
        $del = ProductMaterialModel::delMaterial($data);

        if($del){
            return json(['code'=>200, 'message'=>"删除成功"]);
        }else{
            return json(['code'=>404, 'message'=>"删除失败"]);
        }
    }

    /**
     * checkProductMaterial 验证产品
     * 
     * @author wuyinghua
     * @param $main_organization_id
     * @param $product_id
     * @param $product_material_name
     * @param $product_material_no
	 * @return void
     */
    public function checkProductMaterial($main_organization_id, $product_id, $product_material_name, $product_material_no) {
        $list = ProductMaterialModel::getAllProductMaterials($main_organization_id, $product_id)->toArray();

        $combine_product_materials = array();
        foreach ($list as $key => $value) {
            $combine_product_materials[$key] = $value['product_material_name'] . '-' . $value['product_material_no'];
        }

        $combine_product_material = $product_material_name . '-' . $product_material_no;

        if (in_array($combine_product_material, $combine_product_materials)) {
            return false;
        } else {
            return  true;
        }
    }

	//======================================================================
	// PROTECTED FUNCTIONS
	//======================================================================

    /**
     * validateForm 验证
     * 
     * @author wuyinghua
	 * @return void
     */
    protected function validateForm() {
        $data = request()->param();

        try {
            validate(ProductMaterialValidate::class)->check($data);

            return  true;
        } catch (ValidateException $e) {
            
            return $e->getError();
        }
    }
}