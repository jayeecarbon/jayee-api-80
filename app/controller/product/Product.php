<?php
namespace app\controller\product;

use app\BaseController;
use app\validate\ProductValidate;
use think\exception\ValidateException;
use app\model\product\ProductModel;
use app\model\system\OperationModel;
use think\Request;

/**
 * Product
 */
class Product extends BaseController {

	//======================================================================
	// PUBLIC FUNCTIONS
	//======================================================================

    /**
     * index 产品列表
     * 
     * @author wuyinghua
	 * @return void
     */
    public function index() {
        $data_redis = $this->request->middleware('data_redis');
        $page_size = isset($_GET['pageSize']) ? $_GET['pageSize'] : '';
        $page_index = isset($_GET['pageIndex']) ? $_GET['pageIndex'] : '';
        $main_organization_id = isset($data_redis['main_organization_id']) ? $data_redis['main_organization_id'] : '';

        // 过滤条件：产品名称、产品编号、产品规格、产品状态
        $filters = [
            'filter_product_name' => isset($_GET['filterProductName']) ? $_GET['filterProductName'] : '',
            'filter_product_no' => isset($_GET['filterProductNo']) ? $_GET['filterProductNo'] : '',
            'filter_product_spec' => isset($_GET['filterProductSpec']) ? $_GET['filterProductSpec'] : '',
            'filter_product_state' => isset($_GET['filterState']) ? $_GET['filterState'] : '',
            'main_organization_id' => $main_organization_id
        ];

        $list = ProductModel::getProducts($page_size, $page_index, $filters)->toArray();

        foreach ($list['data'] as $key => $value) {
            $calculate_list = ProductModel::seeProductCalculate($value['id'])->toarray();
            $list['data'][$key]['count'] = count($calculate_list);
        }

        $active_products = ProductModel::getActiveProducts($main_organization_id)->toArray();

        $array = array();
        foreach ($list['data'] as $key => $value) {
            if ($value['files'] != NULL) {
                foreach (explode(',', $value['files']) as $file_key => $file_value) {
                    $array[$file_key] = ProductModel::getFiles($file_value);
                }
            }

            $list['data'][$key]['files'] = $array;
        }

        $data['code'] = 200;
        $data['data']['list'] = $list['data'];
        $data['data']['total'] = $list['total'];
        $data['data']['active'] = $active_products;

        return json($data);
    }

    /**
     * add 产品添加
     * 
     * @author wuyinghua
     * @param $request
	 * @return void
     */
    public function add(Request $request) {

        if (request()->isPost() && $this->validateForm() === true) {
            $data_redis = $request->middleware('data_redis');

            $data['virtual_id'] = empty($_POST['virtual_id']) ? NULL : $_POST['virtual_id']; // 虚拟id
            $data['files'] = isset($_POST['files']) ? json_decode($_POST['files'], true) : NULL; // 产品文件
            $data['main_organization_id'] = isset($data_redis['main_organization_id']) ? $data_redis['main_organization_id'] : '';
            $data['product_name'] = $_POST['product_name'];
            $data['product_no'] = $_POST['product_no'];
            $data['product_spec'] = empty($_POST['product_spec']) ? NULL : $_POST['product_spec'];
            $data['state'] = ProductModel::STATE_YES;
            $data['create_by'] = $data_redis['userid'];
            $data['modify_by'] = $data_redis['userid'];
            $data['create_time'] = date('Y-m-d H:i:s');
            $data['modify_time'] = date('Y-m-d H:i:s');

            // 验证当前产品（产品名称 + 产品编号）是否存在
            if ($this->checkProduct($data['main_organization_id'], $data['product_name'], $data['product_no'])) {
                $add = ProductModel::addProduct($data);
            } else {
                return json(['code'=>201, 'message'=>"产品已存在，请重新输入"]);
            }

            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '产品碳足迹';
            $data_log['type'] = '功能操作';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $request->pathinfo();
            $data_log['log'] = '添加产品：' . $data['product_name'];
            OperationModel::addOperation($data_log);

            if ($add) {
                return json(['code'=>200, 'message'=>"添加成功"]);
            } else {
                return json(['code'=>404, 'message'=>"添加失败"]);
            }

        } else {
            return json(['code'=>404, 'message'=>$this->validateForm()]);
        }
    }

    /**
     * edit 产品修改
     * 
     * @author wuyinghua
     * @param $request
	 * @return void
     */
    public function edit(Request $request) {

        if (request()->isPost() && $this->validateForm() === true) {
            $data_redis = $request->middleware('data_redis');

            $data['id'] = $_POST['id'];
            $data['product_name'] = $_POST['product_name'];
            $data['product_no'] = $_POST['product_no'];
            $data['files'] = isset($_POST['files']) ? json_decode($_POST['files'], true) : NULL; // 产品文件
            $data['product_spec'] = empty($_POST['product_spec']) ? NULL : $_POST['product_spec'];
            $data['modify_by'] = $data_redis['userid'];
            $data['modify_time'] = date('Y-m-d H:i:s');

            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '产品碳足迹';
            $data_log['type'] = '功能操作';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $request->pathinfo();
            $data_log['log'] = '编辑产品：' . $data['product_name'];

            OperationModel::addOperation($data_log);
            $edit = ProductModel::editProduct($data);

            if ($edit) {
                return json(['code'=>200, 'message'=>"修改成功"]);
            } else {
                return json(['code'=>404, 'message'=>"修改失败"]);
            }

        } else {
            return json(['code'=>404, 'message'=>$this->validateForm()]);
        }
    }

    /**
     * state 更新产品状态
     * 
     * @author wuyinghua
     * @param $request
	 * @return void
     */
    public function state(Request $request) {

        if (request()->isPost()) {
            $data_redis = $request->middleware('data_redis');
            $data['id'] = $_POST['id'];
            $data['modify_by'] = $data_redis['userid'];
            $data['state'] = $_POST['state'];
            $data['modify_time'] = date('Y-m-d H:i:s');
            $product_info = ProductModel::getDataById($data['id']);

            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '产品碳足迹';
            $data_log['type'] = '功能操作';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $request->pathinfo();
            $data_log['log'] = '更新产品状态：' . $product_info['product_name'];

            OperationModel::addOperation($data_log);
            $edit = ProductModel::updateState($data);

            if ($edit) {
                return json(['code'=>200, 'message'=>"修改成功"]);
            } else {
                return json(['code'=>404, 'message'=>"修改失败"]);
            }

        } else {
            return json(['code'=>404, 'message'=>"修改失败"]);
        }
    }

    /**
     * see 查看产品详情
     * 
     * @author wuyinghua
	 * @return void
     */
    public function see() {
        $id = $_GET['id'];
        $product_data = ProductModel::getDataById($id);

        if ($product_data['files'] != NULL) {
            foreach (explode(',', $product_data['files']) as $file_key => $file_value) {
                $product_data['file_arr'][$file_key] = ProductModel::getFiles($file_value);
            }
        }

        $list = ProductModel::seeProduct($id)->toarray();
        $calculate_list = ProductModel::seeProductCalculate($id);

        $data['code'] = 200;
        $data['data']['product'] = $product_data;
        $data['data']['list'] = $list;
        $data['data']['calculate_list'] = $calculate_list;

        return json($data);
    }

    /**
     * checkProduct 验证产品
     * 
     * @author wuyinghua
     * @param $main_organization_id
     * @param $product_name
     * @param $product_no
	 * @return void
     */
    public static function checkProduct($main_organization_id, $product_name, $product_no) {
        $list = ProductModel::getAllProducts($main_organization_id)->toArray();

        $combine_products = [];
        foreach ($list as $key => $value) {
            $combine_products[$key] = $value['product_name'] . '-' . $value['product_no'];
        }

        $combine_product = $product_name . '-' . $product_no;

        if (in_array($combine_product, $combine_products)) {
            return false;
        } else {
            return  true;
        }
    }

    /**
     * upload 上传文件
     * 
     * @author wuyinghua
     * @param $request
	 * @return void
     */
    public function upload(Request $request){
        $data_redis = $request->middleware('data_redis');
        $file = $request->file();

        try{
            // 验证文件大小格式
            validate(['file'=>'fileSize:1048576|fileExt:png,jpg,doc,docx,pdf,xls,xlsx'])
            ->check($file);
 
            $savename = \think\facade\Filesystem::disk('public')->putFile('uploads', $file['file']);
            $url = \think\facade\Filesystem::getDiskConfig('public', 'url') . '/' . str_replace('\\', '/', $savename);
            $data['virtual_id'] = isset($_POST['virtual_id']) ? $_POST['virtual_id'] : NULL;
            $data['file_path'] = $url;
            $data['source_name'] = $_FILES['file']['name'];
            $data['file_size'] = $_FILES['file']['size'];
            $data['create_by'] = $data_redis['userid'];
            $data['modify_by'] = $data_redis['userid'];
            $data['create_time'] = date('Y-m-d H:i:s');
            $data['modify_time'] = date('Y-m-d H:i:s');
            $data['id'] = guid();
            $add = ProductModel::addProductFile($data);

            if ($add) {
                $datasmg['code'] = 200;
                $datasmg['id'] = $data['id'];
                $datasmg['name'] = $data['source_name'];
                $datasmg['msg'] = "上传成功"; // 前端要求， mseeage -> msg，不提示弹框
            } else {
                $datasmg['code'] = 404; // 前端要求，上传失败 404 -> 200，不提示弹框
                $datasmg['name'] = $data['source_name']; // 前端要求，上传失败返回文件名
                $datasmg['msg'] = "上传失败";
            }

            return json($datasmg);
        }catch(ValidateException $e){
            $datasmg['code'] = 200; // 前端要求，验证失败 404 -> 200，不提示弹框
            $datasmg['name'] = $_FILES['file']['name']; // 前端要求，验证失败返回文件名
            $datasmg['msg'] = $e->getError();

            return json($datasmg);
        }
    }

	//======================================================================
	// PROTECTED FUNCTIONS
	//======================================================================

    /**
     * validateForm 验证
     * 
     * @author wuyinghua
	 * @return void
     */
    protected function validateForm() {
        $data = request()->param();

        try {
            validate(ProductValidate::class)->check($data);

            return  true;
        } catch (ValidateException $e) {
            
            return $e->getError();
        }
    }
}