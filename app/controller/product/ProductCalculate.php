<?php
namespace app\controller\product;

use app\BaseController;
use app\validate\ProductCalculateValidate;
use think\exception\ValidateException;
use app\model\product\ProductModel;
use app\model\product\ProductCalculateModel;
use app\model\system\OperationModel;

/**
 * Product
 */
class ProductCalculate extends BaseController {

	//======================================================================
	// PUBLIC FUNCTIONS
	//======================================================================

    /**
     * index 产品核算列表
     * 
     * @author wuyinghua
	 * @return void
     */
    public function index() {
        $data_redis = $this->request->middleware('data_redis');
        $page_size = isset($_GET['pageSize']) ? $_GET['pageSize'] : '';
        $page_index = isset($_GET['pageIndex']) ? $_GET['pageIndex'] : '';

        // 过滤条件：产品名称、产品编号、产品状态、核算周期开始、核算周期结束
        $filters = [
            'filter_product_name' => isset($_GET['filterProductName']) ? $_GET['filterProductName'] : '',
            'filter_product_no' => isset($_GET['filterProductNo']) ? $_GET['filterProductNo'] : '',
            'filter_product_state' => isset($_GET['filterState']) ? $_GET['filterState'] : '',
            'filter_week_start' => isset($_GET['filterWeekStart']) ? $_GET['filterWeekStart'] : '',
            'filter_week_end' => isset($_GET['filterWeekEnd']) ? $_GET['filterWeekEnd'] : '',
            'filter_state' => isset($_GET['state']) ? $_GET['state'] : '',
            'main_organization_id' => isset($data_redis['main_organization_id']) ? $data_redis['main_organization_id'] : ''
        ];

        $list = ProductCalculateModel::getCalculates($page_size, $page_index, $filters)->toArray();

        $array = array();
        foreach ($list['data'] as $key => $value) {
            if ($value['files'] != NULL) {
                foreach (explode(',', $value['files']) as $file_key => $file_value) {
                    $array[$file_key] = ProductModel::getFiles($file_value);
                }
            }

            $list['data'][$key]['files'] = $array;
        }

        $data['code'] = 200;
        $data['data']['list'] = $list['data'];
        $data['data']['total'] = $list['total'];

        return json($data);
    }

    /**
     * add 添加产品核算
     * 
     * @author wuyinghua
	 * @return void
     */
    public function add() {

        if (request()->isPost() && $this->validateForm() === true) {
            $data_redis = $this->request->middleware('data_redis');
            $data['main_organization_id'] = isset($data_redis['main_organization_id']) ? $data_redis['main_organization_id'] : '';
            $data['product_id'] = $_POST['product_id'];
            $data['week_start'] = $_POST['week_start'];
            $data['week_end'] = $_POST['week_end'];
            $data['number'] = $_POST['number'];
            $data['unit_type'] = $_POST['unit_type'];
            $data['unit'] = $_POST['unit'];
            $data['scope'] = $_POST['scope'];
            $data['stage'] = isset($_POST['stage']) ? $_POST['stage'] : '';
            $data['remarks'] = isset($_POST['remarks']) ? $_POST['remarks'] : '';
            $data['state'] = '3'; // 产品核算状态默认 '3'，编辑中
            $data['create_by'] = $data_redis['userid'];
            $data['modify_by'] = $data_redis['userid'];
            $data['create_time'] = date('Y-m-d H:i:s');
            $data['modify_time'] = date('Y-m-d H:i:s');

            // 验证当前产品下，核算周期是否存在
            if ($this->checkCalculateWeek($data['product_id'], $data['week_start'], $data['week_end'])) {
                $add = ProductCalculateModel::addCalculate($data);
            } else {
                return json(['code'=>201, 'message'=>"核算周期已存在，请重新选择"]);
            }

            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '产品碳足迹';
            $data_log['type'] = '功能操作';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $this->request->pathinfo();
            $data_log['log'] = '新建产品核算：' . $_POST['product_name'];
            OperationModel::addOperation($data_log);

            if ($add) {
                return json(['code'=>200, 'message'=>"添加成功"]);
            } else {
                return json(['code'=>404, 'message'=>"添加失败"]);
            }

        } else {
            return json(['code'=>404, 'message'=>$this->validateForm()]);
        }
    }

    /**
     * checkCalculateWeek 验证核算周期
     * 
     * @author wuyinghua
     * @param $product_id
     * @param $week_start
     * @param $week_end
	 * @return void
     */
    public function checkCalculateWeek($product_id, $week_start, $week_end) {
        $weeks = ProductCalculateModel::getCalculateWeek($product_id)->toArray();

        $combine_weeks = array();
        foreach ($weeks as $key => $value) {
            $combine_weeks[$key] = $value['week_start'] . '-' . $value['week_end'];
        }

        $combine_week = $week_start . '-' . $week_end;
        if (in_array($combine_week, $combine_weeks)) {
            return false;
        } else {
            return  true;
        }
    }

    /**
     * edit 修改产品核算
     * 
     * @author wuyinghua
	 * @return void
     */
    public function edit() {

        if (request()->isPost() && $this->validateForm() === true) {
            $data_redis = $this->request->middleware('data_redis');

            $data['id'] = $_POST['id'];
            $data['product_id'] = $_POST['product_id'];
            $data['week_start'] = $_POST['week_start'];
            $data['week_end'] = $_POST['week_end'];
            $data['number'] = $_POST['number'];
            $data['unit_type'] = $_POST['unit_type'];
            $data['unit'] = $_POST['unit'];
            $data['scope'] = $_POST['scope'];
            $data['stage'] = $_POST['stage'];
            $data['remarks'] = $_POST['remarks'];
            $data['state'] = '3'; // 产品核算状态默认 '3'，编辑中
            $data['modify_by'] = $data_redis['userid'];
            $data['modify_time'] = date('Y-m-d H:i:s');

            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '产品碳足迹';
            $data_log['type'] = '功能操作';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $this->request->pathinfo();
            $data_log['log'] = '编辑产品核算：' . $_POST['product_name'];

            OperationModel::addOperation($data_log);
            $edit = ProductCalculateModel::editCalculate($data);

            if ($edit) {
                return json(['code'=>200, 'message'=>"修改成功"]);
            } else {
                return json(['code'=>404, 'message'=>"修改失败"]);
            }

        } else {
            return json(['code'=>404, 'message'=>$this->validateForm()]);
        }
    }

    /**
     * del 删除产品核算
     * 
     * @author wuyinghua
	 * @return void
     */
    public function del() {
        $data_redis = $this->request->middleware('data_redis');

        $id = $_POST['id'];
        $calculate_info = ProductCalculateModel::getCalculate($id);

        // 添加操作日志
        $data_log['main_organization_id'] = $data_redis['main_organization_id'];
        $data_log['user_id'] = $data_redis['userid'];
        $data_log['module'] = '产品碳足迹';
        $data_log['type'] = '功能操作';
        $data_log['time'] = date('Y-m-d H:i:s');
        $data_log['url'] = $this->request->pathinfo();
        $data_log['log'] = '删除产品核算：' . $calculate_info['product_name'] . $calculate_info['week'];

        OperationModel::addOperation($data_log);
        $del = ProductCalculateModel::delCalculate($id);

        if($del){
            return json(['code'=>200, 'message'=>"删除成功"]);
        }else{
            return json(['code'=>404, 'message'=>"删除失败"]);
        }
    }

    /**
     * state 更新产品核算状态
     * 
     * @author wuyinghua
	 * @return void
     */
    public function state() {

        if (request()->isPost()) {
            $data_redis = $this->request->middleware('data_redis');
            $data['id'] = $_POST['id'];
            $data['modify_by'] = $data_redis['userid'];
            $data['state'] = $_POST['state'];
            $data['modify_time'] = date('Y-m-d H:i:s');

            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '产品碳足迹';
            $data_log['type'] = '功能操作';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $this->request->pathinfo();
            $data_log['log'] = '更新产品核算状态：' . $_POST['product_name'];

            OperationModel::addOperation($data_log);
            $edit = ProductCalculateModel::updateState($data);

            if ($edit) {
                return json(['code'=>200, 'message'=>"修改成功"]);
            } else {
                return json(['code'=>404, 'message'=>"修改失败"]);
            }

        } else {
            return json(['code'=>404, 'message'=>"修改失败"]);
        }
    }

    /**
     * find 核算编辑回显
     * 
     * @author wuyinghua
	 * @return void
     */
    public function find() {
        $id = $_GET['id'];
        $list = ProductCalculateModel::getCalculate($id);

        $data['code'] = 200;
        $data['data'] = $list;

        return json($data);
    }

    /**
     * see 查看产品核算详情
     * 
     * @author wuyinghua
	 * @return void
     */
    public function see() {
        $id = $_GET['id'];
        $transport_ids = [8,12,14,16,18];

        $list = ProductCalculateModel::getCalculate($id);
        $list_stage = explode(',', $list['stage']);
        $list_all = ProductCalculateModel::seeCalculateData($id);

        $array = array();
        foreach ($list_stage as $list_key => $list_value) {
            $name = ProductCalculateModel::getStageName($list_key + 1);
            $array[$list_key]['data_stage'] = $name['name'];
            $array[$list_key]['total_emission'] = 0;
            foreach ($list_all as $value) {
                if ($value['data_stage'] == $list_value) {

                    if (in_array($value['category_id'], $transport_ids)) {
                        $value['unit_str'] = $value['unit_str'] . 'km';
                    }

                    $array[$list_key]['total_emission'] += $value['emissions'];
                    $array[$list_key]['datas'][] = $value;
                }
            }
        }

        $data['code'] = 200;
        $data['data']['list'] = $array;
        $data['data']['product_name'] = $list['product_name'];
        $data['data']['product_no'] = $list['product_no'];
        $data['data']['product_spec'] = $list['product_spec'];
        $data['data']['week'] = $list['week'];
        $data['data']['number'] = $list['number'];
        $data['data']['coefficient'] = $list['coefficient'];
        $data['data']['remarks'] = $list['remarks'];
        $data['data']['unit'] = $list['unit'];
        $data['data']['unit_str'] = $list['unit_str'];
        $data['data']['state'] = $list['state'];
        $data['data']['modify_time'] = $list['modify_time'];

        $array = array();
        $product = ProductModel::getDataById($list['product_id']);

        if ($product['files'] != NULL) {
            foreach (explode(',', $product['files']) as $file_key => $file_value) {
                $array[$file_key] = ProductModel::getFiles($file_value);
            }
        }

        $data['data']['files'] = $array;
        $data['data']['total_emissions'] = $list['emissions'];

        return json($data);
    }

    /**
     * create 生成核算报告
     * 
     * @author wuyinghua
	 * @return void
     */
    public function create() {
        $id = $_GET['id'];
        $list = ProductCalculateModel::getCalculate($id);
        $report['product_id'] = $list['product_id'];
        $report['product_calculate_id'] = $id;
        $report['name'] = $list['product_name'] . '-' . $list['product_no'];
        $report['count_date'] = $list['week_start'] . '-' . $list['week_end'];

        $data['code'] = 200;
        $data['data'] = $report;

        return json($data);
    }

	//======================================================================
	// PROTECTED FUNCTIONS
	//======================================================================

    /**
     * validateForm 验证
     * 
     * @author wuyinghua
	 * @return void
     */
    protected function validateForm() {
        $data = request()->param();

        try {
            validate(ProductCalculateValidate::class)->check($data);

            return  true;
        } catch (ValidateException $e) {
            
            return $e->getError();
        }
    }
}