<?php

namespace app\api\controller\file;

use app\BaseController;
use app\model\admin\FactorModel;
use app\model\file\FileModel;
use app\model\system\OperationModel;
use app\validate\ReductionValidate;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use \PhpOffice\PhpSpreadsheet\IOFactory;
use think\facade\Config;
use think\facade\Db;
use think\Request;


/**
 * Reduction
 */
class FactorFile extends BaseController
{

    //======================================================================
    // PUBLIC FUNCTIONS
    //======================================================================

    public function up(Request $request)
    {

        $db = new FactorModel();
        $file[] = $request->file('file1');

        try {
            $a = validate(['file' => 'filesize:512000000|fileExt:xls,xlsx,csv'])
                ->check($file);


            $savename = \think\facade\Filesystem::disk('public')->putFile('file', $file[0]);

            $fileExtendName = substr(strrchr($savename, '.'), 1);
            if (!in_array(strtolower($fileExtendName), ['xls', 'xlsx', 'csv'])) {
                echo('数据格式为空！');
                exit();
            };

            if ($fileExtendName == 'xlsx') {
                $objReader = IOFactory::createReader('Xlsx');
            } else {
                $objReader = IOFactory::createReader('Csv');
            }

            $objReader->setReadDataOnly(TRUE);

            $objPHPExcel = $objReader->load('storage/' . $savename);
            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();
            \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn);
            $lines = $highestRow - 1;

            if ($lines <= 0) {
                echo('数据不能为空！');
                exit();
            }
            if ($lines > 200000) {
                echo('导入数据量太大，请分批次导入！');
                exit();
            }

            for ($j = 2; $j <= $highestRow; $j++) {
                $unit = explode('/', $objPHPExcel->getActiveSheet()->getCell("O" . $j)->getValue());
                $one = '';
                $two = '';
                if ($unit) {
                    $one = trim($unit[0], "");
                    $two = isset($unit[1]) ? trim($unit[1], "") : "";
                }

                $data[$j - 2] = [
                    'country' => $objPHPExcel->getActiveSheet()->getCell("B" . $j)->getValue(),
                    'title' => $objPHPExcel->getActiveSheet()->getCell("J" . $j)->getValue()." ".$objPHPExcel->getActiveSheet()->getCell("L" . $j)->getValue(),
                    'model' => rtrim($objPHPExcel->getActiveSheet()->getCell("F" . $j)->getValue()),
                    'year' => $objPHPExcel->getActiveSheet()->getCell("C" . $j)->getValue(),

                    'factor_value' => $objPHPExcel->getActiveSheet()->getCell("N" . $j)->getValue(),
                    'molecule' => $one,
                    'denominator' =>$two,
                    'describtion' => $objPHPExcel->getActiveSheet()->getCell("P" . $j)->getValue(),
                    'file_name' => $objPHPExcel->getActiveSheet()->getCell("Q" . $j)->getValue(),


                   // 'region' => $objPHPExcel->getActiveSheet()->getCell("K" . $j)->getValue(),


                    'language' => '中文',
                    // 'denominator' => $objPHPExcel->getActiveSheet()->getCell("M" . $j)->getValue(),

                    //  'title_language' => $objPHPExcel->getActiveSheet()->getCell("C" . $j)->getValue(),

                    //   'mechanism' => $objPHPExcel->getActiveSheet()->getCell("F" . $j)->getValue(),
                    /*halt($data);*/          // 'mechanism_short' => $objPHPExcel->getActiveSheet()->getCell("G" . $j)->getValue(),
                    //'grade' => $objPHPExcel->getActiveSheet()->getCell("H" . $j)->getValue(),
                    //  'file_name' => $objPHPExcel->getActiveSheet()->getCell("L" . $j)->getValue(),
                    //
                    //'molecule' => $objPHPExcel->getActiveSheet()->getCell("N" . $j)->getValue(),
                    'organization_id' => 1,
                    'factor_source' => 1,
                    'factor_id' => mt_rand(1, 9) . mt_rand(1, 9) . mt_rand(1, 9) . mt_rand(10, 99) . mt_rand(1, 9) . mt_rand(1, 9) . mt_rand(1, 9) . mt_rand(1, 9),
                    'create_time' => date('Y-m-d H:i:s', time()),
                ];
            }

            $add = $db->addFactor($data);

            if ($add) {
                $datasmg['code'] = 200;
                $datasmg['message'] = "上传成功";
            } else {
                $datasmg['code'] = 404;
                $datasmg['message'] = "上传失败";
            }

            return json($datasmg);
        } catch (\think\exception\ValidateException $e) {

            return $e->getMessage();
        }
    }
    public function upId(Request $request)
    {
        $file[] = $request->file('file1');

        try {
            $a = validate(['file' => 'filesize:512000000|fileExt:xls,xlsx,csv'])
                ->check($file);


            $savename = \think\facade\Filesystem::disk('public')->putFile('file', $file[0]);

            $fileExtendName = substr(strrchr($savename, '.'), 1);
            if (!in_array(strtolower($fileExtendName), ['xls', 'xlsx', 'csv'])) {
                echo('数据格式为空！');
                exit();
            };

            if ($fileExtendName == 'xlsx') {
                $objReader = IOFactory::createReader('Xlsx');
            } else {
                $objReader = IOFactory::createReader('Csv');
            }

            $objReader->setReadDataOnly(TRUE);

            $objPHPExcel = $objReader->load('storage/' . $savename);
            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();
            \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn);
            $lines = $highestRow - 1;

            if ($lines <= 0) {
                echo('数据不能为空！');
                exit();
            }
            if ($lines > 200000) {
                echo('导入数据量太大，请分批次导入！');
                exit();
            }

            for ($j = 2; $j <= $highestRow; $j++) {
                $data[$j - 2] = [
                    'id' => $objPHPExcel->getActiveSheet()->getCell("A" . $j)->getValue(),
                ];
            }

            $add = Db::table('jy_factor_id')->insertAll($data);

            if ($add) {
                $datasmg['code'] = 200;
                $datasmg['message'] = "上传成功";
            } else {
                $datasmg['code'] = 404;
                $datasmg['message'] = "上传失败";
            }

            return json($datasmg);
        } catch (\think\exception\ValidateException $e) {

            return $e->getMessage();
        }
    }
        public function delid(){
            $one=[1,5001];
            $one1=[5000,10001];
            $one2=[10000,15001];
            $one3=[15000,20001];
            $one4=[20000,25001];
            $one5=[25000,30001];
            $one6=[30000,35001];
            $one7=[35000,40001];
            $one8=[40000,45001];
            $one9=[45000,50001];
            $one10=[50000,55001];
            $one11=[55000,60001];
            $one12=[60000,65001];
            $one13=[65000,70001];
            $one14=[70000,75001];
            $one15=[75000,80001];
            $one16=[80000,85001];
            $factor_id =  Db::table('jy_factor_id')->whereBetween('ids',$one)->select()->toArray();
            $use = array_column($factor_id,'id');
          // halt($use);
            $del = Db::table('jy_factor')->whereIn('id',$use)->delete();
            halt($del);
    }




}