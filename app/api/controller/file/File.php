<?php

namespace app\api\controller\file;

use app\BaseController;
use app\model\admin\FactorModel;
use app\model\file\FileModel;
use app\model\system\OperationModel;
use app\validate\ReductionValidate;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use \PhpOffice\PhpSpreadsheet\IOFactory;
use think\facade\Config;
use think\facade\Db;
use think\Request;


/**
 * File
 * 下载管理
 * by njf
 */
class File extends BaseController
{

    //======================================================================
    // PUBLIC FUNCTIONS
    //======================================================================

    /**
     * index 文件列表
     *
     * @return void
     */
    public function index(Request $request)
    {
        $data_redis = $request->middleware('data_redis');
        $main_organization_id = $data_redis['main_organization_id'];

        $page_size = isset($_GET['pageSize']) ? $_GET['pageSize'] : '10';
        $page_index = isset($_GET['pageIndex']) ? $_GET['pageIndex'] : '1';

        $filters = [
            'module' => isset($_GET['module']) ? $_GET['module'] : '',
            'organization_id' => isset($_GET['organization_id']) ? $_GET['organization_id'] : '',
            'main_organization_id' => $main_organization_id,
        ];

        $db = new FileModel();
        $list = $db->getList($page_size, $page_index, $filters);

        //组织列表
        $organizationList = $db->organizationList($main_organization_id);
        //产品模块
        $module = Config::get('emission.module');

        $data['code'] = 200;
        $data['data']['list'] = $list;
        $data['data']['module'] = $module;
        $data['data']['organizationList'] = $organizationList;

        return json($data);
    }

    /**
     * down 下载
     *
     * @return void
     */
    public function down(Request $request)
    {

        if (request()->isPost()) {
            $params_payload = $this->req_payload;
            $params_payload_arr = json_decode($params_payload, true);

            $data_redis = $request->middleware('data_redis');
            $data['id'] = $params_payload_arr['id'];
            $info = FileModel::getTitleById($params_payload_arr['id']);
            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '下载管理';
            $data_log['type'] = '系统操作';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $request->pathinfo();
            $data_log['log'] = '下载文件名称名称：' . $info['source_name'];

            OperationModel::addOperation($data_log);

            $file_path = $info['file_path'];

            if ($info) {
                $datasmg['code'] = 200;
                $datasmg['file_path'] = $file_path;
                $datasmg['message'] = "请求成功";
            } else {
                $datasmg['code'] = 404;
                $datasmg['message'] = "请求失败";
            }

        } else {
            $datasmg['code'] = 404;
            $datasmg['message'] = '请求方法错误';
        }

        return json($datasmg);
    }


    public function up(Request $request)
    {

        $db = new FactorModel();
        $file[] = $request->file('file1');

        try {
            $a = validate(['file' => 'filesize:512000000|fileExt:xls,xlsx,csv'])
                ->check($file);


            $savename = \think\facade\Filesystem::disk('public')->putFile('file', $file[0]);

            $fileExtendName = substr(strrchr($savename, '.'), 1);
            if (!in_array(strtolower($fileExtendName), ['xls', 'xlsx', 'csv'])) {
                echo('数据格式为空！');
                exit();
            };

            if ($fileExtendName == 'xlsx') {
                $objReader = IOFactory::createReader('Xlsx');
            } else {
                $objReader = IOFactory::createReader('Csv');
            }

            $objReader->setReadDataOnly(TRUE);

            $objPHPExcel = $objReader->load('storage/' . $savename);
            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();
            \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn);
            $lines = $highestRow - 1;

            if ($lines <= 0) {
                echo('数据不能为空！');
                exit();
            }
            if ($lines > 200000) {
                echo('导入数据量太大，请分批次导入！');
                exit();
            }

            for ($j = 2; $j <= $highestRow; $j++) {

                $unit = explode('/', $objPHPExcel->getActiveSheet()->getCell("O" . $j)->getValue());
                $one = '';
                $two = '';
                if ($unit) {
                    $one = trim($unit[0], "");
                    $two = isset($unit[1]) ? trim($unit[1], "") : "";
                }

                $data[$j - 2] = [
                    'country' => $objPHPExcel->getActiveSheet()->getCell("A" . $j)->getValue(),
                    'title' => $objPHPExcel->getActiveSheet()->getCell("I" . $j)->getValue(),
                    'region' => $objPHPExcel->getActiveSheet()->getCell("K" . $j)->getValue(),
                    'language' => '中文',
                    // 'denominator' => $objPHPExcel->getActiveSheet()->getCell("M" . $j)->getValue(),
                    'model' => rtrim($objPHPExcel->getActiveSheet()->getCell("H" . $j)->getValue()) . " " . $objPHPExcel->getActiveSheet()->getCell("D" . $j)->getValue(),
                    'describtion' => trim($objPHPExcel->getActiveSheet()->getCell("F" . $j)->getValue()) . " " . trim($objPHPExcel->getActiveSheet()->getCell("G" . $j)->getValue()) . " " . trim($objPHPExcel->getActiveSheet()->getCell("E" . $j)->getValue()) . " " . trim($objPHPExcel->getActiveSheet()->getCell("J" . $j)->getValue())
                        . " " . trim($objPHPExcel->getActiveSheet()->getCell("L" . $j)->getValue()) . " " . trim($objPHPExcel->getActiveSheet()->getCell("P" . $j)->getValue()),
                    'year' => substr($objPHPExcel->getActiveSheet()->getCell("C" . $j)->getValue(), 1, 4),
                    'file_name' => $objPHPExcel->getActiveSheet()->getCell("B" . $j)->getValue() . '' . $objPHPExcel->getActiveSheet()->getCell("Q" . $j)->getValue(),
                    //  'title_language' => $objPHPExcel->getActiveSheet()->getCell("C" . $j)->getValue(),
                    'molecule' => $one,
                    'denominator' => $two,
                    'unit_type' => $objPHPExcel->getActiveSheet()->getCell("M" . $j)->getValue(),
                    'factor_value' => $objPHPExcel->getActiveSheet()->getCell("N" . $j)->getValue(),
                    //   'mechanism' => $objPHPExcel->getActiveSheet()->getCell("F" . $j)->getValue(),
                    // 'mechanism_short' => $objPHPExcel->getActiveSheet()->getCell("G" . $j)->getValue(),
                    //'grade' => $objPHPExcel->getActiveSheet()->getCell("H" . $j)->getValue(),
                    //  'file_name' => $objPHPExcel->getActiveSheet()->getCell("L" . $j)->getValue(),
                    //
                    //'molecule' => $objPHPExcel->getActiveSheet()->getCell("N" . $j)->getValue(),
                    'organization_id' => 1,
                    'factor_source' => 1,
                    'factor_id' => mt_rand(1, 9) . mt_rand(1, 9) . mt_rand(1, 9) . mt_rand(10, 99) . mt_rand(1, 9) . mt_rand(1, 9) . mt_rand(1, 9) . mt_rand(1, 9),
                    'create_time' => date('Y-m-d H:i:s', time()),
                ];
            }

            $add = $db->addFactor($data);

            if ($add) {
                $datasmg['code'] = 200;
                $datasmg['message'] = "上传成功";
            } else {
                $datasmg['code'] = 404;
                $datasmg['message'] = "上传失败";
            }

            return json($datasmg);
        } catch (\think\exception\ValidateException $e) {

            return $e->getMessage();
        }
    }

    public function ghgExcel(Request $request)
    {

        ini_set('max_execution_time', '999999999');
        $data_redis = $request->middleware('data_redis');
        $main_organization_id = $data_redis['main_organization_id'];
        $id = $_GET['id'];

        $path = 'static/exc/';
        $end = '.xlsx';
        $template = 'ghg';
        // $info = Db::table()->where('id',$id)->find();

        $template_name = $path . $template . $end;
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($template_name);



        //将填充好数据的工作表保存
        $title = '温室气体排放清册ghg'. date('Y-m-d H:i:s');
        $file_path='温室气体排放清册ghg'.time();
        $path= 'storage/uploads/'.date('Ymd');
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }





        //核算报告表
        $report = Db::table('jy_organization_calculate_report r')
            ->field('r.organization_id,r.username,r.telephone,r.email,r.organization_calculate_id,
            o.name organization_name,oc.calculate_year,r.organization_id')
            ->leftJoin('jy_organization o', 'r.organization_id=o.id')
            ->leftJoin('jy_organization_calculate oc', 'r.organization_calculate_id=oc.id')
            ->where('r.id', $id)->find();

        $organization_id=$report['organization_id'];
        $guid = guid();
        $insert_id=  Db::table('jy_file')->insertGetId([
            'main_organization_id'=>$main_organization_id,
            'organization_id'=>$organization_id,
            'id'=>$guid,
            'type'=>2,
            'status'=>1,
            'module'=>'组织碳核算',
            'file_path'=>'/'.$path.'/'.$file_path.'.xlsx',
            'source_name'=>$title.'.xlsx',
            'create_by'=>$data_redis['userid'],
            'modify_by'=>$data_redis['userid'],
            'create_time'=>date('Y-m-d H:i:s'),
            'modify_time'=>date('Y-m-d H:i:s')
        ]);


        //组织边界
        $boundaries = Db::table('jy_organizational_boundaries')->where('organization_id',$organization_id)->find();
        //组织基本信息表
        $one = Db::table('jy_organization_info')->where('organization_id', $organization_id)->find();

        $emission = Db::table('jy_organization_calculate_detail_emission n')
            ->field('n.id,n.ghg_cate,n.calculate_model_id,n.emission_name,n.emissions,n.active_value,n.emissions,n.active,n.ghg_cate,n.ghg_type,n.active_unit,
            n.active_department,n.active_type,n.active_score,n.factor_score,n.factor_source,gh.name ghg_cate_name,
            g.name ghg_name,n.active_data,n.factor_type,n.active_value_standard,
            c.molecule,c.gases,c.co2e_factor_value,c.tco2e,c.co2_factor_value,c.co2_quality,c.co2_gwp,c.co2_tco2e,
            c.ch4_factor_value,c.ch4_quality,c.ch4_gwp,c.ch4_tco2e,c.n2o_factor_value,c.n2o_quality,c.n2o_gwp,
            c.n2o_tco2e,c.hfcs_factor_value,c.hfcs_quality,c.hfcs_gwp,c.hfcs_tco2e,c.pfcs_factor_value,
            c.pfcs_quality,c.pfcs_gwp,c.pfcs_tco2e,c.sf6_factor_value,c.sf6_quality,c.sf6_gwp,
            c.sf6_tco2e,c.nf3_factor_value,c.nf3_quality,c.nf3_gwp,c.nf3_tco2e')
            ->leftJoin('jy_ghg_template g', 'g.id=n.ghg_type')
            ->leftJoin('jy_ghg_template gh', 'gh.id=n.ghg_cate')
            ->leftJoin('jy_organization_calculate_count_gas c','c.emi_id=n.id')
            ->where('n.organization_calculate_id', $report['organization_calculate_id'])
            ->order('n.id asc')
            ->select()
            ->toArray();



        $emission_one = [];
        $emission_two = [];
        $emission_three = [];
        if ($emission) {
            foreach ($emission as $v) {

                if ($v['ghg_cate_name'] == '范围一') {
                    $emission_one[] = $v;
                };
                if ($v['ghg_cate_name'] == '范围二') {
                    $emission_two[] = $v;
                };
                if ($v['ghg_cate_name'] == '范围三') {
                    $emission_three[] = $v;
                };
            }
        }

        $spreadsheet->setActiveSheetIndex(0);
        $worksheet = $spreadsheet->getActiveSheet();

        //填充数据
        // $worksheet->mergeCells('A1:J1');//合并单元格（如果要拆分单元格是需要先合并再拆分的，否则程序会报错）

        if ($report) {
            $name='';
            $production_address_detail='';
            if($one){
                $city = Db::table('jy_city')->whereIn('id', $one['production_address'])->select()->toArray();
                $name = implode(',', array_column($city, 'name'));
                $production_address_detail = $one['production_address_detail'];
            }
            $organization_desc='';
            if($boundaries){
                $organization_desc= $boundaries['organization_desc'];
            }

            $worksheet->getCell('B4')->setValue('【'.$report['organization_name'].'】');
            $worksheet->getCell('B5')->setValue('【'.$report['calculate_year'].'】');
            $worksheet->getCell('B6')->setValue('【'.$organization_desc.'】');
            $worksheet->getCell('B7')->setValue('【'.$name . '-' . $production_address_detail.'】');
            $worksheet->getCell('B8')->setValue('【'.$report['username'].'】');
            $worksheet->getCell('F8')->setValue('【'.date('Y-m-d').'】');
            $worksheet->getCell('B9')->setValue('【'.$report['telephone'].'】');
            $worksheet->getCell('F9')->setValue('【'.$report['email'].'】');

            $worksheet->getProtection()->setSheet(true);
            //组织运营边界调查表
            $spreadsheet->setActiveSheetIndex(1);
            $worksheet = $spreadsheet->getActiveSheet();
            $one_count = 0;
            $two_count = 0;
            $three_count = 0;
                $worksheet->getCell('B4')->setValue('【'.$report['organization_name'].'】');
                $worksheet->getCell('B5')->setValue('【'.$report['calculate_year'].'】');
                $worksheet->getCell('B6')->setValue('【'.$organization_desc.'】');
                $worksheet->getCell('B7')->setValue('【'.$name . '-' .$production_address_detail.'】');
                $worksheet->getCell('B8')->setValue('【'.$report['username'].'】');
                $worksheet->getCell('F8')->setValue('【'.date('Y-m-d').'】');
                $worksheet->getCell('B9')->setValue('【'.$report['telephone'].'】');
                $worksheet->getCell('F9')->setValue('【'.$report['email'].'】');
            if ($emission) {
                $two_count = count($emission_two);
                $three_count = count($emission_three);
                $one_count = count($emission_one);
                if (count($emission_one) > 0) {
                    $worksheet->getStyle('A11')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                    $worksheet->getStyle('B11')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                    //$worksheet->getStyle('A1')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                    //$worksheet->getStyle('A1')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                    $worksheet->mergeCells('A11:'.'A'.(11+$one_count-1));
                    $worksheet->mergeCells('B11:'.'B'.(11+$one_count-1));
                    $worksheet->getCell('A11')->setValue('直接排放');
                    $worksheet->getCell('B11')->setValue('范围一');
                    foreach ($emission_one as $k => $v) {
                        $worksheet->getStyle('E'.(11+$k))->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                        $worksheet->mergeCells('C'.(11+$k).':D'.(11+$k));
                        $worksheet->mergeCells('E'.(11+$k).':F'.(11+$k));
                        $worksheet->getCell('C' . (11 + $k))->setValue($v['ghg_name']);
                        $worksheet->getCell('E' . (11 + $k))->setValue($v['emission_name']);
                        $worksheet->getCell('G' . (11 + $k))->setValue($v['active']);
                    }
                }

                if (count($emission_two) > 0) {
                    $worksheet->getStyle('A'.(11+$one_count))->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                    $worksheet->getStyle('B'.(11+$one_count))->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                    $worksheet->mergeCells('A'.(11+$one_count).':'.'A'.(11+$one_count+$two_count+$three_count-1));
                    $worksheet->mergeCells('B'.(11+$one_count).':'.'b'.(11+$one_count+$two_count-1));
                    $worksheet->getCell('A' . (11 + $one_count))->setValue('间接排放');
                    $worksheet->getCell('B' . (11 + $one_count))->setValue('范围二');
                    foreach ($emission_two as $k => $v) {
                        $worksheet->getStyle('E'.(11+$one_count+$k))->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                        $worksheet->mergeCells('C'.(11+$one_count+$k).':D'.(11+$one_count+$k));
                        $worksheet->mergeCells('E'.(11+$one_count+$k).':F'.(11+$one_count+$k));
                        $worksheet->getCell('C' . (11 + $one_count + $k))->setValue($v['ghg_name']);
                        $worksheet->getCell('E' . (11 + $one_count + $k))->setValue($v['emission_name']);
                        $worksheet->getCell('G' . (11 + $one_count + $k))->setValue($v['active']);
                    }
                }
                if (count($emission_three) > 0) {
                    //左右居中
                    $worksheet->getStyle('B'.(11+$one_count+$two_count))->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                    $worksheet->mergeCells('B'.(11+$one_count+$two_count).':'.'b'.(11+$one_count+$two_count+$three_count-1));
                    $worksheet->getCell('B' . (11 + $one_count + $two_count))->setValue('范围三');
                    foreach ($emission_three as $k => $v) {
                        $worksheet->getStyle('E'.(11+$one_count+$k+$two_count))->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                        $worksheet->mergeCells('C'.(11+$one_count+$k+$two_count).':D'.(11+$one_count+$k+$two_count));
                        $worksheet->mergeCells('E'.(11+$one_count+$k+$two_count).':F'.(11+$one_count+$k+$two_count));
                        $worksheet->getCell('C' . (11 + $one_count + $two_count + $k))->setValue($v['ghg_name']);
                        $worksheet->getCell('E' . (11 + $one_count + $two_count + $k))->setValue($v['emission_name']);
                        $worksheet->getCell('G' . (11 + $one_count + $two_count + $k))->setValue($v['active']);
                    }
                }
            }

            //排放源识别表
            $spreadsheet->setActiveSheetIndex(2);
            $worksheet = $spreadsheet->getActiveSheet();

            if ($emission) {
                foreach ($emission as $k => $v) {
                    $worksheet->getStyle('A'.(5+$k))->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                    $worksheet->getStyle('D'.(5+$k))->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                    $worksheet->mergeCells('F'.(5+$k).':G'.(5+$k));
                    $worksheet->getCell('A' . (5 + $k))->setValue($v['id']);
                    $worksheet->getCell('B' . (5 + $k))->setValue($v['emission_name']);
                    $worksheet->getCell('C' . (5 + $k))->setValue($v['active']);
                    $worksheet->getCell('D' . (5 + $k))->setValue($v['ghg_cate_name']);
                    $worksheet->getCell('E' . (5 + $k))->setValue($v['active_department']);
                    $worksheet->getCell('F' . (5 + $k))->setValue($v['gases']);
                }
            }

            //活动数据收集表
            $spreadsheet->setActiveSheetIndex(3);
            $worksheet = $spreadsheet->getActiveSheet();
            if ($emission) {
                foreach ($emission as $k => $v) {

                    $unit_two = Db::table('jy_unit')->field('name')->where('id', $v['active_unit'][2])->find();
                    $active_type = Config::get('emission.active_type')[$v['active_type'] - 1]['name'];
                    $worksheet->getStyle('A'.(5+$k))->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                    $worksheet->getCell('A' . (5 + $k))->setValue($v['id']);
                    $worksheet->getCell('B' . (5 + $k))->setValue($v['emission_name']);
                    $worksheet->getCell('C' . (5 + $k))->setValue($v['active']);
                    $worksheet->getCell('D' . (5 + $k))->setValue($v['ghg_cate_name']);
                    $worksheet->getCell('E' . (5 + $k))->setValue($v['active_value_standard']);
                    $worksheet->getCell('F' . (5 + $k))->setValue( $unit_two['name']);
                    $worksheet->getCell('G' . (5 + $k))->setValue($active_type);
                    $worksheet->getCell('H' . (5 + $k))->setValue($v['active_score']);
                    $worksheet->getCell('I' . (5 + $k))->setValue($v['active_data']);
                    $worksheet->getCell('J' . (5 + $k))->setValue($v['active_department']);
                    $worksheet->getCell('K' . (5 + $k))->setValue($v['gases']);
                }
            }
            //排放因子选择表
            $spreadsheet->setActiveSheetIndex(4);
            $worksheet = $spreadsheet->getActiveSheet();
            if ($emission) {
                foreach ($emission as $k => $v) {
                    $factor_type_one='';
                    $factor_type_one = Config::get('emission.factor_type')[$v['factor_type'] - 1]['name'];
                    if($v['molecule']){
                        $molecule= Config::get('emission.molecule')[$v['molecule']-1]['name'];
                    }else{
                        $molecule= "";
                    }

                    $worksheet->getStyle('A'.(5+$k))->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                    $worksheet->getCell('A' . (5 + $k))->setValue($v['id']);
                    $worksheet->getCell('B' . (5 + $k))->setValue($v['emission_name']);
                    $worksheet->getCell('C' . (5 + $k))->setValue($v['active']);
                    $worksheet->getCell('D' . (5 + $k))->setValue($v['gases']);
                    $worksheet->getCell('E' . (5 + $k))->setValue($v['co2e_factor_value']);
                    $worksheet->getCell('F' . (5 + $k))->setValue($v['co2e_factor_value'] ? $molecule:"");
                    $worksheet->getCell('G' . (5 + $k))->setValue($v['co2_factor_value']);
                    $worksheet->getCell('H' . (5 + $k))->setValue($v['co2_factor_value'] ?$molecule:"");
                    $worksheet->getCell('I' . (5 + $k))->setValue($v['ch4_factor_value']);
                    $worksheet->getCell('J' . (5 + $k))->setValue($v['ch4_factor_value'] ? $molecule :"");
                    $worksheet->getCell('K' . (5 + $k))->setValue($v['n2o_factor_value']);
                    $worksheet->getCell('L' . (5 + $k))->setValue($v['n2o_factor_value'] ? $molecule :"");
                    $worksheet->getCell('M' . (5 + $k))->setValue($v['hfcs_factor_value']);
                    $worksheet->getCell('N' . (5 + $k))->setValue($v['hfcs_factor_value'] ? $molecule :"");
                    $worksheet->getCell('O' . (5 + $k))->setValue($v['pfcs_factor_value']);
                    $worksheet->getCell('P' . (5 + $k))->setValue($v['pfcs_factor_value'] ? $molecule:"");
                    $worksheet->getCell('Q' . (5 + $k))->setValue($v['sf6_factor_value']);
                    $worksheet->getCell('R' . (5 + $k))->setValue($v['sf6_factor_value'] ?$molecule:"");
                    $worksheet->getCell('S' . (5 + $k))->setValue($v['nf3_factor_value']);
                    $worksheet->getCell('T' . (5 + $k))->setValue($v['nf3_factor_value'] ? $molecule :"");
                    $worksheet->getCell('U' . (5 + $k))->setValue($factor_type_one);
                    $worksheet->getCell('V' . (5 + $k))->setValue($v['factor_score']);
                    $worksheet->getCell('W' . (5 + $k))->setValue($v['factor_source']);
                }
            }
        }
        //排放量计算表
        $spreadsheet->setActiveSheetIndex(5);
        $worksheet = $spreadsheet->getActiveSheet();
         if($emission) {
             $all_total=0;
             foreach ($emission as $k => $v) {
                 $total=  $v['co2e_factor_value']*$v['active_value_standard']+$v['co2_factor_value']*$v['active_value_standard']*$v['co2_gwp']+
                     $v['ch4_factor_value']*$v['active_value_standard']*$v['ch4_gwp']+$v['n2o_factor_value']*$v['active_value_standard']*$v['n2o_gwp']+
                     $v['hfcs_factor_value']*$v['active_value_standard']*$v['hfcs_gwp']+$v['pfcs_factor_value']*$v['active_value_standard']*$v['pfcs_gwp']+
                     $v['sf6_factor_value']*$v['active_value_standard']*$v['sf6_gwp']+$v['nf3_factor_value']*$v['active_value_standard']*$v['nf3_gwp'];
                 $all_total +=$total;
             }

             foreach ($emission as $k => $v) {
                 $unit = '';
                 $unit = Db::table('jy_unit')->field('name')->where('id',$v['active_unit'][2])->find();
                 $total=0;
                 $total=  $v['co2e_factor_value']*$v['active_value_standard']+$v['co2_factor_value']*$v['active_value_standard']*$v['co2_gwp']+
                     $v['ch4_factor_value']*$v['active_value_standard']*$v['ch4_gwp']+$v['n2o_factor_value']*$v['active_value_standard']*$v['n2o_gwp']+
                     $v['hfcs_factor_value']*$v['active_value_standard']*$v['hfcs_gwp']+$v['pfcs_factor_value']*$v['active_value_standard']*$v['pfcs_gwp']+
                     $v['sf6_factor_value']*$v['active_value_standard']*$v['sf6_gwp']+$v['nf3_factor_value']*$v['active_value_standard']*$v['nf3_gwp'];
                 if($v['molecule']){
                     $molecule= Config::get('emission.molecule')[$v['molecule']-1]['name'];
                 }else{
                     $molecule= "";
                 }
                 $worksheet->getStyle('A'.(6+$k))->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                 $worksheet->getCell('A'.(6+$k))->setValue($v['id']);
                 $worksheet->getCell('B'.(6+$k))->setValue($v['emission_name']);
                 $worksheet->getCell('C'.(6+$k))->setValue($v['active']);
                 $worksheet->getCell('D'.(6+$k))->setValue($v['ghg_cate_name']);
                 $worksheet->getCell('E'.(6+$k))->setValue($v['active_value']);
                 $worksheet->getCell('F'.(6+$k))->setValue($unit['name']);
                 $worksheet->getCell('G'.(6+$k))->setValue($v['co2e_factor_value']);
                 $worksheet->getCell('H'.(6+$k))->setValue($v['co2e_factor_value'] ? $molecule :"");
                 $worksheet->getCell('I'.(6+$k))->setValue($v['co2e_factor_value']);
                 $worksheet->getCell('J'.(6+$k))->setValue($v['co2_factor_value']);
                 $worksheet->getCell('K'.(6+$k))->setValue($v['co2_factor_value'] ? $molecule:"");
                 $worksheet->getCell('L'.(6+$k))->setValue($v['co2_quality']);
                 $worksheet->getCell('M'.(6+$k))->setValue($v['co2_gwp']);
                 $worksheet->getCell('N'.(6+$k))->setValue($v['co2_tco2e']);
                 $worksheet->getCell('O'.(6+$k))->setValue($v['ch4_factor_value']);
                 $worksheet->getCell('P'.(6+$k))->setValue($v['ch4_factor_value'] ? $molecule : "");
                 $worksheet->getCell('Q'.(6+$k))->setValue($v['ch4_quality']);
                 $worksheet->getCell('R'.(6+$k))->setValue($v['ch4_gwp']);
                 $worksheet->getCell('S'.(6+$k))->setValue($v['ch4_tco2e']);
                 $worksheet->getCell('T'.(6+$k))->setValue($v['n2o_factor_value']);
                 $worksheet->getCell('U'.(6+$k))->setValue($v['n2o_factor_value'] ? $molecule :"");
                 $worksheet->getCell('V'.(6+$k))->setValue($v['n2o_quality']);
                 $worksheet->getCell('W'.(6+$k))->setValue($v['n2o_gwp']);
                 $worksheet->getCell('X'.(6+$k))->setValue($v['n2o_tco2e']);
                 $worksheet->getCell('Y'.(6+$k))->setValue($v['hfcs_factor_value']);
                 $worksheet->getCell('Z'.(6+$k))->setValue($v['hfcs_factor_value'] ? $molecule :"");
                 $worksheet->getCell('AA'.(6+$k))->setValue($v['hfcs_quality']);
                 $worksheet->getCell('AB'.(6+$k))->setValue($v['hfcs_gwp']);
                 $worksheet->getCell('AC'.(6+$k))->setValue($v['hfcs_tco2e']);
                 $worksheet->getCell('AD'.(6+$k))->setValue($v['pfcs_factor_value']);
                 $worksheet->getCell('AE'.(6+$k))->setValue($v['pfcs_factor_value'] ? $molecule :"");
                 $worksheet->getCell('AF'.(6+$k))->setValue($v['pfcs_quality']);
                 $worksheet->getCell('AG'.(6+$k))->setValue($v['pfcs_gwp']);
                 $worksheet->getCell('AH'.(6+$k))->setValue($v['pfcs_tco2e']);
                 $worksheet->getCell('AI'.(6+$k))->setValue($v['sf6_factor_value']);
                 $worksheet->getCell('AJ'.(6+$k))->setValue($v['sf6_factor_value'] ? $molecule :"");
                 $worksheet->getCell('AK'.(6+$k))->setValue($v['sf6_quality']);
                 $worksheet->getCell('AL'.(6+$k))->setValue($v['sf6_gwp']);
                 $worksheet->getCell('AM'.(6+$k))->setValue($v['sf6_tco2e']);
                 $worksheet->getCell('AN'.(6+$k))->setValue($v['nf3_factor_value']);
                 $worksheet->getCell('AO'.(6+$k))->setValue($v['nf3_factor_value'] ? $molecule : "");
                 $worksheet->getCell('AP'.(6+$k))->setValue($v['nf3_quality']);
                 $worksheet->getCell('AQ'.(6+$k))->setValue($v['nf3_gwp']);
                 $worksheet->getCell('AR'.(6+$k))->setValue($v['nf3_tco2e']);
                 $worksheet->getCell('AS'.(6+$k))->setValue($total);
                 $worksheet->getCell('AT'.(6+$k))->setValue($all_total ?$total/$all_total :0);
                     }
         }

        //温室气体排放清单
        $spreadsheet->setActiveSheetIndex(6);
        $worksheet = $spreadsheet->getActiveSheet();

        //直接排放
        $indirect_total = 0;
        //间接排放
        $indirect_total = 0;
        //范围一总量
        $one_total = 0;
        //范围二总量
        $two_total = 0;
        //范围三总量
        $three_total = 0;
        //混合气体
        $co2e_one = 0;
        $co2e_two = 0;
        $co2e_three = 0;
        //二氧化碳
        $co2_one = 0;
        $co2_two = 0;
        $co2_three = 0;
        $ch4_one = 0;
        $ch4_two = 0;
        $ch4_three = 0;
        $n2o_one = 0;
        $n2o_two = 0;
        $n2o_three = 0;
        $hfcs_one = 0;
        $hfcs_two = 0;
        $hfcs_three = 0;
        $pfcs_one = 0;
        $pfcs_two = 0;
        $pfcs_three = 0;
        $sf6_one = 0;
        $sf6_two = 0;
        $sf6_three = 0;
        $nf3_one = 0;
        $nf3_two = 0;
        $nf3_three = 0;

        if ($emission_one) {
            foreach ($emission_one as $k => $v) {
                    $co2e_one += $v['co2e_factor_value']*$v['active_value_standard'];
                    $co2_one += $v['co2_factor_value']*$v['active_value_standard']*$v['co2_gwp'];
                    $ch4_one += $v['ch4_factor_value']*$v['active_value_standard']*$v['ch4_gwp'];
                    $n2o_one += $v['n2o_factor_value']*$v['active_value_standard']*$v['n2o_gwp'];
                    $hfcs_one += $v['hfcs_factor_value']*$v['active_value_standard']*$v['hfcs_gwp'];
                    $pfcs_one += $v['pfcs_factor_value']*$v['active_value_standard']*$v['pfcs_gwp'];
                    $sf6_one += $v['sf6_factor_value']*$v['active_value_standard']*$v['sf6_gwp'];
                    $nf3_one += $v['nf3_factor_value']*$v['active_value_standard']*$v['nf3_gwp'];
            }
        }
        if ($emission_two) {
            foreach ($emission_two as $v) {
                $co2e_two += $v['co2e_factor_value']*$v['active_value_standard'];
                $co2_two += $v['co2_factor_value']*$v['active_value_standard']*$v['co2_gwp'];
                $ch4_two += $v['ch4_factor_value']*$v['active_value_standard']*$v['ch4_gwp'];
                $n2o_two += $v['n2o_factor_value']*$v['active_value_standard']*$v['n2o_gwp'];
                $hfcs_two += $v['hfcs_factor_value']*$v['active_value_standard']*$v['hfcs_gwp'];
                $pfcs_two += $v['pfcs_factor_value']*$v['active_value_standard']*$v['pfcs_gwp'];
                $sf6_two += $v['sf6_factor_value']*$v['active_value_standard']*$v['sf6_gwp'];
                $nf3_two += $v['nf3_factor_value']*$v['active_value_standard']*$v['nf3_gwp'];
            }
        }
        if ($emission_three) {
            foreach ($emission_three as $v) {
                $co2e_three += $v['co2e_factor_value']*$v['active_value_standard'];
                $co2_three += $v['co2_factor_value']*$v['active_value_standard']*$v['co2_gwp'];
                $ch4_three += $v['ch4_factor_value']*$v['active_value_standard']*$v['ch4_gwp'];
                $n2o_three += $v['n2o_factor_value']*$v['active_value_standard']*$v['n2o_gwp'];
                $hfcs_three += $v['hfcs_factor_value']*$v['active_value_standard']*$v['hfcs_gwp'];
                $pfcs_three += $v['pfcs_factor_value']*$v['active_value_standard']*$v['pfcs_gwp'];
                $sf6_three += $v['sf6_factor_value']*$v['active_value_standard']*$v['sf6_gwp'];
                $nf3_three += $v['nf3_factor_value']*$v['active_value_standard']*$v['nf3_gwp'];
            }
        }

        //范围一总量
        $one_total =  $co2_one + $ch4_one + $n2o_one + $hfcs_one + $pfcs_one + $sf6_one + $nf3_one + $co2e_one;
        //范围二总量
        $two_total =  $co2_two + $ch4_two + $n2o_two + $hfcs_two + $pfcs_two + $sf6_two + $nf3_two + $co2e_two;
        //范围三总量
        $three_total = $co2_three + $ch4_three + $n2o_three + $hfcs_three + $pfcs_three + $sf6_three + $nf3_three + $co2e_three;
        //直接排放
        $direct_total = $one_total;
        //间接排放
        $indirect_total = $two_total+$three_total;
        //排放总量
        $direct_all = $direct_total+$indirect_total;

        $worksheet->getCell('B5')->setValue($direct_total);
        $worksheet->getCell('C5')->setValue($direct_all ? $direct_total/$direct_all:0);
        $worksheet->getCell('B6')->setValue($indirect_total);
        $worksheet->getCell('C6')->setValue($direct_all ? $indirect_total/$direct_all:0);
        $worksheet->getCell('B7')->setValue($direct_all);
        $worksheet->getCell('C7')->setValue($direct_all ? 1:0);

        $worksheet->getCell('B10')->setValue($one_total);
        $worksheet->getCell('C10')->setValue($direct_all ?$one_total/$direct_all : 0);
        $worksheet->getCell('B11')->setValue($two_total);
        $worksheet->getCell('C11')->setValue($direct_all ? $two_total/$direct_all : 0);
        $worksheet->getCell('B12')->setValue($three_total);
        $worksheet->getCell('C12')->setValue($direct_all ? $three_total/$direct_all : 0);
        $worksheet->getCell('B13')->setValue($direct_all);
        $worksheet->getCell('C13')->setValue($direct_all ? 1:0);

        $worksheet->getCell('B16')->setValue($co2_one);
        $worksheet->getCell('C16')->setValue($co2_two);
        $worksheet->getCell('D16')->setValue($co2_three);
        $worksheet->getCell('E16')->setValue($co2_one + $co2_two + $co2_three);

        $worksheet->getCell('B17')->setValue($ch4_one);
        $worksheet->getCell('C17')->setValue($ch4_two);
        $worksheet->getCell('D17')->setValue($ch4_three);
        $worksheet->getCell('E17')->setValue($ch4_one + $ch4_two + $ch4_three);

        $worksheet->getCell('B18')->setValue($n2o_one);
        $worksheet->getCell('C18')->setValue($n2o_two);
        $worksheet->getCell('D18')->setValue($n2o_three);
        $worksheet->getCell('E18')->setValue($n2o_one + $n2o_two + $n2o_three);

        $worksheet->getCell('B19')->setValue($hfcs_one);
        $worksheet->getCell('C19')->setValue($hfcs_two);
        $worksheet->getCell('D19')->setValue($hfcs_three);
        $worksheet->getCell('E19')->setValue($hfcs_one + $hfcs_two + $hfcs_three);

        $worksheet->getCell('B20')->setValue($pfcs_one);
        $worksheet->getCell('C20')->setValue($pfcs_two);
        $worksheet->getCell('D20')->setValue($pfcs_three);
        $worksheet->getCell('E20')->setValue($pfcs_one + $pfcs_two + $pfcs_three);

        $worksheet->getCell('B21')->setValue($sf6_one);
        $worksheet->getCell('C21')->setValue($sf6_two);
        $worksheet->getCell('D21')->setValue($sf6_three);
        $worksheet->getCell('E21')->setValue($sf6_one + $sf6_two + $sf6_three);

        $worksheet->getCell('B22')->setValue($nf3_one);
        $worksheet->getCell('C22')->setValue($nf3_two);
        $worksheet->getCell('D22')->setValue($nf3_three);
        $worksheet->getCell('E22')->setValue($nf3_one + $nf3_two + $nf3_three);

        $worksheet->getCell('B23')->setValue($co2e_one);
        $worksheet->getCell('C23')->setValue($co2e_two);
        $worksheet->getCell('D23')->setValue($co2e_three);
        $worksheet->getCell('E23')->setValue($co2e_one + $co2e_two + $co2e_three);

        $worksheet->getCell('B24')->setValue($one_total);
        $worksheet->getCell('C24')->setValue($two_total);
        $worksheet->getCell('D24')->setValue($three_total);
        $worksheet->getCell('E24')->setValue($direct_all);

        //数据分类评价
        $spreadsheet->setActiveSheetIndex(7);
        $worksheet = $spreadsheet->getActiveSheet();
        if($emission){
            //单个分类总量
            $emi_one = 0;
            $emi_two = 0;
            $emi_three = 0;

            if ($emission_one) {
                foreach ($emission_one as $v) {
                    $emi_one += $v['emissions'];
                }
            }
            if ($emission_two) {
                foreach ($emission_two as $v) {
                    $emi_two += $v['emissions'];
                }
            }
            if ($emission_three) {
                foreach ($emission_three as $v) {
                    $emi_three += $v['emissions'];
                }
            }

            //总排放量
            $emi_all = $emi_one + $emi_two + $emi_three ;
            //单个分类加权平均评分
            $emi_cate_one=0;
            $emi_cate_two=0;
            $emi_cate_three=0;

            foreach ($emission as $k=>$v){

                //占排放分类排放量的比例（%
                $emi_pro=0;
                //排放分类加权平均评分
                $emi_cate = 0;
                if($v['ghg_cate_name']=='范围一'){
                    $emi_pro=$emi_one ? $v['emissions'] / $emi_one : 0;
                   // dump($emi_pro); dump($v['factor_score']);dump($v['active_score']);exit();
                    $emi_cate = $v['factor_score']*$v['active_score']*$emi_pro;
                    $emi_cate_one +=$emi_cate;
                }elseif($v['ghg_cate_name']=='范围二'){
                    $emi_pro=$emi_two ? $v['emissions'] / $emi_two : 0;
                    $emi_cate = $v['factor_score']*$v['active_score']*$emi_pro;
                    $emi_cate_two +=$emi_cate;
                }elseif($v['ghg_cate_name']=='范围三') {
                    $emi_pro = $emi_three ? $v['emissions'] / $emi_three : 0;
                    $emi_cate = $v['factor_score'] * $v['active_score'] * $emi_pro;
                    $emi_cate_three += $emi_cate;
                }
                $emi_all_pro = 0;
                //占总排放量的比例（%）
                $emi_all_pro =  $emi_all ? $v['emissions']/$emi_all:0;
                $worksheet->getStyle('A'.(11+$k))->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $worksheet->getCell('A'.(11+$k))->setValue($v['id']);
                $worksheet->getCell('B'.(11+$k))->setValue($v['emission_name']);
                $worksheet->getCell('C'.(11+$k))->setValue($v['active']);
                $worksheet->getCell('D'.(11+$k))->setValue($v['factor_score']);
                $worksheet->getCell('E'.(11+$k))->setValue($v['active_score']);
                $worksheet->getCell('F'.(11+$k))->setValue($v['factor_score']*$v['active_score']?$v['factor_score']*$v['active_score']:"");
                $worksheet->getCell('G'.(11+$k))->setValue(number_format($emi_pro,4) ? number_format($emi_pro*100,2) .'%':"");
                $worksheet->getCell('H'.(11+$k))->setValue($v['factor_score']*$v['active_score']*$emi_pro ? number_format($v['factor_score']*$v['active_score']*$emi_pro,2):"");
                $worksheet->getCell('I'.(11+$k))->setValue(number_format($emi_all_pro,4) ? number_format($emi_all_pro*100,2) .'%':"");
                $worksheet->getCell('J'.(11+$k))->setValue(number_format($v['factor_score']*$v['active_score']*$emi_all_pro,2) ? number_format($v['factor_score']*$v['active_score']*$emi_all_pro*100,2) .'%':"");
            }
            //总加权平均分
            $emi_cate_all=$emi_cate_one+$emi_cate_two+$emi_cate_three;
            $grade_one ="";
            $grade_two ="";
            $grade_three ="";
            $grade_all="";

            if($emi_cate_one>=0&&$emi_cate_one<7){
                $grade_one ='6级';
            }elseif($emi_cate_one>=7&&$emi_cate_one<13){
                $grade_one ='5级';
            }elseif($emi_cate_one>=13&&$emi_cate_one<19){
                $grade_one ='4级';
            }elseif($emi_cate_one>=19&&$emi_cate_one<25){
                $grade_one ='3级';
            }elseif($emi_cate_one>=25&&$emi_cate_one<31){
                $grade_one ='2级';
            }elseif($emi_cate_one>=31&&$emi_cate_one<37){
                $grade_one ='1级';
            }

            if($emi_cate_two>=0&&$emi_cate_two<7){
                $grade_two ='6级';
            }elseif($emi_cate_two>=7&&$emi_cate_two<13){
                $grade_two ='5级';
            }elseif($emi_cate_two>=13&&$emi_cate_two<19){
                $grade_two ='4级';
            }elseif($emi_cate_two>=19&&$emi_cate_two<25){
                $grade_two ='3级';
            }elseif($emi_cate_two>=25&&$emi_cate_two<31){
                $grade_two ='2级';
            }elseif($emi_cate_two>=31&&$emi_cate_two<37){
                $grade_two ='1级';
            }

            if($emi_cate_three>=0&&$emi_cate_three<7){
                $grade_three ='6级';
            }elseif($emi_cate_three>=7&&$emi_cate_three<13){
                $grade_three ='5级';
            }elseif($emi_cate_three>=13&&$emi_cate_three<19){
                $grade_three ='4级';
            }elseif($emi_cate_three>=19&&$emi_cate_three<25){
                $grade_three ='3级';
            }elseif($emi_cate_three>=25&&$emi_cate_three<31){
                $grade_three ='2级';
            }elseif($emi_cate_three>=31&&$emi_cate_three<37){
                $grade_three ='1级';
            }
            if($emi_cate_all>=0&&$emi_cate_all<7){
                $grade_all ='6级';
            }elseif($emi_cate_all>=7&&$emi_cate_all<13){
                $grade_all ='5级';
            }elseif($emi_cate_all>=13&&$emi_cate_all<19){
                $grade_all ='4级';
            }elseif($emi_cate_all>=19&&$emi_cate_all<25){
                $grade_all ='3级';
            }elseif($emi_cate_all>=25&&$emi_cate_all<31){
                $grade_all ='2级';
            }elseif($emi_cate_all>=31&&$emi_cate_all<37){
                $grade_all ='1级';
            }

            $worksheet->getCell('B5')->setValue($emi_cate_one);
            $worksheet->getCell('C5')->setValue($grade_one);
            $worksheet->getCell('B6')->setValue($emi_cate_two);
            $worksheet->getCell('C6')->setValue($grade_two);
            $worksheet->getCell('B7')->setValue($emi_cate_three);
            $worksheet->getCell('C7')->setValue($grade_three);
            $worksheet->getCell('B8')->setValue($emi_cate_all);
            $worksheet->getCell('C8')->setValue($grade_all);
        }

        $writer = new Xlsx($spreadsheet);
        $writer->save($path.'/'.$file_path.'.xlsx');
        $update =  Db::table('jy_file')->where('id',$guid)->update([
            'status'=>2,
            'modify_by'=>$data_redis['userid'],
            'modify_time'=>date('Y-m-d H:i:s')
        ]);
        if($update){
            return json(['code'=>200,'message'=>'ghg清册手册生成成功']);
        }else{
            return json(['code'=>404,'message'=>'ghg清册手册生成失败']);
        }
    }

    public function isoExcel(Request $request)
    {
        ini_set('max_execution_time', '999999999');
        $data_redis = $request->middleware('data_redis');
        $main_organization_id = $data_redis['main_organization_id'];

        $id = $_GET['id'];
        $path = 'static/exc/';
        $end = '.xlsx';
        $template = 'iso';

        $template_name = $path . $template . $end;
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($template_name);

        //将填充好数据的工作表保存
        $title = '温室气体排放清册iso'. date('Y-m-d H:i:s');
        $file_path='温室气体排放清册iso'.time();
        $path= 'storage/uploads/'.date('Ymd');
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        //核算报告表
        $report = Db::table('jy_organization_calculate_report r')
            ->field('r.organization_id,r.organization_id,r.username,r.telephone,r.email,r.organization_calculate_id,
            o.name organization_name,oc.calculate_year,r.organization_id')
            ->leftJoin('jy_organization o', 'r.organization_id=o.id')
            ->leftJoin('jy_organization_calculate oc', 'r.organization_calculate_id=oc.id')
            ->where('r.id', $id)->find();
        $organization_id = $report['organization_id'];
        $guid = guid();
        $insert_id =  Db::table('jy_file')->insertGetId([
            'main_organization_id'=>$main_organization_id,
            'organization_id'=>$report['organization_id'],
            'type'=>2,
            'id'=>$guid,
            'status'=>1,
            'module'=>'组织碳核算',
            'file_path'=>'/'.$path.'/'.$file_path.'.xlsx',
            'source_name'=>$title.'.xlsx',
            'create_by'=>$data_redis['userid'],
            'modify_by'=>$data_redis['userid'],
            'create_time'=>date('Y-m-d H:i:s'),
            'modify_time'=>date('Y-m-d H:i:s')
        ]);

        //组织边界
        $boundaries = Db::table('jy_organizational_boundaries')->where('organization_id',$organization_id)->find();
        //组织基本信息表
        $one = Db::table('jy_organization_info')->where('organization_id', $organization_id)->find();

        $emission = Db::table('jy_organization_calculate_detail_emission n')
            ->field('n.id,n.iso_cate,n.iso_type,n.ghg_cate,n.calculate_model_id,
            n.emission_name,n.emissions,n.active_value,n.emissions,n.active,n.ghg_cate,
            n.ghg_type,n.active_unit,n.active_department,n.active_type,n.active_score,
            n.factor_score,n.factor_source,n.active_data,n.factor_type,n.active_value_standard,
            gh.name ghg_cate_name,g.name ghg_name,
            c.molecule,c.gases,c.co2e_factor_value,c.tco2e,c.co2_factor_value,c.co2_quality,c.co2_gwp,
            c.co2_tco2e,c.ch4_factor_value,c.ch4_quality,c.ch4_gwp,c.ch4_tco2e,c.n2o_factor_value,
            c.n2o_quality,c.n2o_gwp,c.n2o_tco2e,c.hfcs_factor_value,c.hfcs_quality,c.hfcs_gwp,c.hfcs_tco2e,
            c.pfcs_factor_value,c.pfcs_quality,c.pfcs_gwp,c.pfcs_tco2e,c.sf6_factor_value,c.sf6_quality,
            c.sf6_gwp,c.sf6_tco2e,c.nf3_factor_value,c.nf3_quality,c.nf3_gwp,c.nf3_tco2e')
            ->leftJoin('jy_ghg_template g', 'g.id=n.iso_type')
            ->leftJoin('jy_ghg_template gh', 'gh.id=n.iso_cate')
            ->leftJoin('jy_organization_calculate_count_gas c','c.emi_id=n.id')
            ->where('n.organization_calculate_id', $report['organization_calculate_id'])
            ->order('n.id asc')
            ->select()
            ->toArray();


        $emission_one = [];
        $emission_two = [];
        $emission_three = [];
        $emission_four = [];
        $emission_five = [];
        $emission_six = [];
        if ($emission) {
            foreach ($emission as $v) {

                if ($v['ghg_cate_name'] == '直接排放或清除') {
                    $emission_one[] = $v;
                };
                if ($v['ghg_cate_name'] == '能源间接排放') {
                    $emission_two[] = $v;
                };
                if ($v['ghg_cate_name'] == '运输间接排放') {
                    $emission_three[] = $v;
                };
                if ($v['ghg_cate_name'] == '外购产品或服务间接排放') {
                    $emission_four[] = $v;
                };
                if ($v['ghg_cate_name'] == '供应链下游排放') {
                    $emission_five[] = $v;
                };
                if ($v['ghg_cate_name'] == '其他间接排放') {
                    $emission_six[] = $v;
                };
            }
        }

        $spreadsheet->setActiveSheetIndex(0);
        $worksheet = $spreadsheet->getActiveSheet();


        //填充数据

        if ($report) {
            $name='';
            $production_address_detail='';
            if($one){
                $city = Db::table('jy_city')->whereIn('id', $one['production_address'])->select()->toArray();
                $name = implode(',', array_column($city, 'name'));
                $production_address_detail = $one['production_address_detail'];
            }
            $organization_desc='';
            if($boundaries){
                $organization_desc= $boundaries['organization_desc'];
            }

            $worksheet->getCell('B4')->setValue('【'.$report['organization_name'].'】');
            $worksheet->getCell('B5')->setValue('【'.$report['calculate_year'].'】');
            $worksheet->getCell('B6')->setValue('【'.$organization_desc.'】');
            $worksheet->getCell('B7')->setValue('【'.$name . '-' . $production_address_detail.'】');
            $worksheet->getCell('B8')->setValue('【'.$report['username'].'】');
            $worksheet->getCell('F8')->setValue('【'.date('Y-m-d').'】');
            $worksheet->getCell('B9')->setValue('【'.$report['telephone'].'】');
            $worksheet->getCell('F9')->setValue('【'.$report['email'].'】');


            //组织运营边界调查表
            $spreadsheet->setActiveSheetIndex(1);
            $worksheet = $spreadsheet->getActiveSheet();
            $one_count = 0;
            $two_count = 0;
            $three_count = 0;

            if ($emission) {

                $worksheet->getCell('B4')->setValue('【'.$report['organization_name'].'】');
                $worksheet->getCell('B5')->setValue('【'.$report['calculate_year'].'】');
                $worksheet->getCell('B6')->setValue('【'.$organization_desc.'】');
                $worksheet->getCell('B7')->setValue('【'.$name . '-' . $production_address_detail.'】');
                $worksheet->getCell('B8')->setValue('【'.$report['username'].'】');
                $worksheet->getCell('F8')->setValue('【'.date('Y-m-d').'】');
                $worksheet->getCell('B9')->setValue('【'.$report['telephone'].'】');
                $worksheet->getCell('F9')->setValue('【'.$report['email'].'】');
                $two_count = count($emission_two);
                $three_count = count($emission_three);
                $one_count = count($emission_one);
                $worksheet->getStyle('A11')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $worksheet->getStyle('A'.(11+$one_count))->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $worksheet->getStyle('A1')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $worksheet->getStyle('A1')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                if (count($emission_one) > 0) {
                    $worksheet->mergeCells('A11:'.'A'.(11+$one_count-1));
                    $worksheet->mergeCells('B11:'.'B'.(11+$one_count-1));
                    $worksheet->getCell('A11')->setValue('直接排放');
                    $worksheet->getCell('B11')->setValue('直接排放或清除');
                    foreach ($emission_one as $k => $v) {
                        $worksheet->getRowDimension((11+$k))->setRowHeight(30);
                        $worksheet->mergeCells('C'.(11+$k).':D'.(11+$k));
                        $worksheet->mergeCells('E'.(11+$k).':F'.(11+$k));
                        $worksheet->getCell('C' . (11 + $k))->setValue($v['ghg_name']);
                        $worksheet->getCell('E' . (11 + $k))->setValue($v['emission_name']);
                        $worksheet->getCell('G' . (11 + $k))->setValue($v['active']);
                    }
                }

                if (count($emission_two) > 0) {
                    $worksheet->mergeCells('A'.(11+$one_count).':'.'A'.(11+$one_count+$two_count+$three_count-1));
                    $worksheet->mergeCells('B'.(11+$one_count).':'.'b'.(11+$one_count+$two_count-1));
                    $worksheet->getCell('A' . (11 + $one_count))->setValue('间接排放');
                    $worksheet->getCell('B' . (11 + $one_count))->setValue('能源间接排放');
                    foreach ($emission_two as $k => $v) {
                        $worksheet->getRowDimension((11+$one_count+$k))->setRowHeight(30);
                        $worksheet->mergeCells('C'.(11+$one_count+$k).':D'.(11+$one_count+$k));
                        $worksheet->mergeCells('E'.(11+$one_count+$k).':F'.(11+$one_count+$k));
                        $worksheet->getCell('C' . (11 + $one_count + $k))->setValue($v['ghg_name']);
                        $worksheet->getCell('E' . (11 + $one_count + $k))->setValue($v['emission_name']);
                        $worksheet->getCell('G' . (11 + $one_count + $k))->setValue($v['active']);
                    }
                }
                if (count($emission_three) > 0) {
                    if(count($emission_two)==0){
                        $worksheet->mergeCells('A'.(11+$one_count).':'.'A'.(11+$one_count+$two_count+$three_count-1));
                        $worksheet->getCell('A' . (11 + $one_count))->setValue('间接排放');
                    }
                    $worksheet->mergeCells('B'.(11+$one_count+$two_count).':'.'b'.(11+$one_count+$two_count+$three_count-1));
                    $worksheet->getCell('B' . (11 + $one_count + $two_count))->setValue('运输间接排放');
                    foreach ($emission_three as $k => $v) {
                        $worksheet->getRowDimension((11+$one_count+$k+$two_count))->setRowHeight(30);
                        $worksheet->mergeCells('C'.(11+$one_count+$k+$two_count).':D'.(11+$one_count+$k+$two_count));
                        $worksheet->mergeCells('E'.(11+$one_count+$k+$two_count).':F'.(11+$one_count+$k+$two_count));
                        $worksheet->getCell('C' . (11 + $one_count + $two_count + $k))->setValue($v['ghg_name']);
                        $worksheet->getCell('E' . (11 + $one_count + $two_count + $k))->setValue($v['emission_name']);
                        $worksheet->getCell('G' . (11 + $one_count + $two_count + $k))->setValue($v['active']);
                    }
                }
            }

            //排放源识别表
            $spreadsheet->setActiveSheetIndex(2);
            $worksheet = $spreadsheet->getActiveSheet();
            if ($emission) {
                foreach ($emission as $k => $v) {
                    $gas = '';
                    $gas_name = '';
                    $unit_two = Db::table('jy_unit')->field('name')->where('id', $v['active_unit'][2])->find();
                    $active_type = Config::get('emission.active_type')[$v['active_type'] - 1]['name'];

                    $v['gas_name']= $gas_name;
                    $worksheet->getStyle('A'.(5+$k))->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                    $worksheet->mergeCells('F'.(5+$k).':G'.(5+$k));
                    $worksheet->getCell('A' . (5 + $k))->setValue($v['id']);
                    $worksheet->getCell('B' . (5 + $k))->setValue($v['emission_name']);
                    $worksheet->getCell('C' . (5 + $k))->setValue($v['active']);
                    $worksheet->getCell('D' . (5 + $k))->setValue($v['ghg_cate_name']);
                    $worksheet->getCell('E' . (5 + $k))->setValue($v['active_department']);
                    $worksheet->getCell('F' . (5 + $k))->setValue($v['gases']);
                }
            }

            //活动数据收集表
            $spreadsheet->setActiveSheetIndex(3);
            $worksheet = $spreadsheet->getActiveSheet();
            $four_use=[];
            if ($emission) {
                foreach ($emission as $k => $v) {
                    $unit_two = '';
                    $active_type='';
                    $unit_two = Db::table('jy_unit')->field('name')->where('id', $v['active_unit'][2])->find();
                    $active_type = Config::get('emission.active_type')[$v['active_type'] - 1]['name'];
                    $v['unit_two_name'] = $unit_two['name'];
                    $worksheet->getStyle('A'.(5+$k))->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                    $worksheet->getCell('A' . (5 + $k))->setValue($v['id']);
                    $worksheet->getCell('B' . (5 + $k))->setValue($v['emission_name']);
                    $worksheet->getCell('C' . (5 + $k))->setValue($v['active']);
                    $worksheet->getCell('D' . (5 + $k))->setValue($v['ghg_cate_name']);
                    $worksheet->getCell('E' . (5 + $k))->setValue($v['active_value']);
                    $worksheet->getCell('F' . (5 + $k))->setValue( $v['unit_two_name']);
                    $worksheet->getCell('G' . (5 + $k))->setValue($active_type);
                    $worksheet->getCell('H' . (5 + $k))->setValue($v['active_score']);
                    $worksheet->getCell('I' . (5 + $k))->setValue($v['active_data']);
                    $worksheet->getCell('J' . (5 + $k))->setValue($v['active_department']);
                    $worksheet->getCell('K' . (5 + $k))->setValue($v['gases']);
                }
            }
            //排放因子选择表
            $spreadsheet->setActiveSheetIndex(4);
            $worksheet = $spreadsheet->getActiveSheet();

            if ($emission) {
                foreach ($emission as $k => $v) {
                   if($v['molecule']){
                       $molecule= Config::get('emission.molecule')[$v['molecule']-1]['name'];
                   }else{
                       $molecule= "";
                   }

                    $factor_type_one='';
                    $worksheet->getStyle('A'.(5+$k))->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                  //  $worksheet->getDefaultRowDimension(5+$k)->setRowHeight(40);
                    $factor_type_one = Config::get('emission.factor_type')[$v['factor_type'] - 1]['name'];
                    $worksheet->getCell('A' . (5 + $k))->setValue($v['id']);
                    $worksheet->getCell('B' . (5 + $k))->setValue($v['emission_name']);
                    $worksheet->getCell('C' . (5 + $k))->setValue($v['active']);
                    $worksheet->getCell('D' . (5 + $k))->setValue($v['gases']);
                    $worksheet->getCell('E' . (5 + $k))->setValue($v['co2e_factor_value']);
                    $worksheet->getCell('F' . (5 + $k))->setValue($v['co2e_factor_value']?$molecule:"");
                    $worksheet->getCell('G' . (5 + $k))->setValue($v['co2_factor_value']);
                    $worksheet->getCell('H' . (5 + $k))->setValue($v['co2_factor_value'] ?$molecule:"");
                    $worksheet->getCell('I' . (5 + $k))->setValue($v['ch4_factor_value']);
                    $worksheet->getCell('J' . (5 + $k))->setValue( $v['ch4_factor_value'] ? $molecule:"");
                    $worksheet->getCell('K' . (5 + $k))->setValue($v['n2o_factor_value']);
                    $worksheet->getCell('L' . (5 + $k))->setValue($v['n2o_factor_value'] ? $molecule:"");
                    $worksheet->getCell('M' . (5 + $k))->setValue($v['hfcs_factor_value']);
                    $worksheet->getCell('N' . (5 + $k))->setValue($v['hfcs_factor_value'] ?$molecule :"");
                    $worksheet->getCell('O' . (5 + $k))->setValue($v['pfcs_factor_value']);
                    $worksheet->getCell('P' . (5 + $k))->setValue($v['pfcs_factor_value'] ? $molecule:"");
                    $worksheet->getCell('Q' . (5 + $k))->setValue($v['sf6_factor_value']);
                    $worksheet->getCell('R' . (5 + $k))->setValue($v['sf6_factor_value'] ? $molecule:"");
                    $worksheet->getCell('S' . (5 + $k))->setValue($v['nf3_factor_value']);
                    $worksheet->getCell('T' . (5 + $k))->setValue($v['nf3_factor_value'] ?$molecule:"");
                    $worksheet->getCell('U' . (5 + $k))->setValue($factor_type_one);
                    $worksheet->getCell('V' . (5 + $k))->setValue($v['factor_score']);
                    $worksheet->getCell('W' . (5 + $k))->setValue($v['factor_source']);
                }
            }
        }
        //排放量计算表
        $spreadsheet->setActiveSheetIndex(5);
        $worksheet = $spreadsheet->getActiveSheet();

        if($emission) {
            $all_total=0;
            foreach ($emission as $k => $v) {
                $total=0;
                $total=  $v['co2e_factor_value']*$v['active_value_standard']+$v['co2_factor_value']*$v['active_value_standard']*$v['co2_gwp']+
                    $v['ch4_factor_value']*$v['active_value_standard']*$v['ch4_gwp']+$v['n2o_factor_value']*$v['active_value_standard']*$v['n2o_gwp']+
                    $v['hfcs_factor_value']*$v['active_value_standard']*$v['hfcs_gwp']+$v['pfcs_factor_value']*$v['active_value_standard']*$v['pfcs_gwp']+
                    $v['sf6_factor_value']*$v['active_value_standard']*$v['sf6_gwp']+$v['nf3_factor_value']*$v['active_value_standard']*$v['nf3_gwp'];
                $all_total +=$total;
            }
            foreach ($emission as $k => $v) {
                $unit = '';
                if($v['molecule']){
                    $molecule= Config::get('emission.molecule')[$v['molecule']-1]['name'];
                }else{
                    $molecule= "";
                }

                $unit = Db::table('jy_unit')->field('name')->where('id',$v['active_unit'][2])->find();
                $total=0;
                $total=  $v['co2e_factor_value']*$v['active_value_standard']+$v['co2_factor_value']*$v['active_value_standard']*$v['co2_gwp']+
                    $v['ch4_factor_value']*$v['active_value_standard']*$v['ch4_gwp']+$v['n2o_factor_value']*$v['active_value_standard']*$v['n2o_gwp']+
                    $v['hfcs_factor_value']*$v['active_value_standard']*$v['hfcs_gwp']+$v['pfcs_factor_value']*$v['active_value_standard']*$v['pfcs_gwp']+
                    $v['sf6_factor_value']*$v['active_value_standard']*$v['sf6_gwp']+$v['nf3_factor_value']*$v['active_value_standard']*$v['nf3_gwp'];
                $worksheet->getStyle('A'.(6+$k))->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $worksheet->getCell('A'.(6+$k))->setValue($v['id']);
                $worksheet->getCell('B'.(6+$k))->setValue($v['emission_name']);
                $worksheet->getCell('C'.(6+$k))->setValue($v['active']);
                $worksheet->getCell('D'.(6+$k))->setValue($v['ghg_cate_name']);
                $worksheet->getCell('E'.(6+$k))->setValue($v['active_value']);
                $worksheet->getCell('F'.(6+$k))->setValue($unit['name']);
                $worksheet->getCell('G'.(6+$k))->setValue($v['co2e_factor_value']);
                $worksheet->getCell('H'.(6+$k))->setValue($v['co2e_factor_value'] ? $molecule :"");
                $worksheet->getCell('I'.(6+$k))->setValue($v['co2e_factor_value']);
                $worksheet->getCell('J'.(6+$k))->setValue($v['co2_factor_value']);
                $worksheet->getCell('K'.(6+$k))->setValue($v['co2_factor_value'] ? $molecule:"");
                $worksheet->getCell('L'.(6+$k))->setValue($v['co2_quality']);
                $worksheet->getCell('M'.(6+$k))->setValue($v['co2_gwp']);
                $worksheet->getCell('N'.(6+$k))->setValue($v['co2_tco2e']);
                $worksheet->getCell('O'.(6+$k))->setValue($v['ch4_factor_value']);
                $worksheet->getCell('P'.(6+$k))->setValue($v['ch4_factor_value'] ? $molecule:"");
                $worksheet->getCell('Q'.(6+$k))->setValue($v['ch4_quality']);
                $worksheet->getCell('R'.(6+$k))->setValue($v['ch4_gwp']);
                $worksheet->getCell('S'.(6+$k))->setValue($v['ch4_tco2e']);
                $worksheet->getCell('T'.(6+$k))->setValue($v['n2o_factor_value']);
                $worksheet->getCell('U'.(6+$k))->setValue($v['n2o_factor_value'] ? $molecule:"");
                $worksheet->getCell('V'.(6+$k))->setValue($v['n2o_quality']);
                $worksheet->getCell('W'.(6+$k))->setValue($v['n2o_gwp']);
                $worksheet->getCell('X'.(6+$k))->setValue($v['n2o_tco2e']);
                $worksheet->getCell('Y'.(6+$k))->setValue($v['hfcs_factor_value']);
                $worksheet->getCell('Z'.(6+$k))->setValue($v['hfcs_factor_value'] ? $molecule:"");
                $worksheet->getCell('AA'.(6+$k))->setValue($v['hfcs_quality']);
                $worksheet->getCell('AB'.(6+$k))->setValue($v['hfcs_gwp']);
                $worksheet->getCell('AC'.(6+$k))->setValue($v['hfcs_tco2e']);
                $worksheet->getCell('AD'.(6+$k))->setValue($v['pfcs_factor_value']);
                $worksheet->getCell('AE'.(6+$k))->setValue($v['pfcs_factor_value'] ? $molecule:"");
                $worksheet->getCell('AF'.(6+$k))->setValue($v['pfcs_quality']);
                $worksheet->getCell('AG'.(6+$k))->setValue($v['pfcs_gwp']);
                $worksheet->getCell('AH'.(6+$k))->setValue($v['pfcs_tco2e']);
                $worksheet->getCell('AI'.(6+$k))->setValue($v['sf6_factor_value']);
                $worksheet->getCell('AJ'.(6+$k))->setValue($v['sf6_factor_value'] ? $molecule:"");
                $worksheet->getCell('AK'.(6+$k))->setValue($v['sf6_quality']);
                $worksheet->getCell('AL'.(6+$k))->setValue($v['sf6_gwp']);
                $worksheet->getCell('AM'.(6+$k))->setValue($v['sf6_tco2e']);
                $worksheet->getCell('AN'.(6+$k))->setValue($v['nf3_factor_value']);
                $worksheet->getCell('AO'.(6+$k))->setValue($v['nf3_factor_value'] ? $molecule:"");
                $worksheet->getCell('AP'.(6+$k))->setValue($v['nf3_quality']);
                $worksheet->getCell('AQ'.(6+$k))->setValue($v['nf3_gwp']);
                $worksheet->getCell('AR'.(6+$k))->setValue($v['nf3_tco2e']);
                $worksheet->getCell('AS'.(6+$k))->setValue($total);
                $worksheet->getCell('AT'.(6+$k))->setValue($all_total?$total/$all_total:0);
            }
        }

        //温室气体排放清单
        $spreadsheet->setActiveSheetIndex(6);
        $worksheet = $spreadsheet->getActiveSheet();
        $co2e_one = 0;
        $co2e_two = 0;
        $co2e_three = 0;
        $co2e_four = 0;
        $co2e_five = 0;
        $co2e_six = 0;

        //二氧化碳
        $co2_one = 0;
        $co2_two = 0;
        $co2_three = 0;
        $co2_four = 0;
        $co2_five = 0;
        $co2_six = 0;

        $ch4_one = 0;
        $ch4_two = 0;
        $ch4_three = 0;
        $ch4_four = 0;
        $ch4_five = 0;
        $ch4_six = 0;

        $n2o_one = 0;
        $n2o_two = 0;
        $n2o_three = 0;
        $n2o_four = 0;
        $n2o_five = 0;
        $n2o_six = 0;

        $hfcs_one = 0;
        $hfcs_two = 0;
        $hfcs_three = 0;
        $hfcs_four = 0;
        $hfcs_five = 0;
        $hfcs_six = 0;

        $pfcs_one = 0;
        $pfcs_two = 0;
        $pfcs_three = 0;
        $pfcs_four = 0;
        $pfcs_five = 0;
        $pfcs_six = 0;

        $sf6_one = 0;
        $sf6_two = 0;
        $sf6_three = 0;
        $sf6_four = 0;
        $sf6_five = 0;
        $sf6_six = 0;

        $nf3_one = 0;
        $nf3_two = 0;
        $nf3_three = 0;
        $nf3_four = 0;
        $nf3_five = 0;
        $nf3_six = 0;


        if ($emission_one) {
                foreach ($emission_three as $v) {
                    $co2e_one += $v['co2e_factor_value']*$v['active_value_standard'];
                    $co2_one += $v['co2_factor_value']*$v['active_value_standard']*$v['co2_gwp'];
                    $ch4_one += $v['ch4_factor_value']*$v['active_value_standard']*$v['ch4_gwp'];
                    $n2o_one += $v['n2o_factor_value']*$v['active_value_standard']*$v['n2o_gwp'];
                    $hfcs_one += $v['hfcs_factor_value']*$v['active_value_standard']*$v['hfcs_gwp'];
                    $pfcs_one += $v['pfcs_factor_value']*$v['active_value_standard']*$v['pfcs_gwp'];
                    $sf6_one += $v['sf6_factor_value']*$v['active_value_standard']*$v['sf6_gwp'];
                    $nf3_one += $v['nf3_factor_value']*$v['active_value_standard']*$v['nf3_gwp'];
                }
        }
        if ($emission_two) {
            foreach ($emission_two as $v) {
                $co2e_two += $v['co2e_factor_value']*$v['active_value_standard'];
                $co2_two += $v['co2_factor_value']*$v['active_value_standard']*$v['co2_gwp'];
                $ch4_two += $v['ch4_factor_value']*$v['active_value_standard']*$v['ch4_gwp'];
                $n2o_two += $v['n2o_factor_value']*$v['active_value_standard']*$v['n2o_gwp'];
                $hfcs_two += $v['hfcs_factor_value']*$v['active_value_standard']*$v['hfcs_gwp'];
                $pfcs_two += $v['pfcs_factor_value']*$v['active_value_standard']*$v['pfcs_gwp'];
                $sf6_two += $v['sf6_factor_value']*$v['active_value_standard']*$v['sf6_gwp'];
                $nf3_two += $v['nf3_factor_value']*$v['active_value_standard']*$v['nf3_gwp'];
            }
        }
        if ($emission_three) {
            foreach ($emission_three as $v) {
                $co2e_three += $v['co2e_factor_value']*$v['active_value_standard'];
                $co2_three += $v['co2_factor_value']*$v['active_value_standard']*$v['co2_gwp'];
                $ch4_three += $v['ch4_factor_value']*$v['active_value_standard']*$v['ch4_gwp'];
                $n2o_three += $v['n2o_factor_value']*$v['active_value_standard']*$v['n2o_gwp'];
                $hfcs_three += $v['hfcs_factor_value']*$v['active_value_standard']*$v['hfcs_gwp'];
                $pfcs_three += $v['pfcs_factor_value']*$v['active_value_standard']*$v['pfcs_gwp'];
                $sf6_three += $v['sf6_factor_value']*$v['active_value_standard']*$v['sf6_gwp'];
                $nf3_three += $v['nf3_factor_value']*$v['active_value_standard']*$v['nf3_gwp'];
            }
        }
        if ($emission_four) {
            foreach ($emission_four as $v) {
                $co2e_four += $v['co2e_factor_value']*$v['active_value_standard'];
                $co2_four += $v['co2_factor_value']*$v['active_value_standard']*$v['co2_gwp'];
                $ch4_four += $v['ch4_factor_value']*$v['active_value_standard']*$v['ch4_gwp'];
                $n2o_four += $v['n2o_factor_value']*$v['active_value_standard']*$v['n2o_gwp'];
                $hfcs_four += $v['hfcs_factor_value']*$v['active_value_standard']*$v['hfcs_gwp'];
                $pfcs_four += $v['pfcs_factor_value']*$v['active_value_standard']*$v['pfcs_gwp'];
                $sf6_four += $v['sf6_factor_value']*$v['active_value_standard']*$v['sf6_gwp'];
                $nf3_four += $v['nf3_factor_value']*$v['active_value_standard']*$v['nf3_gwp'];
            }
        }
        if ($emission_five) {
            foreach ($emission_five as $v) {
                $co2e_five += $v['co2e_factor_value']*$v['active_value_standard'];
                $co2_five += $v['co2_factor_value']*$v['active_value_standard']*$v['co2_gwp'];
                $ch4_five += $v['ch4_factor_value']*$v['active_value_standard']*$v['ch4_gwp'];
                $n2o_five += $v['n2o_factor_value']*$v['active_value_standard']*$v['n2o_gwp'];
                $hfcs_five += $v['hfcs_factor_value']*$v['active_value_standard']*$v['hfcs_gwp'];
                $pfcs_five += $v['pfcs_factor_value']*$v['active_value_standard']*$v['pfcs_gwp'];
                $sf6_five += $v['sf6_factor_value']*$v['active_value_standard']*$v['sf6_gwp'];
                $nf3_five += $v['nf3_factor_value']*$v['active_value_standard']*$v['nf3_gwp'];
            }
        }
        if ($emission_six) {
            foreach ($emission_six as $v) {
                $co2e_six += $v['co2e_factor_value']*$v['active_value_standard'];
                $co2_six += $v['co2_factor_value']*$v['active_value_standard']*$v['co2_gwp'];
                $ch4_six += $v['ch4_factor_value']*$v['active_value_standard']*$v['ch4_gwp'];
                $n2o_six += $v['n2o_factor_value']*$v['active_value_standard']*$v['n2o_gwp'];
                $hfcs_six += $v['hfcs_factor_value']*$v['active_value_standard']*$v['hfcs_gwp'];
                $pfcs_six += $v['pfcs_factor_value']*$v['active_value_standard']*$v['pfcs_gwp'];
                $sf6_six += $v['sf6_factor_value']*$v['active_value_standard']*$v['sf6_gwp'];
                $nf3_six += $v['nf3_factor_value']*$v['active_value_standard']*$v['nf3_gwp'];
            }
        }
        $one_total = 0;
        $two_total = 0;
        $three_total = 0;
        $four_total=0;
        $five_total=0;
        $six_total=0;
        $direct_total=0;
        $indirect_total=0;
        //直接排放或清除
        $one_total =  $co2_one + $ch4_one + $n2o_one + $hfcs_one + $pfcs_one + $sf6_one + $nf3_one + $co2e_one;
        //能源间接排放
        $two_total =  $co2_two + $ch4_two + $n2o_two + $hfcs_two + $pfcs_two + $sf6_two + $nf3_two + $co2e_two;
        //运输间接排放
        $three_total=$co2_three + $ch4_three + $n2o_three + $hfcs_three + $pfcs_three + $sf6_three + $nf3_three + $co2e_three;
        //外购产品或服务间接排放
        $four_total=$co2_four + $ch4_four + $n2o_four + $hfcs_four + $pfcs_four + $sf6_four + $nf3_four + $co2e_four;
        //供应链下游间接排放
        $five_total=$co2_five + $ch4_five + $n2o_five + $hfcs_five + $pfcs_five + $sf6_five + $nf3_five + $co2e_five;
        //其他间接排放
        $six_total=$co2_six + $ch4_six + $n2o_six + $hfcs_six + $pfcs_six + $sf6_six + $nf3_six + $co2e_six;
        //直接温室气体排放
        $direct_total = $one_total;
        //间接温室气体排放
        $indirect_total = $two_total+$three_total;
        //排放总量
        $direct_all = $direct_total+$indirect_total;
        //c16排放总量
        $all = $one_total+$two_total+$three_total+$four_total+$five_total+$six_total;
        //h19排放总量
        $co2_all = $co2_one + $co2_two + $co2_three + $co2_four + $co2_five + $co2_six;
        $ch4_all = $ch4_one + $ch4_two + $ch4_three + $ch4_four + $ch4_five + $ch4_six;
        $n2o_all = $n2o_one + $n2o_two + $n2o_three+$n2o_four + $n2o_five + $n2o_six;
        $hfcs_all= $hfcs_one + $hfcs_two + $hfcs_three + $hfcs_four + $hfcs_five + $hfcs_six;
        $pfcs_all = $pfcs_one + $pfcs_two + $pfcs_three + $pfcs_four + $pfcs_five + $pfcs_six;
        $sf6_all= $sf6_one + $sf6_two + $sf6_three + $sf6_four + $sf6_five + $sf6_six;
        $nf3_all= $nf3_one + $nf3_two + $nf3_three + $nf3_four + $nf3_five + $nf3_six;
        $co2e_all= $co2e_one + $co2e_two + $co2e_three + $co2e_four + $co2e_five + $co2e_six;
        //c27总计
        $vertical_one_all = $co2_one + $ch4_one + $n2o_one + $hfcs_one + $pfcs_one + $sf6_one + $nf3_one + $co2e_one;
        $vertical_two_all = $co2_two + $ch4_two + $n2o_two + $hfcs_two + $pfcs_two + $sf6_two + $nf3_two + $co2e_two;
        $vertical_three_all = $co2_three + $ch4_three + $n2o_three + $hfcs_three + $pfcs_three + $sf6_three + $nf3_three + $co2e_three;
        $vertical_four_all = $co2_four + $ch4_four + $n2o_four + $hfcs_four + $pfcs_four + $sf6_four + $nf3_four + $co2e_four;
        $vertical_five_all = $co2_five + $ch4_five + $n2o_five + $hfcs_five + $pfcs_five + $sf6_five + $nf3_five + $co2e_five;
        $vertical_six_all =$co2_six + $ch4_six + $n2o_six + $hfcs_six + $pfcs_six + $sf6_six + $nf3_six + $co2e_six;


        $worksheet->getCell('B5')->setValue($direct_total);
        $worksheet->getCell('C5')->setValue($direct_all ? $direct_total/$direct_all:0);
        $worksheet->getCell('B6')->setValue($indirect_total);
        $worksheet->getCell('C6')->setValue($direct_all ? $indirect_total/$direct_all:0);
        $worksheet->getCell('B7')->setValue($direct_all);
        $worksheet->getCell('C7')->setValue($direct_all ? 1:0);

        $worksheet->getCell('B10')->setValue($one_total);
        $worksheet->getCell('C10')->setValue($all ?$one_total/$all : 0);
        $worksheet->getCell('B11')->setValue($two_total);
        $worksheet->getCell('C11')->setValue($all ? $two_total/$all : 0);
        $worksheet->getCell('B12')->setValue($three_total);
        $worksheet->getCell('C12')->setValue($all ? $three_total/$all : 0);
        $worksheet->getCell('B13')->setValue($four_total);
        $worksheet->getCell('C13')->setValue($all ? $four_total/$all : 0);
        $worksheet->getCell('B14')->setValue($five_total);
        $worksheet->getCell('C14')->setValue($all ? $five_total/$all : 0);
        $worksheet->getCell('B15')->setValue($six_total);
        $worksheet->getCell('C15')->setValue($all ? $six_total/$all : 0);
        $worksheet->getCell('B16')->setValue($all);
        $worksheet->getCell('C16')->setValue($all ? 1 : 0);


        $worksheet->getCell('B19')->setValue($co2_one);
        $worksheet->getCell('C19')->setValue($co2_two);
        $worksheet->getCell('D19')->setValue($co2_three);
        $worksheet->getCell('E19')->setValue($co2_four);
        $worksheet->getCell('F19')->setValue($co2_five);
        $worksheet->getCell('G19')->setValue($co2_six);
        $worksheet->getCell('H19')->setValue($co2_all);

        $worksheet->getCell('B20')->setValue($ch4_one);
        $worksheet->getCell('C20')->setValue($ch4_two);
        $worksheet->getCell('D20')->setValue($ch4_three);
        $worksheet->getCell('E20')->setValue($ch4_four);
        $worksheet->getCell('F20')->setValue($ch4_five);
        $worksheet->getCell('G20')->setValue($ch4_six);
        $worksheet->getCell('H20')->setValue($ch4_all);

        $worksheet->getCell('B21')->setValue($n2o_one);
        $worksheet->getCell('C21')->setValue($n2o_two);
        $worksheet->getCell('D21')->setValue($n2o_three);
        $worksheet->getCell('E21')->setValue($n2o_four);
        $worksheet->getCell('F21')->setValue($n2o_five);
        $worksheet->getCell('G21')->setValue($n2o_six);
        $worksheet->getCell('H21')->setValue($n2o_all);

        $worksheet->getCell('B22')->setValue($hfcs_one);
        $worksheet->getCell('C22')->setValue($hfcs_two);
        $worksheet->getCell('D22')->setValue($hfcs_three);
        $worksheet->getCell('E22')->setValue($hfcs_four);
        $worksheet->getCell('F22')->setValue($hfcs_five);
        $worksheet->getCell('G22')->setValue($hfcs_six);
        $worksheet->getCell('H22')->setValue($hfcs_all);

        $worksheet->getCell('B23')->setValue($pfcs_one);
        $worksheet->getCell('C23')->setValue($pfcs_two);
        $worksheet->getCell('D23')->setValue($pfcs_three);
        $worksheet->getCell('E23')->setValue($pfcs_four);
        $worksheet->getCell('F23')->setValue($pfcs_five);
        $worksheet->getCell('G23')->setValue($pfcs_six);
        $worksheet->getCell('H23')->setValue($pfcs_all);

        $worksheet->getCell('B24')->setValue($sf6_one);
        $worksheet->getCell('C24')->setValue($sf6_two);
        $worksheet->getCell('D24')->setValue($sf6_three);
        $worksheet->getCell('E24')->setValue($sf6_four);
        $worksheet->getCell('F24')->setValue($sf6_five);
        $worksheet->getCell('G24')->setValue($sf6_six);
        $worksheet->getCell('H24')->setValue($sf6_all);

        $worksheet->getCell('B25')->setValue($nf3_one);
        $worksheet->getCell('C25')->setValue($nf3_two);
        $worksheet->getCell('D25')->setValue($nf3_three);
        $worksheet->getCell('E25')->setValue($nf3_four);
        $worksheet->getCell('F25')->setValue($nf3_five);
        $worksheet->getCell('G25')->setValue($nf3_six);
        $worksheet->getCell('H25')->setValue($nf3_all);

        $worksheet->getCell('B26')->setValue($co2e_one);
        $worksheet->getCell('C26')->setValue($co2e_two);
        $worksheet->getCell('D26')->setValue($co2e_three);
        $worksheet->getCell('E26')->setValue($co2e_four);
        $worksheet->getCell('F26')->setValue($co2e_five);
        $worksheet->getCell('G26')->setValue($co2e_six);
        $worksheet->getCell('H26')->setValue($co2e_all);

        $worksheet->getCell('B27')->setValue($vertical_one_all);
        $worksheet->getCell('C27')->setValue($vertical_two_all);
        $worksheet->getCell('D27')->setValue($vertical_three_all);
        $worksheet->getCell('E27')->setValue($vertical_four_all);
        $worksheet->getCell('F27')->setValue($vertical_five_all);
        $worksheet->getCell('G27')->setValue($vertical_six_all);

        $worksheet->getCell('H27')->setValue($vertical_one_all+$vertical_two_all+$vertical_three_all+$vertical_four_all+$vertical_five_all+$vertical_six_all);

        //数据分类评价
        $spreadsheet->setActiveSheetIndex(7);
        $worksheet = $spreadsheet->getActiveSheet();
        if($emission){
            //单个分类排放总量
            $emi_one = 0;
            $emi_two = 0;
            $emi_three = 0;
            $emi_four = 0;
            $emi_five = 0;
            $emi_six = 0;

            if ($emission_one) {
                foreach ($emission_one as $v) {
                    $emi_one += $v['emissions'];
                }
            }
            if ($emission_two) {
                foreach ($emission_two as $v) {
                    $emi_two += $v['emissions'];
                }
            }
            if ($emission_three) {
                foreach ($emission_three as $v) {
                    $emi_three += $v['emissions'];
                }
            }
            if ($emission_four) {
                foreach ($emission_four as $v) {
                    $emi_four += $v['emissions'];
                }
            }
            if ($emission_five) {
                foreach ($emission_five as $v) {
                    $emi_five += $v['emissions'];
                }
            }
            if ($emission_six) {
                foreach ($emission_six as $v) {
                    $emi_six += $v['emissions'];
                }
            }
            //总排放量
            $emi_all = $emi_one + $emi_two + $emi_three + $emi_four + $emi_five + $emi_six;
           //单个分类总加权平均分
            $emi_cate_one=0;
            $emi_cate_two=0;
            $emi_cate_three=0;
            $emi_cate_four=0;
            $emi_cate_five=0;
            $emi_cate_six=0;

            foreach ($emission as $k=>$v){

               //占排放分类排放量的比例（%）
                $emi_pro=0;
                //排放分类加权平均评分
                $emi_cate = 0;
                if($v['ghg_cate_name'] == '直接排放或清除'){
                    $emi_pro=$emi_one ? $v['emissions'] / $emi_one : 0;
                    $emi_cate = $v['factor_score']*$v['active_score']*$emi_pro;
                    $emi_cate_one +=$emi_cate;
                }elseif($v['ghg_cate_name'] == '能源间接排放'){
                    $emi_pro=$emi_two ? $v['emissions'] / $emi_two : 0;
                    $emi_cate = $v['factor_score']*$v['active_score']*$emi_pro;
                    $emi_cate_two +=$emi_cate;
                }elseif($v['ghg_cate_name'] == '运输间接排放'){
                    $emi_pro=$emi_three ? $v['emissions'] / $emi_three : 0;
                    $emi_cate = $v['factor_score']*$v['active_score']*$emi_pro;
                    $emi_cate_three +=$emi_cate;
                }elseif($v['ghg_cate_name'] == '外购产品或服务间接排放'){
                    $emi_pro=$emi_four ? $v['emissions'] / $emi_four : 0;
                    $emi_cate = $v['factor_score']*$v['active_score']*$emi_pro;
                    $emi_cate_four +=$emi_cate;
                }elseif($v['ghg_cate_name'] == '供应链下游排放'){
                    $emi_pro=$emi_five ? $v['emissions'] / $emi_five : 0;
                    $emi_cate = $v['factor_score']*$v['active_score']*$emi_pro;
                    $emi_cate_five +=$emi_cate;
                }elseif($v['ghg_cate_name'] == '其他间接排放'){
                    $emi_pro=$emi_six ? $v['emissions'] / $emi_six : 0;
                    $emi_cate = $v['factor_score']*$v['active_score']*$emi_pro;
                    $emi_cate_six +=$emi_cate;
                }

                $emi_all_pro = 0;
                $emi_all_pro =  $emi_all ? $v['emissions']/$emi_all:0;
                $worksheet->getStyle('A'.(14+$k))->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $worksheet->getCell('A'.(14+$k))->setValue($v['id']);
                $worksheet->getCell('B'.(14+$k))->setValue($v['emission_name']);
                $worksheet->getCell('C'.(14+$k))->setValue($v['active']);
                $worksheet->getCell('D'.(14+$k))->setValue($v['factor_score']);
                $worksheet->getCell('E'.(14+$k))->setValue($v['active_score']);
                $worksheet->getCell('F'.(14+$k))->setValue($v['factor_score']*$v['active_score']);
                $worksheet->getCell('G'.(14+$k))->setValue(number_format($emi_pro,4)?number_format($emi_pro*100,2).'%':"");
                $worksheet->getCell('H'.(14+$k))->setValue(number_format($v['factor_score']*$v['active_score']*$emi_pro,2)?number_format($v['factor_score']*$v['active_score']*$emi_pro,2):"");
                $worksheet->getCell('I'.(14+$k))->setValue(number_format($emi_all_pro,4)?number_format($emi_all_pro*100,2).'%':"");
                $worksheet->getCell('J'.(14+$k))->setValue(number_format($v['factor_score']*$v['active_score']*$emi_all_pro,2)?number_format($v['factor_score']*$v['active_score']*$emi_all_pro,2):"");
            }
            //总加权平均分
            $emi_cate_all=$emi_cate_one+$emi_cate_two+$emi_cate_three+$emi_cate_four+$emi_cate_five+$emi_cate_six;
           //单个加权平均分等级
            $grade_one ="";
            $grade_two ="";
            $grade_three ="";
            $grade_four ="";
            $grade_five ="";
            $grade_six ="";
            $grade_all="";
            if($emi_cate_one>=0&&$emi_cate_one<7){
                $grade_one ='6级';
            }elseif($emi_cate_one>=7&&$emi_cate_one<13){
                $grade_one ='5级';
            }elseif($emi_cate_one>=13&&$emi_cate_one<19){
                $grade_one ='4级';
            }elseif($emi_cate_one>=19&&$emi_cate_one<25){
                $grade_one ='3级';
            }elseif($emi_cate_one>=25&&$emi_cate_one<31){
                $grade_one ='2级';
            }elseif($emi_cate_one>=31&&$emi_cate_one<37){
                $grade_one ='1级';
            }
            if($emi_cate_two>=0&&$emi_cate_two<7){
                $grade_two ='6级';
            }elseif($emi_cate_two>=7&&$emi_cate_two<13){
                $grade_two ='5级';
            }elseif($emi_cate_two>=13&&$emi_cate_two<19){
                $grade_two ='4级';
            }elseif($emi_cate_two>=19&&$emi_cate_two<25){
                $grade_two ='3级';
            }elseif($emi_cate_two>=25&&$emi_cate_two<31){
                $grade_two ='2级';
            }elseif($emi_cate_two>=31&&$emi_cate_two<37){
                $grade_two ='1级';
            }
            if($emi_cate_three>=0&&$emi_cate_three<7){
                $grade_three ='6级';
            }elseif($emi_cate_three>=7&&$emi_cate_three<13){
                $grade_three ='5级';
            }elseif($emi_cate_three>=13&&$emi_cate_three<19){
                $grade_three ='4级';
            }elseif($emi_cate_three>=19&&$emi_cate_three<25){
                $grade_three ='3级';
            }elseif($emi_cate_three>=25&&$emi_cate_three<31){
                $grade_three ='2级';
            }elseif($emi_cate_three>=31&&$emi_cate_three<37){
                $grade_three ='1级';
            }
            if($emi_cate_four>=0&&$emi_cate_four<7){
                $grade_four ='6级';
            }elseif($emi_cate_four>=7&&$emi_cate_four<13){
                $grade_four ='5级';
            }elseif($emi_cate_four>=13&&$emi_cate_four<19){
                $grade_four ='4级';
            }elseif($emi_cate_four>=19&&$emi_cate_four<25){
                $grade_four ='3级';
            }elseif($emi_cate_four>=25&&$emi_cate_four<31){
                $grade_four ='2级';
            }elseif($emi_cate_four>=31&&$emi_cate_four<37){
                $grade_four ='1级';
            }
            if($emi_cate_five>=0&&$emi_cate_five<7){
                $grade_five ='6级';
            }elseif($emi_cate_five>=7&&$emi_cate_five<13){
                $grade_five ='5级';
            }elseif($emi_cate_five>=13&&$emi_cate_five<19){
                $grade_five ='4级';
            }elseif($emi_cate_five>=19&&$emi_cate_five<25){
                $grade_five ='3级';
            }elseif($emi_cate_five>=25&&$emi_cate_five<31){
                $grade_five ='2级';
            }elseif($emi_cate_five>=31&&$emi_cate_five<37){
                $grade_five ='1级';
            }
            if($emi_cate_six>=0&&$emi_cate_six<7){
                $grade_six ='6级';
            }elseif($emi_cate_two>=7&&$emi_cate_six<13){
                $grade_six ='5级';
            }elseif($emi_cate_six>=13&&$emi_cate_six<19){
                $grade_six ='4级';
            }elseif($emi_cate_six>=19&&$emi_cate_six<25){
                $grade_six ='3级';
            }elseif($emi_cate_six>=25&&$emi_cate_six<31){
                $grade_six ='2级';
            }elseif($emi_cate_six>=31&&$emi_cate_six<37){
                $grade_six ='1级';
            }
            if($emi_cate_all>=0&&$emi_cate_all<7){
                $grade_all ='6级';
            }elseif($emi_cate_all>=7&&$emi_cate_all<13){
                $grade_all ='5级';
            }elseif($emi_cate_all>=13&&$emi_cate_all<19){
                $grade_all ='4级';
            }elseif($emi_cate_all>=19&&$emi_cate_all<25){
                $grade_all ='3级';
            }elseif($emi_cate_all>=25&&$emi_cate_all<31){
                $grade_all ='2级';
            }elseif($emi_cate_all>=31&&$emi_cate_all<37){
                $grade_all ='1级';
            }
            $worksheet->getCell('B5')->setValue($emi_cate_one);
            $worksheet->getCell('C5')->setValue($grade_one);
            $worksheet->getCell('B6')->setValue($emi_cate_two);
            $worksheet->getCell('C6')->setValue($grade_two);
            $worksheet->getCell('B7')->setValue($emi_cate_three);
            $worksheet->getCell('C7')->setValue($grade_three);
            $worksheet->getCell('B8')->setValue($emi_cate_four);
            $worksheet->getCell('C8')->setValue($grade_four);
            $worksheet->getCell('B9')->setValue($emi_cate_five);
            $worksheet->getCell('C9')->setValue($grade_five);
            $worksheet->getCell('B10')->setValue($emi_cate_six);
            $worksheet->getCell('C10')->setValue($grade_six);
            $worksheet->getCell('B11')->setValue($emi_cate_all);
            $worksheet->getCell('C11')->setValue($grade_all);
        }



        $writer = new Xlsx($spreadsheet);
        $writer->save($path.'/'.$file_path.'.xlsx');
       $update =  Db::table('jy_file')->where('id',$guid)->update([
            'status'=>2,
            'modify_by'=>$data_redis['userid'],
            'modify_time'=>date('Y-m-d H:i:s')
        ]);
       if($update){
           return json(['code'=>200,'message'=>'iso清册手册生成成功']);
       }else{
           return json(['code'=>404,'message'=>'iso清册手册生成失败']);
       }

    }

}