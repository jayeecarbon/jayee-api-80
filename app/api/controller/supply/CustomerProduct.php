<?php

namespace app\api\controller\supply;

use app\BaseController;
use app\model\admin\FileModel;
use app\model\supply\CustomerProductDischargeModel;
use app\model\supply\CustomerProductModel;
use app\model\supply\FactorTypeModel;
use app\controller\system\File;
use app\model\supply\SupplierModel;
use app\model\system\OperationModel;
use app\validate\CustomerProductValidate;
use think\exception\ValidateException;
use think\facade\Db;
use think\Request;

class CustomerProduct extends BaseController
{
    /**
     * 查看的是采购者的产品信息
     */

    /**
     * @notes 客户产品列表
     * @author fengweizhe
     */
    public function index(Request $request) {
        $data_redis = $request->middleware('data_redis');
        $product_name = isset($_GET['product_name']) ? $_GET['product_name'] : '';
        $product_type = isset($_GET['product_type']) ? $_GET['product_type'] : '';
        $supplier_name = isset($_GET['supplier_name']) ? $_GET['supplier_name'] : '';

        // 从产品碳足迹选择供应产品
        if (isset($_GET['state'])) {
            $audit_state = $_GET['state'];
        // 从供应链查看供应产品列表
        } elseif (isset($_GET['audit_state'])) {
            $audit_state = $_GET['audit_state'];
        } else {
            $audit_state = '';
        }

        $auth = isset($_GET['auth']) ? $_GET['auth'] : '';
        $supplier_id = isset($_GET['supplier_id']) ? $_GET['supplier_id'] : '';
        $customer_id = isset($data_redis['main_organization_id']) ? $data_redis['main_organization_id'] : '';
        $page_size = isset($_GET['pageSize']) ?$_GET['pageSize'] : CustomerProductModel::pageSize;
        $page_index = isset($_GET['pageIndex']) ? $_GET['pageIndex'] : CustomerProductModel::pageIndex;
        $list = CustomerProductModel::getList($page_size, $page_index, ['product_name'=>$product_name, 'product_type'=>$product_type,
            'supplier_name'=>$supplier_name, 'audit_state'=>$audit_state, 'auth'=>$auth, 'customer_id'=>$customer_id, 'supplier_id'=>$supplier_id])->toArray();

        $audit_state_map = CustomerProductModel::AUDIT_STATE_MAP;
        $auth_map = CustomerProductModel::AUTH_MAP;
        $list_new = [];
        foreach ($list['data'] as $k => $v) {

            if (isset($audit_state_map[$v['audit_state']])) {
                $v['audit_state_name'] = $audit_state_map[$v['audit_state']];
            } else {
                $v['audit_state_name'] = '';
            }
            if (isset($auth_map[$v['auth']])) {
                $v['auth_name'] = $auth_map[$v['auth']];
            } else {
                $v['auth_name'] = '';
            }

            $list_new[$k] = $v;
        }

        unset($list['data']);
        $data['code'] = 200;
        $data['data']['list'] = $list_new;
        $data['data']['audit_state'] = CustomerProductModel::AUDIT_STATE_SELECT_MAP;
        $data['data']['auth'] = CustomerProductModel::AUTH_SELECT_MAP;
        $data['data']['total'] = $list['total'];

        return json($data);
    }

    /**
     * @notes 客户产品列表-产品详情
     * @author fengweizhe
     */
    public function info() {
        $id = isset($_GET['id']) ? $_GET['id'] : '';
        $customer_product_info = CustomerProductModel::getDataById($id);
        if ($customer_product_info == null) {
            return json(['code'=>201, 'message'=>"未查询到相客户产品相关信息"]);
        }

        // 第三方认证
        $auth_map = CustomerProductModel::AUTH_MAP;
        if (isset($auth_map[$customer_product_info['auth']])) {
            $customer_product_info['auth_name'] = $auth_map[$customer_product_info['auth']];
        } else {
            $customer_product_info['auth_name'] = '';
        }

        //把文件的相关信息补全
        $file_url = $customer_product_info['file_url'];
        $customer_product_info['files'] = [];
        if ($file_url != '') {
            $file_url_arr = explode(',', $file_url);
            foreach ($file_url_arr as $k => $v) {
                $file_data = FileModel::getInfoById($v);
                $customer_product_info['files'][] = $file_data;
            }
        }
        //根据产品id查询碳核算信息
        $discharge_info = CustomerProductDischargeModel::getDataByProductId($customer_product_info['id'])->toArray();
        $type_id_arr = array_column($discharge_info, 'type_id');
        $type_id_arr = array_unique($type_id_arr);
        $factor_map = FactorTypeModel::getList();
        $factor_map_new = [];
        foreach ($factor_map as $k => $v) {
            $factor_map_new[$v['id']] = $v['name'];
        }
        //封装排放源结构
        $discharge_info_new = [];
        foreach ($type_id_arr as $kk => $vv) {
            $arr['type_name'] = $factor_map_new[$vv];
            $tmp = [];
            foreach ($discharge_info as $kkk => $vvv) {
                $vvv['unit'] = (float)$vvv;
                $vvv['unit_type'] = (float)$vvv;
                if ($vvv['type_id'] == $vv) {
                    $tmp[] = $vvv;
                }
            }
            $arr['datas'] = $tmp;
            $discharge_info_new[] = $arr;
        }

        if (!empty($discharge_info)) {
            $customer_product_info['discharge'] = $discharge_info_new;
        } else {
            $customer_product_info['discharge'] = [];
        }

        $data['code'] = 200;
        $data['data'] = $customer_product_info;

        return json($data);
    }

    /**
     * @notes 客户产品新增
     * @author fengweizhe
     */
    public function add(Request $request) {
        if (request()->isPost() && $this->validateForm() === true) {
            $params_payload = $this->req_payload;
            $params_payload_arr = json_decode($params_payload, true);
            $data_redis = $request->middleware('data_redis');
            $data['product_name'] = $params_payload_arr['product_name'];
            $data['product_model'] = $params_payload_arr['product_model'];
            $data['product_type'] = $params_payload_arr['product_type'];
            $data['supplier_id'] = $params_payload_arr['supplier_id'];
            $data['audit_state'] = CustomerProductModel::AUDIT_STATE_EDITING; // 新建产品后填报状态为编辑中
            $data['create_by'] = isset($data_redis['userid']) ? $data_redis['userid'] : '';
            $data['modify_by'] = isset($data_redis['userid']) ? $data_redis['userid'] : '';
            $data['customer_id'] = isset($data_redis['main_organization_id']) ? $data_redis['main_organization_id'] : '';
            $data['create_time'] = date('Y-m-d H:i:s');
            $data['modify_time'] = date('Y-m-d H:i:s');

            // 验证选择的供应商，当前产品（产品名称 + 产品编号）是否存在
            if (self::checkProduct($data['supplier_id'], $data['product_name'], $data['product_model'])) {
                $add = CustomerProductModel::addCustomerProduct($data);
            } else {
                return json(['code'=>201, 'message'=>"产品已存在，请重新输入"]);
            }

            if ($add) {
                // 添加操作日志
                $data_log['main_organization_id'] = $data_redis['main_organization_id'];
                $data_log['user_id'] = $data_redis['userid'];
                $data_log['module'] = '供应链碳管理';
                $data_log['type'] = '功能操作';
                $data_log['time'] = date('Y-m-d H:i:s');
                $data_log['url'] = $request->pathinfo();
                $data_log['log'] = '新增产品：' . $data['product_name'];
                OperationModel::addOperation($data_log);

                return json(['code'=>200, 'message'=>"添加成功"]);
            } else {
                return json(['code'=>201, 'message'=>"添加失败"]);
            }
        } else {
            return json(['code'=>404, 'message'=>$this->validateForm()]);
        }
    }

    /**
     * @notes 客户产品编辑
     * @author fengweizhe
     */
    public function edit(Request $request) {
        if (request()->isPost() && $this->validateForm() === true) {
            $params_payload = $this->req_payload;
            $params_payload_arr = json_decode($params_payload, true);
            $data_redis = $request->middleware('data_redis');
            $data['id'] = $params_payload_arr['id'];
            $data['product_name'] = $params_payload_arr['product_name'];
            $data['product_model'] = $params_payload_arr['product_model'];
            $data['product_type'] = $params_payload_arr['product_type'];
            $data['supplier_id'] = $params_payload_arr['supplier_id'];
            $data['modify_by'] = isset($data_redis['userid']) ? $data_redis['userid'] : '';
            $data['create_time'] = date('Y-m-d H:i:s');
            $data['modify_time'] = date('Y-m-d H:i:s');

            Db::startTrans();
            try {
                // 验证选择的供应商，当前产品（产品名称 + 产品编号）是否存在
                if (self::checkProduct($data['supplier_id'], $data['product_name'], $data['product_model'], $data['id'])) {
                    CustomerProductModel::editCustomerProduct($data);
                } else {
                    return json(['code'=>201, 'message'=>"产品已存在，请重新输入"]);
                }

                CustomerProductModel::updateAuditState($data['id'], CustomerProductModel::AUDIT_STATE_EDITING); // 未填报状态的产品支持进行编辑，编辑后将变成编辑中状态

                // 添加操作日志
                $data_log['main_organization_id'] = $data_redis['main_organization_id'];
                $data_log['user_id'] = $data_redis['userid'];
                $data_log['module'] = '供应链碳管理';
                $data_log['type'] = '功能操作';
                $data_log['time'] = date('Y-m-d H:i:s');
                $data_log['url'] = $request->pathinfo();
                $data_log['log'] = '编辑产品：' . $data['product_name'];
                OperationModel::addOperation($data_log);

                Db::commit();
                return json(['code'=>200, 'message'=>"编辑成功"]);
            } catch (\Exception $e) {
                Db::rollback();
                return json(['code'=>201, 'message'=>"编辑失败"]);
            }

        } else {
            return json(['code'=>404, 'message'=>$this->validateForm()]);
        }
    }

    /**
     * @notes 客户产品删除
     * @author fengweizhe
     */
    public function del(Request $request) {
        $data_redis = $request->middleware('data_redis');
        $params_payload = $this->req_payload;
        $params_payload_arr = json_decode($params_payload, true);
        $data['id'] = isset($params_payload_arr['id']) ? $params_payload_arr['id'] : '';
        if ($data['id'] == '' || $data['id'] == null || $data['id'] == 0) {
            return json(['code'=>201, 'message'=>"参数id错误"]);
        }

        $data['is_del'] = CustomerProductModel::IS_DEL_DEL;
        $del = CustomerProductModel::delCustomerProduct($data);
        $product_data = CustomerProductModel::getDataById($data['id']);

        if ($del) {
            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '供应链碳管理';
            $data_log['type'] = '功能操作';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $request->pathinfo();
            $data_log['log'] = '删除产品：' . $product_data['product_name'];
            OperationModel::addOperation($data_log);
        
            return json(['code'=>200, 'message'=>"删除成功"]);
        } else {
            return json(['code'=>201, 'message'=>"删除失败"]);
        }

    }

    /**
     * @notes 客户产品启用/禁用
     * @author fengweizhe
     */
    public function state(Request $request) {
        $data_redis = $request->middleware('data_redis');
        $params_payload = $this->req_payload;
        $params_payload_arr = json_decode($params_payload, true);
        $data['id'] = isset($params_payload_arr['id']) ? $params_payload_arr['id'] : '';
        if ($data['id'] == '' || $data['id'] == null || $data['id'] == 0) {
            return json(['code'=>201, 'message'=>"参数id错误"]);
        }
        $data['state'] = isset($params_payload_arr['state']) ? $params_payload_arr['state'] : '';
        if ($data['state'] == '' || $data['state'] == null || $data['state'] == 0) {
            return json(['code'=>201, 'message'=>"参数state错误"]);
        }
        if (!in_array($data['state'], [CustomerProductModel::STATE_YES, CustomerProductModel::STATE_NO])) {
            return json(['code'=>201, 'message'=>"参数state错误"]);
        }
        if ($data['state'] == CustomerProductModel::STATE_YES) {
            $data['audit_state'] = CustomerProductModel::AUDIT_STATE_VERIFY;
        } else {
            $data['audit_state'] = CustomerProductModel::AUDIT_STATE_NOT_USE;
        }

        CustomerProductModel::updateAuditState($data['id'], $data['audit_state']);
        $product_data = CustomerProductModel::getDataById($data['id']);
        $state_select_map = SupplierModel::STATE_SELECT_MAP;

        // 添加操作日志
        $data_log['main_organization_id'] = $data_redis['main_organization_id'];
        $data_log['user_id'] = $data_redis['userid'];
        $data_log['module'] = '供应链碳管理';
        $data_log['type'] = '功能操作';
        $data_log['time'] = date('Y-m-d H:i:s');
        $data_log['url'] = $request->pathinfo();
        $data_log['log'] = '变更产品：' . $product_data['product_name'] . '的状态' . '为：' . $state_select_map[$data['state'] - 1]['name'];
        OperationModel::addOperation($data_log);

        if ($data['state'] == CustomerProductModel::STATE_YES) {
            return json(['code'=>200, 'message'=>"启用成功"]);
        } else {
            return json(['code'=>200, 'message'=>"禁用成功"]);
        }
    }

    /**
     * @notes 客户产品excel上传
     * 
     * @author wuyinghua
     * @return array|Json
     */
    public function import(Request $request) {

        Db::startTrans();
        try {
            $data_redis = $request->middleware('data_redis');
            $files = $request->file("file");
            $data = File::importExcel($files);

            foreach ($data as $key => $val){
                $newData[$key] = [
                    'product_name'  => $val['A'],
                    'product_model' => $val['B'],
                    'product_type'  => $val['C'],
                    'customer_id'   => isset($data_redis['main_organization_id']) ? $data_redis['main_organization_id'] : '',
                    'audit_state'   => CustomerProductModel::AUDIT_STATE_EDITING,
                    'create_by'     => isset($data_redis['userid']) ? $data_redis['userid'] : '',
                    'modify_by'     => isset($data_redis['userid']) ? $data_redis['userid'] : '',
                    'create_time'   => date('Y-m-d H:i:s'),
                    'modify_time'   => date('Y-m-d H:i:s'),
                ];

                $combineData[] = $val['A'] . $val['B'];
            }

            // 导入数据中重复数据以最后一条为准
            foreach ($combineData as $key => $val) {
                unset($combineData[$key]);
                if(in_array($val, $combineData)) {
                    unset($newData[$key]);
                }
            }

            $result = CustomerProductModel::addCustomerProducts($newData);
            if (!$result) {
                throw new \Exception('导入数据失败!');
            }

            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '供应链碳管理';
            $data_log['type'] = '功能操作';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $request->pathinfo();
            $data_log['log'] = '导入产品';
            OperationModel::addOperation($data_log);

            // 提交事务
            Db::commit();
            return json(['code'=>200, 'message'=>'文件上传成功，已经导入' . $result . '件产品']);
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            return json(['code'=>404, 'message'=>'导入数据失败，' . $e->getMessage()]);
        }
    }

    /**
     * @notes  报送审批列表
     * @author fengweizhe
     */
    public function auditList(Request $request) {
        $data_redis = $request->middleware('data_redis');
        $product_name = isset($_GET['product_name']) ? $_GET['product_name'] : '';
        $supplier_name = isset($_GET['supplier_name']) ? $_GET['supplier_name'] : '';
        $audit_state = CustomerProductModel::AUDIT_STATE_NOT_VERIFY;
        $customer_id = isset($data_redis['main_organization_id']) ? $data_redis['main_organization_id'] : '';
        $page_size = isset($_GET['pageSize']) ?$_GET['pageSize'] : CustomerProductModel::pageSize;
        $page_index = isset($_GET['pageIndex']) ? $_GET['pageIndex'] : CustomerProductModel::pageIndex;
        $list = CustomerProductModel::getAuditList($page_size, $page_index, ['product_name'=>$product_name,
            'supplier_name'=>$supplier_name, 'audit_state'=>$audit_state, 'customer_id'=>$customer_id])->toArray();
        $list_new = [];
        $auth_map = CustomerProductModel::AUTH_MAP;
        foreach ($list['data'] as $k => $v) {
            if (isset($auth_map[$v['auth']])) {
                $v['auth_name'] = $auth_map[$v['auth']];
            } else {
                $v['auth_name'] = '';
            }
            $list_new[$k] = $v;
        }

        $data['code'] = 200;
        $data['data']['list'] = $list_new;
        $data['data']['total'] = $list['total'];

        return json($data);
    }

    /**
     * @notes 客户产品审核/驳回
     * @author fengweizhe
     */
    public function auditState(Request $request) {
        $data_redis = $request->middleware('data_redis');
        $params_payload = $this->req_payload;
        $params_payload_arr = json_decode($params_payload, true);
        $data['id'] = isset($params_payload_arr['id']) ? $params_payload_arr['id'] : '';
        if ($data['id'] == '' || $data['id'] == null || $data['id'] == 0) {
            return json(['code'=>201, 'message'=>"参数id错误"]);
        }

        $data['audit_state'] = isset($params_payload_arr['audit_state']) ? $params_payload_arr['audit_state'] : '';
        if ($data['audit_state'] == '' || $data['audit_state'] == null || $data['audit_state'] == 0) {
            return json(['code'=>201, 'message'=>"参数audit_state错误"]);
        }

        if (!in_array($data['audit_state'], [CustomerProductModel::AUDIT_STATE_VERIFY, CustomerProductModel::AUDIT_STATE_EDITING])) {
            return json(['code'=>201, 'message'=>"参数audit_state错误"]);
        }

        // 获取当前产品数据
        $customer_product = CustomerProductModel::getDataById($data['id']);

        $update_product_data['id'] = $data['id'];
        $update_product_data['auth'] = '';
        $update_product_data['week_start'] = '';
        $update_product_data['week_end'] = '';
        $update_product_data['number'] = '';
        $update_product_data['unit_type'] = '';
        $update_product_data['unit'] = '';
        $update_product_data['emissions'] = '';
        $update_product_data['coefficient'] = '';

        // 添加操作日志
        $data_log['main_organization_id'] = $data_redis['main_organization_id'];
        $data_log['user_id'] = $data_redis['userid'];
        $data_log['module'] = '供应链碳管理';
        $data_log['type'] = '功能操作';
        $data_log['time'] = date('Y-m-d H:i:s');
        $data_log['url'] = $request->pathinfo();

        Db::startTrans();
        try {
            CustomerProductModel::updateAuditState($data['id'], $data['audit_state']);
            if ($data['audit_state'] == CustomerProductModel::AUDIT_STATE_VERIFY) {

                // 验证选择的供应商，当前产品（产品名称 + 产品编号）是否存在，存在将在产品型号后+1
                if (!self::checkProduct($customer_product['supplier_id'], $customer_product['product_name'], $customer_product['product_model'], $data['id'])) {
                    CustomerProductModel::updateProductModel($customer_product['id'], $customer_product['product_model'] . '+1');
                }

                // 审核通过后显示在供应产品管理列表中
                CustomerProductModel::updateState($data['id'], CustomerProductModel::STATE_YES);

                $data_log['log'] = '审核通过，产品：' . $customer_product['product_name'];
                OperationModel::addOperation($data_log);

                Db::commit();
                return json(['code'=>200, 'message'=>"审核成功"]);
            } else {
                // 驳回后清空采购者供应产品表碳排放信息
                CustomerProductModel::editCustomerProduct($update_product_data);
                CustomerProductDischargeModel::delProductDischarge($data['id']);

                $data_log['log'] = '审核驳回，产品：'. $customer_product['product_name'];
                OperationModel::addOperation($data_log);

                Db::commit();
                return json(['code'=>200, 'message'=>"驳回成功"]);
            }

        } catch (\Exception $e) {
            Db::rollback();
            return json(['code'=>201, 'message'=>$e->getMessage().$e->getLine()]);
        }
    }

    /**
     * @notes 客户产品要求填报
     * @author fengweizhe
     */
    public function requestFill(Request $request) {
        $data_redis = $request->middleware('data_redis');
        $params_payload = $this->req_payload;
        $params_payload_arr = json_decode($params_payload, true);
        $data['id'] = isset($params_payload_arr['id']) ? $params_payload_arr['id'] : '';
        if ($data['id'] == '' || $data['id'] == null || $data['id'] == 0) {
            return json(['code'=>201, 'message'=>"参数id错误"]);
        }

        $data['audit_state'] = isset($params_payload_arr['audit_state']) ? $params_payload_arr['audit_state'] : '';
        if ($data['audit_state'] == '' || $data['audit_state'] == null || $data['audit_state'] == 0) {
            return json(['code'=>201, 'message'=>"参数audit_state错误"]);
        }

        if (!in_array($data['audit_state'], [CustomerProductModel::AUDIT_STATE_NOT_FILL])) {
            return json(['code'=>201, 'message'=>"参数audit_state错误"]);
        }

        $update = CustomerProductModel::updateAuditState($data['id'], $data['audit_state']);
        $customer_product = CustomerProductModel::getDataById($data['id']);
        if ($customer_product['supplier_id'] == NULL) {
            return json(['code'=>201, 'message'=>"请先选择需要填报的供应商"]);
        }

        if ($update) {
            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '供应链碳管理';
            $data_log['type'] = '功能操作';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $request->pathinfo();
            $data_log['log'] = '要求填报产品：' . $customer_product['product_name'];
            OperationModel::addOperation($data_log);

            return json(['code'=>200, 'message'=>"要求填报成功"]);
        } else {
            return json(['code'=>404, 'message'=>"要求填报失败"]);
        }

    }

    /**
     * @notes 获取产品添加时的表单信息
     *
     * @author wuyinghua
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function addForm(Request $request) {
        $data_redis = $request->middleware('data_redis');
        $main_id = isset($data_redis['main_organization_id']) ? $data_redis['main_organization_id'] : '';
        $supplier_list = SupplierModel::getCustomerProductAddFormData($main_id);
        $data['code'] = 200;
        $data['data']['list'] = $supplier_list;

        return json($data);
    }

    /**
     * checkProduct 验证产品
     * 
     * @author wuyinghua
     * @param $supplier_id
     * @param $product_name
     * @param $product_model
     * @param $id
	 * @return void
     */
    public function checkProduct($supplier_id, $product_name, $product_model, $id = NULL) {
        $list = CustomerProductModel::getAllProducts($supplier_id, $id)->toArray();

        $combine_products = [];
        foreach ($list as $key => $value) {
            $combine_products[$key] = $value['product_name'] . '-' . $value['product_model'];
        }

        $combine_product = $product_name . '-' . $product_model;

        if (in_array($combine_product, $combine_products)) {
            return false;
        } else {
            return  true;
        }
    }

    /**
     * validateForm 验证
     *
     * @author wuyinghua
     * @return void
     */
    protected function validateForm() {
        $data = request()->param();

        try {
            validate(CustomerProductValidate::class)->check($data);

            return  true;
        } catch (ValidateException $e) {

            return $e->getError();
        }
    }
}