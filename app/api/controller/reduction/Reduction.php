<?php
namespace app\api\controller\reduction;

use app\BaseController;
use app\model\reduction\ReductionModel;
use app\model\system\OperationModel;
use app\validate\ReductionValidate;
use Psr\Log\NullLogger;
use think\facade\Config;
use think\facade\Db;
use think\Request;


/**
 * Reduction
 * 减排场景管理
 * by njf
 */
class Reduction extends BaseController {

    //======================================================================
    // PUBLIC FUNCTIONS
    //======================================================================

    /**
     * index 减排场景列表
     *
     * @return void
     */
    public function index(Request $request) {
        $data_redis = $request->middleware('data_redis');
        $main_organization_id = $data_redis['main_organization_id'];

        $page_size = isset($_GET['pageSize']) ? $_GET['pageSize'] : '10';
        $page_index = isset($_GET['pageIndex']) ? $_GET['pageIndex'] : '1';

        $filters = [
            'name' => isset($_GET['name']) ? $_GET['name'] : '',
            'organization_id' => isset($_GET['organization_id']) ? $_GET['organization_id'] : '',
            'main_organization_id'=>$main_organization_id
        ];

        $db = new ReductionModel();
        $list = $db->getReductionList($page_size,$page_index,$filters);

        //组织列表
        $organizationList = $db->organizationList($main_organization_id);
        //分子单位
        $molecule = Config::get('emission.molecule');
        //分母单位
        $denominator = Db::table('jy_unit_type')
            ->field('id,unit_name')
            ->order('id asc')->select()->toArray();
        foreach ($denominator as $k=>$v){
            $unit_type = Db::table('jy_unit')
                ->field('id,name unit_name,type_id unit_id')
                ->where('type_id',$v['id'])
                ->order('id asc')->select()->toArray();
            $v['children'] = $unit_type;
            $denominator[$k]=$v;
        }


        $data['code'] = 200;
        $data['data']['list'] = $list;
        $data['data']['organizationList']=$organizationList;
        $data['data']['molecule']=$molecule;
        $data['data']['denominator']=$denominator;

        return json($data);
    }

    /**
     * add 添加
     *
     * @return void
     */
    public function add(Request $request) {

        if (request()->isPost() && $this->validateForm() === true) {
            $params_payload = $this->req_payload;
            $params_payload_arr = json_decode($params_payload, true);

            $data_redis = $request->middleware('data_redis');
            $data['name'] = $params_payload_arr['name'];
            $data['main_organization_id'] = $data_redis['main_organization_id'];
            $data['organization_id'] = $params_payload_arr['organization_id'];
            $data['type'] =  $params_payload_arr['type'];
            $data['desc'] = $params_payload_arr['desc'];

            if($data['type']=='1'){
                //总减排
                $data['molecule'] =NULL;
                $data['denominator'] = NULL;
                $data['total_desc'] =  $params_payload_arr['total_desc'];
                $data['reduction_type'] = $params_payload_arr['reduction_type'];
                if($params_payload_arr['reduction_type']==1){
                    $data['total_reduction_low'] = $params_payload_arr['total_reduction_low'] ?$params_payload_arr['total_reduction_low']:NULL;
                    $data['total_reduction_high']=NULL;
                }else{
                    $data['total_reduction_low'] = $params_payload_arr['total_reduction_low'] ?$params_payload_arr['total_reduction_low']:NULL;
                    $data['total_reduction_high'] = $params_payload_arr['total_reduction_high']?$params_payload_arr['total_reduction_high']:NULL ;
                        if($params_payload_arr['total_reduction_low']>$params_payload_arr['total_reduction_high']){
                            $datasmg['code'] = 404;
                            $datasmg['message'] = "总减排量最大值须大于总减排量最小值";
                            return json($datasmg);
                        }
                }

                $data['total_unit'] =  $params_payload_arr['total_unit'] ?$params_payload_arr['total_unit']:Null;
                $data['unit_desc'] = NULL;
                $data['unit_type'] =NULL;
                $data['unit_low'] = NULL;
                $data['unit_high'] = NULL;
            }elseif($data['type']=='2'){
                //单位减排
                $data['molecule'] = $params_payload_arr['molecule'] ?$params_payload_arr['molecule']:NULL;
                $data['denominator'] =  $params_payload_arr['denominator'];
                $data['total_desc'] = NULL;
                $data['reduction_type'] = NULL;
                $data['total_reduction_low'] = NULL;
                $data['total_reduction_high'] =NULL;
                $data['total_unit'] = NULL;
                $data['unit_desc'] = $params_payload_arr['unit_desc'];
                $data['unit_type'] = $params_payload_arr['unit_type'];
                if($params_payload_arr['unit_type']==1){
                    $data['unit_low'] = $params_payload_arr['unit_low'] ?$params_payload_arr['unit_low']:NULL;
                    $data['unit_high'] =NULL;
                }else{
                    $data['unit_low'] = $params_payload_arr['unit_low'] ?$params_payload_arr['unit_low']:NULL;
                    $data['unit_high'] = $params_payload_arr['unit_high'] ?$params_payload_arr['unit_high']:NULL;
                    if($params_payload_arr['unit_low']>$params_payload_arr['unit_high']){
                        $datasmg['code'] = 404;
                        $datasmg['message'] = "单位减排量最大值须大于单位减排量最小值";
                        return json($datasmg);
                    }
                }

            }elseif($data['type']=='1,2'|| $data['type']=='2,1'){
                //2个都选
                $data['molecule'] = $params_payload_arr['molecule'] ?$params_payload_arr['molecule']:NULL;
                $data['denominator'] =  $params_payload_arr['denominator'];
                $data['total_desc'] =  $params_payload_arr['total_desc'];
                $data['reduction_type'] = $params_payload_arr['reduction_type'];

                $data['total_unit'] =  $params_payload_arr['total_unit'] ?$params_payload_arr['total_unit']:Null;
                $data['unit_desc'] = $params_payload_arr['unit_desc'];
                $data['unit_type'] = $params_payload_arr['unit_type'];
                if($params_payload_arr['reduction_type']==1){
                    $data['total_reduction_low'] = $params_payload_arr['total_reduction_low'] ?$params_payload_arr['total_reduction_low']:NULL;
                    $data['total_reduction_high']=NULL;
                }else{
                    $data['total_reduction_low'] = $params_payload_arr['total_reduction_low'] ?$params_payload_arr['total_reduction_low']:NULL;
                    $data['total_reduction_high'] = $params_payload_arr['total_reduction_high']?$params_payload_arr['total_reduction_high']:NULL ;
                    if($params_payload_arr['total_reduction_low']>$params_payload_arr['total_reduction_high']){
                        $datasmg['code'] = 404;
                        $datasmg['message'] = "总减排量最大值须大于总减排量最小值";
                        return json($datasmg);
                    }
                }
                if($params_payload_arr['unit_type']==1){
                    $data['unit_low'] = $params_payload_arr['unit_low'] ?$params_payload_arr['unit_low']:NULL;
                    $data['unit_high']=NULL;
                }else{
                    $data['unit_low'] = $params_payload_arr['unit_low'] ?$params_payload_arr['unit_low']:NULL;
                    $data['unit_high'] = $params_payload_arr['unit_high'] ?$params_payload_arr['unit_high']:NULL;
                    if($params_payload_arr['unit_low']>$params_payload_arr['unit_high']){
                        $datasmg['code'] = 404;
                        $datasmg['message'] = "单位减排量最大值须大于单位减排量最小值";
                        return json($datasmg);
                    }
                }
            }

            $data['create_by'] = $data_redis['userid'];
            $data['modify_by'] = $data_redis['userid'];
            $data['create_time'] = date('Y-m-d H:i:s');
            $data['modify_time'] = date('Y-m-d H:i:s');


            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '减排场景管理';
            $data_log['type'] = '系统操作';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $request->pathinfo();
            $data_log['log'] = '新建减排场景名称：' . $data['name'];

            OperationModel::addOperation($data_log);

            $add = ReductionModel::add($data);

            if ($add) {
                $datasmg['code'] = 200;
                $datasmg['message'] = "添加成功";
            } else {
                $datasmg['code'] = 404;
                $datasmg['message'] = "添加失败";
            }

        } else {
            $datasmg['code'] = 404;
            $datasmg['message'] = $this->validateForm();
        }

        return json($datasmg);
    }
    /**
     * edit 修改
     *
     * @return void
     */
    public function edit(Request $request) {

        if (request()->isPost() && $this->validateForm() === true) {
            $params_payload = $this->req_payload;
            $params_payload_arr = json_decode($params_payload, true);
            $data_redis = $request->middleware('data_redis');
            $data['name'] = $params_payload_arr['name'];
            $data['id'] = $params_payload_arr['id'];
            $data['main_organization_id'] = $data_redis['main_organization_id'];
            $data['organization_id'] = $params_payload_arr['organization_id'];
            $data['type'] =  $params_payload_arr['type'];
            $data['desc'] = $params_payload_arr['desc'];
            $data['modify_by'] = $data_redis['userid'];
            $data['create_time'] = date('Y-m-d H:i:s');
            $data['modify_time'] = date('Y-m-d H:i:s');
            if($data['type']=='1'){
                //总减排
                $data['molecule'] =NULL;
                $data['denominator'] = NULL;
                $data['total_desc'] =  $params_payload_arr['total_desc'];
                $data['reduction_type'] = $params_payload_arr['reduction_type'];
                if($params_payload_arr['reduction_type']==1){
                    $data['total_reduction_low'] = $params_payload_arr['total_reduction_low'] ?$params_payload_arr['total_reduction_low']:NULL;
                    $data['total_reduction_high']=NULL;
                }else{
                    $data['total_reduction_low'] = $params_payload_arr['total_reduction_low'] ?$params_payload_arr['total_reduction_low']:NULL;
                    $data['total_reduction_high'] = $params_payload_arr['total_reduction_high']?$params_payload_arr['total_reduction_high']:NULL ;
                    if($params_payload_arr['total_reduction_low']>$params_payload_arr['total_reduction_high']){
                        $datasmg['code'] = 404;
                        $datasmg['message'] = "总减排量最大值须大于总减排量最小值";
                        return json($datasmg);
                    }
                }

                $data['total_unit'] =  $params_payload_arr['total_unit'] ?$params_payload_arr['total_unit']:Null;
                $data['unit_desc'] = NULL;
                $data['unit_type'] =NULL;
                $data['unit_low'] = NULL;
                $data['unit_high'] = NULL;
            }elseif($data['type']=='2'){
                //单位减排
                $data['molecule'] = $params_payload_arr['molecule'] ?$params_payload_arr['molecule']:NULL;
                $data['denominator'] =  $params_payload_arr['denominator'];
                $data['total_desc'] = NULL;
                $data['reduction_type'] = NULL;
                $data['total_reduction_low'] = NULL;
                $data['total_reduction_high'] =NULL;
                $data['total_unit'] = NULL;
                $data['unit_desc'] = $params_payload_arr['unit_desc'];
                $data['unit_type'] = $params_payload_arr['unit_type'];

                if($params_payload_arr['unit_type']==1){
                    $data['unit_low'] = $params_payload_arr['unit_low'] ?$params_payload_arr['unit_low']:NULL;
                    $data['unit_high']=NULL;
                }else{
                    $data['unit_low'] = $params_payload_arr['unit_low'] ?$params_payload_arr['unit_low']:NULL;
                    $data['unit_high'] = $params_payload_arr['unit_high'] ?$params_payload_arr['unit_high']:NULL;
                    if($params_payload_arr['unit_low']>$params_payload_arr['unit_high']){
                        $datasmg['code'] = 404;
                        $datasmg['message'] = "单位减排量最大值须大于单位减排量最小值";
                        return json($datasmg);
                    }
                }
            }elseif($data['type']=='1,2' || $data['type']=='2,1'){
                //2个都选
                $data['molecule'] = $params_payload_arr['molecule'] ?$params_payload_arr['molecule']:NULL;
                $data['denominator'] =  $params_payload_arr['denominator'];
                $data['total_desc'] =  $params_payload_arr['total_desc'];
                $data['reduction_type'] = $params_payload_arr['reduction_type'];

                $data['total_unit'] =  $params_payload_arr['total_unit'] ?$params_payload_arr['total_unit']:Null;
                $data['unit_desc'] = $params_payload_arr['unit_desc'];
                $data['unit_type'] = $params_payload_arr['unit_type'];
                if($params_payload_arr['reduction_type']==1){
                    $data['total_reduction_low'] = $params_payload_arr['total_reduction_low'] ?$params_payload_arr['total_reduction_low']:NULL;
                    $data['total_reduction_high']=NULL;
                }else{
                    $data['total_reduction_low'] = $params_payload_arr['total_reduction_low'] ?$params_payload_arr['total_reduction_low']:NULL;
                    $data['total_reduction_high'] = $params_payload_arr['total_reduction_high']?$params_payload_arr['total_reduction_high']:NULL ;
                    if($params_payload_arr['total_reduction_low']>$params_payload_arr['total_reduction_high']){
                        $datasmg['code'] = 404;
                        $datasmg['message'] = "总减排量最大值须大于总减排量最小值";
                        return json($datasmg);
                    }
                }
                if($params_payload_arr['unit_type']==1){
                    $data['unit_low'] = $params_payload_arr['unit_low'] ?$params_payload_arr['unit_low']:NULL;
                    $data['unit_high']=NULL;
                }else{
                    $data['unit_low'] = $params_payload_arr['unit_low'] ?$params_payload_arr['unit_low']:NULL;
                    $data['unit_high'] = $params_payload_arr['unit_high'] ?$params_payload_arr['unit_high']:NULL;
                    if($params_payload_arr['unit_low']>$params_payload_arr['unit_high']){
                        $datasmg['code'] = 404;
                        $datasmg['message'] = "单位减排量最大值须大于单位减排量最小值";
                        return json($datasmg);
                    }
                }

            }

            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '减排场景管理';
            $data_log['type'] = '系统操作';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $request->pathinfo();
            $data_log['log'] = '编辑减排场景：' . $data['name'];

            OperationModel::addOperation($data_log);
            $edit = ReductionModel::edit($data);

            if ($edit) {
                $datasmg['code'] = 200;
                $datasmg['message'] = "修改成功";
            } else {
                $datasmg['code'] = 404;
                $datasmg['message'] = "修改失败";
            }

        } else {
            $datasmg['code'] = 404;
            $datasmg['message'] = $this->validateForm();
        }

        return json($datasmg);
    }

    /**
     * del 删除
     *
     * @return void
     */
    public function del(Request $request) {
        $id = $_GET['id'];
        $data_redis = $request->middleware('data_redis');
        $info = ReductionModel::getInfoById($id);

        // 添加操作日志
        $data_log['main_organization_id'] = $data_redis['main_organization_id'];
        $data_log['user_id'] = $data_redis['userid'];
        $data_log['module'] = '减排场景管理';
        $data_log['type'] = '系统操作';
        $data_log['time'] = date('Y-m-d H:i:s');
        $data_log['url'] = $request->pathinfo();
        $data_log['log'] = '删除减排场景：' . $info['name'];

        OperationModel::addOperation($data_log);
        $del = ReductionModel::del($id);

        if ($del) {
            $datasmg['code'] = 200;
            $datasmg['message'] = "删除成功";
        } else {
            $datasmg['code'] = 404;
            $datasmg['message'] = "删除失败";
        }
        return json($datasmg);
    }

    /**
     * find 详情
     *
     * @return void
     */
    public function find(Request $request) {
        $id = $_GET['id'];
        $list = ReductionModel::getInfoById($id);
        $data_redis = $request->middleware('data_redis');
        $main_organization_id = $data_redis['main_organization_id'];
        //组织列表
        $db = new ReductionModel();
        $organizationList = $db->organizationList($main_organization_id);

        $data['code'] = 200;
        $data['data']['list'] = $list;
        $data['data']['organizationList'] = $organizationList;


        return json($data);
    }
    /**
     * validateForm 验证
     *
     * @return void
     */
    protected function validateForm() {
        $data = request()->param();

        try {
             validate(ReductionValidate::class)->check($data);

            return  true;
        } catch (\think\exception\ValidateException $e) {

            return $e->getError();
        }
    }




}