<?php
namespace app\api\controller\emission;

use app\BaseController;
use app\model\emission\EmissionModel;
use app\model\system\OperationModel;
use app\validate\EmissionValidate;

use think\facade\Config;
use think\facade\Db;
use think\Request;


/**
 * Emission
 * 排放源管理
 * by njf
 */
class Emission extends BaseController {

    //======================================================================
    // PUBLIC FUNCTIONS
    //======================================================================

    /**
     * index 排放源管理列表
     *
     * @return void
     */
    public function index(Request $request) {
        $data_redis = $request->middleware('data_redis');
        $main_organization_id = $data_redis['main_organization_id'];

        $page_size = isset($_GET['pageSize']) ? $_GET['pageSize'] : '10';
        $page_index = isset($_GET['pageIndex']) ? $_GET['pageIndex'] : '1';

        $filters = [
            'name' =>isset($_GET['name']) ? $_GET['name'] : '',
            'active' =>isset($_GET['active']) ? $_GET['active'] : '',
            'source_id' => isset($_GET['source_id']) ? $_GET['source_id'] : '',
            'ghg_cate' => isset($_GET['ghg_cate']) ? $_GET['ghg_cate'] : '',
            'iso_cate' => isset($_GET['iso_cate']) ? $_GET['iso_cate'] : '',
            'main_organization_id'=>$main_organization_id
        ];

        $db = new EmissionModel();
        $list = $db->getList($page_size,$page_index,$filters);
        //ghg分类
        $ghg = self::ghgList(1);
        //iso分类
        $iso =self::ghgList(2);
        //分子单位
        $molecule = Config::get('emission.molecule');
        //分母单位
        $denominator = Db::table('jy_unit_type')
            ->field('id,unit_name')
            ->order('id asc')
            ->select()->toArray();
        foreach ($denominator as $k=>$v){
            $unit_type = Db::table('jy_unit')
                ->field('id,name unit_name,type_id unit_id')
                ->where('type_id',$v['id'])
                ->order('id asc')
                ->select()->toArray();
            $v['children'] = $unit_type;
            $denominator[$k]=$v;
        }

        //活动数据类别
        $active_type = Config::get('emission.active_type');
        //排放因子类别
        $factor_type = Config::get('emission.factor_type');
        //氢氟碳化物
        $hfcs = Db::table('jy_gases')->field('id,name')->where('type','氢氟碳化物（HFCs）')
            ->select()->toArray();
        //全氟碳化物
        $pfcs = Db::table('jy_gases')->field('id,name')->where('type','全氟碳化物（PFCs）')
            ->select()->toArray();

        $source_id = substr(implode(NULL, array_map('ord', str_split(substr(uniqid(), 7, 13), 1))), 0, 10);
        $is_exist = Db::table('jy_emission_source')->where('source_id',$source_id)->find();
        if($is_exist){
            $source_id_one = substr(implode(NULL, array_map('ord', str_split(substr(uniqid(), 7, 13), 1))), 0, 10);
            $is_exist_one = Db::table('jy_emission_source')->where('source_id',$source_id_one)->find();
            $source_id = $source_id_one;
            if($is_exist_one){
                $source_id =substr(implode(NULL, array_map('ord', str_split(substr(uniqid(), 7, 13), 1))), 0, 10);;
            }
        }
        $data['code'] = 200;
        $data['data']['list'] = $list;
        $data['data']['source_id'] = $source_id;
        $data['data']['ghg'] = $ghg;
        $data['data']['iso'] = $iso;
        $data['data']['molecule'] = $molecule;
        $data['data']['denominator'] = $denominator;
        $data['data']['active_type'] = $active_type;
        $data['data']['factor_type'] = $factor_type;
        $data['data']['hfcs'] = $hfcs;
        $data['data']['pfcs'] = $pfcs;

        return json($data);
    }

    /**
     * add 添加
     *
     * @return void
     */
    public function add(Request $request) {

        if (request()->isPost() && $this->validateForm() === true) {
            $params_payload = $this->req_payload;
            $params_payload_arr = json_decode($params_payload, true);

            $data_redis = $request->middleware('data_redis');
            $data['main_organization_id'] = $data_redis['main_organization_id'];

            $data['name'] = $params_payload_arr['name'];
            $data['source_id'] = $params_payload_arr['source_id'];
            $data['active'] =  $params_payload_arr['active'];
            $data['ghg_cate'] =  $params_payload_arr['ghg_cate'];
            $data['ghg_type'] = $params_payload_arr['ghg_type'];
            $data['iso_cate'] = $params_payload_arr['iso_cate'];
            $data['iso_type'] = $params_payload_arr['iso_type'];
            $data['active_data'] =  $params_payload_arr['active_data'];
            $data['active_unit'] =  $params_payload_arr['active_unit'];
            $data['active_department'] = $params_payload_arr['active_department'];
            $data['active_type'] = $params_payload_arr['active_type'];
            $data['active_score'] = (int)$params_payload_arr['active_score'];
           // $data['unitary_ratio'] =  floatval($params_payload_arr['unitary_ratio']);
            $data['factor_score'] =  (int)$params_payload_arr['factor_score'];
            $data['factor_source'] = $params_payload_arr['factor_source'];
            $data['factor_type'] = $params_payload_arr['factor_type'];
            $data['year'] = $params_payload_arr['year'];
            $data['gas'] = $params_payload_arr['gas'];

            $data['create_by'] = $data_redis['userid'];
            $data['create_time'] = date('Y-m-d H:i:s');
            $data['modify_by'] = $data_redis['userid'];
            $data['modify_time'] = date('Y-m-d H:i:s');


            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '新增排放源管理';
            $data_log['type'] = '系统操作';
            $data_log['time'] = $data['create_time'];
            $data_log['url'] = $request->pathinfo();
            $data_log['log'] = '新增排放源：'.  $data['name'] ;


            OperationModel::addOperation($data_log);

            $add = EmissionModel::add($data);

            if ($add===2) {
                $datasmg['code'] = 404;
                $datasmg['message'] = "活动数据单位与选择因子分母不一致";
            }elseif ($add===3) {
                $datasmg['code'] = 404;
                $datasmg['message'] = "选择因子单位不一致";
            } elseif($add===1) {
                $datasmg['code'] = 200;
                $datasmg['message'] = "添加成功";
            }else{
                $datasmg['code'] = 200;
                $datasmg['message'] = "添加失败";
            }

        } else {
            $datasmg['code'] = 404;
            $datasmg['message'] = $this->validateForm();
        }

        return json($datasmg);
    }
    /**
     * edit 修改
     *
     * @return void
     */
    public function edit(Request $request) {

        if (request()->isPost() && $this->validateForm() === true) {
            $params_payload = $this->req_payload;
            $params_payload_arr = json_decode($params_payload, true);
            $data_redis = $request->middleware('data_redis');
            $data['main_organization_id'] = $data_redis['main_organization_id'];

            $data['id'] = $params_payload_arr['id'];
            $data['name'] = $params_payload_arr['name'];
            $data['source_id'] = $params_payload_arr['source_id'];
            $data['active'] =  $params_payload_arr['active'];
            $data['ghg_cate'] =  $params_payload_arr['ghg_cate'];
            $data['ghg_type'] = $params_payload_arr['ghg_type'];
            $data['iso_cate'] = $params_payload_arr['iso_cate'];
            $data['iso_type'] = $params_payload_arr['iso_type'];
            $data['active_data'] =  $params_payload_arr['active_data'];
            $data['active_unit'] =  $params_payload_arr['active_unit'];
            $data['active_department'] = $params_payload_arr['active_department'];
            $data['active_type'] = $params_payload_arr['active_type'];
            $data['active_score'] = $params_payload_arr['active_score'];
         //   $data['unitary_ratio'] = floatval($params_payload_arr['unitary_ratio']);
            $data['factor_score'] =  $params_payload_arr['factor_score'];
            $data['factor_source'] = $params_payload_arr['factor_source'];
            $data['factor_type'] = $params_payload_arr['factor_type'];
            $data['year'] = $params_payload_arr['year'];
            $data['gas'] = $params_payload_arr['gas'];
            $data['modify_by'] = $data_redis['userid'];
            $data['modify_time'] = date('Y-m-d H:i:s');

            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '新增排放源管理';
            $data_log['type'] = '系统操作';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $request->pathinfo();
            $data_log['log'] = '修改排放源'. $data['name'];
     

            OperationModel::addOperation($data_log);
            $edit = EmissionModel::edit($data);


            if ($edit===2) {
                $datasmg['code'] = 404;
                $datasmg['message'] = "活动数据单位与选择因子分母不一致";
            }elseif ($edit===3) {
                $datasmg['code'] = 404;
                $datasmg['message'] = "选择因子单位不一致";
            }elseif($edit===1) {
                $datasmg['code'] = 200;
                $datasmg['message'] = "编辑成功";
            }elseif($edit===4) {
                $datasmg['code'] = 404;
                $datasmg['message'] = "气体不能为空";
            }else{
                $datasmg['code'] = 200;
                $datasmg['message'] = "编辑失败";
            }

        } else {
            $datasmg['code'] = 404;
            $datasmg['message'] = $this->validateForm();
        }
        return json($datasmg);
    }

    /**
     * del 删除
     *
     * @return void
     */
    public function del(Request $request) {
        $id = $_GET['id'];

        $data_redis = $request->middleware('data_redis');

        $info = EmissionModel::getInfoById($id);

        // 添加操作日志
        $data_log['main_organization_id'] = $data_redis['main_organization_id'];
        $data_log['user_id'] = $data_redis['userid'];
        $data_log['module'] = '新增排放源管理';
        $data_log['type'] = '系统操作';
        $data_log['time'] = date('Y-m-d H:i:s');
        $data_log['url'] = $request->pathinfo();
        $data_log['log'] = '删除排放源：名称'.$info['name'] ;
        OperationModel::addOperation($data_log);
        $del = EmissionModel::del($id);
        if ($del) {
            $datasmg['code'] = 200;
            $datasmg['message'] = "删除成功";
        } else {
            $datasmg['code'] = 404;
            $datasmg['message'] = "删除失败";
        }
        return json($datasmg);
    }


    public function find() {
        $id = $_GET['id'];
        $list = EmissionModel::getInfoById($id);

        $data['code'] = 200;
        $data['data']['list'] = $list;

        return json($data);
    }
    /**
     * validateForm 验证
     *
     * @return void
     */
    protected function validateForm() {
        $data = request()->param();

        try {
             validate(EmissionValidate::class)->check($data);

            return  true;
        } catch (\think\exception\ValidateException $e) {

            return $e->getError();
        }
    }
    //复制
    public function copy(Request $request) {
        $id = $_GET['id'];

        $data_redis = $request->middleware('data_redis');
        $main_organization_id = $data_redis['main_organization_id'];
        $userid = $data_redis['userid'];

        $info = EmissionModel::getInfoById($id);


        // 添加操作日志
        $data_log['main_organization_id'] = $data_redis['main_organization_id'];
        $data_log['user_id'] = $data_redis['userid'];
        $data_log['module'] = '新增排放源管理';
        $data_log['type'] = '系统操作';
        $data_log['time'] = date('Y-m-d H:i:s');
        $data_log['url'] = $request->pathinfo();
        $data_log['log'] = '复制排放源:'.$info['name'];
        OperationModel::addOperation($data_log);
        $copy = EmissionModel::copy($id,$main_organization_id,$userid);

        if ($copy) {
            $datasmg['code'] = 200;
            $datasmg['message'] = "复制成功";
        } else {
            $datasmg['code'] = 404;
            $datasmg['message'] = "复制失败";
        }
        return json($datasmg);
    }

    public static function ghgList($type){
        $list = Db::table('jy_ghg_template')
            ->field('name,id')
            ->where(['type'=>$type,'pid'=>0])
            ->order('id asc')
            ->select()
            ->toArray();
        foreach ($list as $k=>$v){
            $children = Db::table('jy_ghg_template')
                ->field('name,id')
                ->where(['type'=>$type,'pid'=>$v['id']])
                ->order('id asc')
                ->select()
                ->toArray();
            $v['children'] = $children;
            $list[$k] = $v;
        }
        return $list;

    }



}