<?php

namespace app\api\controller\datum;

use app\BaseController;
use app\model\datum\DatumModel;
use app\model\system\OperationModel;
use app\validate\DatumValidate;
use app\validate\ReductionValidate;
use Psr\Log\NullLogger;
use think\Request;


/**
 * Datum
 * 排放基准管理
 * by njf
 */
class Datum extends BaseController
{

    //======================================================================
    // PUBLIC FUNCTIONS
    //======================================================================

    /**
     * index 基准排放列表
     *
     * @return void
     */
    public function index(Request $request)
    {
        $data_redis = $request->middleware('data_redis');
        $main_organization_id = $data_redis['main_organization_id'];

        $page_size = isset($_GET['pageSize']) ? $_GET['pageSize'] : '10';
        $page_index = isset($_GET['pageIndex']) ? $_GET['pageIndex'] : '1';

        $filters = [
            'organization_id' => isset($_GET['organization_id']) ? $_GET['organization_id'] : '',
            'main_organization_id' => $main_organization_id
        ];

        $db = new DatumModel();
        $list = $db->getList($page_size, $page_index, $filters)->toArray();
        //组织列表
        $organizationList = $db->organizationList($main_organization_id);

        $data['code'] = 200;
        $data['data']['list'] = $list;
        $data['data']['organizationList'] = $organizationList;

        return json($data);
    }

    /**
     * add 添加
     *
     * @return void
     */
    public function add(Request $request)
    {

        if (request()->isPost() && $this->validateForm() === true) {
            $params_payload = $this->req_payload;
            $params_payload_arr = json_decode($params_payload, true);

            $data_redis = $request->middleware('data_redis');
            $data['type'] = $params_payload_arr['type'];
            $data['main_organization_id'] = $data_redis['main_organization_id'];
            $data['organization_id'] = $params_payload_arr['organization_id'];

            $data['policy'] = $params_payload_arr['policy'];

            $data['ghg1'] =  $params_payload_arr['ghg1'] ? $params_payload_arr['ghg1'] : NULL;
            $data['ghg2'] =  $params_payload_arr['ghg2'] ? $params_payload_arr['ghg2'] : NULL;
            $data['ghg3'] = $params_payload_arr['ghg3'] ? $params_payload_arr['ghg3'] : NULL;
            $data['iso1'] = $params_payload_arr['iso1'] ? $params_payload_arr['iso1'] : NULL;
            $data['iso2'] =  $params_payload_arr['iso2'] ? $params_payload_arr['iso2'] : NULL;
            $data['iso3'] = $params_payload_arr['iso3'] ? $params_payload_arr['iso3'] : NULL;
            $data['iso4'] = $params_payload_arr['iso4'] ? $params_payload_arr['iso4'] : NULL;
            $data['iso5'] = $params_payload_arr['iso5'] ? $params_payload_arr['iso5'] : NULL;
            $data['iso6'] = $params_payload_arr['iso6'] ? $params_payload_arr['iso6'] : NULL;
            $data['total'] = $params_payload_arr['total'] ? $params_payload_arr['total'] : NULL;
            $data['create_by'] = $data_redis['userid'];
            $data['modify_by'] = $data_redis['userid'];
            $data['create_time'] = date('Y-m-d H:i:s');
            $data['modify_time'] = date('Y-m-d H:i:s');
            if ($data['type'] == 1) {
                $data['start_year'] =  $params_payload_arr['start_year'];
                $data['end_year'] =  NULL;
            } elseif ($data['type'] == 2) {
                $data['start_year'] =  $params_payload_arr['start_year'];
                $data['end_year'] =  $params_payload_arr['end_year'];
            }
            /*if(!empty($data['ghg1'])){
                $data['total'] = $data['ghg1']+$data['ghg2']+$data['ghg3'];
            }else{
                $data['total'] = $data['iso1']+$data['iso2']+$data['iso3']+$data['iso4']+$data['iso5']+$data['iso6'];
            }*/

            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '排放基准管理';
            $data_log['type'] = '系统操作';
            $data_log['time'] = $data['create_time'];
            $data_log['url'] = $request->pathinfo();
            $data_log['log'] = '新建排放基准';

            OperationModel::addOperation($data_log);

            $add = DatumModel::add($data);

            if ($add===1) {
                $datasmg['code'] = 200;
                $datasmg['message'] = "添加成功";
            }elseif($add===2){
                $datasmg['code'] = 404;
                $datasmg['message'] = "该组织已经添加过排放基准";
            } else {
                $datasmg['code'] = 404;
                $datasmg['message'] = "添加失败";
            }
        } else {
            $datasmg['code'] = 404;
            $datasmg['message'] = $this->validateForm();
        }

        return json($datasmg);
    }
    /**
     * edit 修改
     *
     * @return void
     */
    public function edit(Request $request)
    {

        if (request()->isPost() && $this->validateForm() === true) {
            $params_payload = $this->req_payload;
            $params_payload_arr = json_decode($params_payload, true);
            $data_redis = $request->middleware('data_redis');
            $data['id'] = $params_payload_arr['id'];
            $data['type'] = $params_payload_arr['type'];
            $data['main_organization_id'] = $data_redis['main_organization_id'];
            $data['organization_id'] = $params_payload_arr['organization_id'];

            $data['policy'] = $params_payload_arr['policy'];

            $data['ghg1'] =  $params_payload_arr['ghg1'] ? $params_payload_arr['ghg1'] : NULL;
            $data['ghg2'] =  $params_payload_arr['ghg2'] ? $params_payload_arr['ghg2'] : NULL;
            $data['ghg3'] = $params_payload_arr['ghg3'] ? $params_payload_arr['ghg3'] : NULL;
            $data['iso1'] = $params_payload_arr['iso1'] ? $params_payload_arr['iso1'] : NULL;
            $data['iso2'] =  $params_payload_arr['iso2'] ? $params_payload_arr['iso2'] : NULL;
            $data['iso3'] = $params_payload_arr['iso3'] ? $params_payload_arr['iso3'] : NULL;
            $data['iso4'] = $params_payload_arr['iso4'] ? $params_payload_arr['iso4'] : NULL;
            $data['iso5'] = $params_payload_arr['iso5'] ? $params_payload_arr['iso5'] : NULL;
            $data['iso6'] = $params_payload_arr['iso6'] ? $params_payload_arr['iso6'] : NULL;
            $data['total'] = $params_payload_arr['total'] ? $params_payload_arr['total'] : NULL;
            if ($data['type'] == 1) {
                $data['start_year'] =  $params_payload_arr['start_year'];
                $data['end_year'] =  NULL;
            } elseif ($data['type'] == 2) {
                $data['start_year'] =  $params_payload_arr['start_year'];
                $data['end_year'] =  $params_payload_arr['end_year'];
            }


            $data['modify_by'] = $data_redis['userid'];
            $data['create_time'] = date('Y-m-d H:i:s');
            $data['modify_time'] = date('Y-m-d H:i:s');

            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '排放基准管理';
            $data_log['type'] = '系统操作';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $request->pathinfo();
            $data_log['log'] = '编辑排放基准';

            OperationModel::addOperation($data_log);
            $edit = DatumModel::edit($data);

            if ($edit) {
                $datasmg['code'] = 200;
                $datasmg['message'] = "修改成功";
            } else {
                $datasmg['code'] = 404;
                $datasmg['message'] = "修改失败";
            }
        } else {
            $datasmg['code'] = 404;
            $datasmg['message'] = $this->validateForm();
        }

        return json($datasmg);
    }

    /**
     * find 详情
     *
     * @return void
     */
    public function find(Request $request)
    {
        $id = $_GET['id'];
        $list = DatumModel::getInfoById($id);
        $data_redis = $request->middleware('data_redis');
        $main_organization_id = $data_redis['main_organization_id'];
        //组织列表
        $db = new DatumModel();
        $organizationList = $db->organizationList($main_organization_id);

        $data['code'] = 200;
        $data['data']['list'] = $list;
        $data['data']['organizationList'] = $organizationList;

        return json($data);
    }
    /**
     * validateForm 验证
     *
     * @return void
     */
    protected function validateForm()
    {
        $data = request()->param();
        try {
            validate(DatumValidate::class)->check($data);
            return  true;
        } catch (\think\exception\ValidateException $e) {
            return $e->getError();
        }
    }
}
