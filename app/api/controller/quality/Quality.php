<?php
namespace app\api\controller\quality;

use app\BaseController;
use app\model\quality\QualityModel;
use app\model\system\OperationModel;
use app\validate\DataOrganizationValidate;
use app\validate\GhgValidate;
use app\validate\IsoValidate;
use app\validate\QualityValidate;
use think\facade\Db;
use think\Request;


/**
 * Quality
 * 数据质量管理
 * by njf
 */
class Quality extends BaseController {

    //======================================================================
    // PUBLIC FUNCTIONS
    //======================================================================


    public function index(Request $request) {
        $data_redis = $request->middleware('data_redis');
        $main_organization_id = $data_redis['main_organization_id'];

        $page_size = isset($_GET['pageSize']) ? $_GET['pageSize'] : '10';
        $page_index = isset($_GET['pageIndex']) ? $_GET['pageIndex'] : '1';

        $filters = [
            'version' => isset($_GET['version']) ? $_GET['version'] : '',
            'organization_id' => isset($_GET['organization_id']) ? $_GET['organization_id'] : '',
            'main_organization_id'=>$main_organization_id
        ];

        $db = new QualityModel();
        $list = $db->getList($page_size,$page_index,$filters)->toArray();
        //组织列表
        $organizationList = $db->organizationList($main_organization_id);

        $data['code'] = 200;
        $data['data']['list'] = $list;
        $data['data']['organizationList']=$organizationList;

        return json($data);
    }

    public function add(Request $request) {

        if (request()->isPost() && $this->validateForm('add') === true) {
            $params_payload = $this->req_payload;
            $params_payload_arr = json_decode($params_payload, true);

            $data_redis = $request->middleware('data_redis');
            $data['version'] = $params_payload_arr['version'];
            $data['main_organization_id'] = $data_redis['main_organization_id'];
            $data['organization_id'] = $params_payload_arr['organization_id'];
            $data['content'] =  $params_payload_arr['content'];
            $data['development_time'] =  $params_payload_arr['development_time'];
            $data['remark'] = $params_payload_arr['remark'];

            $data['create_by'] = $data_redis['userid'];
            $data['create_time'] = date('Y-m-d H:i:s');
            $data['modify_by'] = $data_redis['userid'];
            $data['modify_time'] = date('Y-m-d H:i:s');


            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '数据质量管理';
            $data_log['type'] = '系统操作';
            $data_log['time'] = $data['create_time'];
            $data_log['url'] = $request->pathinfo();
            $data_log['log'] = '新建数据质量控制计划,版本号'.$data['version'] ;

            OperationModel::addOperation($data_log);

            $add = QualityModel::add($data);
            return $add;


        } else {
            $datasmg['code'] = 404;
            $datasmg['message'] = $this->validateForm('add');
        }

        return json($datasmg);
    }

    public function edit(Request $request) {

        if (request()->isPost() && $this->validateForm('edit') === true) {
            $params_payload = $this->req_payload;
            $params_payload_arr = json_decode($params_payload, true);
            $data_redis = $request->middleware('data_redis');
            $data['version'] = $params_payload_arr['version'];
            $data['id'] = $params_payload_arr['id'];
            $data['main_organization_id'] = $data_redis['main_organization_id'];
            $data['content'] =  $params_payload_arr['content'];
            $data['development_time'] =  $params_payload_arr['development_time'];
            $data['remark'] = $params_payload_arr['remark'];

            $data['modify_by'] = $data_redis['userid'];
            $data['modify_time'] = date('Y-m-d H:i:s');

            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '数据质量管理';
            $data_log['type'] = '系统操作';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $request->pathinfo();
            $data_log['log'] = '编辑数据质量控制计划,版本号'. $data['version'];

            OperationModel::addOperation($data_log);
            $edit = QualityModel::edit($data);
            return $edit;

        } else {
            $datasmg['code'] = 404;
            $datasmg['message'] = $this->validateForm('edit');
        }

        return json($datasmg);
    }

    public function del(Request $request) {
        $id = $_GET['id'];
        $data_redis = $request->middleware('data_redis');
        $info = QualityModel::getInfoById($id);
        // 添加操作日志
        $data_log['main_organization_id'] = $data_redis['main_organization_id'];
        $data_log['user_id'] = $data_redis['userid'];
        $data_log['module'] = '数据质量管理';
        $data_log['type'] = '系统操作';
        $data_log['time'] = date('Y-m-d H:i:s');
        $data_log['url'] = $request->pathinfo();
        $data_log['log'] = '删除数据质量控制计划，版本号'.$info['version'];

        OperationModel::addOperation($data_log);
        $del = QualityModel::del($id);

        if ($del) {
            $datasmg['code'] = 200;
            $datasmg['message'] = "删除成功";
        } else {
            $datasmg['code'] = 404;
            $datasmg['message'] = "删除失败";
        }
        return json($datasmg);
    }

    public function find(Request $request) {
        $id = $_GET['id'];
        $list = QualityModel::getInfoById($id);
        $data_redis = $request->middleware('data_redis');
        $main_organization_id = $data_redis['main_organization_id'];
        //组织列表
        $db = new QualityModel();
        $organizationList = $db->organizationList($main_organization_id);


        $data['code'] = 200;
        $data['data'] = $list;
        $data['organizationList'] = $organizationList;

        return json($data);
    }

    public function copy(Request $request) {
        $id = $_GET['id'];
        $data_redis = $request->middleware('data_redis');
        $info = QualityModel::getInfoById($id);
        // 添加操作日志
        $data_log['main_organization_id'] = $data_redis['main_organization_id'];
        $data_log['user_id'] = $data_redis['userid'];
        $data_log['module'] = '数据质量管理';
        $data_log['type'] = '系统操作';
        $data_log['time'] = date('Y-m-d H:i:s');
        $data_log['url'] = $request->pathinfo();
        $data_log['log'] = '复制数据质量控制计划，版本号'.$info['version'];
        $copy = QualityModel::copy($id,$data_redis['main_organization_id'],$data_redis['userid']);

        if ($copy) {
            $datasmg['code'] = 200;
            $datasmg['message'] = "复制成功";
        } else {
            $datasmg['code'] = 404;
            $datasmg['message'] = "复制失败";
        }
        return json($datasmg);
    }

    //组织基本信息
    //组织基本信息编辑
    public function organizationalEdit(Request $request) {

        if (request()->isPost()&& $this->validateForm('organizationalEdit') === true) {
            $params_payload = $this->req_payload;
            $data_redis = $request->middleware('data_redis');
            $params_payload_arr = json_decode($params_payload, true);
            $data['id'] = $params_payload_arr['id'];
            $data['name'] = $params_payload_arr['name'];
            $data['department'] = $params_payload_arr['department'];
            $data['register_address'] = $params_payload_arr['register_address'];
            $data['register_address_detail'] = $params_payload_arr['register_address_detail'];
            $data['production_address'] = $params_payload_arr['production_address'];
            $data['production_address_detail'] = $params_payload_arr['production_address_detail'];
            $data['business_desc'] = $params_payload_arr['business_desc'];
            $data['geo_desc'] = $params_payload_arr['geo_desc'];
            $data['organization_file'] = $params_payload_arr['organization_file'];
            $data['organization_file_desc'] = $params_payload_arr['organization_file_desc'];
            $data['manage_build'] = $params_payload_arr['manage_build'];
            $data['manage_build_desc'] = $params_payload_arr['manage_build_desc'];
            $data['quality_id'] = $params_payload_arr['quality_id'];
            $data['main_organization_id'] = $data_redis['main_organization_id'];
            //查询组织id
            $quality_info =  Db::table('jy_data_quality')->where('id',$params_payload_arr['quality_id'])->find();
            $data['organization_id'] = $quality_info['organization_id'];

            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '组织基本信息管理';
            $data_log['type'] = '系统操作';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $request->pathinfo();
            $data_log['log'] = '编辑组织基本信息:'.$data['name'];
            OperationModel::addOperation($data_log);
            $edit = QualityModel::organizationalEdit($data);

            if ($edit) {
                $datasmg['code'] = 200;
                $datasmg['message'] = "修改成功";
            } else {
                $datasmg['code'] = 404;
                $datasmg['message'] = "修改失败";
            }

        } else {
            $datasmg['code'] = 404;
            $datasmg['message'] = $this->validateForm('organizationalEdit');
        }

        return json($datasmg);
    }
    //组织基本信息详情
    public function organizationalInfo(Request $request) {

        $data_redis = $request->middleware('data_redis');
        $main_organization_id = $data_redis['main_organization_id'];
        //查询组织id
        $quality_id =$_GET['quality_id'];

        $list = QualityModel::organizationalInfo($quality_id);
        $data['code'] = 200;
        $data['data'] = $list;
        return json($data);
    }
    //组织边界管理
    public function organizationalBoundariesEdit(Request $request) {

        if (request()->isPost()) {
            $params_payload = $this->req_payload;
            $data_redis = $request->middleware('data_redis');
            $params_payload_arr = json_decode($params_payload, true);
            $data['id'] = $params_payload_arr['id'];
            $data['organization_setting'] = $params_payload_arr['organization_setting'];
            $data['organization_desc'] = $params_payload_arr['organization_desc'];
            $data['organization_change'] = $params_payload_arr['organization_change'];
            $data['quality_id'] = $params_payload_arr['quality_id'];
            $data['main_organization_id'] = $data_redis['main_organization_id'];

            //查询组织id
            $quality_info =  Db::table('jy_data_quality')->where('id',$params_payload_arr['quality_id'])->find();
            $data['organization_id'] = $quality_info['organization_id'];

            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '组织边界管理';
            $data_log['type'] = '系统操作';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $request->pathinfo();
            $data_log['log'] = '编辑组织边界';
            OperationModel::addOperation($data_log);
            $edit = QualityModel::organizationalBoundariesEdit($data);

            if ($edit) {
                $datasmg['code'] = 200;
                $datasmg['message'] = "修改成功";
            } else {
                $datasmg['code'] = 404;
                $datasmg['message'] = "修改失败";
            }

        } else {
            $datasmg['code'] = 404;
            $datasmg['message'] = '请求方法错误';
        }

        return json($datasmg);
    }
    //组织边界详情
    public function organizationalBoundariesInfo(Request $request) {
        $data_redis = $request->middleware('data_redis');
        $main_organization_id = $data_redis['main_organization_id'];
        //查询组织id
        $quality_id =$_GET['quality_id'];

        $list = QualityModel::organizationalBoundariesInfo($quality_id);
        $data['code'] = 200;
        $data['data'] = $list;
        $data['organization_setting']=[
            ['id'=>1,'name'=>'运营控制权法'],
            ['id'=>2,'name'=>'财务控制权发'],
            ['id'=>3,'name'=>'股权比例法'],
        ];

        return json($data);
    }

    //ghg
    public function ghgIndex(Request $request) {
        $data_redis = $request->middleware('data_redis');
        $main_organization_id = $data_redis['main_organization_id'];
        //查询组织id
        $quality_id =$_GET['quality_id'];
        $db = new QualityModel();
        $list = $db->ghgList($quality_id);

        $data['code'] = 200;
        $data['data']['list'] = $list;

        return json($data);
    }

    public function ghgEdit(Request $request) {

        if (request()->isPost()  && $this->validateForm('ghgEdit') === true) {
            $params_payload = $this->req_payload;
            $data_redis = $request->middleware('data_redis');
            $params_payload_arr = json_decode($params_payload, true);
            $data['id'] = $params_payload_arr['id'];
            $data['active_desc'] =  $params_payload_arr['active_desc'];
            $data['is_check'] =  $params_payload_arr['is_check'];
            $data['collection_instructions'] =  $params_payload_arr['collection_instructions'];
            $data['calculation_description'] =  $params_payload_arr['calculation_description'];
            $data['data_description'] =  $params_payload_arr['data_description'];
            $data['quality_id'] =  $params_payload_arr['quality_id'];
            $data['create_time'] =  date('Y-m-d H:i:s');

            //查询组织id
            $quality_info =  Db::table('jy_data_quality')->where('id',$params_payload_arr['quality_id'])->find();
            $data['organization_id'] = $quality_info['organization_id'];

            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '数据管理计划-GHG';
            $data_log['type'] = '系统操作';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $request->pathinfo();
            $data_log['log'] = '编辑数据管理计划GHG'. $params_payload_arr['name'];

            OperationModel::addOperation($data_log);
            $edit = QualityModel::ghgedit($data);

            if ($edit) {
                $datasmg['code'] = 200;
                $datasmg['message'] = "修改成功";
            } else {
                $datasmg['code'] = 404;
                $datasmg['message'] = "修改失败";
            }

        } else {
            $datasmg['code'] = 404;
            $datasmg['message'] = $this->validateForm('ghgEdit');
        }

        return json($datasmg);
    }

    public function ghgInfo() {
        $id = $_GET['id'];
        $list = QualityModel::ghgInfo($id);

        $data['code'] = 200;
        $data['data'] = $list;
       ;

        return json($data);
    }
    //iso
    public function isoIndex(Request $request) {

        $data_redis = $request->middleware('data_redis');
        $main_organization_id = $data_redis['main_organization_id'];
        //查询组织id
        $quality_id =$_GET['quality_id'];

        $db = new QualityModel();
        $list = $db->isoList($quality_id);

        $data['code'] = 200;
        $data['data']['list'] = $list;

        return json($data);
    }

    public function isoEdit(Request $request) {

        if (request()->isPost()  && $this->validateForm('isoEdit') === true) {
            $params_payload = $this->req_payload;
            $data_redis = $request->middleware('data_redis');
            $params_payload_arr = json_decode($params_payload, true);
            $data['id'] = $params_payload_arr['id'];
            $data['active_desc'] =  $params_payload_arr['active_desc'];
            $data['is_check'] =  $params_payload_arr['is_check'];
            $data['collection_instructions'] =  $params_payload_arr['collection_instructions'];
            $data['calculation_description'] =  $params_payload_arr['calculation_description'];
            $data['data_description'] =  $params_payload_arr['data_description'];
            $data['quality_id'] =  $params_payload_arr['quality_id'];
            $data['create_time'] = date('Y-m-d H:i:s');
            //查询组织id
            $quality_info =  Db::table('jy_data_quality')->where('id',$params_payload_arr['quality_id'])->find();
            $data['organization_id'] = $quality_info['organization_id'];

            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '数据管理计划-ISO';
            $data_log['type'] = '系统操作';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $request->pathinfo();
            $data_log['log'] = '编辑数据管理计划ISO'.$params_payload_arr['name'];
            OperationModel::addOperation($data_log);
            $edit = QualityModel::isoedit($data);


            if ($edit) {
                $datasmg['code'] = 200;
                $datasmg['message'] = "修改成功";
            } else {
                $datasmg['code'] = 404;
                $datasmg['message'] = "修改失败";
            }

        } else {
            $datasmg['code'] = 404;
            $datasmg['message'] = $this->validateForm('isoEdit');
        }

        return json($datasmg);
    }

    public function isoInfo() {
        $id = $_GET['id'];
        $list = QualityModel::isoInfo($id);
        $data['code'] = 200;
        $data['data'] = $list;

        return json($data);
    }

    //数据质量管理
    //数据质量管理规定编辑
    public function dataQualityRoleEdit(Request $request) {

        if (request()->isPost()  && $this->validateForm('dataQualityRoleEdit') === true ) {
            $params_payload = $this->req_payload;
            $data_redis = $request->middleware('data_redis');
            $params_payload_arr = json_decode($params_payload, true);
            $data['id'] = $params_payload_arr['id'];
            $data['content'] = $params_payload_arr['content'];
            $data['main_organization_id'] = $data_redis['main_organization_id'];
            $data['quality_id'] = $params_payload_arr['quality_id'];
            //查询组织id
            $quality_info =  Db::table('jy_data_quality')->where('id',$params_payload_arr['quality_id'])->find();

            $data['organization_id'] = $quality_info['organization_id'];

            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '数据质量管理规定';
            $data_log['type'] = '系统操作';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $request->pathinfo();
            $data_log['log'] = '编辑数据质量规定';
            OperationModel::addOperation($data_log);

            $edit = QualityModel::dataQualityRoleEdit($data);

            if ($edit) {
                $datasmg['code'] = 200;
                $datasmg['message'] = "修改成功";
            } else {
                $datasmg['code'] = 404;
                $datasmg['message'] = "修改失败";
            }

        } else {
            $datasmg['code'] = 404;
            $datasmg['message'] = $this->validateForm('dataQualityRoleEdit');
        }

        return json($datasmg);
    }
    //数据质量管理规定详情
    public function dataQualityRoleInfo(Request $request) {
        $data_redis = $request->middleware('data_redis');
        $main_organization_id = $data_redis['main_organization_id'];
        //查询组织id
        $quality_id =$_GET['quality_id'];
        $quality_info =  Db::table('jy_data_quality')->where('id',$quality_id)->find();

        $organization_id = $quality_info['organization_id'];

        $list = QualityModel::dataQualityRoleInfo($quality_id);
        $data['code'] = 200;
        $data['data'] = $list;

        return json($data);
    }

    protected function validateForm($type) {
        $data = request()->param();

        try {
            if($type=='add'|| $type=='edit'){
                validate(QualityValidate::class)->check($data);
            }elseif($type=='organizationalEdit'){
                validate(DataOrganizationValidate::class)->check($data);
            }elseif($type=='ghgEdit'){
                validate(GhgValidate::class)->check($data);
            }elseif($type=='isoEdit'){
                validate(IsoValidate::class)->check($data);
            }

            return  true;
        } catch (\think\exception\ValidateException $e) {
            return $e->getError();
        }
    }



}