<?php

namespace app\api\controller\attestation;

use app\BaseController;
use app\model\attestation\AttestationModel;
use app\model\product\ProductModel;
use app\model\system\FileModel;
use app\model\system\OperationModel;
use app\validate\AttestationEditValidate;
use app\validate\AttestationOriginalAddValidate;
use app\validate\AttestationOriginalFormValidate;
use app\validate\AttestationOriginalValidate;
use app\validate\AttestationValidate;
use think\exception\ValidateException;
use think\Request;


/**
 * Factor
 * 认证管理
 * by njf
 */
class Attestation extends BaseController
{
    const ip = "http://47.122.18.241:81";
    //======================================================================
    // PUBLIC FUNCTIONS
    //======================================================================

    /**
     * getcount 统计认证数据
     *
     * @return void
     */
    public function getcount(Request $request)
    {
        $data_redis = $request->middleware('data_redis');
        $main_organization_id = $data_redis['main_organization_id'];

        $db = new AttestationModel();
        $list = $db->getcount($main_organization_id);

        $data['code'] = 200;
        $data['data']['list'] = $list;

        return json($data);
    }

    /**
     * index 认证列表
     *
     * @return void
     */
    public function index(Request $request)
    {
        $page_size = isset($_GET['pageSize']) ? $_GET['pageSize'] : '10';
        $page_index = isset($_GET['pageIndex']) ? $_GET['pageIndex'] : '1';
        $data_redis = $request->middleware('data_redis');
        $main_organization_id = $data_redis['main_organization_id'];

        $filters = [
            'product_name' => isset($_GET['product_name']) ? $_GET['product_name'] : '',
            'status' => isset($_GET['status']) ? $_GET['status'] : '',
            'create_time_start' => isset($_GET['create_time_start']) ? $_GET['create_time_start'] : '',
            'create_time_end' => isset($_GET['create_time_end']) ? $_GET['create_time_end'] : '',
            'attestation_type' => isset($_GET['attestation_type']) ? $_GET['attestation_type'] : '',
            'main_organization_id'=>$main_organization_id ?$main_organization_id :"",
        ];
        $db = new AttestationModel();
        $list = $db->getList($page_size, $page_index, $filters)->toArray();
        $status = [
            ['id' => 1, 'name' => '待付款'],
            ['id' => 2, 'name' => '待认证'],
            ['id' => 3, 'name' => '待线上核查'],
            ['id' => 4, 'name' => '待现场核查'],
            ['id' => 5, 'name' => '已驳回'],
            ['id' => 6, 'name' => '现场核查中'],
            ['id' => 7, 'name' => '认证完成'],
            ['id' => 8, 'name' => '待补充材料'],
            ['id' => 10, 'name' => '证书已派发'],
            ['id' => 12, 'name' => '已取消'],
            ['id' => 13, 'name' => '已退款'],
        ];
        $data['code'] = 200;
        $data['data']['list'] = $list;
        $data['data']['type'] = 'detail';
        $data['data']['status'] = $status;


        return json($data);
    }

    /**
     * add 添加认证
     *
     * @return void
     */
    public function add(Request $request)
    {

        if (request()->isPost() && $this->validateForm($type = 1) === true) {
            $params_payload = $this->req_payload;
            $params_payload_arr = json_decode($params_payload, true);

            $data_redis = $request->middleware('data_redis');

            $data['username'] = $params_payload_arr['username'];
            $data['mobile'] = $params_payload_arr['mobile'];
            $data['email'] = $params_payload_arr['email'];
            $data['product_name'] = $params_payload_arr['product_name'];
            $data['product_no'] = $params_payload_arr['product_no'];
            $data['client_name'] = $params_payload_arr['client_name'];
            $data['client_address'] = $params_payload_arr['client_address'];
            $data['client_detail_address'] = $params_payload_arr['client_detail_address'];
            $data['produce_name'] = $params_payload_arr['produce_name'];
            $data['produce_address'] = $params_payload_arr['produce_address'];
            $data['produce_detail_address'] = $params_payload_arr['produce_detail_address'];
            $data['company_name'] = $params_payload_arr['company_name'];
            $data['company_address'] = $params_payload_arr['company_address'];
            $data['company_detail_address'] = $params_payload_arr['company_detail_address'];
            $data['main_organization_id'] = $data_redis['main_organization_id'];
            $data['status'] = 1;
            $data['data_from'] = 1;
            $data['create_time'] = date('Y_m-d H:i:s');
            $data['modify_time'] = date('Y_m-d H:i:s');
            $data['create_by'] = $data_redis['userid'];
            $data['modify_by'] = $data_redis['userid'];


            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '认证管理';
            $data_log['type'] = '功能操作';
            $data_log['time'] = $data['create_time'];
            $data_log['url'] = $request->pathinfo();
            $data_log['log'] = '新建认证管理：' . $params_payload_arr['product_name'];


            OperationModel::addOperation($data_log);

            $add = AttestationModel::add($data, $data_redis['userid']);

            if ($add) {
                $datasmg['code'] = 200;
                $datasmg['message'] = "添加成功";
            } else {
                $datasmg['code'] = 404;
                $datasmg['message'] = "添加失败";
            }

        } else {
            $datasmg['code'] = 404;
            $datasmg['message'] = $this->validateForm($type = 1);
        }

        return json($datasmg);
    }

    /**
     * edit 修改认证
     *
     * @return void
     */
    public function edit(Request $request)
    {

        if (request()->isPost() && $this->validateForm($type = 2) === true) {
            $params_payload = $this->req_payload;
            $params_payload_arr = json_decode($params_payload, true);
            $data_redis = $request->middleware('data_redis');
            $data['id'] = $params_payload_arr['id'];
            $data['username'] = $params_payload_arr['username'];
            $data['mobile'] = $params_payload_arr['mobile'];
            $data['email'] = $params_payload_arr['email'];
            $data['product_name'] = $params_payload_arr['product_name'];
            $data['product_no'] = $params_payload_arr['product_no'];
            $data['client_name'] = $params_payload_arr['client_name'];
            $data['client_address'] = $params_payload_arr['client_address'];
            $data['client_detail_address'] = $params_payload_arr['client_detail_address'];
            $data['produce_name'] = $params_payload_arr['produce_name'];
            $data['produce_address'] = $params_payload_arr['produce_address'];
            $data['produce_detail_address'] = $params_payload_arr['produce_detail_address'];
            $data['company_name'] = $params_payload_arr['company_name'];
            $data['company_address'] = $params_payload_arr['company_address'];
            $data['company_detail_address'] = $params_payload_arr['company_detail_address'];
            $data['data_time_border_start'] = $params_payload_arr['data_time_border_start'];
            $data['data_time_border_end'] = $params_payload_arr['data_time_border_end'];
            $data['system_boder'] = $params_payload_arr['system_boder'];
            $data['functions'] = $params_payload_arr['functions'];
            $data['files'] = $params_payload_arr['files'];
            $data['product_calculate_id'] = isset($params_payload_arr['product_calculate_id']) ? $params_payload_arr['product_calculate_id'] : "";
            $data['from'] = $params_payload_arr['from'];
            $data['modify_time'] = date('Y-m-d H:i:s');
            $data['modify_by'] = $data_redis['userid'];

            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '认证管理修改';
            $data_log['type'] = '功能操作';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $request->pathinfo();
            $data_log['log'] = '编辑认证管理：' . $data['product_name'];

            OperationModel::addOperation($data_log);
            $edit = AttestationModel::updates($data);

            if ($edit) {
                $datasmg['code'] = 200;
                $datasmg['message'] = "修改成功";
            } else {
                $datasmg['code'] = 404;
                $datasmg['message'] = "修改失败";
            }

        } else {
            $datasmg['code'] = 404;
            $datasmg['message'] = $this->validateForm($type = 2);
        }

        return json($datasmg);
    }

    /**
     * back 撤回
     *
     * @return void
     */
    public function back(Request $request)
    {
        if (request()->isPost()) {
            $data_redis = $request->middleware('data_redis');
            $params_payload = $this->req_payload;
            $params_payload_arr = json_decode($params_payload, true);

            $id = isset($params_payload_arr['id']) ? $params_payload_arr['id'] : '';
            if ($id == '' || $id == null || $id == 0) {
                return json(['code' => 201, 'message' => "参数id错误"]);
            }


            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '认证管理';
            $data_log['type'] = '功能操作';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $request->pathinfo();
            //获取认证标题
            $title = AttestationModel::getTitleById($id);
            $data_log['log'] = '撤回：认证管理' . $title;

            OperationModel::addOperation($data_log);
            $data['id'] = $id;
            $back = AttestationModel::back($id, $data_redis['userid']);


            if ($back) {
                $datasmg['code'] = 200;
                $datasmg['message'] = "操作成功";
            } else {
                $datasmg['code'] = 404;
                $datasmg['message'] = "操作失败";
            }

            return json($datasmg);
        } else {
            $datasmg = ['code' => 404, 'message' => "请求方式错误"];
            return json($datasmg);
        }
    }

    /**
     * del 删除
     *
     * @return void
     */
    public function del(Request $request)
    {
        if (request()->isPost()) {
            $data_redis = $request->middleware('data_redis');
            $params_payload = $this->req_payload;
            $params_payload_arr = json_decode($params_payload, true);

            $id = isset($params_payload_arr['id']) ? $params_payload_arr['id'] : '';
            if ($id == '' || $id == null || $id == 0) {
                return json(['code' => 201, 'message' => "参数id错误"]);
            }
            $data['id'] = $id;

            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '认证管理';
            $data_log['type'] = '功能操作';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $request->pathinfo();
            $title = AttestationModel::getTitleById($id);
            $data_log['log'] = '删除：认证管理' . $title;


            OperationModel::addOperation($data_log);
            $del = AttestationModel::del($data);

            if ($del) {
                $datasmg['code'] = 200;
                $datasmg['message'] = "操作成功";
            } else {
                $datasmg['code'] = 404;
                $datasmg['message'] = "操作失败";
            }

            return json($datasmg);
        } else {
            $datasmg = ['code' => 404, 'message' => "请求方式错误"];
            return json($datasmg);
        }
    }

    /**
     * submitattestation 提交认证
     *
     * @return void
     */
    public function submitattestation(Request $request)
    {
        if (request()->isPost()) {
            $data_redis = $request->middleware('data_redis');
            $params_payload = $this->req_payload;
            $params_payload_arr = json_decode($params_payload, true);

            $id = isset($params_payload_arr['id']) ? $params_payload_arr['id'] : '';
            if ($id == '' || $id == null || $id == 0) {
                return json(['code' => 201, 'message' => "参数id错误"]);
            }
            $data['id'] = $id;

            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '认证管理';
            $data_log['type'] = '功能操作';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $request->pathinfo();
            $title = AttestationModel::getTitleById($id);
            $data_log['log'] = '认证管理：图片上传' . $title;

            OperationModel::addOperation($data_log);
            $sub = AttestationModel::submitattestation($id, $data_redis['userid']);

            if ($sub) {

                if($sub===2){
                    $datasmg['code'] = 404;
                    $datasmg['message'] = "数据不完整，请补充完整后提交";
                }elseif($sub===3){
                    $datasmg['code'] = 404;
                    $datasmg['message'] = "数据不完整，请补充完整后提交";
                }else{
                    $datasmg['code'] = 200;
                    $datasmg['message'] = "操作成功";
                }

            } else {
                $datasmg['code'] = 404;
                $datasmg['message'] = "操作失败";
            }

            return json($datasmg);
        } else {
            $datasmg = ['code' => 404, 'message' => "请求方法错误"];
            return json($datasmg);
        }
    }

    /**
     * find 查看认证详情
     *
     * @return void
     */
    public function find()
    {
        $id = $_GET['id'];
        $list = AttestationModel::getInfoById($id);
        $data['code'] = 200;
        $data['data'] = $list;

        return json($data);
    }

    /**
     * validateForm 验证
     *
     * @return void
     */
    protected function validateForm($type)
    {
        $data = request()->param();
        try {
            if ($type == 1) {
                validate(AttestationValidate::class)->check($data);
            } elseif ($type == 2) {
                validate(AttestationEditValidate::class)->check($data);
            } elseif ($type == 3) {
                validate(AttestationOriginalValidate::class)->check($data);
            } elseif ($type = 4) {
                validate(AttestationOriginalFormValidate::class)->check($data);
            }
            return true;
        } catch (\think\exception\ValidateException $e) {

            return $e->getError();
        }
    }

    //上传图片
    public function upload()
    {

        $data_redis = $this->request->middleware('data_redis');
        $file = $this->request->file();
        try {
            // 验证文件大小格式
            validate(['file' => 'fileSize:52428800|fileExt:png,jpg'])
                ->check($file);

            $savename = \think\facade\Filesystem::disk('admin_cooperate_uploads')->putFile('admin_cooperate_uploads', $file['file']);
            $url = str_replace('\\', '/', $savename);
            $guid=guid();
            $data['virtual_id'] = isset($_POST['virtual_id']) ? $_POST['virtual_id'] : NULL;
            $data['file_path'] = str_replace('admin_cooperate_uploads', '', $url);
            $data['source_name'] = $_FILES['file']['name'];
            $data['file_size'] = $_FILES['file']['size'];
            $data['main_organization_id'] = $data_redis['main_organization_id'];
            $data['create_by'] = $data_redis['userid'];
            $data['modify_by'] = $data_redis['userid'];
            $data['create_time'] = date('Y-m-d H:i:s');
            $data['modify_time'] = date('Y-m-d H:i:s');
            $data['id'] = $guid;
            if($this->request->pathinfo()=='organizational_upload'){
                $data['module'] = '组织碳核算';
            }else{
                $data['module'] = '认证服务';
            }

            $add_id = FileModel::addFile($data);

            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '系统设置';
            $data_log['type'] = '系统设置';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $this->request->pathinfo();
            $data_log['log'] = '上传文件：' . $data['source_name'];
            OperationModel::addOperation($data_log);

            if ($add_id) {
                $datasmg['code'] = 200;
                $datasmg['id'] = $guid;
                $datasmg['name'] = $data['source_name'];
                $datasmg['msg'] = "上传成功"; // 前端要求， mseeage -> msg，不提示弹框
            } else {
                $datasmg['code'] = 404; // 前端要求，上传失败 404 -> 200，不提示弹框
                $datasmg['name'] = $data['source_name']; // 前端要求，上传失败返回文件名
                $datasmg['msg'] = "上传失败";
            }

            return json($datasmg);
        } catch (ValidateException $e) {
            $datasmg['code'] = 200; // 前端要求，验证失败 404 -> 200，不提示弹框
            $datasmg['name'] = $_FILES['file']['name']; // 前端要求，验证失败返回文件名
            $datasmg['msg'] = $e->getError();

            return json($datasmg);
        }
    }

    //上传文件
    public function uploadform()
    {
        $data_redis = $this->request->middleware('data_redis');
        $file = $this->request->file();
        try {
            // 验证文件大小格式
            validate(['file' => 'fileSize:52428800|fileExt:doc,docx,pdf,xls,xlsx'])
                ->check($file);
            $savename = \think\facade\Filesystem::disk('admin_cooperate_uploads')->putFile('admin_cooperate_uploads', $file['file']);
            $url =  str_replace('\\', '/', $savename);
            $guid=guid();
            $data['virtual_id'] = isset($_POST['virtual_id']) ? $_POST['virtual_id'] : NULL;
            $data['module'] = isset($_POST['module']) ? $_POST['module'] : NULL;
            $data['file_path'] =str_replace('admin_cooperate_uploads', '', $url);;
            $data['source_name'] = $_FILES['file']['name'];
            $data['file_size'] = $_FILES['file']['size'];
            $data['main_organization_id'] = $data_redis['main_organization_id'];
            $data['create_by'] = $data_redis['userid'];
            $data['modify_by'] = $data_redis['userid'];
            $data['create_time'] = date('Y-m-d H:i:s');
            $data['modify_time'] = date('Y-m-d H:i:s');
            $data['module'] = '认证服务';
            $data['id'] = $guid;
            $add_id = FileModel::addFile($data);

            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '系统设置';
            $data_log['type'] = '系统设置';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $this->request->pathinfo();
            $data_log['log'] = '上传文件：' . $data['source_name'];
            OperationModel::addOperation($data_log);

            if ($add_id) {
                $datasmg['code'] = 200;
                $datasmg['id'] = $guid;
                $datasmg['name'] = $data['source_name'];
                $datasmg['msg'] = "上传成功"; // 前端要求， mseeage -> msg，不提示弹框
            } else {
                $datasmg['code'] = 404; // 前端要求，上传失败 404 -> 200，不提示弹框
                $datasmg['name'] = $data['source_name']; // 前端要求，上传失败返回文件名
                $datasmg['msg'] = "上传失败";
            }

            return json($datasmg);
        } catch (ValidateException $e) {
            $datasmg['code'] = 200; // 前端要求，验证失败 404 -> 200，不提示弹框
            $datasmg['name'] = $_FILES['file']['name']; // 前端要求，验证失败返回文件名
            $datasmg['msg'] = $e->getError();

            return json($datasmg);
        }
    }


    //获取表单数据
    public function leftmenu()
    {
        $db = new AttestationModel();
        $id = $_GET['id'];
        $list = $db->getdata($id);
        if ($list) {
            $return['code'] = 200;
            $return['data'] = $list;
            return json($return);
        } else {
            $return['code'] = 404;
            return json($return);
        }
    }

    /**
     * addform 添加认证表单
     *
     * @return void
     */
    public function addform(Request $request)
    {

        if (request()->isPost() && $this->validateForm($type = 3) === true) {
            $params_payload = $this->req_payload;
            $params_payload_arr = json_decode($params_payload, true);

            $data_redis = $request->middleware('data_redis');
            $data['data_stage'] = $params_payload_arr['data_stage'];
            $data['data_stage_code'] = $params_payload_arr['data_stage_code'];
            $data['category'] = $params_payload_arr['category'];
            $data['category_code'] = $params_payload_arr['category_code'];
            $data['organization_id'] = $data_redis['main_organization_id'];
            $material = $params_payload_arr['material'];

            $data['attestation_id'] = $params_payload_arr['attestation_id'];
            $data['name'] = $material['name'];
            $data['number'] = $material['number'];
            $data['unit_type'] = $material['unit_type'];
            $data['unit'] = $material['unit'];
            $data['source'] = $material['source'];
            $data['files'] = $params_payload_arr['files'];
            $data['factor_id'] = $params_payload_arr['factor_id'];
            $data['content'] = $params_payload_arr['content'];
            $data['compareDataList'] = $params_payload_arr['compareDataList'];
            $data['transport'] = isset($params_payload_arr['transport']) ? $params_payload_arr['transport'] : "";
            $data['create_time'] = date('Y_m-d H:i:s');
            $data['create_by'] = $data_redis['userid'];
            $data['data_from'] = 1;//saas端
            //因子数据

            $data['factor_id'] = $params_payload_arr['factor']['id'];
            $data['factor_title'] = $params_payload_arr['factor']['title'];
            $data['factor_denominator'] = $params_payload_arr['factor']['denominator'];
            $data['factor_value'] = $params_payload_arr['factor']['factor_value'];
            $data['factor_mechanism'] = $params_payload_arr['factor']['mechanism'];
            $data['factor_molecule'] = $params_payload_arr['factor']['molecule'];
            $data['factor_year'] = $params_payload_arr['factor']['year'];


            foreach ($data['compareDataList'] as $v) {
                $return = $this->validateAddForm($v);
                if ($return === true) {
                    continue;
                } else {
                    $datasmg['code'] = 404;
                    $datasmg['message'] = $return;
                    return json($datasmg);
                }
            };

            if (in_array($data['category_code'], [8, 12, 14, 16, 18])) {
                if (empty($data['transport'])) {
                    $datasmg['code'] = 404;
                    $datasmg['message'] = '运输物品不能为空';
                    return json($datasmg);
                }
            }

            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '认证管理';
            $data_log['type'] = '功能操作';
            $data_log['time'] = $data['create_time'];
            $data_log['url'] = $request->pathinfo();
            $data_log['log'] = '新建认证表单：' . $material['name'];

            OperationModel::addOperation($data_log);
            $insert_data = [];
            $insert_data[] = $data;
            $add = AttestationModel::addform($data);

            if ($add) {
                $datasmg['code'] = 200;
                $datasmg['message'] = "添加成功";
            } else {
                $datasmg['code'] = 404;
                $datasmg['message'] = "添加失败或数据已存在";
            }

        } else {
            $datasmg['code'] = 404;
            $datasmg['message'] = $this->validateForm($type = 3);
        }

        return json($datasmg);
    }

    protected function validateAddForm($data)
    {

        try {
            validate(AttestationOriginalAddValidate::class)->check($data);
            return true;
        } catch (\think\exception\ValidateException $e) {

            return $e->getError();
        }
    }

    /**
     * editform 编辑认证表单
     *
     * @return void
     */
    public function editform(Request $request)
    {

        if (request()->isPost() && $this->validateForm($type = 4) === true) {
            $params_payload = $this->req_payload;
            $params_payload_arr = json_decode($params_payload, true);

            $data_redis = $request->middleware('data_redis');
            $data['id'] = $params_payload_arr['id'];
            $data['attestation_id'] = $params_payload_arr['attestation_id'];
            $data['data_stage'] = $params_payload_arr['data_stage'];
            $data['data_stage_code'] = $params_payload_arr['data_stage_code'];
            $data['category'] = $params_payload_arr['category'];
            $data['category_code'] = $params_payload_arr['category_code'];
            $material = $params_payload_arr['material'];
            $data['organization_id'] = $data_redis['main_organization_id'];

            $data['name'] = $material['name'];
            $data['number'] = $material['number'];
            $data['unit_type'] = $material['unit_type'];
            $data['unit'] = $material['unit'];

            $data['source'] = $material['source'];
            $data['files'] = $params_payload_arr['files'];

            $data['content'] = $params_payload_arr['content'];
            $data['compareDataList'] = $params_payload_arr['compareDataList'];
            $data['transport'] = isset($params_payload_arr['transport']) ? $params_payload_arr['transport'] : "";
            $data['del_transport'] = isset($params_payload_arr['del_transport']) ? $params_payload_arr['del_transport'] : "";
            $data['del_compareDataList'] = isset($params_payload_arr['del_compareDataList']) ? $params_payload_arr['del_compareDataList'] : "";
            $data['modify_time'] = date('Y_m-d H:i:s');
            $data['modify_by'] = $data_redis['userid'];
            $data['data_from'] = 1;//saas端
            $factory_data_list =$params_payload_arr['factory_data_list'];
            $data['factor_id'] ="";
            if($factory_data_list===NULL){
                $data['factor_id'] = $params_payload_arr['factor_id'];
                if(empty($params_payload_arr['factor']['id'])){
                    $datasmg['code'] = 404;
                    $datasmg['message'] = '缺少因子id';
                    return json($datasmg);
                }
                //因子数据
                $data['factor_id'] = $params_payload_arr['factor']['id'];
                $data['factor_title'] = $params_payload_arr['factor']['title'];
                $data['factor_denominator'] = $params_payload_arr['factor']['denominator'];
                $data['factor_value'] = $params_payload_arr['factor']['factor_value'];
                $data['factor_mechanism'] = $params_payload_arr['factor']['mechanism'];
                $data['factor_molecule'] = $params_payload_arr['factor']['molecule'];
                $data['factor_year'] = $params_payload_arr['factor']['year'];
            }

            foreach ($data['compareDataList'] as $v) {
                $return = $this->validateAddForm($v);
                if ($return === true) {
                    continue;
                } else {
                    $datasmg['code'] = 404;
                    $datasmg['message'] = $return;
                    return json($datasmg);
                }
            };

            if (in_array($data['category_code'], [8, 12, 14, 16, 18])) {
                if (empty($data['transport'])) {
                    $datasmg['code'] = 404;
                    $datasmg['message'] = '运输物品不能为空';
                    return json($datasmg);
                }
            }

            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '认证管理';
            $data_log['type'] = '功能操作';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $request->pathinfo();
            $data_log['log'] = '编辑认证表单：' . $material['name'];

            OperationModel::addOperation($data_log);
            $add = AttestationModel::editform($data);

            if ($add==1) {
                $datasmg['code'] = 200;
                $datasmg['message'] = "编辑成功";
            } elseif($add==2){
                $datasmg['code'] = 404;
                $datasmg['message'] = "编辑的数据名称重复";
            }else {
                $datasmg['code'] = 404;
                $datasmg['message'] = "编辑失败或数据已存在";
            }

        } else {
            $datasmg['code'] = 404;
            $datasmg['message'] = $this->validateForm($type = 4);
        }

        return json($datasmg);
    }

    /**
     * delform 删除单个表单
     *
     * @return void
     */
    public function delform(Request $request)
    {
        if (request()->isPost()) {
            $data_redis = $request->middleware('data_redis');
            $params_payload = $this->req_payload;
            $params_payload_arr = json_decode($params_payload, true);

            $id = isset($params_payload_arr['id']) ? $params_payload_arr['id'] : '';
            $category_code = isset($params_payload_arr['category_code']) ? $params_payload_arr['category_code'] : '';
            if ($id == '' || $id == null || $id == 0) {
                return json(['code' => 201, 'message' => "参数id错误"]);
            }
            $data['id'] = $id;
            $data['category_code'] = $category_code;

            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '认证管理';
            $data_log['type'] = '功能操作';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $request->pathinfo();
            //获取表单标题
            $title = AttestationModel::getFromTitleById($id);
            $data_log['log'] = '删除表单数据：' . $title;

            OperationModel::addOperation($data_log);
            $del = AttestationModel::delform($data);

            if ($del) {
                $datasmg['code'] = 200;
                $datasmg['message'] = "操作成功";
            } else {
                $datasmg['code'] = 404;
                $datasmg['message'] = "操作失败";
            }

            return json($datasmg);
        } else {
            $datasmg = ['code' => 404, 'message' => "请求方法错误"];
            return json($datasmg);
        }
    }

    //获取城市列表
    public function getCity()
    {
        $citys = AttestationModel::getCitys();

        $city_tree = $this->getTree($citys, 0, 3);
        $return['code'] = 200;
        $return['list'] = $city_tree;
        return json($return);
    }

    public static function getTree($data, $pid, $level)
    {
        $tree = [];
        foreach ($data as $k => $v) {
            $level_value = isset($v['level']) ? $v['level'] : '';
            if ($level_value == $level) {
                break;
            }
            if ($v['pid'] == $pid) {
                $v['children'] = self::getTree($data, $v['id'], $level);
                $tree[] = $v;
                unset($data[$k]);
            }
        }

        return $tree;
    }



    //获取产品列表

    public function productList(Request $request)
    {
        $page_size = isset($_GET['pageSize']) ? $_GET['pageSize'] : '';
        $page_index = isset($_GET['pageIndex']) ? $_GET['pageIndex'] : '';
        $data_redis = $request->middleware('data_redis');
        $main_organization_id = $data_redis['main_organization_id'];
        // 过滤条件：产品名称、产品编号、产品状态、核算周期开始、核算周期结束
        $filters = [
            'filter_product_name' => isset($_GET['filter_product_name']) ? $_GET['filter_product_name'] : '',
            'filter_product_no' => isset($_GET['filter_product_no']) ? $_GET['filter_product_no'] : '',
            'filter_week_start' => isset($_GET['filter_week_start']) ? $_GET['filter_week_start'] : '',
            'filter_week_end' => isset($_GET['filter_week_end']) ? $_GET['filter_week_end'] : '',
            'filter_state' => 1,
            'main_organization_id'=>$main_organization_id ? $main_organization_id : ""
        ];



        $list = AttestationModel::getCalculates($page_size, $page_index, $filters)->toArray();

        $array = array();
        foreach ($list['data'] as $key => $value) {
            if ($value['files'] != NULL) {
                foreach (explode(',', $value['files']) as $file_key => $file_value) {
                    $array[$file_key] = ProductModel::getFiles($file_value);
                }
            }

            $list['data'][$key]['files'] = $array;
        }

        $data['code'] = 200;
        $data['data']['list'] = $list['data'];
        $data['data']['total'] = $list['total'];

        return json($data);
    }

    /**
     * download 文件下载
     *
     * @return void
     */
    public function download()
    {
        $ids = $_GET['id'];
        if(empty($ids)){
            return json(['code'=>404,'message'=>'缺少参数']);
        }
        $id_arr = explode(',', $ids);
        $pathArr = [];
        foreach ($id_arr as $key => $value) {
            $list = ProductModel::getFile($value);

            $file['path'] = dirname(dirname(dirname(dirname(dirname(__FILE__))))) . '/admin_cooperate_uploads' . $list['file_path'];;

            $file['name'] = $list['source_name'];
            array_push($pathArr, $file);

        }

        $zipName = "下载文件.zip";
        self::makeZip($pathArr, $zipName);

        // 输出压缩文件提供下载
        header("Cache-Control: max-age=0");
        header("Content-Description: File Transfer");
        header('Content-disposition: attachment; filename=' . $zipName); // 文件名
        header("Content-Type: application/zip"); // zip格式
        header("Content-Transfer-Encoding: binary");
        header('Content-Length: ' . filesize($zipName));
        ob_clean();
        flush();
        readfile($zipName); // 输出文件;

        unlink($zipName); // 删除压缩包临时文件
    }

    /**
     * makeZip
     *
     * @return void
     */
    public function makeZip($pathArr, $zipName)
    {
        $zip = new \ZipArchive();

        if ($zip->open($zipName, \ZipArchive::CREATE | \ZipArchive::OVERWRITE)) {
            foreach ($pathArr as $file) {
                if (!file_exists($file['path'])) {
                    continue;
                }

                // 向压缩包中添加文件
                $zip->addFile($file['path'], $file['name']);
            }

            $zip->close();
            return ['code' => 200, 'msg' => "创建成功", 'path' => $zipName];
        } else {
            return ['code' => 404, 'msg' => '创建失败'];
        }
    }

    public function shotFind() {
        $id = $_GET['id'];
        $list = AttestationModel::getShotFactor($id);

        $data['code'] = 200;
        $data['data'] = $list;

        return json($data);
    }


}