<?php
namespace app\api\controller\examine;

use app\BaseController;
use app\model\datum\DatumModel;
use app\model\examine\ExamineModel;
use app\model\system\OperationModel;
use app\validate\DatumValidate;
use app\validate\ReductionValidate;
use think\Request;


/**
 * Examine
 * 审核管理
 * by njf
 */
class Examine extends BaseController {

    //======================================================================
    // PUBLIC FUNCTIONS
    //======================================================================

    /**
     * index 审核列表
     *
     * @return void
     */
    public function index(Request $request) {
        $data_redis = $request->middleware('data_redis');
        $main_organization_id = $data_redis['main_organization_id'];

        $page_size = isset($_GET['pageSize']) ? $_GET['pageSize'] : '10';
        $page_index = isset($_GET['pageIndex']) ? $_GET['pageIndex'] : '1';

        $filters = [
            'organization_id' => isset($_GET['organization_id']) ? $_GET['organization_id'] : '',
            'calculate_name' => isset($_GET['calculate_name']) ? $_GET['calculate_name'] : '',
            'calculate_year' => isset($_GET['calculate_year']) ? $_GET['calculate_year'] : '',
            'calculate_month' => isset($_GET['calculate_month']) ? $_GET['calculate_month'] : '',
            'state' => isset($_GET['state']) ? $_GET['state'] : '',
            'main_organization_id'=>$main_organization_id
        ];
        $state = [
            ['id'=>3,'name'=>'待工厂审核'],
            ['id'=>4,'name'=>'待分公司审核'],
            ['id'=>5,'name'=>'待母公司审核'],
            ['id'=>7,'name'=>'审核不通过'],
        ];

        $db = new ExamineModel();
        $list = $db->getList($page_size,$page_index,$filters);
        //组织列表
        $organizationList = $db->organizationList($main_organization_id);

        //状态1-待收集2-待提交3-工厂审核中4-分公司审核中5-母公司审核中6-审核通过7-审核不通过


        $data['code'] = 200;
        $data['data']['list'] = $list;
        $data['data']['state'] = $state;
        $data['data']['organizationList']=$organizationList;

        return json($data);
    }

    /**
     * status 审批
     *
     * @return void
     */
    public function status(Request $request) {

        if (request()->isPost()) {
            $params_payload = $this->req_payload;
            $params_payload_arr = json_decode($params_payload, true);

            $data_redis = $request->middleware('data_redis');
            $data['id'] = $params_payload_arr['id'];
            $data['state'] = $params_payload_arr['state'];

            $data['modify_by'] = $data_redis['userid'];
            $data['modify_time'] = date('Y-m-d H:i:s');


            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '审批管理';
            $data_log['type'] = '系统操作';
            $data_log['time'] =date('Y-m-d H:i:s');
            $data_log['url'] = $request->pathinfo();
            $data_log['log'] = '审批管理修改状态';

            OperationModel::addOperation($data_log);

            $add = ExamineModel::status($data);

            if ($add) {
                $datasmg['code'] = 200;
                $datasmg['message'] = "修改成功";
            } else {
                $datasmg['code'] = 404;
                $datasmg['message'] = "修改失败";
            }

        } else {
            $datasmg['code'] = 404;
            $datasmg['message'] = '请求方法错误';
        }

        return json($datasmg);
    }
    /**
     * find 详情
     *
     * @return void
     */
    public function find(Request $request) {
        $id = $_GET['id'];
        $list = ExamineModel::getInfoById($id);

        $data['code'] = 200;
        $data['data']['list'] = $list;

        return json($data);
    }



}