<?php
// 这是系统自动生成的公共文件

use Endroid\QrCode\Color\Color;
use Endroid\QrCode\Encoding\Encoding;
use Endroid\QrCode\RoundBlockSizeMode\RoundBlockSizeModeMargin;
use Endroid\QrCode\Writer\PngWriter;
use Endroid\QrCode\ErrorCorrectionLevel;
use Endroid\QrCode\QrCode;
/**
 * 生成二维码
 * @param string $url [字符]
 * @param  [type] $is_save [是否保存]
 * @param  [type] $pid     [唯一标识符]
 * @return string  [type]  [返回图片标签字符串]
 */
function qrcode(string $url = '', $is_save = true, $pid = 0)
{
    $writer = new PngWriter();
    $qrCode = QrCode::create($url)//跳转的url地址
    ->setEncoding(new Encoding('UTF-8'))    //设置编码格式
    ->setErrorCorrectionLevel(new ErrorCorrectionLevel\ErrorCorrectionLevelLow())    //设置纠错级别为低
    ->setSize(150)      //大小
    ->setMargin(20)     //边距
    ->setRoundBlockSizeMode(new RoundBlockSizeModeMargin())     //设置圆轮大小边距
    ->setForegroundColor(new Color(0, 0, 0))        //前景色
    ->setBackgroundColor(new Color(255, 255, 255));       //背景色
//        $label = Label::create('测试')      //二维码下面的文字
//        ->setTextColor(new Color(0, 0, 0)); //文字的颜色
    $result = $writer->write($qrCode, null, null);
//        header('Content-Type: '.$result->getMimeType());
//        $result->getString();
//        if(!$is_save){
//            $dataUri = $result->getDataUri();       //DATA－URI 是指可以在Web 页面中包含图片但无需任何额外的HTTP 请求的一类URI.
//            return "<img src='$dataUri' alt='测试二维码'>";
//        }
    $file_path = 'static/qrcode/';
    if(!file_exists($file_path)){
        mkdir($file_path,0777,true);
    }
    $qrcode='qrcode_'.time().mt_rand(1,9).'.png';   //二维码文件名称,mt_rand()运行速度要比rand()快很多
    $result->saveToFile('static/qrcode/'.$qrcode);
    return $qrcode;
}

function exchange_arr_key_value($arr) {
    var_dump($arr);die;
}

/**
 * @notes 这里是把第二个单位 \
 * @param $unit_one
 * @param $unit_two
 * @param $unit_two_value
 */

function unit_convert($unit_one, $unit_two, $unit_two_value) {
    //转换的方法是 先把第二个单位转为顶级单位 然后再将顶级单位改为第一个单位
    $unit_two_info = \app\model\admin\UnitModel::getInfoByName($unit_two);
    var_dump($unit_two_info);die;


}
function receiveData($originalData,$useData){
    $count = count($useData);
    $return =[];
    for ($i=0;$i<$count;$i++){
        $return[$useData[$i]] = $originalData[$useData[$i]] ? $originalData[$useData[$i]] : NULL;
    }
    return $return;
}