<?php
declare (strict_types = 1);

namespace app\validate;

use think\Validate;

class ProductMaterialValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @author wuyinghua
     * @var array
     */
    protected $rule = [
        'product_material_name' => 'require|length:2,20',
        'product_material_no'   => 'require|length:2,20',
        'number'                => 'require|float',
        'unit_type'             => 'require',
        'unit'                  => 'require',
        'weight'                => 'float',
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @author wuyinghua
     * @var array
     */
    protected $message = [
        'product_material_name.require' => '原材料名称不能为空',
        'product_material_name.length'  => '原材料名称长度需在2-20个字符之间',
        'product_material_no.require'   => '原材料编码不能为空',
        'product_material_no.length'    => '原材料编码长度需在2-20个字符之间',
        'number.require'                => '原材料数量不能为空',
        'number.float'                  => '原材料数量需要是数字',
        'unit_type.require'             => '原材料单位类型不能为空',
        'unit.require'                  => '原材料单位不能为空',
        'weight.float'                  => '原材料质量需要是数字',
    ];
}
