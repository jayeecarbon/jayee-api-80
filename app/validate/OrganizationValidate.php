<?php
declare (strict_types = 1);

namespace app\validate;

use think\Validate;

class OrganizationValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @author wuyinghua
     * @var array
     */
    protected $rule = [
        'name'     => 'require|length:2,20',
        // 'type'     => 'require',
        'pid'      => 'require',
        'province' => 'require',
        'city'     => 'require',
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @author wuyinghua
     * @var array
     */
    protected $message = [
        'name.require'     => '组织名称不能为空',
        'name.length'      => '组织名称长度需在2-20个字符之间',
        // 'type.require'     => '组织类型不能为空',
        'pid.require'      => '上级组织不能为空',
        'province.require' => '省不能为空',
        'city.require'     => '市不能为空',
    ];
}
