<?php
declare (strict_types=1);

namespace app\validate;

use think\Validate;

class AttestationValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'username' => 'require',
        'mobile' => 'require|mobile',
        'email' => 'require|email',
        'product_name' => 'require',
        'product_no' => 'require',
        'client_name' => 'require',
        'client_address' => 'require',
        'client_detail_address' => 'require',
        'produce_name' => 'require',
        'produce_address' => 'require',
        'produce_detail_address' => 'require',
        'company_name' => 'require',
        'company_address' => 'require',
        'company_detail_address' => 'require',
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [
        'username.require' => '联系人姓名不能为空',
        'mobile.require' => '手机号不能为空',
        'email.require' => '邮箱不能为空',
        'email.email' => '邮箱不正确',
        'product_name.require' => '产品名称不能为空',
        'product_no.require' => '产品型号不能为空',
        'client_name.require' => '委托人名称不能为空',
        'client_address.require' => '委托人地址不能为空',
        'client_detail_address.require' => '委托人具体地址不能为空',
        'produce_name.require' => '生产者名称不能为空',
        'produce_address.require' => '生产者地址不能为空',
        'produce_detail_address.require' => '生产者具体地址不能为空',
        'company_name.require' => '生产企业名称不能为空',
        'company_address.require' => '生产企业地址不能为空',
        'company_detail_address.require' => '生产企业具体地址不能为空',
        'mobile.mobile' => '联系电话无效',
    ];
}
