<?php
declare (strict_types=1);

namespace app\validate;

use think\Validate;

class ReductionValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'name' => 'require',
        'organization_id' => 'require',
        'type' => 'require',
        'desc' => 'require',


    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [
        'name.require' => '名称不能为空',
        'organization_id.require' => '组织不能为空',
        'type.require' => '减排量量化类型不能为空',
        'desc.require' => '减排场景描述不能为空',
    ];
}
