<?php


namespace app\validate;


use think\Validate;

class SupplierProductValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @author wuyinghua
     * @var array
     */
    protected $rule = [
        'product_name'                  => 'require',
        'product_model'                 => 'require',
        'product_type'                  => 'require',
        'week_start'                    => 'require',
        'week_end'                      => 'require',
        'number'                        => 'require',
        'unit_type'                     => 'require',
        'unit'                          => 'require',
        'auth'                          => 'require',
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @author wuyinghua
     * @var array
     */
    protected $message = [
        'product_name.require'           => '产品名称不能为空',
        'product_model.require'          => '产品型号不能为空',
        'product_type.require'           => '产品类型不能为空',
        'week_start.require'             => '核算周期开始时间不能为空',
        'week_end.require'               => '核算周期结束时间不能为空',
        'number.require'                 => '核算数量不能为空',
        'unit_type.require'              => '核算单位类型不能为空',
        'unit.require'                   => '核算单位不能为空',
        'auth.require'                   => '是否第三方认证不能为空',
    ];
}