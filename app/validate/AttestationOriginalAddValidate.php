<?php
declare (strict_types=1);

namespace app\validate;

use think\Validate;

class AttestationOriginalAddValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'from' => 'require',
        'number' => 'require',
        'unit_type' => 'require',
        'unit' => 'require',
        'files' => 'require'

    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [
        'from.require' => '缺少来源',
        'number.require' => '缺少数量',
        'unit_type.require' => '缺少单位',
        'unit.require' => '缺少单位',
        'files.array' => '缺少文件'
    ];
}
