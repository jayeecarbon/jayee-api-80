<?php
declare (strict_types = 1);

namespace app\validate;

use think\Validate;

class FacilityValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @author wuyinghua
     * @var array
     */
    protected $rule = [
        'facility_name'          => 'require|length:2,20',
        'facility_no'            => 'require|length:2,20',
        'organization_id'        => 'require',
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @author wuyinghua
     * @var array
     */
    protected $message = [
        'facility_name.require'          => '设备名称不能为空',
        'facility_name.length'           => '设备名称长度需在2-20个字符之间',
        'facility_no.require'            => '设备编码不能为空',
        'facility_no.length'             => '设备编码长度需在2-20个字符之间',
        'organization_id.require'        => '所属组织不能为空',
    ];
}
