<?php
declare (strict_types = 1);

namespace app\validate;

use think\Validate;

class CalculateExampleValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @author wuyinghua
     * @var array
     */
    protected $rule = [
        'model_name'             => 'require|length:2,20',
        'gwp_id'                 => 'require',
        'organization_id'        => 'require',
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @author wuyinghua
     * @var array
     */
    protected $message = [
        'model_name.require'             => '模型名称不能为空',
        'model_name.length'              => '模型名称长度需在2-20个字符之间',
        'gwp_id.require'                 => 'GWP信息不能为空',
        'organization_id.require'        => '所属组织不能为空',
    ];
}
