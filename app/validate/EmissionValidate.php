<?php
declare (strict_types = 1);

namespace app\validate;

use think\Validate;

class EmissionValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'name'              => 'require',
        'source_id'              => 'require',
        'active'       => 'require',
        'ghg_cate'           => 'require',
        'ghg_type'        => 'require',
        'iso_cate'          => 'require',
        'iso_type'               => 'require',
        'active_data'            => 'require',
        'active_unit'            => 'require',
        'active_department'            => 'require',
        'active_type'            => 'require',
        'active_score'            => 'require',
      //  'unitary_ratio'            => 'require',
        'factor_type'            => 'require',
        'factor_score'            => 'require',
        'factor_source'            => 'require',
        'year'            => 'require',
        'gas'               =>'require'

    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [
        'name.require'          => '排放源名称不能为空',
        'source_id.require'           => '排放源id不能为空',
        'active.require'          => '排放活动不能为空',
        'ghg_cate.require'           => 'ghg分类',
        'ghg_type.require'          => 'ghg类别',
        'iso_cate.require'          => 'iso分类',
        'iso_type.require'           => 'iso类别',
        'active_data.require'          => '活动记录方式',
        'active_unit.require'          => '活动数据单位',
        'active_department.require'           => '活动数据保存部门不能为空',
        'active_type.require'          => '活动数据类别不能为空',
        'active_score.require'           => '活动数据评分不能为空',
     //   'unitary_ratio.require'          => '单位换算比不能为空',
        'factor_type.require'          => '排放因子类别不能为空',
        'factor_score.require'           => '排放因子评分不能为空',
        'factor_source'                    =>'排放因子来源不能为空',
        'year.require'          => '发布年份不能为空',
        'gas.require'          => '温室气体排放不能为空',
    ];
}
